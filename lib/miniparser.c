/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1


/* Substitute the variable and function names.  */
#define yyparse         miniyyparse
#define yylex           miniyylex
#define yyerror         miniyyerror
#define yydebug         miniyydebug
#define yynerrs         miniyynerrs


/* Copy the first part of user declarations.  */
#line 64 "miniparser.y" /* yacc.c:339  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "base-functions.h"
#include "expression.h"
#include "assignment.h"
#include "chain.h"
#include "general.h"
#include "execute.h"
#include "miniparser.h"

/* Mess with the mallocs used by the parser */
extern void *parserCalloc(size_t, size_t);
extern void *parserMalloc(size_t);
extern void *parserRealloc(void *, size_t);
extern void parserFree(void *);
#undef malloc
#undef realloc
#undef calloc
#undef free
#define malloc parserMalloc
#define realloc parserRealloc
#define calloc parserCalloc
#define free safeFree
/* End of the malloc mess */

#define YYERROR_VERBOSE 1
#define YYFPRINTF sollyaFprintf

extern int miniyylex(YYSTYPE *lvalp, void *scanner);

void miniyyerror(void *myScanner, const char *message) {
  UNUSED_PARAM(myScanner);
  printMessage(1,SOLLYA_MSG_SYNTAX_ERROR_ENCOUNTERED_WHILE_PARSING,"Warning: %s. Will try to continue parsing (expecting \";\"). May leak memory.\n",message);
}


#line 112 "miniparser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_MINIYY_MINIPARSER_H_INCLUDED
# define YY_MINIYY_MINIPARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int miniyydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    CONSTANTTOKEN = 258,
    MIDPOINTCONSTANTTOKEN = 259,
    DYADICCONSTANTTOKEN = 260,
    HEXCONSTANTTOKEN = 261,
    HEXADECIMALCONSTANTTOKEN = 262,
    BINARYCONSTANTTOKEN = 263,
    PITOKEN = 264,
    IDENTIFIERTOKEN = 265,
    STRINGTOKEN = 266,
    LPARTOKEN = 267,
    RPARTOKEN = 268,
    LBRACKETTOKEN = 269,
    RBRACKETTOKEN = 270,
    EQUALTOKEN = 271,
    ASSIGNEQUALTOKEN = 272,
    COMPAREEQUALTOKEN = 273,
    COMMATOKEN = 274,
    EXCLAMATIONTOKEN = 275,
    SEMICOLONTOKEN = 276,
    STARLEFTANGLETOKEN = 277,
    LEFTANGLETOKEN = 278,
    RIGHTANGLEUNDERSCORETOKEN = 279,
    RIGHTANGLEDOTTOKEN = 280,
    RIGHTANGLESTARTOKEN = 281,
    RIGHTANGLETOKEN = 282,
    DOTSTOKEN = 283,
    DOTTOKEN = 284,
    QUESTIONMARKTOKEN = 285,
    VERTBARTOKEN = 286,
    ATTOKEN = 287,
    DOUBLECOLONTOKEN = 288,
    COLONTOKEN = 289,
    DOTCOLONTOKEN = 290,
    COLONDOTTOKEN = 291,
    EXCLAMATIONEQUALTOKEN = 292,
    APPROXTOKEN = 293,
    ANDTOKEN = 294,
    ORTOKEN = 295,
    PLUSTOKEN = 296,
    MINUSTOKEN = 297,
    MULTOKEN = 298,
    DIVTOKEN = 299,
    POWTOKEN = 300,
    SQRTTOKEN = 301,
    EXPTOKEN = 302,
    FREEVARTOKEN = 303,
    LOGTOKEN = 304,
    LOG2TOKEN = 305,
    LOG10TOKEN = 306,
    SINTOKEN = 307,
    COSTOKEN = 308,
    TANTOKEN = 309,
    ASINTOKEN = 310,
    ACOSTOKEN = 311,
    ATANTOKEN = 312,
    SINHTOKEN = 313,
    COSHTOKEN = 314,
    TANHTOKEN = 315,
    ASINHTOKEN = 316,
    ACOSHTOKEN = 317,
    ATANHTOKEN = 318,
    ABSTOKEN = 319,
    ERFTOKEN = 320,
    ERFCTOKEN = 321,
    LOG1PTOKEN = 322,
    EXPM1TOKEN = 323,
    DOUBLETOKEN = 324,
    SINGLETOKEN = 325,
    HALFPRECISIONTOKEN = 326,
    QUADTOKEN = 327,
    DOUBLEDOUBLETOKEN = 328,
    TRIPLEDOUBLETOKEN = 329,
    DOUBLEEXTENDEDTOKEN = 330,
    CEILTOKEN = 331,
    FLOORTOKEN = 332,
    NEARESTINTTOKEN = 333,
    HEADTOKEN = 334,
    REVERTTOKEN = 335,
    SORTTOKEN = 336,
    TAILTOKEN = 337,
    MANTISSATOKEN = 338,
    EXPONENTTOKEN = 339,
    PRECISIONTOKEN = 340,
    ROUNDCORRECTLYTOKEN = 341,
    PRECTOKEN = 342,
    POINTSTOKEN = 343,
    DIAMTOKEN = 344,
    DISPLAYTOKEN = 345,
    VERBOSITYTOKEN = 346,
    SHOWMESSAGENUMBERSTOKEN = 347,
    CANONICALTOKEN = 348,
    AUTOSIMPLIFYTOKEN = 349,
    TAYLORRECURSIONSTOKEN = 350,
    TIMINGTOKEN = 351,
    TIMETOKEN = 352,
    FULLPARENTHESESTOKEN = 353,
    MIDPOINTMODETOKEN = 354,
    DIEONERRORMODETOKEN = 355,
    SUPPRESSWARNINGSTOKEN = 356,
    RATIONALMODETOKEN = 357,
    HOPITALRECURSIONSTOKEN = 358,
    ONTOKEN = 359,
    OFFTOKEN = 360,
    DYADICTOKEN = 361,
    POWERSTOKEN = 362,
    BINARYTOKEN = 363,
    HEXADECIMALTOKEN = 364,
    FILETOKEN = 365,
    POSTSCRIPTTOKEN = 366,
    POSTSCRIPTFILETOKEN = 367,
    PERTURBTOKEN = 368,
    MINUSWORDTOKEN = 369,
    PLUSWORDTOKEN = 370,
    ZEROWORDTOKEN = 371,
    NEARESTTOKEN = 372,
    HONORCOEFFPRECTOKEN = 373,
    TRUETOKEN = 374,
    FALSETOKEN = 375,
    DEFAULTTOKEN = 376,
    MATCHTOKEN = 377,
    WITHTOKEN = 378,
    ABSOLUTETOKEN = 379,
    DECIMALTOKEN = 380,
    RELATIVETOKEN = 381,
    FIXEDTOKEN = 382,
    FLOATINGTOKEN = 383,
    ERRORTOKEN = 384,
    QUITTOKEN = 385,
    FALSEQUITTOKEN = 386,
    FALSERESTARTTOKEN = 387,
    LIBRARYTOKEN = 388,
    LIBRARYCONSTANTTOKEN = 389,
    DIFFTOKEN = 390,
    DIRTYSIMPLIFYTOKEN = 391,
    REMEZTOKEN = 392,
    ANNOTATEFUNCTIONTOKEN = 393,
    BASHEVALUATETOKEN = 394,
    GETSUPPRESSEDMESSAGESTOKEN = 395,
    GETBACKTRACETOKEN = 396,
    FPMINIMAXTOKEN = 397,
    HORNERTOKEN = 398,
    EXPANDTOKEN = 399,
    SIMPLIFYSAFETOKEN = 400,
    TAYLORTOKEN = 401,
    TAYLORFORMTOKEN = 402,
    CHEBYSHEVFORMTOKEN = 403,
    AUTODIFFTOKEN = 404,
    DEGREETOKEN = 405,
    NUMERATORTOKEN = 406,
    DENOMINATORTOKEN = 407,
    SUBSTITUTETOKEN = 408,
    COMPOSEPOLYNOMIALSTOKEN = 409,
    COEFFTOKEN = 410,
    SUBPOLYTOKEN = 411,
    ROUNDCOEFFICIENTSTOKEN = 412,
    RATIONALAPPROXTOKEN = 413,
    ACCURATEINFNORMTOKEN = 414,
    ROUNDTOFORMATTOKEN = 415,
    EVALUATETOKEN = 416,
    LENGTHTOKEN = 417,
    OBJECTNAMETOKEN = 418,
    INFTOKEN = 419,
    MIDTOKEN = 420,
    SUPTOKEN = 421,
    MINTOKEN = 422,
    MAXTOKEN = 423,
    READXMLTOKEN = 424,
    PARSETOKEN = 425,
    PRINTTOKEN = 426,
    PRINTXMLTOKEN = 427,
    PLOTTOKEN = 428,
    PRINTHEXATOKEN = 429,
    PRINTFLOATTOKEN = 430,
    PRINTBINARYTOKEN = 431,
    SUPPRESSMESSAGETOKEN = 432,
    UNSUPPRESSMESSAGETOKEN = 433,
    PRINTEXPANSIONTOKEN = 434,
    BASHEXECUTETOKEN = 435,
    EXTERNALPLOTTOKEN = 436,
    WRITETOKEN = 437,
    ASCIIPLOTTOKEN = 438,
    RENAMETOKEN = 439,
    BINDTOKEN = 440,
    INFNORMTOKEN = 441,
    SUPNORMTOKEN = 442,
    FINDZEROSTOKEN = 443,
    FPFINDZEROSTOKEN = 444,
    DIRTYINFNORMTOKEN = 445,
    GCDTOKEN = 446,
    EUCLDIVTOKEN = 447,
    EUCLMODTOKEN = 448,
    NUMBERROOTSTOKEN = 449,
    INTEGRALTOKEN = 450,
    DIRTYINTEGRALTOKEN = 451,
    WORSTCASETOKEN = 452,
    IMPLEMENTPOLYTOKEN = 453,
    IMPLEMENTCONSTTOKEN = 454,
    CHECKINFNORMTOKEN = 455,
    ZERODENOMINATORSTOKEN = 456,
    ISEVALUABLETOKEN = 457,
    SEARCHGALTOKEN = 458,
    GUESSDEGREETOKEN = 459,
    DIRTYFINDZEROSTOKEN = 460,
    IFTOKEN = 461,
    THENTOKEN = 462,
    ELSETOKEN = 463,
    FORTOKEN = 464,
    INTOKEN = 465,
    FROMTOKEN = 466,
    TOTOKEN = 467,
    BYTOKEN = 468,
    DOTOKEN = 469,
    BEGINTOKEN = 470,
    ENDTOKEN = 471,
    LEFTCURLYBRACETOKEN = 472,
    RIGHTCURLYBRACETOKEN = 473,
    WHILETOKEN = 474,
    READFILETOKEN = 475,
    ISBOUNDTOKEN = 476,
    EXECUTETOKEN = 477,
    EXTERNALPROCTOKEN = 478,
    VOIDTOKEN = 479,
    CONSTANTTYPETOKEN = 480,
    FUNCTIONTOKEN = 481,
    OBJECTTOKEN = 482,
    RANGETOKEN = 483,
    INTEGERTOKEN = 484,
    STRINGTYPETOKEN = 485,
    BOOLEANTOKEN = 486,
    LISTTOKEN = 487,
    OFTOKEN = 488,
    VARTOKEN = 489,
    PROCTOKEN = 490,
    PROCEDURETOKEN = 491,
    RETURNTOKEN = 492,
    NOPTOKEN = 493
  };
#endif
/* Tokens.  */
#define CONSTANTTOKEN 258
#define MIDPOINTCONSTANTTOKEN 259
#define DYADICCONSTANTTOKEN 260
#define HEXCONSTANTTOKEN 261
#define HEXADECIMALCONSTANTTOKEN 262
#define BINARYCONSTANTTOKEN 263
#define PITOKEN 264
#define IDENTIFIERTOKEN 265
#define STRINGTOKEN 266
#define LPARTOKEN 267
#define RPARTOKEN 268
#define LBRACKETTOKEN 269
#define RBRACKETTOKEN 270
#define EQUALTOKEN 271
#define ASSIGNEQUALTOKEN 272
#define COMPAREEQUALTOKEN 273
#define COMMATOKEN 274
#define EXCLAMATIONTOKEN 275
#define SEMICOLONTOKEN 276
#define STARLEFTANGLETOKEN 277
#define LEFTANGLETOKEN 278
#define RIGHTANGLEUNDERSCORETOKEN 279
#define RIGHTANGLEDOTTOKEN 280
#define RIGHTANGLESTARTOKEN 281
#define RIGHTANGLETOKEN 282
#define DOTSTOKEN 283
#define DOTTOKEN 284
#define QUESTIONMARKTOKEN 285
#define VERTBARTOKEN 286
#define ATTOKEN 287
#define DOUBLECOLONTOKEN 288
#define COLONTOKEN 289
#define DOTCOLONTOKEN 290
#define COLONDOTTOKEN 291
#define EXCLAMATIONEQUALTOKEN 292
#define APPROXTOKEN 293
#define ANDTOKEN 294
#define ORTOKEN 295
#define PLUSTOKEN 296
#define MINUSTOKEN 297
#define MULTOKEN 298
#define DIVTOKEN 299
#define POWTOKEN 300
#define SQRTTOKEN 301
#define EXPTOKEN 302
#define FREEVARTOKEN 303
#define LOGTOKEN 304
#define LOG2TOKEN 305
#define LOG10TOKEN 306
#define SINTOKEN 307
#define COSTOKEN 308
#define TANTOKEN 309
#define ASINTOKEN 310
#define ACOSTOKEN 311
#define ATANTOKEN 312
#define SINHTOKEN 313
#define COSHTOKEN 314
#define TANHTOKEN 315
#define ASINHTOKEN 316
#define ACOSHTOKEN 317
#define ATANHTOKEN 318
#define ABSTOKEN 319
#define ERFTOKEN 320
#define ERFCTOKEN 321
#define LOG1PTOKEN 322
#define EXPM1TOKEN 323
#define DOUBLETOKEN 324
#define SINGLETOKEN 325
#define HALFPRECISIONTOKEN 326
#define QUADTOKEN 327
#define DOUBLEDOUBLETOKEN 328
#define TRIPLEDOUBLETOKEN 329
#define DOUBLEEXTENDEDTOKEN 330
#define CEILTOKEN 331
#define FLOORTOKEN 332
#define NEARESTINTTOKEN 333
#define HEADTOKEN 334
#define REVERTTOKEN 335
#define SORTTOKEN 336
#define TAILTOKEN 337
#define MANTISSATOKEN 338
#define EXPONENTTOKEN 339
#define PRECISIONTOKEN 340
#define ROUNDCORRECTLYTOKEN 341
#define PRECTOKEN 342
#define POINTSTOKEN 343
#define DIAMTOKEN 344
#define DISPLAYTOKEN 345
#define VERBOSITYTOKEN 346
#define SHOWMESSAGENUMBERSTOKEN 347
#define CANONICALTOKEN 348
#define AUTOSIMPLIFYTOKEN 349
#define TAYLORRECURSIONSTOKEN 350
#define TIMINGTOKEN 351
#define TIMETOKEN 352
#define FULLPARENTHESESTOKEN 353
#define MIDPOINTMODETOKEN 354
#define DIEONERRORMODETOKEN 355
#define SUPPRESSWARNINGSTOKEN 356
#define RATIONALMODETOKEN 357
#define HOPITALRECURSIONSTOKEN 358
#define ONTOKEN 359
#define OFFTOKEN 360
#define DYADICTOKEN 361
#define POWERSTOKEN 362
#define BINARYTOKEN 363
#define HEXADECIMALTOKEN 364
#define FILETOKEN 365
#define POSTSCRIPTTOKEN 366
#define POSTSCRIPTFILETOKEN 367
#define PERTURBTOKEN 368
#define MINUSWORDTOKEN 369
#define PLUSWORDTOKEN 370
#define ZEROWORDTOKEN 371
#define NEARESTTOKEN 372
#define HONORCOEFFPRECTOKEN 373
#define TRUETOKEN 374
#define FALSETOKEN 375
#define DEFAULTTOKEN 376
#define MATCHTOKEN 377
#define WITHTOKEN 378
#define ABSOLUTETOKEN 379
#define DECIMALTOKEN 380
#define RELATIVETOKEN 381
#define FIXEDTOKEN 382
#define FLOATINGTOKEN 383
#define ERRORTOKEN 384
#define QUITTOKEN 385
#define FALSEQUITTOKEN 386
#define FALSERESTARTTOKEN 387
#define LIBRARYTOKEN 388
#define LIBRARYCONSTANTTOKEN 389
#define DIFFTOKEN 390
#define DIRTYSIMPLIFYTOKEN 391
#define REMEZTOKEN 392
#define ANNOTATEFUNCTIONTOKEN 393
#define BASHEVALUATETOKEN 394
#define GETSUPPRESSEDMESSAGESTOKEN 395
#define GETBACKTRACETOKEN 396
#define FPMINIMAXTOKEN 397
#define HORNERTOKEN 398
#define EXPANDTOKEN 399
#define SIMPLIFYSAFETOKEN 400
#define TAYLORTOKEN 401
#define TAYLORFORMTOKEN 402
#define CHEBYSHEVFORMTOKEN 403
#define AUTODIFFTOKEN 404
#define DEGREETOKEN 405
#define NUMERATORTOKEN 406
#define DENOMINATORTOKEN 407
#define SUBSTITUTETOKEN 408
#define COMPOSEPOLYNOMIALSTOKEN 409
#define COEFFTOKEN 410
#define SUBPOLYTOKEN 411
#define ROUNDCOEFFICIENTSTOKEN 412
#define RATIONALAPPROXTOKEN 413
#define ACCURATEINFNORMTOKEN 414
#define ROUNDTOFORMATTOKEN 415
#define EVALUATETOKEN 416
#define LENGTHTOKEN 417
#define OBJECTNAMETOKEN 418
#define INFTOKEN 419
#define MIDTOKEN 420
#define SUPTOKEN 421
#define MINTOKEN 422
#define MAXTOKEN 423
#define READXMLTOKEN 424
#define PARSETOKEN 425
#define PRINTTOKEN 426
#define PRINTXMLTOKEN 427
#define PLOTTOKEN 428
#define PRINTHEXATOKEN 429
#define PRINTFLOATTOKEN 430
#define PRINTBINARYTOKEN 431
#define SUPPRESSMESSAGETOKEN 432
#define UNSUPPRESSMESSAGETOKEN 433
#define PRINTEXPANSIONTOKEN 434
#define BASHEXECUTETOKEN 435
#define EXTERNALPLOTTOKEN 436
#define WRITETOKEN 437
#define ASCIIPLOTTOKEN 438
#define RENAMETOKEN 439
#define BINDTOKEN 440
#define INFNORMTOKEN 441
#define SUPNORMTOKEN 442
#define FINDZEROSTOKEN 443
#define FPFINDZEROSTOKEN 444
#define DIRTYINFNORMTOKEN 445
#define GCDTOKEN 446
#define EUCLDIVTOKEN 447
#define EUCLMODTOKEN 448
#define NUMBERROOTSTOKEN 449
#define INTEGRALTOKEN 450
#define DIRTYINTEGRALTOKEN 451
#define WORSTCASETOKEN 452
#define IMPLEMENTPOLYTOKEN 453
#define IMPLEMENTCONSTTOKEN 454
#define CHECKINFNORMTOKEN 455
#define ZERODENOMINATORSTOKEN 456
#define ISEVALUABLETOKEN 457
#define SEARCHGALTOKEN 458
#define GUESSDEGREETOKEN 459
#define DIRTYFINDZEROSTOKEN 460
#define IFTOKEN 461
#define THENTOKEN 462
#define ELSETOKEN 463
#define FORTOKEN 464
#define INTOKEN 465
#define FROMTOKEN 466
#define TOTOKEN 467
#define BYTOKEN 468
#define DOTOKEN 469
#define BEGINTOKEN 470
#define ENDTOKEN 471
#define LEFTCURLYBRACETOKEN 472
#define RIGHTCURLYBRACETOKEN 473
#define WHILETOKEN 474
#define READFILETOKEN 475
#define ISBOUNDTOKEN 476
#define EXECUTETOKEN 477
#define EXTERNALPROCTOKEN 478
#define VOIDTOKEN 479
#define CONSTANTTYPETOKEN 480
#define FUNCTIONTOKEN 481
#define OBJECTTOKEN 482
#define RANGETOKEN 483
#define INTEGERTOKEN 484
#define STRINGTYPETOKEN 485
#define BOOLEANTOKEN 486
#define LISTTOKEN 487
#define OFTOKEN 488
#define VARTOKEN 489
#define PROCTOKEN 490
#define PROCEDURETOKEN 491
#define RETURNTOKEN 492
#define NOPTOKEN 493

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 115 "miniparser.y" /* yacc.c:355  */

  doubleNode *dblnode;
  struct entryStruct *association;
  char *value;
  node *tree;
  chain *list;
  int *integerval;
  int count;
  void *other;

#line 639 "miniparser.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int miniyyparse (void *myScanner);

#endif /* !YY_MINIYY_MINIPARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 655 "miniparser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  316
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   8741

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  239
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  46
/* YYNRULES -- Number of rules.  */
#define YYNRULES  401
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  1134

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   493

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   436,   436,   445,   449,   454,   460,   464,   470,   474,
     480,   484,   488,   492,   496,   500,   504,   508,   514,   518,
     526,   531,   536,   544,   548,   554,   558,   564,   571,   575,
     581,   585,   589,   593,   597,   601,   605,   609,   613,   617,
     621,   625,   629,   633,   637,   641,   645,   649,   653,   657,
     661,   665,   669,   673,   681,   685,   689,   693,   697,   701,
     705,   709,   713,   717,   721,   725,   729,   733,   737,   741,
     745,   749,   753,   757,   761,   765,   769,   773,   777,   781,
     785,   791,   796,   801,   805,   809,   816,   820,   824,   828,
     834,   839,   844,   849,   854,   859,   864,   868,   874,   881,
     885,   889,   893,   897,   901,   905,   909,   913,   917,   921,
     925,   929,   933,   937,   941,   947,   951,   955,   959,   963,
     967,   971,   975,   979,   983,   987,   991,   995,   999,  1003,
    1007,  1013,  1017,  1023,  1027,  1033,  1037,  1043,  1053,  1057,
    1063,  1067,  1071,  1075,  1081,  1090,  1094,  1098,  1102,  1106,
    1110,  1114,  1118,  1124,  1128,  1132,  1136,  1140,  1144,  1150,
    1154,  1158,  1162,  1169,  1173,  1180,  1184,  1188,  1192,  1199,
    1206,  1210,  1216,  1220,  1224,  1231,  1235,  1239,  1246,  1250,
    1254,  1258,  1262,  1266,  1270,  1274,  1278,  1282,  1286,  1290,
    1294,  1298,  1302,  1306,  1310,  1314,  1318,  1322,  1326,  1330,
    1334,  1338,  1342,  1346,  1350,  1354,  1358,  1362,  1366,  1370,
    1374,  1378,  1389,  1393,  1398,  1403,  1408,  1413,  1417,  1421,
    1425,  1429,  1433,  1437,  1441,  1446,  1451,  1456,  1460,  1464,
    1468,  1474,  1478,  1484,  1488,  1492,  1496,  1500,  1504,  1508,
    1512,  1516,  1522,  1527,  1532,  1537,  1542,  1547,  1552,  1560,
    1564,  1568,  1572,  1578,  1582,  1586,  1592,  1596,  1600,  1606,
    1610,  1614,  1618,  1622,  1626,  1632,  1636,  1640,  1644,  1648,
    1652,  1656,  1660,  1664,  1669,  1673,  1677,  1681,  1685,  1689,
    1693,  1697,  1701,  1705,  1709,  1713,  1717,  1721,  1725,  1729,
    1733,  1737,  1741,  1745,  1749,  1753,  1757,  1761,  1765,  1769,
    1773,  1777,  1781,  1785,  1789,  1793,  1797,  1801,  1805,  1809,
    1813,  1817,  1821,  1825,  1829,  1833,  1837,  1841,  1845,  1849,
    1853,  1857,  1861,  1865,  1869,  1873,  1877,  1881,  1885,  1889,
    1893,  1897,  1901,  1905,  1909,  1913,  1917,  1921,  1925,  1929,
    1933,  1937,  1941,  1945,  1949,  1953,  1957,  1961,  1965,  1969,
    1973,  1977,  1981,  1985,  1989,  1993,  1997,  2001,  2005,  2009,
    2013,  2017,  2021,  2025,  2031,  2036,  2042,  2046,  2050,  2054,
    2058,  2062,  2066,  2070,  2074,  2078,  2082,  2086,  2090,  2094,
    2098,  2102,  2109,  2115,  2121,  2127,  2133,  2139,  2145,  2151,
    2157,  2163,  2169,  2175,  2181,  2187,  2195,  2201,  2208,  2212,
    2218,  2222
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "\"decimal constant\"", "\"interval\"",
  "\"dyadic constant\"", "\"constant in memory notation\"",
  "\"hexadecimal constant\"", "\"binary constant\"", "\"pi\"",
  "\"identifier\"", "\"character string\"", "\"(\"", "\")\"", "\"[\"",
  "\"]\"", "\"=\"", "\":=\"", "\"==\"", "\",\"", "\"!\"", "\";\"",
  "\"*<\"", "\"<\"", "\">_\"", "\">.\"", "\">*\"", "\">\"", "\"...\"",
  "\".\"", "\"?\"", "\"|\"", "\"@\"", "\"::\"", "\":\"", "\".:\"",
  "\":.\"", "\"!=\"", "\"~\"", "\"&&\"", "\"||\"", "\"+\"", "\"-\"",
  "\"*\"", "\"/\"", "\"^\"", "\"sqrt\"", "\"exp\"", "\"_x_\"", "\"log\"",
  "\"log2\"", "\"log10\"", "\"sin\"", "\"cos\"", "\"tan\"", "\"asin\"",
  "\"acos\"", "\"atan\"", "\"sinh\"", "\"cosh\"", "\"tanh\"", "\"asinh\"",
  "\"acosh\"", "\"atanh\"", "\"abs\"", "\"erf\"", "\"erfc\"", "\"log1p\"",
  "\"expm1\"", "\"D\"", "\"SG\"", "\"HP\"", "\"QD\"", "\"DD\"", "\"TD\"",
  "\"DE\"", "\"ceil\"", "\"floor\"", "\"nearestint\"", "\"head\"",
  "\"revert\"", "\"sort\"", "\"tail\"", "\"mantissa\"", "\"exponent\"",
  "\"precision\"", "\"roundcorrectly\"", "\"prec\"", "\"points\"",
  "\"diam\"", "\"display\"", "\"verbosity\"", "\"showmessagenumbers\"",
  "\"canonical\"", "\"autosimplify\"", "\"taylorrecursions\"",
  "\"timing\"", "\"time\"", "\"fullparentheses\"", "\"midpointmode\"",
  "\"dieonerrormode\"", "\"roundingwarnings\"", "\"rationalmode\"",
  "\"hopitalrecursions\"", "\"on\"", "\"off\"", "\"dyadic\"", "\"powers\"",
  "\"binary\"", "\"hexadecimal\"", "\"file\"", "\"postscript\"",
  "\"postscriptfile\"", "\"perturb\"", "\"RD\"", "\"RU\"", "\"RZ\"",
  "\"RN\"", "\"honorcoeffprec\"", "\"true\"", "\"false\"", "\"default\"",
  "\"match\"", "\"with\"", "\"absolute\"", "\"decimal\"", "\"relative\"",
  "\"fixed\"", "\"floating\"", "\"error\"", "\"quit\"",
  "\"quit in an included file\"", "\"restart\"", "\"library\"",
  "\"libraryconstant\"", "\"diff\"", "\"dirtysimplify\"", "\"remez\"",
  "\"annotatefunction\"", "\"bashevaluate\"", "\"getsuppressedmessages\"",
  "\"getbacktrace\"", "\"fpminimax\"", "\"horner\"", "\"expand\"",
  "\"simplify\"", "\"taylor\"", "\"taylorform\"", "\"chebyshevform\"",
  "\"autodiff\"", "\"degree\"", "\"numerator\"", "\"denominator\"",
  "\"substitute\"", "\"composepolynomials\"", "\"coeff\"", "\"subpoly\"",
  "\"roundcoefficients\"", "\"rationalapprox\"", "\"accurateinfnorm\"",
  "\"round\"", "\"evaluate\"", "\"length\"", "\"objectname\"", "\"inf\"",
  "\"mid\"", "\"sup\"", "\"min\"", "\"max\"", "\"readxml\"", "\"parse\"",
  "\"print\"", "\"printxml\"", "\"plot\"", "\"printhexa\"",
  "\"printfloat\"", "\"printbinary\"", "\"suppressmessage\"",
  "\"unsuppressmessage\"", "\"printexpansion\"", "\"bashexecute\"",
  "\"externalplot\"", "\"write\"", "\"asciiplot\"", "\"rename\"",
  "\"bind\"", "\"infnorm\"", "\"supnorm\"", "\"findzeros\"",
  "\"fpfindzeros\"", "\"dirtyinfnorm\"", "\"gcd\"", "\"div\"", "\"mod\"",
  "\"numberroots\"", "\"integral\"", "\"dirtyintegral\"", "\"worstcase\"",
  "\"implementpoly\"", "\"implementconst\"", "\"checkinfnorm\"",
  "\"zerodenominators\"", "\"isevaluable\"", "\"searchgal\"",
  "\"guessdegree\"", "\"dirtyfindzeros\"", "\"if\"", "\"then\"",
  "\"else\"", "\"for\"", "\"in\"", "\"from\"", "\"to\"", "\"by\"",
  "\"do\"", "\"begin\"", "\"end\"", "\"{\"", "\"}\"", "\"while\"",
  "\"readfile\"", "\"isbound\"", "\"execute\"", "\"externalproc\"",
  "\"void\"", "\"constant\"", "\"function\"", "\"object\"", "\"range\"",
  "\"integer\"", "\"string\"", "\"boolean\"", "\"list\"", "\"of\"",
  "\"var\"", "\"proc\"", "\"procedure\"", "\"return\"", "\"nop\"",
  "$accept", "startsymbol", "startsymbolwitherr", "beginsymbol",
  "endsymbol", "command", "ifcommand", "forcommand", "commandlist",
  "variabledeclarationlist", "variabledeclaration", "identifierlist",
  "procbody", "simplecommand", "assignment", "simpleassignment",
  "structuring", "stateassignment", "stillstateassignment", "thinglist",
  "structelementlist", "structelementseparator", "structelement", "thing",
  "supermegaterm", "indexing", "megaterm", "hyperterm", "unaryplusminus",
  "term", "subterm", "basicthing", "matchlist", "matchelement", "constant",
  "list", "simplelist", "range", "debound", "headfunction",
  "egalquestionmark", "statedereference", "externalproctype",
  "extendedexternalproctype", "externalproctypesimplelist",
  "externalproctypelist", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,   484,
     485,   486,   487,   488,   489,   490,   491,   492,   493
};
# endif

#define YYPACT_NINF -1013

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-1013)))

#define YYTABLE_NINF -131

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    3846, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013,    22,
   -1013,  6642,  4079,  7341,  6642,  8506,   -13,   -13,    55,    77,
      86,   154,   160,   169,   222,   230,   302,   311,   338,   341,
     346,   349,   448,   457,   465,   472,   477,   485,   500,   505,
     515,   522,   532,   543,   558,   575,   581,   618,   619,   621,
     628,   632,   633,   636,   641,   647,   669,   670,   674,    89,
      89,    89,    89,    89,    89,     9,    89,    89,    89,   675,
      89,    89,    89,    89,    89,    89, -1013, -1013, -1013, -1013,
   -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
   -1013, -1013, -1013, -1013,  6642, -1013, -1013, -1013, -1013, -1013,
   -1013,   702,   705,   710,   727,   759,   760,   761,   762,   787,
     790,   807,   825,   829,   830,   832,   833,   835,   884,   890,
     894,   910,   915,   916,   923,   930,   933,   938,   940,   942,
     952,   964,   966,   967,   976,   979,   980,   988,   995,   998,
    1001,  1016,  1022,  1029,  1040,  1042,  1046,  1047,  1063,  1072,
    1079,  1082,  1088,  1098,  1110,  1114,  1123,    45,  1125,  1127,
   -1013,  1137,  1138,   120, -1013,   280, -1013, -1013,    41,   376,
    8506,   117, -1013,    47, -1013, -1013, -1013, -1013, -1013, -1013,
    4545,    23,  4778,    97,   851,    41,  1949, -1013, -1013, -1013,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,   227, -1013, -1013, -1013, -1013, -1013, -1013,  6642, -1013,
   -1013, -1013, -1013,  3610, -1013, -1013, -1013, -1013, -1013, -1013,
     210,     3,  6642,  6642,  6642,  6642,  6642,   316,   359,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,   140,   192,
     168,  6642,   409,  6642,    14, -1013, -1013, -1013,  7341,  7341,
    7341,  6875,  7108,  7341,  7341,  7341,  7341,  7341,  7341,  7341,
   -1013,  7574,  7807,  6642,   530,  8273,  8040, -1013,   460,   138,
    1151,   548,   210,     4, -1013, -1013,  6642,  6642, -1013, -1013,
   -1013,    67,    71,    75,    84,    91,    96,   100,   103,   106,
     108,   112,   114,   116,   124,   128,   131,   136,   145,   245,
     305,   309,   312,   315,   330,   358,   360,   362,   382,   390,
     394,   398,   400,   403,   407,   410,   414,   418,   422,   431,
     442,   446, -1013,   451,   157,   464,   704,   719,   726,   779,
     820,   229,   882,   918,   928,  1020,  1028,  1054,  1062,  1150,
    1162, -1013, -1013,  1157,  1169,  1179,  1183,  1191,  1194,  1200,
    1215,  1219,  1222,  1223,  1224,  1228,  1231,  1232,  6642,   624,
   -1013,    45,  6642,  1236,  1239,   780,  1240,  2194,   462, -1013,
   -1013,   717,   389, -1013,  1246, -1013,   504,    58,  6642,   453,
     455,   385,   468,    60, -1013, -1013,   541,   459,   461,   463,
     644,   688,   824,   838,   466,   470,   475,   861,   864,   870,
     873,   875,   880,   886,   892,   898,   483,   491,   498,   503,
     506,  1049,  1075,   526,   528,   917,   920,   922,   927,   932,
     934,   944,   950,   963,   975,   977,   982,   985,  1111,   992,
     999,  1007,  1167,  1010,  1021,  1253, -1013, -1013, -1013,    45,
     536,  1258,    62,   296,   159,  1260,    41,    41,   376,  7341,
     376,  7341,   376,   376,   376,   117,   117,   117,   117,   117,
    8506,  8506, -1013,  8506,  8506, -1013,    56,  1263,  8506, -1013,
    8506,  8506, -1013, -1013,  6642,  5011, -1013,  5244,  1061,  1267,
      93,   317, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
   -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
   -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
   -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
   -1013, -1013, -1013, -1013,  4312,  6642,  5477,  5477,  5477,  5477,
    5477,  5477,  5477,  5477,  5477,  5477,  5477,  5477,  5477,  5477,
    5477,  5477,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642,  6642,  6642,    12,  6642, -1013,   -21,   417,
   -1013,   -31,  6642,  1273,  1138,  5710, -1013, -1013,  1276, -1013,
    1266,   249,  3374,  1268, -1013, -1013,  6642,  6642, -1013,  6642,
    6642,  1280,    51, -1013,  6642, -1013, -1013,  6642,  6642, -1013,
    6642,  6642, -1013, -1013, -1013,  6642,  6642,  6642,  6642, -1013,
   -1013, -1013,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
    1285,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,  6642,
    6642,  6642,  6642, -1013,  6642,  6642,  6642, -1013,  6642,  6642,
    6642, -1013, -1013, -1013, -1013,  6642,  1345,  1276, -1013,  1250,
     159,   376,   376, -1013, -1013, -1013, -1013, -1013,  6642, -1013,
   -1013, -1013, -1013, -1013,  1367,  1364,   210,  1369, -1013, -1013,
   -1013,  1445,  1449,   210,   210,   533,   834,   847,  1174,  1185,
    1189,  1202,  1206,  1210,  1245,  1254,  1438,  1440,  1442,  1463,
    1479,  1450,   539,  1024,   544,   546,   549,  1451,  1462,   551,
     579,  1026,  1488,  1032,  1491,  1493,  1034,  3610,  6642,  6642,
    3610,   599,  1494, -1013, -1013,   612,  1495, -1013,  3610, -1013,
   -1013,   249,  1271,   210,   210,   210,   210,   231,    18, -1013,
    1050,  1058,   616,  1064,  1066,  1068,  1074,  1076,   630,   649,
     651,   690,   706,   708,  1080,  1090,   711,  1496,  1503,  1092,
     713,   718,   721,   723,   725,   728,   730,   736,   741,  1102,
    1104,   743,   746,  1106,   753,   210,   764,  1504, -1013,  6642,
   -1013,  -185,  2430,  1486,  1507, -1013,  6642, -1013,  6642,  6642,
    1480,  1482,  6642, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
    6642,  1498,  6642,  1511,  1516,  6642,  1323,    26,   -24, -1013,
   -1013,  6642, -1013, -1013, -1013, -1013,  6642,  1722,  6642,  6642,
   -1013,  6642,  6642,  6642,  6642,  6642, -1013, -1013, -1013, -1013,
   -1013, -1013,  6642,  6642, -1013,  6642, -1013,  6642, -1013, -1013,
   -1013, -1013, -1013, -1013, -1013, -1013, -1013,  6642,  6642, -1013,
   -1013,  6642, -1013, -1013,   159,   375,  6642, -1013,  6642, -1013,
     332,  6642, -1013,   340,  2666, -1013,   210,   771,   774,  5943,
    6176,  1596,  1108,  6409,   776,  1603,  1606,  1115,  3610,  3610,
    6642,  1117,   778,  6642, -1013,   622,  2902,  1607,  1119,  1121,
     781,  1680,   783,   788,  1681,   795,   811,  1143,  1145,   813,
    1684,  1958,   249,   602,   767,  6642, -1013,   791,  6642, -1013,
    6642, -1013,   769, -1013, -1013,  6642,   210,  6642,   210, -1013,
    6642,  6642,   210, -1013, -1013, -1013,  6642, -1013, -1013,   -34,
     110, -1013,  1112,  6642, -1013,  6642, -1013,   819, -1013,  6642,
    6642, -1013, -1013, -1013, -1013, -1013, -1013, -1013,  6642,  6642,
   -1013, -1013,  6642, -1013,   949,  3138, -1013,   249,   249,  1198,
     249,  1483,  1490,  6642, -1013,   210,   210,  1149,   210,  1153,
    6642,  3610,  2228, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
   -1013,  1466, -1013, -1013,  1669,   249,  1675,  1677,  6642, -1013,
    1700,  1725,  1158,  1160,  1679,  6642, -1013,  6642, -1013,   986,
   -1013, -1013,   249, -1013,   249,   249,  1714,  6642,  6642,    30,
   -1013,  1721,  1728,   137,  1719, -1013,   249,   249,  1716, -1013,
   -1013,  6642,  6642,   249,  1718,  1726,  6642, -1013, -1013, -1013,
   -1013,   249,  1168,  1171,  3610,  2228, -1013, -1013, -1013, -1013,
   -1013, -1013, -1013, -1013,  1993, -1013, -1013,   249,   816,  1177,
   -1013,   249,   249,  1911, -1013,  6642,  6642, -1013, -1013,  1730,
   -1013, -1013,  6642, -1013, -1013,   249,  1732,  1735, -1013,  1736,
   -1013, -1013, -1013, -1013
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     5,   242,   243,   244,   245,   246,   247,   248,   213,
     211,     0,     0,     0,     0,     0,   159,   160,     0,     0,
     208,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   203,   204,   206,   205,   209,   210,   207,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   365,
     365,   365,   365,   365,   365,   365,   365,   365,   365,     0,
     365,   365,   365,   365,   365,   365,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   195,   196,     0,   198,   197,   199,   200,   201,
     202,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     194,     0,     0,     0,     2,     3,   138,   224,   140,   145,
       0,   153,   163,   172,   212,   217,   218,   219,   220,   223,
       0,     0,     0,     0,     0,   143,     0,   165,   161,   162,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   366,   367,   368,   369,   370,   371,     0,   372,
     373,   374,   375,     0,   376,   377,   378,   380,   379,   381,
       0,   138,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     133,     0,     0,     0,     0,   229,     1,     4,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     164,     0,     0,     0,     0,     0,     0,   216,     0,   131,
     221,     0,   253,     0,   250,   258,     0,     0,   261,   260,
     259,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   364,     0,   213,   365,   365,   365,   365,   365,
     365,   365,   365,   365,   365,   365,   365,   365,   365,   365,
     365,    54,    55,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       6,     7,     0,     0,     0,     0,    56,     0,     0,    10,
      83,    88,     0,    86,     0,    84,   224,   172,     0,     0,
       0,     0,     0,     0,   267,   268,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   222,   135,   136,     0,
       0,     0,     0,    28,     0,     0,   141,   142,   146,     0,
     148,     0,   149,   152,   147,   156,   157,   158,   154,   155,
       0,     0,   166,     0,     0,   167,     0,   225,     0,   176,
       0,     0,   173,   215,     0,     0,   249,     0,     0,     0,
       0,     0,   327,   328,   329,   332,   333,   334,   335,   336,
     337,   338,   339,   340,   341,   342,   343,   344,   345,   346,
     347,   348,   349,   350,   351,   352,   353,   355,   354,   356,
     357,   358,   359,   360,   361,   318,   321,   322,   326,   323,
     324,   325,   319,   278,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    15,     0,     0,
      17,     0,     0,     0,     0,     0,     8,     9,     0,    14,
       0,     0,     0,     0,   230,    89,     0,     0,    87,     0,
       0,     0,     0,   139,   231,   265,   270,     0,     0,   266,
       0,     0,   277,   279,   280,     0,     0,     0,     0,   285,
     286,   287,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   362,   363,   264,   263,   262,   274,   275,   298,   297,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   311,     0,     0,     0,   315,     0,     0,
       0,   134,   320,   214,   330,     0,     0,     0,     7,     0,
       0,   150,   151,   170,   168,   171,   169,   144,     0,   177,
     175,   174,   132,   228,     0,     0,   254,     0,   251,   256,
     257,     0,     0,    90,    91,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   113,   112,
     114,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    85,    58,     0,    28,    27,    23,    11,
      13,     0,    25,    96,    97,    94,    95,   225,     0,   232,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   137,     0,     0,    29,     0,
      33,     0,     0,     0,     0,   227,     0,   252,     0,     0,
      59,    75,     0,    63,    64,    65,    66,    67,    68,    69,
       0,    71,     0,     0,     0,     0,    18,     0,     0,    16,
      76,     0,    57,    24,    12,    26,     0,     0,     0,     0,
     269,     0,     0,     0,     0,     0,   288,   289,   290,   291,
     292,   293,     0,     0,   296,     0,   299,     0,   301,   302,
     303,   304,   305,   306,   307,   308,   309,     0,     0,   313,
     314,     0,   317,   331,     0,     0,     0,    30,     0,    32,
       0,     0,    41,     0,     0,   226,   255,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   240,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    31,     0,     0,    38,
       0,    40,     0,    92,    93,     0,    60,     0,    77,    62,
       0,     0,    72,    74,    80,    81,     0,    19,    22,     0,
       0,   241,     0,     0,   238,     0,   236,     0,   271,     0,
       0,   281,   282,   283,   284,   294,   295,   273,     0,     0,
     312,   316,     0,    49,     0,     0,    37,     0,     0,     0,
       0,     0,     0,     0,    39,    61,    78,     0,    73,     0,
       0,     0,     0,   396,   382,   383,   384,   385,   386,   387,
     388,     0,   397,   400,     0,     0,     0,     0,     0,   234,
       0,     0,     0,     0,     0,     0,    46,     0,    48,     0,
      34,    36,     0,    45,     0,     0,     0,     0,     0,     0,
      20,   398,     0,     0,     0,   239,     0,     0,     0,   272,
     276,     0,     0,     0,     0,     0,     0,    47,    35,    42,
      44,     0,     0,     0,     0,     0,   401,   389,   390,   391,
     392,   393,   394,   395,     0,   237,   235,     0,     0,     0,
      53,     0,     0,     0,    43,     0,     0,    21,   399,     0,
     233,   300,     0,    50,    52,     0,     0,     0,    82,     0,
      51,    70,    79,   310
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1013, -1013, -1013,  -488,   156,  -226, -1013, -1013,  -483,  -715,
   -1013,  -631,  1116, -1013, -1013, -1013, -1013, -1013, -1013,  -167,
    1242, -1013, -1013,     0,  1658,  -206,   -10,  -282,   -15,   951,
      -5,    81,  1105, -1013, -1013, -1013, -1013, -1013, -1013, -1013,
     -17, -1013, -1012,   657,   667, -1013
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,   163,   164,   437,   639,   640,   627,   630,   641,   642,
     643,   515,   315,   439,   440,   441,   442,   443,   444,   445,
     309,   509,   310,   339,   166,   167,   168,   169,   170,   171,
     172,   173,   653,   654,   174,   175,   343,   176,   177,   178,
     232,   179,  1042,  1043,  1072,  1044
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     165,   188,   189,   185,   842,   318,   319,   787,   318,   319,
     187,   181,   184,   338,   186,   318,   319,   438,   318,   319,
    1071,   238,   774,   547,   513,   231,   719,   514,    16,    17,
     876,   636,   548,   637,   180,   549,   340,   446,   518,   520,
     522,   523,   524,   233,   234,   235,   236,   237,   239,   240,
     241,   242,   916,   244,   245,   246,   247,   248,   249,   320,
     775,   333,   318,   319,   321,   318,   319,   190,   322,   318,
     319,   727,   333,   659,   308,   714,   334,   875,   323,   660,
     552,   715,   335,  1071,   553,   798,   838,   651,   554,   191,
     318,   319,   336,   335,   250,   318,   319,   555,   192,   318,
     319,   318,   319,   336,   556,   231,   318,   319,   739,   557,
     318,   319,   344,   558,   318,   319,   559,   481,   482,   560,
     316,   561,  1032,   318,   319,   562,   448,   563,   924,   564,
     318,   319,   318,   319,   498,   318,   319,   565,   502,   318,
     319,   566,   318,   319,   567,   318,   319,   318,   319,   568,
     505,   318,   319,   318,   319,   318,   319,   544,   569,   791,
     331,   332,   946,   318,   319,   330,   193,   318,   319,   180,
     318,   319,   194,   594,   595,   318,   319,   318,   319,  1030,
    1031,   195,   342,   780,   318,   319,   777,   507,   940,   508,
     351,   352,   353,   354,   355,   356,   357,   358,   359,   360,
     361,   362,   363,   364,   365,   366,   367,   368,   369,   370,
     371,   372,   373,   374,   375,   376,   377,   378,   379,   380,
     381,   382,   383,   384,   385,   386,   387,   388,   389,   390,
     391,   446,   843,   430,   196,   718,   841,   721,   393,   722,
     939,   238,   197,   728,  1094,   602,  1015,   -98,   -98,   318,
     319,   324,   449,   450,   451,   452,   453,   392,   570,   456,
     457,   458,   459,   460,   461,   462,   463,   464,   465,   466,
     467,   468,   469,   470,   471,   472,   473,   474,   475,   476,
     477,   478,   479,   480,   318,   319,   483,   484,   485,   486,
     487,   488,   489,   490,   491,   492,   493,   494,   495,   496,
     497,   317,   499,   500,   501,   873,   503,   504,   516,   517,
     877,   510,   716,   512,   198,   717,   531,   534,   571,   318,
     319,   541,   572,   199,   447,   573,   532,   535,   574,   454,
     539,   542,   740,   536,  1033,  1034,  1035,  1036,  1037,  1038,
    1039,  1040,  1041,   575,   318,   319,   550,   551,   318,   319,
     200,   318,   319,   201,   318,   319,   318,   319,   202,   920,
     923,   203,  1097,  1098,  1099,  1100,  1101,  1102,  1103,   318,
     319,   576,   455,   577,   430,   578,   718,   732,   734,   233,
     234,   235,   236,   237,   239,   240,   241,   242,   244,   245,
     246,   247,   248,   249,   945,   579,   962,   318,   319,   318,
     319,   318,   319,   580,   657,   646,   647,   581,   325,   326,
     506,   582,   327,   583,   318,   319,   584,   328,   329,   511,
     585,   318,   319,   586,   318,   319,   961,   587,   628,   318,
     319,   588,   631,   318,   319,   589,   446,   318,   319,   318,
     319,   972,   318,   319,   590,   761,   318,   319,   652,   318,
     319,   767,   768,   318,   319,   591,   772,   318,   319,   592,
     204,   318,   319,   997,   593,   636,   655,   637,   656,   205,
     318,   319,   662,   543,   663,   644,   664,   206,  1014,   669,
     596,   318,   319,   670,   207,   318,   319,   658,   671,   208,
     318,   319,   318,   319,   318,   319,   681,   209,   318,   319,
     318,   319,   318,   319,   682,   318,   319,   318,   319,   318,
     319,   683,   210,   446,   318,   319,   684,   211,   447,   685,
     649,   650,   318,   319,   818,   723,   724,   212,   725,   726,
     318,   319,  1059,   729,   213,   730,   731,   318,   319,   688,
     537,   689,   318,   319,   214,   318,   319,   736,   636,   712,
     637,   866,   851,  -115,   869,   215,   636,   853,   637,   854,
     661,   844,   855,   546,   858,   318,   319,   318,   319,   965,
     216,   446,   318,   319,   446,   318,   319,   968,   318,   319,
     318,   319,   446,   318,   319,   318,   319,   217,   318,   319,
     318,   319,   859,   218,   743,   744,   745,   746,   747,   748,
     749,   750,   751,   752,   753,   754,   755,   756,   757,   758,
     759,   760,   870,   762,   763,   764,   765,   766,   318,   319,
     769,   770,   771,  1017,   773,   872,   776,   778,   779,   880,
     219,   220,   781,   221,   629,   785,   446,   446,   318,   319,
     222,   318,   319,   886,   223,   224,   793,   794,   225,   795,
     796,   318,   319,   226,   652,   318,   319,   800,   801,   227,
     802,   803,   887,   665,   888,   804,   805,   806,   807,   318,
     319,   446,   808,   809,   810,   811,   812,   813,   814,   815,
     816,   228,   229,   318,   319,   931,   230,   243,   318,   319,
     318,   319,   819,   820,   821,   822,   823,   824,   825,   826,
     827,   828,   829,   889,   830,   831,   832,   666,   833,   834,
     835,   947,   987,   988,   252,   836,   951,   253,   446,   890,
     597,   891,   254,   447,   894,   954,   898,   318,   319,   318,
     319,   899,   446,   446,   900,   598,   901,   645,   902,   255,
     446,   903,   599,   904,   960,   318,   319,   318,   319,   905,
     318,   319,   318,   319,   906,   446,   909,   318,   319,   910,
     318,   319,   318,   319,   318,   319,   912,   318,   319,   318,
     319,   256,   257,   258,   259,   318,   319,   913,   867,   868,
     318,   319,   318,   319,   973,   318,   319,   974,  1018,   983,
     634,   991,   318,   319,  1001,   600,  1003,   789,   790,   260,
     447,  1004,   261,   318,   319,  1070,   318,   319,  1006,   446,
     318,   319,  1020,   318,   319,   318,   319,   318,   319,   262,
     318,   319,   318,   319,  1007,   446,  1010,   318,   319,  1121,
     318,   319,  1050,  1051,   318,   319,   601,   263,   636,   915,
     637,   264,   265,   667,   266,   267,   926,   268,   927,   928,
     318,   319,   318,   319,  -116,   318,   319,   668,   447,   993,
     932,   447,   934,   318,   319,   937,   345,  -117,  1117,   447,
     346,   941,   347,   318,   319,   840,   942,   318,   319,   948,
     672,   949,   950,   673,   952,   953,   318,   319,   446,   674,
     318,   319,   675,   955,   676,   956,   269,   957,   603,   677,
     318,   319,   270,   318,   319,   678,   271,   958,   959,   318,
     319,   679,   318,   319,   318,   319,   963,   680,   964,   318,
     319,   967,   272,   447,   447,   318,   319,   273,   274,   976,
     978,   318,   319,   982,   604,   275,   690,   318,   319,   691,
     989,   692,   276,   992,   605,   277,   693,   874,  1126,  1127,
     278,   694,   279,   695,   280,  1129,   318,   319,   447,   318,
     319,   318,   319,   696,   281,  1019,   318,   319,  1021,   697,
    1022,   318,   319,   318,   319,  1025,   282,  1026,   283,   284,
    1027,  1028,   698,   318,   319,   636,  1029,   637,   285,   318,
     319,   286,   287,  1046,   699,  1047,   700,   917,   919,   922,
     288,   701,   318,   319,   702,   447,  1023,   289,  1052,  1053,
     290,   704,  1054,   291,   318,   319,   318,   319,   705,   447,
     447,   318,   319,  1066,   318,   319,   706,   447,   292,   708,
    1069,   318,   319,   944,   293,   636,   606,   637,   318,   319,
     709,   294,   447,   852,   607,   860,   318,   319,  1078,   318,
     319,   862,   295,   865,   296,  1084,  1048,  1085,   297,   298,
     318,   319,   686,   318,   319,   318,   319,  1092,  1093,   878,
     608,   318,   319,   318,   319,   299,   966,   879,   609,   969,
     971,  1108,  1109,   881,   300,   882,  1113,   883,   687,   318,
     319,   301,   737,   884,   302,   885,   447,   318,   319,   892,
     303,   994,   996,   318,   319,   318,   319,   318,   319,   893,
     304,   897,   447,   318,   319,   318,   319,  1013,  1016,   318,
     319,   907,   305,   908,   703,   911,   306,   980,  1024,   318,
     319,   318,   319,  1045,   986,   307,   990,   311,   999,   312,
    1000,   318,   319,   318,   319,   318,   319,   318,   319,   313,
     314,   318,   319,  1049,   318,   319,   318,   319,   318,   319,
     318,   319,  1008,   545,  1009,   636,   610,   637,  1067,   612,
    1056,  1058,  1068,  1060,  1061,   447,  1063,  1081,   611,  1082,
     707,   613,   318,   319,   318,   319,  1055,  1115,   318,   319,
    1116,   614,   318,   319,  -118,   615,  1122,   318,   319,   318,
     319,  1075,   636,   616,   637,  -119,   617,   318,   319,  -120,
     318,   319,   618,   318,   319,  1087,   318,   319,  1088,  1062,
    1089,  1090,  -121,  1086,   318,   319,  -122,   619,   318,   319,
    -123,   620,  1105,  1106,   621,   622,   623,   318,   319,  1110,
     624,   318,   319,   625,   626,   318,   319,  1114,   632,   318,
     319,   633,   635,     2,     3,     4,     5,     6,     7,     8,
     394,    10,    11,  1120,    12,  -124,   648,  1123,  1124,   710,
      13,   713,    14,   720,  -125,   728,   525,   526,   527,   528,
     529,  1130,   738,   782,   318,   319,   786,   788,    15,   792,
     797,    16,    17,   318,   319,   817,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,   395,   396,   397,
     398,   399,   400,   401,   402,   403,   404,    69,   405,   406,
     407,   408,   409,   410,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,   837,    95,    96,    97,    98,    99,   100,
     845,   411,   412,   846,   847,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   413,   414,   415,   416,   417,   418,   419,   420,   421,
     422,   423,   424,   425,   426,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   427,   149,   150,
     151,   152,   153,   154,   155,   156,   428,   848,  -126,   429,
    -127,   849,  -129,   850,   856,   430,   636,   431,   637,   432,
     158,   159,   433,   434,   160,   857,   161,   318,   319,   318,
     319,   318,   319,  -128,   638,   162,   435,   839,   436,     2,
       3,     4,     5,     6,     7,     8,   394,    10,    11,  -130,
      12,   861,   318,   319,  1064,   638,    13,   929,    14,   930,
     863,  1065,   864,   871,   717,   895,   896,   914,   318,   319,
     925,   935,   318,   319,    15,   933,   936,    16,    17,   318,
     319,   938,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,   395,   396,   397,   398,   399,   400,   401,
     402,   403,   404,    69,   405,   406,   407,   408,   409,   410,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,   979,
      95,    96,    97,    98,    99,   100,   984,   411,   412,   985,
     998,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   413,   414,   415,
     416,   417,   418,   419,   420,   421,   422,   423,   424,   425,
     426,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   427,   149,   150,   151,   152,   153,   154,
     155,   156,   428,  1002,  1005,   429,  1076,  1011,  1077,  1073,
    1083,   430,   636,   431,   637,   432,   158,   159,   433,   434,
     160,  1074,   161,  1079,   318,   319,   318,   319,   318,   319,
     638,   162,   435,   921,   436,     2,     3,     4,     5,     6,
       7,     8,   394,    10,    11,  1091,    12,  1107,  1080,  1111,
    1095,  1096,    13,  1128,    14,  1131,  1104,  1112,  1132,  1133,
     783,   711,   251,   318,   319,   318,   319,   318,   319,   799,
      15,  1119,  1118,    16,    17,   318,   319,     0,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,   395,
     396,   397,   398,   399,   400,   401,   402,   403,   404,    69,
     405,   406,   407,   408,   409,   410,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,     0,    95,    96,    97,    98,
      99,   100,     0,   411,   412,     0,     0,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   413,   414,   415,   416,   417,   418,   419,
     420,   421,   422,   423,   424,   425,   426,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   427,
     149,   150,   151,   152,   153,   154,   155,   156,   428,     0,
       0,   429,  1125,     0,     0,     0,     0,   430,   636,   431,
     637,   432,   158,   159,   433,   434,   160,     0,   161,     0,
     318,   319,     0,     0,     0,     0,   638,   162,   435,   943,
     436,     2,     3,     4,     5,     6,     7,     8,   394,    10,
      11,     0,    12,   348,   349,   350,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,   318,   319,
       0,     0,     0,     0,     0,     0,    15,     0,     0,    16,
      17,     0,     0,     0,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,   395,   396,   397,   398,   399,
     400,   401,   402,   403,   404,    69,   405,   406,   407,   408,
     409,   410,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,     0,    95,    96,    97,    98,    99,   100,     0,   411,
     412,     0,     0,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   413,
     414,   415,   416,   417,   418,   419,   420,   421,   422,   423,
     424,   425,   426,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   427,   149,   150,   151,   152,
     153,   154,   155,   156,   428,     0,     0,   429,     0,     0,
       0,     0,     0,   430,   636,   431,   637,   432,   158,   159,
     433,   434,   160,     0,   161,     0,     0,     0,     0,     0,
       0,     0,   638,   162,   435,  1012,   436,     2,     3,     4,
       5,     6,     7,     8,   394,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,  1033,  1034,  1035,
    1036,  1037,  1038,  1039,  1040,  1041,     0,     0,     0,     0,
       0,     0,    15,     0,     0,    16,    17,     0,     0,     0,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,   395,   396,   397,   398,   399,   400,   401,   402,   403,
     404,    69,   405,   406,   407,   408,   409,   410,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,     0,    95,    96,
      97,    98,    99,   100,     0,   411,   412,     0,     0,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   413,   414,   415,   416,   417,
     418,   419,   420,   421,   422,   423,   424,   425,   426,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   427,   149,   150,   151,   152,   153,   154,   155,   156,
     428,     0,     0,   429,     0,     0,     0,     0,     0,   430,
     636,   431,   637,   432,   158,   159,   433,   434,   160,     0,
     161,     0,     0,     0,     0,     0,     0,     0,   638,   162,
     435,     0,   436,     2,     3,     4,     5,     6,     7,     8,
     394,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,  1034,  1035,  1036,  1037,  1038,  1039,  1040,
    1041,     0,     0,     0,     0,     0,     0,     0,    15,     0,
       0,    16,    17,     0,     0,     0,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,   395,   396,   397,
     398,   399,   400,   401,   402,   403,   404,    69,   405,   406,
     407,   408,   409,   410,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,     0,    95,    96,    97,    98,    99,   100,
       0,   411,   412,     0,     0,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   413,   414,   415,   416,   417,   418,   419,   420,   421,
     422,   423,   424,   425,   426,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   427,   149,   150,
     151,   152,   153,   154,   155,   156,   428,     0,     0,   429,
       0,     0,     0,     0,     0,   430,   636,   431,   637,   432,
     158,   159,   433,   434,   160,     0,   161,     0,     0,     0,
       0,     0,     0,     0,     0,   162,   435,   918,   436,     2,
       3,     4,     5,     6,     7,     8,   394,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,   395,   396,   397,   398,   399,   400,   401,
     402,   403,   404,    69,   405,   406,   407,   408,   409,   410,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,     0,
      95,    96,    97,    98,    99,   100,     0,   411,   412,     0,
       0,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   413,   414,   415,
     416,   417,   418,   419,   420,   421,   422,   423,   424,   425,
     426,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   427,   149,   150,   151,   152,   153,   154,
     155,   156,   428,     0,     0,   429,     0,     0,     0,     0,
       0,   430,   636,   431,   637,   432,   158,   159,   433,   434,
     160,     0,   161,     0,     0,     0,     0,     0,     0,     0,
       0,   162,   435,   970,   436,     2,     3,     4,     5,     6,
       7,     8,   394,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      15,     0,     0,    16,    17,     0,     0,     0,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,   395,
     396,   397,   398,   399,   400,   401,   402,   403,   404,    69,
     405,   406,   407,   408,   409,   410,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,     0,    95,    96,    97,    98,
      99,   100,     0,   411,   412,     0,     0,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   413,   414,   415,   416,   417,   418,   419,
     420,   421,   422,   423,   424,   425,   426,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   427,
     149,   150,   151,   152,   153,   154,   155,   156,   428,     0,
       0,   429,     0,     0,     0,     0,     0,   430,   636,   431,
     637,   432,   158,   159,   433,   434,   160,     0,   161,     0,
       0,     0,     0,     0,     0,     0,     0,   162,   435,   995,
     436,     2,     3,     4,     5,     6,     7,     8,   394,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    15,     0,     0,    16,
      17,     0,     0,     0,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,   395,   396,   397,   398,   399,
     400,   401,   402,   403,   404,    69,   405,   406,   407,   408,
     409,   410,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,     0,    95,    96,    97,    98,    99,   100,     0,   411,
     412,     0,     0,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   413,
     414,   415,   416,   417,   418,   419,   420,   421,   422,   423,
     424,   425,   426,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   427,   149,   150,   151,   152,
     153,   154,   155,   156,   428,     0,     0,   429,     0,     0,
       0,     0,     0,   430,   636,   431,   637,   432,   158,   159,
     433,   434,   160,     0,   161,     0,     0,     0,     0,     0,
       0,     0,     0,   162,   435,  1057,   436,     2,     3,     4,
       5,     6,     7,     8,   394,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    15,     0,     0,    16,    17,     0,     0,     0,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,   395,   396,   397,   398,   399,   400,   401,   402,   403,
     404,    69,   405,   406,   407,   408,   409,   410,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,     0,    95,    96,
      97,    98,    99,   100,     0,   411,   412,     0,     0,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   413,   414,   415,   416,   417,
     418,   419,   420,   421,   422,   423,   424,   425,   426,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   427,   149,   150,   151,   152,   153,   154,   155,   156,
     428,     0,     0,   429,     0,     0,     0,     0,     0,   430,
     636,   431,   637,   432,   158,   159,   433,   434,   160,     0,
     161,     0,     0,     0,     0,     0,     0,     0,     0,   162,
     435,     0,   436,     2,     3,     4,     5,     6,     7,     8,
     394,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    15,     0,
       0,    16,    17,     0,     0,     0,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,   395,   396,   397,
     398,   399,   400,   401,   402,   403,   404,    69,   405,   406,
     407,   408,   409,   410,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,     0,    95,    96,    97,    98,    99,   100,
       0,   411,   412,     0,     0,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   413,   414,   415,   416,   417,   418,   419,   420,   421,
     422,   423,   424,   425,   426,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   427,   149,   150,
     151,   152,   153,   154,   155,   156,   428,     0,     0,   429,
       0,     0,     0,     0,     0,   430,     0,   431,     0,   432,
     158,   159,   433,   434,   160,     0,   161,     0,     0,     0,
       0,     0,     0,     0,     0,   162,   435,     1,   436,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,     0,
      95,    96,    97,    98,    99,   100,     0,     0,     0,     0,
       0,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,     0,   149,   150,   151,   152,   153,   154,
     155,   156,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   157,     0,     0,   158,   159,     0,     0,
     160,     0,   161,     0,     0,     0,     0,     0,     0,     0,
       0,   162,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,     0,    12,     0,     0,     0,     0,     0,    13,
       0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
     182,     0,     0,     0,     0,     0,     0,    15,     0,   183,
      16,    17,     0,     0,     0,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,     0,    95,    96,    97,    98,    99,   100,     0,
       0,     0,     0,     0,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,     0,   149,   150,   151,
     152,   153,   154,   155,   156,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   157,     0,     0,   158,
     159,     0,     0,   160,     0,   161,     0,     0,     0,     0,
       0,     0,     0,     0,   162,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      15,     0,     0,    16,    17,     0,     0,     0,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,     0,    95,    96,    97,    98,
      99,   100,     0,     0,     0,   741,   742,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,     0,
     149,   150,   151,   152,   153,   154,   155,   156,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   157,
       0,     0,   158,   159,     0,     0,   160,     0,   161,     0,
       0,     0,     0,     0,     0,     0,     0,   162,     2,     3,
       4,     5,     6,     7,     8,     9,    10,    11,   337,    12,
       0,     0,     0,     0,     0,    13,     0,    14,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    15,     0,     0,    16,    17,     0,     0,
       0,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,     0,    95,
      96,    97,    98,    99,   100,     0,     0,     0,     0,     0,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,     0,   149,   150,   151,   152,   153,   154,   155,
     156,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   157,     0,     0,   158,   159,     0,     0,   160,
       0,   161,     0,     0,     0,     0,     0,     0,     0,     0,
     162,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,   341,
       0,     0,     0,     0,     0,     0,    15,     0,     0,    16,
      17,     0,     0,     0,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,     0,    95,    96,    97,    98,    99,   100,     0,     0,
       0,     0,     0,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,     0,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   157,     0,     0,   158,   159,
       0,     0,   160,     0,   161,     0,     0,     0,     0,     0,
       0,     0,     0,   162,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,   733,    12,     0,     0,     0,     0,
       0,    13,     0,    14,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    15,
       0,     0,    16,    17,     0,     0,     0,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,     0,    95,    96,    97,    98,    99,
     100,     0,     0,     0,     0,     0,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,     0,   149,
     150,   151,   152,   153,   154,   155,   156,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   157,     0,
       0,   158,   159,     0,     0,   160,     0,   161,     0,     0,
       0,     0,     0,     0,     0,     0,   162,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,   735,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    15,     0,     0,    16,    17,     0,     0,     0,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,     0,    95,    96,
      97,    98,    99,   100,     0,     0,     0,     0,     0,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,     0,   149,   150,   151,   152,   153,   154,   155,   156,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   157,     0,     0,   158,   159,     0,     0,   160,     0,
     161,     0,     0,     0,     0,     0,     0,     0,     0,   162,
       2,     3,     4,     5,     6,     7,     8,     9,    10,    11,
       0,    12,     0,     0,     0,     0,     0,    13,     0,    14,
       0,     0,     0,     0,     0,     0,     0,   392,     0,     0,
       0,     0,     0,     0,     0,    15,     0,     0,    16,    17,
       0,     0,     0,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
       0,    95,    96,    97,    98,    99,   100,     0,     0,     0,
       0,     0,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,     0,   149,   150,   151,   152,   153,
     154,   155,   156,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   157,     0,     0,   158,   159,     0,
       0,   160,     0,   161,     0,     0,     0,     0,     0,     0,
       0,     0,   162,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,   784,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    15,     0,
       0,    16,    17,     0,     0,     0,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,     0,    95,    96,    97,    98,    99,   100,
       0,     0,     0,     0,     0,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,     0,   149,   150,
     151,   152,   153,   154,   155,   156,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   157,     0,     0,
     158,   159,     0,     0,   160,     0,   161,     0,     0,     0,
       0,     0,     0,     0,     0,   162,     2,     3,     4,     5,
       6,     7,     8,     9,    10,    11,     0,    12,     0,     0,
       0,     0,     0,    13,     0,    14,     0,     0,     0,     0,
     975,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    15,     0,     0,    16,    17,     0,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,     0,    95,    96,    97,
      98,    99,   100,     0,     0,     0,     0,     0,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
       0,   149,   150,   151,   152,   153,   154,   155,   156,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     157,     0,     0,   158,   159,     0,     0,   160,     0,   161,
       0,     0,     0,     0,     0,     0,     0,     0,   162,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,   977,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    15,     0,     0,    16,    17,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,     0,
      95,    96,    97,    98,    99,   100,     0,     0,     0,     0,
       0,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,     0,   149,   150,   151,   152,   153,   154,
     155,   156,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   157,     0,     0,   158,   159,     0,     0,
     160,     0,   161,     0,     0,     0,     0,     0,     0,     0,
       0,   162,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,     0,    12,     0,     0,     0,     0,     0,    13,
       0,    14,     0,     0,     0,     0,   981,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    15,     0,     0,
      16,    17,     0,     0,     0,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,     0,    95,    96,    97,    98,    99,   100,     0,
       0,     0,     0,     0,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,     0,   149,   150,   151,
     152,   153,   154,   155,   156,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   157,     0,     0,   158,
     159,     0,     0,   160,     0,   161,     0,     0,     0,     0,
       0,     0,     0,     0,   162,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      15,     0,     0,    16,    17,     0,     0,     0,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,     0,    95,    96,    97,    98,
      99,   100,     0,     0,     0,     0,     0,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,     0,
     149,   150,   151,   152,   153,   154,   155,   156,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   157,
       0,     0,   158,   159,     0,     0,   160,     0,   161,     0,
       0,     0,     0,     0,     0,     0,     0,   162,     2,     3,
       4,     5,     6,     7,     8,     9,    10,    11,     0,    12,
       0,   519,     0,     0,     0,     0,     0,    14,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    15,     0,     0,    16,    17,     0,     0,
       0,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,     0,     0,    95,
      96,    97,    98,    99,   100,     0,     0,     0,     0,     0,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,     0,   149,   150,   151,   152,   153,   154,   155,
     156,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   157,     0,     0,   158,   159,     0,     0,   160,
       0,   161,     0,     0,     0,     0,     0,     0,     0,     0,
     162,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,     0,    12,     0,   521,     0,     0,     0,     0,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    15,     0,     0,    16,
      17,     0,     0,     0,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
       0,     0,    95,    96,    97,    98,    99,   100,     0,     0,
       0,     0,     0,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,     0,   149,   150,   151,   152,
     153,   154,   155,   156,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   157,     0,     0,   158,   159,
       0,     0,   160,     0,   161,     0,     0,     0,     0,     0,
       0,     0,     0,   162,     2,     3,     4,     5,     6,     7,
       8,     9,    10,    11,     0,    12,     0,     0,     0,     0,
       0,     0,     0,    14,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    15,
       0,     0,    16,    17,     0,     0,     0,    18,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,     0,     0,    95,    96,    97,    98,    99,
     100,     0,     0,     0,     0,     0,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,     0,   149,
     150,   151,   152,   153,   154,   155,   156,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   157,     0,
       0,   158,   159,     0,     0,   160,     0,   161,     0,     0,
       0,     0,     0,     0,     0,     0,   162,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,     0,    12,     0,
       0,     0,     0,     0,     0,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   530,     0,     0,    16,    17,     0,     0,     0,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,     0,     0,    95,    96,
      97,    98,    99,   100,     0,     0,     0,     0,     0,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,     0,   149,   150,   151,   152,   153,   154,   155,   156,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   157,     0,     0,   158,   159,     0,     0,   160,     0,
     161,     0,     0,     0,     0,     0,     0,     0,     0,   162,
       2,     3,     4,     5,     6,     7,     8,     9,    10,    11,
       0,    12,     0,     0,     0,     0,     0,     0,     0,    14,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   533,     0,     0,    16,    17,
       0,     0,     0,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,     0,
       0,    95,    96,    97,    98,    99,   100,     0,     0,     0,
       0,     0,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,     0,   149,   150,   151,   152,   153,
     154,   155,   156,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   157,     0,     0,   158,   159,     0,
       0,   160,     0,   161,     0,     0,     0,     0,     0,     0,
       0,     0,   162,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,     0,    12,     0,     0,     0,     0,     0,
       0,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   540,     0,
       0,    16,    17,     0,     0,     0,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,     0,     0,    95,    96,    97,    98,    99,   100,
       0,     0,     0,     0,     0,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,     0,   149,   150,
     151,   152,   153,   154,   155,   156,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   157,     0,     0,
     158,   159,     0,     0,   160,     0,   161,     0,     0,     0,
       0,     0,     0,     0,     0,   162,     2,     3,     4,     5,
       6,     7,     8,     9,    10,    11,     0,    12,     0,     0,
       0,     0,     0,     0,     0,    14,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   538,     0,     0,     0,     0,     0,     0,     0,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,     0,     0,    95,    96,    97,
      98,    99,   100,     0,     0,     0,     0,     0,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
       0,   149,   150,   151,   152,   153,   154,   155,   156,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     157,     0,     0,   158,   159,     0,     0,   160,     0,   161,
       0,     0,     0,     0,     0,     0,     0,     0,   162,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,     0,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,     0,     0,
      95,    96,    97,    98,    99,   100,     0,     0,     0,     0,
       0,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,     0,   149,   150,   151,   152,   153,   154,
     155,   156,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   157,     0,     0,   158,   159,     0,     0,
     160,     0,   161,     0,     0,     0,     0,     0,     0,     0,
       0,   162
};

static const yytype_int16 yycheck[] =
{
       0,    16,    17,    13,   719,    39,    40,   638,    39,    40,
      15,    11,    12,   180,    14,    39,    40,   243,    39,    40,
    1032,    12,    10,    19,    10,    16,   514,    13,    41,    42,
      12,   216,    28,   218,    12,    31,    13,   243,   320,   321,
     322,   323,   324,    60,    61,    62,    63,    64,    65,    66,
      67,    68,   237,    70,    71,    72,    73,    74,    75,    18,
      48,    14,    39,    40,    23,    39,    40,    12,    27,    39,
      40,    15,    14,    13,    29,    13,    29,   792,    37,    19,
      13,    19,    35,  1095,    13,    34,   717,    29,    13,    12,
      39,    40,    45,    35,    94,    39,    40,    13,    12,    39,
      40,    39,    40,    45,    13,    16,    39,    40,    15,    13,
      39,    40,    15,    13,    39,    40,    13,   284,   285,    13,
       0,    13,    12,    39,    40,    13,   123,    13,   843,    13,
      39,    40,    39,    40,   301,    39,    40,    13,   305,    39,
      40,    13,    39,    40,    13,    39,    40,    39,    40,    13,
      10,    39,    40,    39,    40,    39,    40,    19,    13,   642,
      43,    44,   877,    39,    40,   170,    12,    39,    40,    12,
      39,    40,    12,    16,    17,    39,    40,    39,    40,   213,
     214,    12,   182,   214,    39,    40,   207,    19,   212,    21,
     190,   191,   192,   193,   194,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,   207,   208,   209,
     210,   211,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,   225,   226,   227,   228,   229,
     230,   437,   720,   215,    12,   217,   719,   519,   238,   521,
     214,    12,    12,    12,   214,    16,   961,    16,    17,    39,
      40,   210,   252,   253,   254,   255,   256,    30,    13,   259,
     260,   261,   262,   263,   264,   265,   266,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   276,   277,   278,   279,
     280,   281,   282,   283,    39,    40,   286,   287,   288,   289,
     290,   291,   292,   293,   294,   295,   296,   297,   298,   299,
     300,    21,   302,   303,   304,   788,   306,   307,   318,   319,
     798,   311,    16,   313,    12,    19,   331,   332,    13,    39,
      40,   336,    13,    12,   243,    13,   331,   332,    13,    13,
     335,   336,    15,   333,   224,   225,   226,   227,   228,   229,
     230,   231,   232,    13,    39,    40,   346,   347,    39,    40,
      12,    39,    40,    12,    39,    40,    39,    40,    12,   842,
     843,    12,   225,   226,   227,   228,   229,   230,   231,    39,
      40,    13,    13,    13,   215,    13,   217,   544,   545,   396,
     397,   398,   399,   400,   401,   402,   403,   404,   405,   406,
     407,   408,   409,   410,   877,    13,    21,    39,    40,    39,
      40,    39,    40,    13,    19,    16,    17,    13,    32,    33,
     218,    13,    36,    13,    39,    40,    13,    41,    42,    10,
      13,    39,    40,    13,    39,    40,   914,    13,   428,    39,
      40,    13,   432,    39,    40,    13,   642,    39,    40,    39,
      40,   924,    39,    40,    13,   612,    39,    40,   448,    39,
      40,   618,   619,    39,    40,    13,   623,    39,    40,    13,
      12,    39,    40,   946,    13,   216,    13,   218,    13,    12,
      39,    40,    13,    13,    13,    13,    13,    12,   961,    13,
      16,    39,    40,    13,    12,    39,    40,    19,    13,    12,
      39,    40,    39,    40,    39,    40,    13,    12,    39,    40,
      39,    40,    39,    40,    13,    39,    40,    39,    40,    39,
      40,    13,    12,   719,    39,    40,    13,    12,   437,    13,
      16,    17,    39,    40,   691,   530,   531,    12,   533,   534,
      39,    40,  1015,   538,    12,   540,   541,    39,    40,    13,
      10,    13,    39,    40,    12,    39,    40,   547,   216,    13,
     218,   777,    13,    20,   780,    12,   216,    13,   218,    13,
      19,   728,    13,    15,    13,    39,    40,    39,    40,   237,
      12,   777,    39,    40,   780,    39,    40,   237,    39,    40,
      39,    40,   788,    39,    40,    39,    40,    12,    39,    40,
      39,    40,    13,    12,   594,   595,   596,   597,   598,   599,
     600,   601,   602,   603,   604,   605,   606,   607,   608,   609,
     610,   611,    13,   613,   614,   615,   616,   617,    39,    40,
     620,   621,   622,    21,   624,    13,   626,   210,   211,    13,
      12,    12,   632,    12,    10,   635,   842,   843,    39,    40,
      12,    39,    40,    13,    12,    12,   646,   647,    12,   649,
     650,    39,    40,    12,   654,    39,    40,   657,   658,    12,
     660,   661,    13,    19,    13,   665,   666,   667,   668,    39,
      40,   877,   672,   673,   674,   675,   676,   677,   678,   679,
     680,    12,    12,    39,    40,   852,    12,    12,    39,    40,
      39,    40,   692,   693,   694,   695,   696,   697,   698,   699,
     700,   701,   702,    13,   704,   705,   706,    19,   708,   709,
     710,   878,   938,   939,    12,   715,   883,    12,   924,    13,
      16,    13,    12,   642,    13,   892,    13,    39,    40,    39,
      40,    13,   938,   939,    13,    16,    13,    20,    13,    12,
     946,    13,    16,    13,   911,    39,    40,    39,    40,    13,
      39,    40,    39,    40,    13,   961,    13,    39,    40,    13,
      39,    40,    39,    40,    39,    40,    13,    39,    40,    39,
      40,    12,    12,    12,    12,    39,    40,    13,   778,   779,
      39,    40,    39,    40,    13,    39,    40,    13,    21,    13,
      10,    13,    39,    40,    13,    16,    13,   641,   642,    12,
     719,    13,    12,    39,    40,  1031,    39,    40,    13,  1015,
      39,    40,    21,    39,    40,    39,    40,    39,    40,    12,
      39,    40,    39,    40,    13,  1031,    13,    39,    40,    13,
      39,    40,   999,  1000,    39,    40,    16,    12,   216,   839,
     218,    12,    12,    19,    12,    12,   846,    12,   848,   849,
      39,    40,    39,    40,    20,    39,    40,    19,   777,   237,
     860,   780,   862,    39,    40,   865,    15,    20,  1094,   788,
      19,   871,    21,    39,    40,   719,   876,    39,    40,   879,
      19,   881,   882,    19,   884,   885,    39,    40,  1094,    19,
      39,    40,    19,   893,    19,   895,    12,   897,    16,    19,
      39,    40,    12,    39,    40,    19,    12,   907,   908,    39,
      40,    19,    39,    40,    39,    40,   916,    19,   918,    39,
      40,   921,    12,   842,   843,    39,    40,    12,    12,   929,
     930,    39,    40,   933,    16,    12,    19,    39,    40,    19,
     940,    19,    12,   943,    16,    12,    19,   791,  1115,  1116,
      12,    19,    12,    19,    12,  1122,    39,    40,   877,    39,
      40,    39,    40,    19,    12,   965,    39,    40,   968,    19,
     970,    39,    40,    39,    40,   975,    12,   977,    12,    12,
     980,   981,    19,    39,    40,   216,   986,   218,    12,    39,
      40,    12,    12,   993,    19,   995,    19,   841,   842,   843,
      12,    19,    39,    40,    19,   924,   237,    12,  1008,  1009,
      12,    19,  1012,    12,    39,    40,    39,    40,    19,   938,
     939,    39,    40,  1023,    39,    40,    19,   946,    12,    19,
    1030,    39,    40,   877,    12,   216,    16,   218,    39,    40,
      19,    12,   961,    19,    16,    19,    39,    40,  1048,    39,
      40,    19,    12,    19,    12,  1055,   237,  1057,    12,    12,
      39,    40,    13,    39,    40,    39,    40,  1067,  1068,    19,
      16,    39,    40,    39,    40,    12,   920,    19,    16,   923,
     924,  1081,  1082,    19,    12,    19,  1086,    19,    13,    39,
      40,    12,    31,    19,    12,    19,  1015,    39,    40,    19,
      12,   945,   946,    39,    40,    39,    40,    39,    40,    19,
      12,    19,  1031,    39,    40,    39,    40,   961,   962,    39,
      40,    19,    12,    19,    13,    19,    12,    19,   972,    39,
      40,    39,    40,    21,    19,    12,    19,    12,    19,    12,
      19,    39,    40,    39,    40,    39,    40,    39,    40,    12,
      12,    39,    40,   997,    39,    40,    39,    40,    39,    40,
      39,    40,    19,    12,    19,   216,    16,   218,    19,    12,
    1014,  1015,    19,  1017,  1018,  1094,  1020,    19,    16,    19,
      13,    12,    39,    40,    39,    40,   237,    19,    39,    40,
      19,    12,    39,    40,    20,    12,    19,    39,    40,    39,
      40,  1045,   216,    12,   218,    20,    12,    39,    40,    20,
      39,    40,    12,    39,    40,  1059,    39,    40,  1062,    21,
    1064,  1065,    20,   237,    39,    40,    20,    12,    39,    40,
      20,    12,  1076,  1077,    12,    12,    12,    39,    40,  1083,
      12,    39,    40,    12,    12,    39,    40,  1091,    12,    39,
      40,    12,    12,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,  1107,    14,    20,    20,  1111,  1112,    16,
      20,    13,    22,    13,    20,    12,   325,   326,   327,   328,
     329,  1125,    15,    10,    39,    40,    10,    21,    38,    21,
      10,    41,    42,    39,    40,    10,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    28,   124,   125,   126,   127,   128,   129,
      13,   131,   132,    19,    15,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,    12,    20,   209,
      20,    12,    20,    13,    13,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    13,   226,    39,    40,    39,
      40,    39,    40,    20,   234,   235,   236,   237,   238,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    20,
      14,    13,    39,    40,    21,   234,    20,    27,    22,    27,
      19,    21,    19,    19,    19,    19,    13,    13,    39,    40,
      13,    10,    39,    40,    38,    27,    10,    41,    42,    39,
      40,   208,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,    13,
     124,   125,   126,   127,   128,   129,    13,   131,   132,    13,
      13,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,    13,    13,   209,    21,    13,    21,   233,
      21,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    42,   226,    13,    39,    40,    39,    40,    39,    40,
     234,   235,   236,   237,   238,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    21,    14,    21,    13,    21,
      19,    13,    20,    13,    22,    13,    27,    21,    13,    13,
     634,   509,    94,    39,    40,    39,    40,    39,    40,   654,
      38,  1104,  1095,    41,    42,    39,    40,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,    -1,   124,   125,   126,   127,
     128,   129,    -1,   131,   132,    -1,    -1,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,    -1,
      -1,   209,    21,    -1,    -1,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,    -1,
      39,    40,    -1,    -1,    -1,    -1,   234,   235,   236,   237,
     238,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    24,    25,    26,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    39,    40,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,    -1,   124,   125,   126,   127,   128,   129,    -1,   131,
     132,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,    -1,    -1,   209,    -1,    -1,
      -1,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   234,   235,   236,   237,   238,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,   224,   225,   226,
     227,   228,   229,   230,   231,   232,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,    -1,   124,   125,
     126,   127,   128,   129,    -1,   131,   132,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,   199,   200,   201,   202,   203,   204,   205,
     206,    -1,    -1,   209,    -1,    -1,    -1,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   234,   235,
     236,    -1,   238,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,   225,   226,   227,   228,   229,   230,   231,
     232,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    -1,   124,   125,   126,   127,   128,   129,
      -1,   131,   132,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,    -1,    -1,   209,
      -1,    -1,    -1,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   235,   236,   237,   238,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,    -1,
     124,   125,   126,   127,   128,   129,    -1,   131,   132,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,    -1,    -1,   209,    -1,    -1,    -1,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   235,   236,   237,   238,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,    -1,   124,   125,   126,   127,
     128,   129,    -1,   131,   132,    -1,    -1,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,    -1,
      -1,   209,    -1,    -1,    -1,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,   236,   237,
     238,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,    -1,   124,   125,   126,   127,   128,   129,    -1,   131,
     132,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,    -1,    -1,   209,    -1,    -1,
      -1,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   235,   236,   237,   238,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,    -1,   124,   125,
     126,   127,   128,   129,    -1,   131,   132,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,   199,   200,   201,   202,   203,   204,   205,
     206,    -1,    -1,   209,    -1,    -1,    -1,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
     236,    -1,   238,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    -1,   124,   125,   126,   127,   128,   129,
      -1,   131,   132,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,    -1,    -1,   209,
      -1,    -1,    -1,    -1,    -1,   215,    -1,   217,    -1,   219,
     220,   221,   222,   223,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   235,   236,     1,   238,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,    -1,
     124,   125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,    -1,   198,   199,   200,   201,   202,   203,
     204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   235,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      31,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    40,
      41,    42,    -1,    -1,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,    -1,   124,   125,   126,   127,   128,   129,    -1,
      -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,    -1,   198,   199,   200,
     201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,
     221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,    -1,   124,   125,   126,   127,
     128,   129,    -1,    -1,    -1,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,    -1,
     198,   199,   200,   201,   202,   203,   204,   205,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,
      -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,    -1,   124,
     125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,    -1,   198,   199,   200,   201,   202,   203,   204,
     205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,
      -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     235,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    31,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,    -1,   124,   125,   126,   127,   128,   129,    -1,    -1,
      -1,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,    -1,   198,   199,   200,   201,
     202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,
      -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   235,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,    -1,   124,   125,   126,   127,   128,
     129,    -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,    -1,   198,
     199,   200,   201,   202,   203,   204,   205,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,
      -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    28,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,    -1,   124,   125,
     126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,    -1,   198,   199,   200,   201,   202,   203,   204,   205,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    30,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,
      -1,    -1,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
      -1,   124,   125,   126,   127,   128,   129,    -1,    -1,    -1,
      -1,    -1,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   185,   186,   187,   188,   189,   190,   191,   192,
     193,   194,   195,   196,    -1,   198,   199,   200,   201,   202,
     203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,
      -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   235,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    -1,   124,   125,   126,   127,   128,   129,
      -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,    -1,   198,   199,
     200,   201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,
     220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,
      27,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,    -1,   124,   125,   126,
     127,   128,   129,    -1,    -1,    -1,    -1,    -1,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
      -1,   198,   199,   200,   201,   202,   203,   204,   205,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    27,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,    -1,
     124,   125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,    -1,   198,   199,   200,   201,   202,   203,
     204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   235,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    -1,    -1,    -1,    27,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,
      41,    42,    -1,    -1,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,    -1,   124,   125,   126,   127,   128,   129,    -1,
      -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,    -1,   198,   199,   200,
     201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,
     221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,    -1,   124,   125,   126,   127,
     128,   129,    -1,    -1,    -1,    -1,    -1,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,    -1,
     198,   199,   200,   201,   202,   203,   204,   205,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,
      -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    -1,    14,
      -1,    16,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,    -1,    -1,   124,
     125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,    -1,   198,   199,   200,   201,   202,   203,   204,
     205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,
      -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     235,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    16,    -1,    -1,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
      -1,    -1,   124,   125,   126,   127,   128,   129,    -1,    -1,
      -1,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,    -1,   198,   199,   200,   201,
     202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,
      -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   235,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,    -1,    -1,   124,   125,   126,   127,   128,
     129,    -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,    -1,   198,
     199,   200,   201,   202,   203,   204,   205,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,
      -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,    -1,    -1,   124,   125,
     126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,    -1,   198,   199,   200,   201,   202,   203,   204,   205,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,
      -1,    -1,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,    -1,
      -1,   124,   125,   126,   127,   128,   129,    -1,    -1,    -1,
      -1,    -1,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   185,   186,   187,   188,   189,   190,   191,   192,
     193,   194,   195,   196,    -1,   198,   199,   200,   201,   202,
     203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,
      -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   235,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,    -1,    -1,   124,   125,   126,   127,   128,   129,
      -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,    -1,   198,   199,
     200,   201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,
     220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,    -1,    -1,   124,   125,   126,
     127,   128,   129,    -1,    -1,    -1,    -1,    -1,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
      -1,   198,   199,   200,   201,   202,   203,   204,   205,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,    -1,    -1,
     124,   125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,    -1,   198,   199,   200,   201,   202,   203,
     204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   235
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     1,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    14,    20,    22,    38,    41,    42,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   124,   125,   126,   127,   128,
     129,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   198,
     199,   200,   201,   202,   203,   204,   205,   217,   220,   221,
     224,   226,   235,   240,   241,   262,   263,   264,   265,   266,
     267,   268,   269,   270,   273,   274,   276,   277,   278,   280,
      12,   262,    31,    40,   262,   265,   262,   269,   267,   267,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    16,   279,   279,   279,   279,   279,   279,    12,   279,
     279,   279,   279,    12,   279,   279,   279,   279,   279,   279,
     262,   263,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    29,   259,
     261,    12,    12,    12,    12,   251,     0,    21,    39,    40,
      18,    23,    27,    37,   210,    32,    33,    36,    41,    42,
     269,    43,    44,    14,    29,    35,    45,    13,   258,   262,
      13,    31,   262,   275,    15,    15,    19,    21,    24,    25,
      26,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,    30,   262,    10,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    98,    99,   100,   101,   102,
     103,   131,   132,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   197,   206,   209,
     215,   217,   219,   222,   223,   236,   238,   242,   244,   252,
     253,   254,   255,   256,   257,   258,   264,   270,   123,   262,
     262,   262,   262,   262,    13,    13,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   258,   258,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   258,   262,
     262,   262,   258,   262,   262,    10,   218,    19,    21,   260,
     262,    10,   262,    10,    13,   250,   265,   265,   266,    16,
     266,    16,   266,   266,   266,   268,   268,   268,   268,   268,
      38,   267,   269,    38,   267,   269,   262,    10,    38,   269,
      38,   267,   269,    13,    19,    12,    15,    19,    28,    31,
     262,   262,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    16,    17,    16,    16,    16,    16,
      16,    16,    16,    16,    16,    16,    16,    16,    16,    16,
      16,    16,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,   245,   262,    10,
     246,   262,    12,    12,    10,    12,   216,   218,   234,   243,
     244,   247,   248,   249,    13,    20,    16,    17,    20,    16,
      17,    29,   262,   271,   272,    13,    13,    19,    19,    13,
      19,    19,    13,    13,    13,    19,    19,    19,    19,    13,
      13,    13,    19,    19,    19,    19,    19,    19,    19,    19,
      19,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      19,    19,    19,    19,    19,    19,    19,    19,    19,    19,
      19,    19,    19,    13,    19,    19,    19,    13,    19,    19,
      16,   259,    13,    13,    13,    19,    16,    19,   217,   242,
      13,   266,   266,   269,   269,   269,   269,    15,    12,   269,
     269,   269,   258,    13,   258,    28,   262,    31,    15,    15,
      15,   133,   134,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   258,   262,   262,   262,   262,   262,   258,   258,   262,
     262,   262,   258,   262,    10,    48,   262,   207,   210,   211,
     214,   262,    10,   251,    13,   262,    10,   250,    21,   243,
     243,   247,    21,   262,   262,   262,   262,    10,    34,   271,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,    10,   258,   262,
     262,   262,   262,   262,   262,   262,   262,   262,   262,   262,
     262,   262,   262,   262,   262,   262,   262,    28,   250,   237,
     243,   247,   248,   242,   258,    13,    19,    15,    12,    12,
      13,    13,    19,    13,    13,    13,    13,    13,    13,    13,
      19,    13,    19,    19,    19,    19,   244,   262,   262,   244,
      13,    19,    13,   247,   243,   248,    12,   242,    19,    19,
      13,    19,    19,    19,    19,    19,    13,    13,    13,    13,
      13,    13,    19,    19,    13,    19,    13,    19,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    19,    19,    13,
      13,    19,    13,    13,    13,   262,   237,   243,   237,   243,
     247,   237,   243,   247,   248,    13,   262,   262,   262,    27,
      27,   258,   262,    27,   262,    10,    10,   262,   208,   214,
     212,   262,   262,   237,   243,   247,   248,   258,   262,   262,
     262,   258,   262,   262,   258,   262,   262,   262,   262,   262,
     258,   242,    21,   262,   262,   237,   243,   262,   237,   243,
     237,   243,   247,    13,    13,    27,   262,    27,   262,    13,
      19,    27,   262,    13,    13,    13,    19,   244,   244,   262,
      19,    13,   262,   237,   243,   237,   243,   247,    13,    19,
      19,    13,    13,    13,    13,    13,    13,    13,    19,    19,
      13,    13,   237,   243,   247,   248,   243,    21,    21,   262,
      21,   262,   262,   237,   243,   262,   262,   262,   262,   262,
     213,   214,    12,   224,   225,   226,   227,   228,   229,   230,
     231,   232,   281,   282,   284,    21,   262,   262,   237,   243,
     258,   258,   262,   262,   262,   237,   243,   237,   243,   247,
     243,   243,    21,   243,    21,    21,   262,    19,    19,   262,
     244,   281,   283,   233,    42,   243,    21,    21,   262,    13,
      13,    19,    19,    21,   262,   262,   237,   243,   243,   243,
     243,    21,   262,   262,   214,    19,    13,   225,   226,   227,
     228,   229,   230,   231,    27,   243,   243,    21,   262,   262,
     243,    21,    21,   262,   243,    19,    19,   244,   283,   282,
     243,    13,    19,   243,   243,    21,   258,   258,    13,   258,
     243,    13,    13,    13
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   239,   240,   241,   241,   241,   242,   242,   243,   243,
     244,   244,   244,   244,   244,   244,   244,   244,   245,   245,
     246,   246,   246,   247,   247,   248,   248,   249,   250,   250,
     251,   251,   251,   251,   251,   251,   251,   251,   251,   251,
     251,   251,   251,   251,   251,   251,   251,   251,   251,   251,
     251,   251,   251,   251,   252,   252,   252,   252,   252,   252,
     252,   252,   252,   252,   252,   252,   252,   252,   252,   252,
     252,   252,   252,   252,   252,   252,   252,   252,   252,   252,
     252,   252,   252,   252,   252,   252,   253,   253,   253,   253,
     254,   254,   254,   254,   254,   254,   254,   254,   255,   256,
     256,   256,   256,   256,   256,   256,   256,   256,   256,   256,
     256,   256,   256,   256,   256,   257,   257,   257,   257,   257,
     257,   257,   257,   257,   257,   257,   257,   257,   257,   257,
     257,   258,   258,   259,   259,   260,   260,   261,   262,   262,
     263,   263,   263,   263,   264,   265,   265,   265,   265,   265,
     265,   265,   265,   266,   266,   266,   266,   266,   266,   267,
     267,   267,   267,   268,   268,   268,   268,   268,   268,   268,
     268,   268,   269,   269,   269,   269,   269,   269,   270,   270,
     270,   270,   270,   270,   270,   270,   270,   270,   270,   270,
     270,   270,   270,   270,   270,   270,   270,   270,   270,   270,
     270,   270,   270,   270,   270,   270,   270,   270,   270,   270,
     270,   270,   270,   270,   270,   270,   270,   270,   270,   270,
     270,   270,   270,   270,   270,   270,   270,   270,   270,   270,
     270,   271,   271,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   273,   273,   273,   273,   273,   273,   273,   274,
     274,   274,   274,   275,   275,   275,   276,   276,   276,   277,
     277,   277,   277,   277,   277,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   278,   278,   278,   278,   278,   278,
     278,   278,   278,   278,   279,   279,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   281,   281,   281,   281,   281,   281,   281,   281,
     281,   281,   281,   281,   281,   281,   282,   282,   283,   283,
     284,   284
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     2,     1,     1,     1,     1,     1,
       1,     3,     4,     3,     2,     2,     4,     2,     3,     5,
       7,     9,     5,     2,     3,     2,     3,     2,     1,     3,
       5,     6,     5,     4,     8,     9,     8,     7,     6,     7,
       6,     5,     9,    10,     9,     8,     8,     9,     8,     7,
      11,    12,    11,    10,     1,     1,     1,     4,     3,     4,
       6,     7,     6,     4,     4,     4,     4,     4,     4,     4,
      12,     4,     6,     7,     6,     4,     4,     6,     7,    12,
       6,     6,    11,     1,     1,     3,     1,     2,     1,     2,
       3,     3,     6,     6,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     1,     3,     1,     3,     1,     1,     4,     1,     4,
       1,     3,     3,     2,     4,     1,     3,     3,     3,     3,
       4,     4,     3,     1,     3,     3,     3,     3,     3,     1,
       1,     2,     2,     1,     2,     2,     3,     3,     4,     4,
       4,     4,     1,     3,     4,     4,     3,     4,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     4,     4,     3,     1,     1,     1,
       1,     3,     3,     1,     1,     3,     6,     6,     5,     2,
       4,     1,     2,     9,     6,     8,     5,     8,     5,     7,
       4,     5,     1,     1,     1,     1,     1,     1,     1,     4,
       3,     5,     6,     1,     3,     5,     5,     5,     3,     3,
       3,     3,     4,     4,     4,     4,     4,     3,     3,     6,
       4,     8,    10,     8,     4,     4,    10,     4,     4,     4,
       4,     8,     8,     8,     8,     4,     4,     4,     6,     6,
       6,     6,     6,     6,     8,     8,     6,     4,     4,     6,
      12,     6,     6,     6,     6,     6,     6,     6,     6,     6,
      14,     4,     8,     6,     6,     4,     8,     6,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     6,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     2,     0,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     1,     1,     1,     1,     1,     1,     1,     3,
       3,     3,     3,     3,     3,     3,     1,     1,     1,     3,
       1,     3
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (myScanner, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, myScanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void *myScanner)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (myScanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void *myScanner)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep, myScanner);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule, void *myScanner)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              , myScanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, myScanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, void *myScanner)
{
  YYUSE (yyvaluep);
  YYUSE (myScanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void *myScanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, myScanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 437 "miniparser.y" /* yacc.c:1646  */
    {
			    minitree = (yyvsp[0].tree);
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
#line 4054 "miniparser.c" /* yacc.c:1646  */
    break;

  case 3:
#line 446 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4062 "miniparser.c" /* yacc.c:1646  */
    break;

  case 4:
#line 450 "miniparser.y" /* yacc.c:1646  */
    {
                            miniparserSemicolonAtEnd = 1;
			    (yyval.tree) = (yyvsp[-1].tree);
			  }
#line 4071 "miniparser.c" /* yacc.c:1646  */
    break;

  case 5:
#line 455 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = NULL;
			  }
#line 4079 "miniparser.c" /* yacc.c:1646  */
    break;

  case 6:
#line 461 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4087 "miniparser.c" /* yacc.c:1646  */
    break;

  case 7:
#line 465 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4095 "miniparser.c" /* yacc.c:1646  */
    break;

  case 8:
#line 471 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4103 "miniparser.c" /* yacc.c:1646  */
    break;

  case 9:
#line 475 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4111 "miniparser.c" /* yacc.c:1646  */
    break;

  case 10:
#line 481 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4119 "miniparser.c" /* yacc.c:1646  */
    break;

  case 11:
#line 485 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCommandList((yyvsp[-1].list));
                          }
#line 4127 "miniparser.c" /* yacc.c:1646  */
    break;

  case 12:
#line 489 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list)));
                          }
#line 4135 "miniparser.c" /* yacc.c:1646  */
    break;

  case 13:
#line 493 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCommandList((yyvsp[-1].list));
                          }
#line 4143 "miniparser.c" /* yacc.c:1646  */
    break;

  case 14:
#line 497 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNop();
                          }
#line 4151 "miniparser.c" /* yacc.c:1646  */
    break;

  case 15:
#line 501 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4159 "miniparser.c" /* yacc.c:1646  */
    break;

  case 16:
#line 505 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeWhile((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 4167 "miniparser.c" /* yacc.c:1646  */
    break;

  case 17:
#line 509 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4175 "miniparser.c" /* yacc.c:1646  */
    break;

  case 18:
#line 515 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIf((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 4183 "miniparser.c" /* yacc.c:1646  */
    break;

  case 19:
#line 519 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIfElse((yyvsp[-4].tree),(yyvsp[-2].tree),(yyvsp[0].tree));
                          }
#line 4191 "miniparser.c" /* yacc.c:1646  */
    break;

  case 20:
#line 527 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFor((yyvsp[-6].value), (yyvsp[-4].tree), (yyvsp[-2].tree), makeConstantDouble(1.0), (yyvsp[0].tree));
			    safeFree((yyvsp[-6].value));
                          }
#line 4200 "miniparser.c" /* yacc.c:1646  */
    break;

  case 21:
#line 532 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFor((yyvsp[-8].value), (yyvsp[-6].tree), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree));
			    safeFree((yyvsp[-8].value));
                          }
#line 4209 "miniparser.c" /* yacc.c:1646  */
    break;

  case 22:
#line 537 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeForIn((yyvsp[-4].value), (yyvsp[-2].tree), (yyvsp[0].tree));
			    safeFree((yyvsp[-4].value));
                          }
#line 4218 "miniparser.c" /* yacc.c:1646  */
    break;

  case 23:
#line 545 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[-1].tree));
			  }
#line 4226 "miniparser.c" /* yacc.c:1646  */
    break;

  case 24:
#line 549 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].tree));
			  }
#line 4234 "miniparser.c" /* yacc.c:1646  */
    break;

  case 25:
#line 555 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[-1].tree));
			  }
#line 4242 "miniparser.c" /* yacc.c:1646  */
    break;

  case 26:
#line 559 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].tree));
			  }
#line 4250 "miniparser.c" /* yacc.c:1646  */
    break;

  case 27:
#line 565 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVariableDeclaration((yyvsp[0].list));
			  }
#line 4258 "miniparser.c" /* yacc.c:1646  */
    break;

  case 28:
#line 572 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].value));
			  }
#line 4266 "miniparser.c" /* yacc.c:1646  */
    break;

  case 29:
#line 576 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].value));
			  }
#line 4274 "miniparser.c" /* yacc.c:1646  */
    break;

  case 30:
#line 582 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4282 "miniparser.c" /* yacc.c:1646  */
    break;

  case 31:
#line 586 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))), makeUnit());
                          }
#line 4290 "miniparser.c" /* yacc.c:1646  */
    break;

  case 32:
#line 590 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4298 "miniparser.c" /* yacc.c:1646  */
    break;

  case 33:
#line 594 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
#line 4306 "miniparser.c" /* yacc.c:1646  */
    break;

  case 34:
#line 598 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4314 "miniparser.c" /* yacc.c:1646  */
    break;

  case 35:
#line 602 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))), (yyvsp[-2].tree));
                          }
#line 4322 "miniparser.c" /* yacc.c:1646  */
    break;

  case 36:
#line 606 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4330 "miniparser.c" /* yacc.c:1646  */
    break;

  case 37:
#line 610 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), (yyvsp[-2].tree));
                          }
#line 4338 "miniparser.c" /* yacc.c:1646  */
    break;

  case 38:
#line 614 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-4].list), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4346 "miniparser.c" /* yacc.c:1646  */
    break;

  case 39:
#line 618 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-5].list), makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))), makeUnit());
                          }
#line 4354 "miniparser.c" /* yacc.c:1646  */
    break;

  case 40:
#line 622 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-4].list), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4362 "miniparser.c" /* yacc.c:1646  */
    break;

  case 41:
#line 626 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-3].list), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
#line 4370 "miniparser.c" /* yacc.c:1646  */
    break;

  case 42:
#line 630 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-7].list), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4378 "miniparser.c" /* yacc.c:1646  */
    break;

  case 43:
#line 634 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-8].list), makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))), (yyvsp[-2].tree));
                          }
#line 4386 "miniparser.c" /* yacc.c:1646  */
    break;

  case 44:
#line 638 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-7].list), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4394 "miniparser.c" /* yacc.c:1646  */
    break;

  case 45:
#line 642 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-6].list), makeCommandList(addElement(NULL, makeNop())), (yyvsp[-2].tree));
                          }
#line 4402 "miniparser.c" /* yacc.c:1646  */
    break;

  case 46:
#line 646 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-6].value), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4410 "miniparser.c" /* yacc.c:1646  */
    break;

  case 47:
#line 650 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-7].value), makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))), makeUnit());
                          }
#line 4418 "miniparser.c" /* yacc.c:1646  */
    break;

  case 48:
#line 654 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-6].value), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4426 "miniparser.c" /* yacc.c:1646  */
    break;

  case 49:
#line 658 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-5].value), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
#line 4434 "miniparser.c" /* yacc.c:1646  */
    break;

  case 50:
#line 662 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-9].value), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4442 "miniparser.c" /* yacc.c:1646  */
    break;

  case 51:
#line 666 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-10].value), makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))), (yyvsp[-2].tree));
                          }
#line 4450 "miniparser.c" /* yacc.c:1646  */
    break;

  case 52:
#line 670 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-9].value), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4458 "miniparser.c" /* yacc.c:1646  */
    break;

  case 53:
#line 674 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-8].value), makeCommandList(addElement(NULL, makeNop())), (yyvsp[-2].tree));
                          }
#line 4466 "miniparser.c" /* yacc.c:1646  */
    break;

  case 54:
#line 682 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeQuit();
			  }
#line 4474 "miniparser.c" /* yacc.c:1646  */
    break;

  case 55:
#line 686 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFalseRestart();
			  }
#line 4482 "miniparser.c" /* yacc.c:1646  */
    break;

  case 56:
#line 690 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNop();
			  }
#line 4490 "miniparser.c" /* yacc.c:1646  */
    break;

  case 57:
#line 694 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNopArg((yyvsp[-1].tree));
			  }
#line 4498 "miniparser.c" /* yacc.c:1646  */
    break;

  case 58:
#line 698 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNopArg(makeDefault());
			  }
#line 4506 "miniparser.c" /* yacc.c:1646  */
    break;

  case 59:
#line 702 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrint((yyvsp[-1].list));
			  }
#line 4514 "miniparser.c" /* yacc.c:1646  */
    break;

  case 60:
#line 706 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNewFilePrint((yyvsp[0].tree), (yyvsp[-3].list));
			  }
#line 4522 "miniparser.c" /* yacc.c:1646  */
    break;

  case 61:
#line 710 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAppendFilePrint((yyvsp[0].tree), (yyvsp[-4].list));
			  }
#line 4530 "miniparser.c" /* yacc.c:1646  */
    break;

  case 62:
#line 714 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePlot(addElement((yyvsp[-1].list), (yyvsp[-3].tree)));
			  }
#line 4538 "miniparser.c" /* yacc.c:1646  */
    break;

  case 63:
#line 718 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintHexa((yyvsp[-1].tree));
			  }
#line 4546 "miniparser.c" /* yacc.c:1646  */
    break;

  case 64:
#line 722 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintFloat((yyvsp[-1].tree));
			  }
#line 4554 "miniparser.c" /* yacc.c:1646  */
    break;

  case 65:
#line 726 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintBinary((yyvsp[-1].tree));
			  }
#line 4562 "miniparser.c" /* yacc.c:1646  */
    break;

  case 66:
#line 730 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressMessage((yyvsp[-1].list));
			  }
#line 4570 "miniparser.c" /* yacc.c:1646  */
    break;

  case 67:
#line 734 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeUnsuppressMessage((yyvsp[-1].list));
			  }
#line 4578 "miniparser.c" /* yacc.c:1646  */
    break;

  case 68:
#line 738 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintExpansion((yyvsp[-1].tree));
			  }
#line 4586 "miniparser.c" /* yacc.c:1646  */
    break;

  case 69:
#line 742 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBashExecute((yyvsp[-1].tree));
			  }
#line 4594 "miniparser.c" /* yacc.c:1646  */
    break;

  case 70:
#line 746 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExternalPlot(addElement(addElement(addElement(addElement((yyvsp[-1].list),(yyvsp[-3].tree)),(yyvsp[-5].tree)),(yyvsp[-7].tree)),(yyvsp[-9].tree)));
			  }
#line 4602 "miniparser.c" /* yacc.c:1646  */
    break;

  case 71:
#line 750 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeWrite((yyvsp[-1].list));
			  }
#line 4610 "miniparser.c" /* yacc.c:1646  */
    break;

  case 72:
#line 754 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNewFileWrite((yyvsp[0].tree), (yyvsp[-3].list));
			  }
#line 4618 "miniparser.c" /* yacc.c:1646  */
    break;

  case 73:
#line 758 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAppendFileWrite((yyvsp[0].tree), (yyvsp[-4].list));
			  }
#line 4626 "miniparser.c" /* yacc.c:1646  */
    break;

  case 74:
#line 762 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAsciiPlot((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 4634 "miniparser.c" /* yacc.c:1646  */
    break;

  case 75:
#line 766 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintXml((yyvsp[-1].tree));
			  }
#line 4642 "miniparser.c" /* yacc.c:1646  */
    break;

  case 76:
#line 770 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExecute((yyvsp[-1].tree));
			  }
#line 4650 "miniparser.c" /* yacc.c:1646  */
    break;

  case 77:
#line 774 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintXmlNewFile((yyvsp[-3].tree),(yyvsp[0].tree));
			  }
#line 4658 "miniparser.c" /* yacc.c:1646  */
    break;

  case 78:
#line 778 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintXmlAppendFile((yyvsp[-4].tree),(yyvsp[0].tree));
			  }
#line 4666 "miniparser.c" /* yacc.c:1646  */
    break;

  case 79:
#line 782 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeWorstCase(addElement(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)), (yyvsp[-9].tree)));
			  }
#line 4674 "miniparser.c" /* yacc.c:1646  */
    break;

  case 80:
#line 786 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRename((yyvsp[-3].value), (yyvsp[-1].value));
			    safeFree((yyvsp[-3].value));
			    safeFree((yyvsp[-1].value));
			  }
#line 4684 "miniparser.c" /* yacc.c:1646  */
    break;

  case 81:
#line 792 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRename("_x_", (yyvsp[-1].value));
			    safeFree((yyvsp[-1].value));
			  }
#line 4693 "miniparser.c" /* yacc.c:1646  */
    break;

  case 82:
#line 797 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExternalProc((yyvsp[-8].value), (yyvsp[-6].tree), addElement((yyvsp[-4].list), (yyvsp[-1].integerval)));
			    safeFree((yyvsp[-8].value));
			  }
#line 4702 "miniparser.c" /* yacc.c:1646  */
    break;

  case 83:
#line 802 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4710 "miniparser.c" /* yacc.c:1646  */
    break;

  case 84:
#line 806 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoprint((yyvsp[0].list));
			  }
#line 4718 "miniparser.c" /* yacc.c:1646  */
    break;

  case 85:
#line 810 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAssignment((yyvsp[-1].value), (yyvsp[0].tree));
			    safeFree((yyvsp[-1].value));
			  }
#line 4727 "miniparser.c" /* yacc.c:1646  */
    break;

  case 86:
#line 817 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4735 "miniparser.c" /* yacc.c:1646  */
    break;

  case 87:
#line 821 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[-1].tree);
			  }
#line 4743 "miniparser.c" /* yacc.c:1646  */
    break;

  case 88:
#line 825 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4751 "miniparser.c" /* yacc.c:1646  */
    break;

  case 89:
#line 829 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[-1].tree);
			  }
#line 4759 "miniparser.c" /* yacc.c:1646  */
    break;

  case 90:
#line 835 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAssignment((yyvsp[-2].value), (yyvsp[0].tree));
			    safeFree((yyvsp[-2].value));
			  }
#line 4768 "miniparser.c" /* yacc.c:1646  */
    break;

  case 91:
#line 840 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloatAssignment((yyvsp[-2].value), (yyvsp[0].tree));
			    safeFree((yyvsp[-2].value));
			  }
#line 4777 "miniparser.c" /* yacc.c:1646  */
    break;

  case 92:
#line 845 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLibraryBinding((yyvsp[-5].value), (yyvsp[-1].tree));
			    safeFree((yyvsp[-5].value));
			  }
#line 4786 "miniparser.c" /* yacc.c:1646  */
    break;

  case 93:
#line 850 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLibraryConstantBinding((yyvsp[-5].value), (yyvsp[-1].tree));
			    safeFree((yyvsp[-5].value));
			  }
#line 4795 "miniparser.c" /* yacc.c:1646  */
    break;

  case 94:
#line 855 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAssignmentInIndexing((yyvsp[-2].dblnode)->a,(yyvsp[-2].dblnode)->b,(yyvsp[0].tree));
			    safeFree((yyvsp[-2].dblnode));
			  }
#line 4804 "miniparser.c" /* yacc.c:1646  */
    break;

  case 95:
#line 860 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloatAssignmentInIndexing((yyvsp[-2].dblnode)->a,(yyvsp[-2].dblnode)->b,(yyvsp[0].tree));
			    safeFree((yyvsp[-2].dblnode));
			  }
#line 4813 "miniparser.c" /* yacc.c:1646  */
    break;

  case 96:
#line 865 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProtoAssignmentInStructure((yyvsp[-2].tree),(yyvsp[0].tree));
			  }
#line 4821 "miniparser.c" /* yacc.c:1646  */
    break;

  case 97:
#line 869 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProtoFloatAssignmentInStructure((yyvsp[-2].tree),(yyvsp[0].tree));
			  }
#line 4829 "miniparser.c" /* yacc.c:1646  */
    break;

  case 98:
#line 875 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeStructAccess((yyvsp[-2].tree),(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 4838 "miniparser.c" /* yacc.c:1646  */
    break;

  case 99:
#line 882 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecAssign((yyvsp[0].tree));
			  }
#line 4846 "miniparser.c" /* yacc.c:1646  */
    break;

  case 100:
#line 886 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePointsAssign((yyvsp[0].tree));
			  }
#line 4854 "miniparser.c" /* yacc.c:1646  */
    break;

  case 101:
#line 890 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiamAssign((yyvsp[0].tree));
			  }
#line 4862 "miniparser.c" /* yacc.c:1646  */
    break;

  case 102:
#line 894 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDisplayAssign((yyvsp[0].tree));
			  }
#line 4870 "miniparser.c" /* yacc.c:1646  */
    break;

  case 103:
#line 898 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVerbosityAssign((yyvsp[0].tree));
			  }
#line 4878 "miniparser.c" /* yacc.c:1646  */
    break;

  case 104:
#line 902 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeShowMessageNumbersAssign((yyvsp[0].tree));
			  }
#line 4886 "miniparser.c" /* yacc.c:1646  */
    break;

  case 105:
#line 906 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalAssign((yyvsp[0].tree));
			  }
#line 4894 "miniparser.c" /* yacc.c:1646  */
    break;

  case 106:
#line 910 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoSimplifyAssign((yyvsp[0].tree));
			  }
#line 4902 "miniparser.c" /* yacc.c:1646  */
    break;

  case 107:
#line 914 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylorRecursAssign((yyvsp[0].tree));
			  }
#line 4910 "miniparser.c" /* yacc.c:1646  */
    break;

  case 108:
#line 918 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTimingAssign((yyvsp[0].tree));
			  }
#line 4918 "miniparser.c" /* yacc.c:1646  */
    break;

  case 109:
#line 922 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFullParenAssign((yyvsp[0].tree));
			  }
#line 4926 "miniparser.c" /* yacc.c:1646  */
    break;

  case 110:
#line 926 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointAssign((yyvsp[0].tree));
			  }
#line 4934 "miniparser.c" /* yacc.c:1646  */
    break;

  case 111:
#line 930 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDieOnErrorAssign((yyvsp[0].tree));
			  }
#line 4942 "miniparser.c" /* yacc.c:1646  */
    break;

  case 112:
#line 934 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalModeAssign((yyvsp[0].tree));
			  }
#line 4950 "miniparser.c" /* yacc.c:1646  */
    break;

  case 113:
#line 938 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressWarningsAssign((yyvsp[0].tree));
			  }
#line 4958 "miniparser.c" /* yacc.c:1646  */
    break;

  case 114:
#line 942 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHopitalRecursAssign((yyvsp[0].tree));
			  }
#line 4966 "miniparser.c" /* yacc.c:1646  */
    break;

  case 115:
#line 948 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecStillAssign((yyvsp[0].tree));
			  }
#line 4974 "miniparser.c" /* yacc.c:1646  */
    break;

  case 116:
#line 952 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePointsStillAssign((yyvsp[0].tree));
			  }
#line 4982 "miniparser.c" /* yacc.c:1646  */
    break;

  case 117:
#line 956 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiamStillAssign((yyvsp[0].tree));
			  }
#line 4990 "miniparser.c" /* yacc.c:1646  */
    break;

  case 118:
#line 960 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDisplayStillAssign((yyvsp[0].tree));
			  }
#line 4998 "miniparser.c" /* yacc.c:1646  */
    break;

  case 119:
#line 964 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVerbosityStillAssign((yyvsp[0].tree));
			  }
#line 5006 "miniparser.c" /* yacc.c:1646  */
    break;

  case 120:
#line 968 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeShowMessageNumbersStillAssign((yyvsp[0].tree));
			  }
#line 5014 "miniparser.c" /* yacc.c:1646  */
    break;

  case 121:
#line 972 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalStillAssign((yyvsp[0].tree));
			  }
#line 5022 "miniparser.c" /* yacc.c:1646  */
    break;

  case 122:
#line 976 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoSimplifyStillAssign((yyvsp[0].tree));
			  }
#line 5030 "miniparser.c" /* yacc.c:1646  */
    break;

  case 123:
#line 980 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylorRecursStillAssign((yyvsp[0].tree));
			  }
#line 5038 "miniparser.c" /* yacc.c:1646  */
    break;

  case 124:
#line 984 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTimingStillAssign((yyvsp[0].tree));
			  }
#line 5046 "miniparser.c" /* yacc.c:1646  */
    break;

  case 125:
#line 988 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFullParenStillAssign((yyvsp[0].tree));
			  }
#line 5054 "miniparser.c" /* yacc.c:1646  */
    break;

  case 126:
#line 992 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointStillAssign((yyvsp[0].tree));
			  }
#line 5062 "miniparser.c" /* yacc.c:1646  */
    break;

  case 127:
#line 996 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDieOnErrorStillAssign((yyvsp[0].tree));
			  }
#line 5070 "miniparser.c" /* yacc.c:1646  */
    break;

  case 128:
#line 1000 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalModeStillAssign((yyvsp[0].tree));
			  }
#line 5078 "miniparser.c" /* yacc.c:1646  */
    break;

  case 129:
#line 1004 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressWarningsStillAssign((yyvsp[0].tree));
			  }
#line 5086 "miniparser.c" /* yacc.c:1646  */
    break;

  case 130:
#line 1008 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHopitalRecursStillAssign((yyvsp[0].tree));
			  }
#line 5094 "miniparser.c" /* yacc.c:1646  */
    break;

  case 131:
#line 1014 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].tree));
			  }
#line 5102 "miniparser.c" /* yacc.c:1646  */
    break;

  case 132:
#line 1018 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].tree));
			  }
#line 5110 "miniparser.c" /* yacc.c:1646  */
    break;

  case 133:
#line 1024 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].association));
			  }
#line 5118 "miniparser.c" /* yacc.c:1646  */
    break;

  case 134:
#line 1028 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].association));
			  }
#line 5126 "miniparser.c" /* yacc.c:1646  */
    break;

  case 135:
#line 1034 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 5134 "miniparser.c" /* yacc.c:1646  */
    break;

  case 136:
#line 1038 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 5142 "miniparser.c" /* yacc.c:1646  */
    break;

  case 137:
#line 1044 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.association) = (entry *) safeMalloc(sizeof(entry));
			    (yyval.association)->name = (char *) safeCalloc(strlen((yyvsp[-2].value)) + 1, sizeof(char));
			    strcpy((yyval.association)->name,(yyvsp[-2].value));
			    safeFree((yyvsp[-2].value));
			    (yyval.association)->value = (void *) ((yyvsp[0].tree));
			  }
#line 5154 "miniparser.c" /* yacc.c:1646  */
    break;

  case 138:
#line 1054 "miniparser.y" /* yacc.c:1646  */
    {
			   (yyval.tree) = (yyvsp[0].tree);
			 }
#line 5162 "miniparser.c" /* yacc.c:1646  */
    break;

  case 139:
#line 1058 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatch((yyvsp[-2].tree),(yyvsp[0].list));
			  }
#line 5170 "miniparser.c" /* yacc.c:1646  */
    break;

  case 140:
#line 1064 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5178 "miniparser.c" /* yacc.c:1646  */
    break;

  case 141:
#line 1068 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAnd((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5186 "miniparser.c" /* yacc.c:1646  */
    break;

  case 142:
#line 1072 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeOr((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5194 "miniparser.c" /* yacc.c:1646  */
    break;

  case 143:
#line 1076 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNegation((yyvsp[0].tree));
			  }
#line 5202 "miniparser.c" /* yacc.c:1646  */
    break;

  case 144:
#line 1082 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.dblnode) = (doubleNode *) safeMalloc(sizeof(doubleNode));
			    (yyval.dblnode)->a = (yyvsp[-3].tree);
			    (yyval.dblnode)->b = (yyvsp[-1].tree);
			  }
#line 5212 "miniparser.c" /* yacc.c:1646  */
    break;

  case 145:
#line 1091 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5220 "miniparser.c" /* yacc.c:1646  */
    break;

  case 146:
#line 1095 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareEqual((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5228 "miniparser.c" /* yacc.c:1646  */
    break;

  case 147:
#line 1099 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareIn((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5236 "miniparser.c" /* yacc.c:1646  */
    break;

  case 148:
#line 1103 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareLess((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5244 "miniparser.c" /* yacc.c:1646  */
    break;

  case 149:
#line 1107 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareGreater((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5252 "miniparser.c" /* yacc.c:1646  */
    break;

  case 150:
#line 1111 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareLessEqual((yyvsp[-3].tree), (yyvsp[0].tree));
			  }
#line 5260 "miniparser.c" /* yacc.c:1646  */
    break;

  case 151:
#line 1115 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareGreaterEqual((yyvsp[-3].tree), (yyvsp[0].tree));
			  }
#line 5268 "miniparser.c" /* yacc.c:1646  */
    break;

  case 152:
#line 1119 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareNotEqual((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5276 "miniparser.c" /* yacc.c:1646  */
    break;

  case 153:
#line 1125 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5284 "miniparser.c" /* yacc.c:1646  */
    break;

  case 154:
#line 1129 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAdd((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5292 "miniparser.c" /* yacc.c:1646  */
    break;

  case 155:
#line 1133 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSub((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5300 "miniparser.c" /* yacc.c:1646  */
    break;

  case 156:
#line 1137 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeConcat((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5308 "miniparser.c" /* yacc.c:1646  */
    break;

  case 157:
#line 1141 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAddToList((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5316 "miniparser.c" /* yacc.c:1646  */
    break;

  case 158:
#line 1145 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAppend((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5324 "miniparser.c" /* yacc.c:1646  */
    break;

  case 159:
#line 1151 "miniparser.y" /* yacc.c:1646  */
    {
                            (yyval.count) = 0;
                          }
#line 5332 "miniparser.c" /* yacc.c:1646  */
    break;

  case 160:
#line 1155 "miniparser.y" /* yacc.c:1646  */
    {
                            (yyval.count) = 1;
                          }
#line 5340 "miniparser.c" /* yacc.c:1646  */
    break;

  case 161:
#line 1159 "miniparser.y" /* yacc.c:1646  */
    {
  	                    (yyval.count) = (yyvsp[0].count);
  	                  }
#line 5348 "miniparser.c" /* yacc.c:1646  */
    break;

  case 162:
#line 1163 "miniparser.y" /* yacc.c:1646  */
    {
  	                    (yyval.count) = (yyvsp[0].count)+1;
                          }
#line 5356 "miniparser.c" /* yacc.c:1646  */
    break;

  case 163:
#line 1170 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
                          }
#line 5364 "miniparser.c" /* yacc.c:1646  */
    break;

  case 164:
#line 1174 "miniparser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = tempNode;
			  }
#line 5375 "miniparser.c" /* yacc.c:1646  */
    break;

  case 165:
#line 1181 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEvalConst((yyvsp[0].tree));
                          }
#line 5383 "miniparser.c" /* yacc.c:1646  */
    break;

  case 166:
#line 1185 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMul((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 5391 "miniparser.c" /* yacc.c:1646  */
    break;

  case 167:
#line 1189 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiv((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 5399 "miniparser.c" /* yacc.c:1646  */
    break;

  case 168:
#line 1193 "miniparser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeMul((yyvsp[-3].tree), tempNode);
			  }
#line 5410 "miniparser.c" /* yacc.c:1646  */
    break;

  case 169:
#line 1200 "miniparser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeDiv((yyvsp[-3].tree), tempNode);
			  }
#line 5421 "miniparser.c" /* yacc.c:1646  */
    break;

  case 170:
#line 1207 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMul((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
			  }
#line 5429 "miniparser.c" /* yacc.c:1646  */
    break;

  case 171:
#line 1211 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiv((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
			  }
#line 5437 "miniparser.c" /* yacc.c:1646  */
    break;

  case 172:
#line 1217 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
                          }
#line 5445 "miniparser.c" /* yacc.c:1646  */
    break;

  case 173:
#line 1221 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePow((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 5453 "miniparser.c" /* yacc.c:1646  */
    break;

  case 174:
#line 1225 "miniparser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makePow((yyvsp[-3].tree), tempNode);
			  }
#line 5464 "miniparser.c" /* yacc.c:1646  */
    break;

  case 175:
#line 1232 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePow((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
			  }
#line 5472 "miniparser.c" /* yacc.c:1646  */
    break;

  case 176:
#line 1236 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrepend((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5480 "miniparser.c" /* yacc.c:1646  */
    break;

  case 177:
#line 1240 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrepend((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
			  }
#line 5488 "miniparser.c" /* yacc.c:1646  */
    break;

  case 178:
#line 1247 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeOn();
			  }
#line 5496 "miniparser.c" /* yacc.c:1646  */
    break;

  case 179:
#line 1251 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeOff();
			  }
#line 5504 "miniparser.c" /* yacc.c:1646  */
    break;

  case 180:
#line 1255 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDyadic();
			  }
#line 5512 "miniparser.c" /* yacc.c:1646  */
    break;

  case 181:
#line 1259 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePowers();
			  }
#line 5520 "miniparser.c" /* yacc.c:1646  */
    break;

  case 182:
#line 1263 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBinaryThing();
			  }
#line 5528 "miniparser.c" /* yacc.c:1646  */
    break;

  case 183:
#line 1267 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHexadecimalThing();
			  }
#line 5536 "miniparser.c" /* yacc.c:1646  */
    break;

  case 184:
#line 1271 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFile();
			  }
#line 5544 "miniparser.c" /* yacc.c:1646  */
    break;

  case 185:
#line 1275 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePostscript();
			  }
#line 5552 "miniparser.c" /* yacc.c:1646  */
    break;

  case 186:
#line 1279 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePostscriptFile();
			  }
#line 5560 "miniparser.c" /* yacc.c:1646  */
    break;

  case 187:
#line 1283 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePerturb();
			  }
#line 5568 "miniparser.c" /* yacc.c:1646  */
    break;

  case 188:
#line 1287 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundDown();
			  }
#line 5576 "miniparser.c" /* yacc.c:1646  */
    break;

  case 189:
#line 1291 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundUp();
			  }
#line 5584 "miniparser.c" /* yacc.c:1646  */
    break;

  case 190:
#line 1295 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundToZero();
			  }
#line 5592 "miniparser.c" /* yacc.c:1646  */
    break;

  case 191:
#line 1299 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundToNearest();
			  }
#line 5600 "miniparser.c" /* yacc.c:1646  */
    break;

  case 192:
#line 1303 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHonorCoeff();
			  }
#line 5608 "miniparser.c" /* yacc.c:1646  */
    break;

  case 193:
#line 1307 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTrue();
			  }
#line 5616 "miniparser.c" /* yacc.c:1646  */
    break;

  case 194:
#line 1311 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeUnit();
			  }
#line 5624 "miniparser.c" /* yacc.c:1646  */
    break;

  case 195:
#line 1315 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFalse();
			  }
#line 5632 "miniparser.c" /* yacc.c:1646  */
    break;

  case 196:
#line 1319 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDefault();
			  }
#line 5640 "miniparser.c" /* yacc.c:1646  */
    break;

  case 197:
#line 1323 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDecimal();
			  }
#line 5648 "miniparser.c" /* yacc.c:1646  */
    break;

  case 198:
#line 1327 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAbsolute();
			  }
#line 5656 "miniparser.c" /* yacc.c:1646  */
    break;

  case 199:
#line 1331 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRelative();
			  }
#line 5664 "miniparser.c" /* yacc.c:1646  */
    break;

  case 200:
#line 1335 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFixed();
			  }
#line 5672 "miniparser.c" /* yacc.c:1646  */
    break;

  case 201:
#line 1339 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloating();
			  }
#line 5680 "miniparser.c" /* yacc.c:1646  */
    break;

  case 202:
#line 1343 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeError();
			  }
#line 5688 "miniparser.c" /* yacc.c:1646  */
    break;

  case 203:
#line 1347 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleSymbol();
			  }
#line 5696 "miniparser.c" /* yacc.c:1646  */
    break;

  case 204:
#line 1351 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSingleSymbol();
			  }
#line 5704 "miniparser.c" /* yacc.c:1646  */
    break;

  case 205:
#line 1355 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeQuadSymbol();
			  }
#line 5712 "miniparser.c" /* yacc.c:1646  */
    break;

  case 206:
#line 1359 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHalfPrecisionSymbol();
			  }
#line 5720 "miniparser.c" /* yacc.c:1646  */
    break;

  case 207:
#line 1363 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleextendedSymbol();
			  }
#line 5728 "miniparser.c" /* yacc.c:1646  */
    break;

  case 208:
#line 1367 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVariable();
			  }
#line 5736 "miniparser.c" /* yacc.c:1646  */
    break;

  case 209:
#line 1371 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleDoubleSymbol();
			  }
#line 5744 "miniparser.c" /* yacc.c:1646  */
    break;

  case 210:
#line 1375 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTripleDoubleSymbol();
			  }
#line 5752 "miniparser.c" /* yacc.c:1646  */
    break;

  case 211:
#line 1379 "miniparser.y" /* yacc.c:1646  */
    {
			    tempString = safeCalloc(strlen((yyvsp[0].value)) + 1, sizeof(char));
			    strcpy(tempString, (yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			    tempString2 = safeCalloc(strlen(tempString) + 1, sizeof(char));
			    strcpy(tempString2, tempString);
			    safeFree(tempString);
			    (yyval.tree) = makeString(tempString2);
			    safeFree(tempString2);
			  }
#line 5767 "miniparser.c" /* yacc.c:1646  */
    break;

  case 212:
#line 1390 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5775 "miniparser.c" /* yacc.c:1646  */
    break;

  case 213:
#line 1394 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTableAccess((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 5784 "miniparser.c" /* yacc.c:1646  */
    break;

  case 214:
#line 1399 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIsBound((yyvsp[-1].value));
			    safeFree((yyvsp[-1].value));
			  }
#line 5793 "miniparser.c" /* yacc.c:1646  */
    break;

  case 215:
#line 1404 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[-3].value), (yyvsp[-1].list));
			    safeFree((yyvsp[-3].value));
			  }
#line 5802 "miniparser.c" /* yacc.c:1646  */
    break;

  case 216:
#line 1409 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[-2].value), NULL);
			    safeFree((yyvsp[-2].value));
			  }
#line 5811 "miniparser.c" /* yacc.c:1646  */
    break;

  case 217:
#line 1414 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5819 "miniparser.c" /* yacc.c:1646  */
    break;

  case 218:
#line 1418 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5827 "miniparser.c" /* yacc.c:1646  */
    break;

  case 219:
#line 1422 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5835 "miniparser.c" /* yacc.c:1646  */
    break;

  case 220:
#line 1426 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5843 "miniparser.c" /* yacc.c:1646  */
    break;

  case 221:
#line 1430 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[-1].tree);
			  }
#line 5851 "miniparser.c" /* yacc.c:1646  */
    break;

  case 222:
#line 1434 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeStructure((yyvsp[-1].list));
			  }
#line 5859 "miniparser.c" /* yacc.c:1646  */
    break;

  case 223:
#line 1438 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5867 "miniparser.c" /* yacc.c:1646  */
    break;

  case 224:
#line 1442 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIndex((yyvsp[0].dblnode)->a, (yyvsp[0].dblnode)->b);
			    safeFree((yyvsp[0].dblnode));
			  }
#line 5876 "miniparser.c" /* yacc.c:1646  */
    break;

  case 225:
#line 1447 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeStructAccess((yyvsp[-2].tree),(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 5885 "miniparser.c" /* yacc.c:1646  */
    break;

  case 226:
#line 1452 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply(makeStructAccess((yyvsp[-5].tree),(yyvsp[-3].value)),(yyvsp[-1].list));
			    safeFree((yyvsp[-3].value));
			  }
#line 5894 "miniparser.c" /* yacc.c:1646  */
    break;

  case 227:
#line 1457 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply((yyvsp[-4].tree),(yyvsp[-1].list));
			  }
#line 5902 "miniparser.c" /* yacc.c:1646  */
    break;

  case 228:
#line 1461 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply((yyvsp[-3].tree),addElement(NULL,makeUnit()));
			  }
#line 5910 "miniparser.c" /* yacc.c:1646  */
    break;

  case 229:
#line 1465 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5918 "miniparser.c" /* yacc.c:1646  */
    break;

  case 230:
#line 1469 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTime((yyvsp[-1].tree));
                          }
#line 5926 "miniparser.c" /* yacc.c:1646  */
    break;

  case 231:
#line 1475 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL,(yyvsp[0].tree));
			  }
#line 5934 "miniparser.c" /* yacc.c:1646  */
    break;

  case 232:
#line 1479 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list),(yyvsp[-1].tree));
			  }
#line 5942 "miniparser.c" /* yacc.c:1646  */
    break;

  case 233:
#line 1485 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-8].tree),makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))),(yyvsp[-2].tree));
			  }
#line 5950 "miniparser.c" /* yacc.c:1646  */
    break;

  case 234:
#line 1489 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-5].tree),makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))),makeUnit());
			  }
#line 5958 "miniparser.c" /* yacc.c:1646  */
    break;

  case 235:
#line 1493 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-7].tree),makeCommandList((yyvsp[-4].list)),(yyvsp[-2].tree));
			  }
#line 5966 "miniparser.c" /* yacc.c:1646  */
    break;

  case 236:
#line 1497 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-4].tree),makeCommandList((yyvsp[-1].list)),makeUnit());
			  }
#line 5974 "miniparser.c" /* yacc.c:1646  */
    break;

  case 237:
#line 1501 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-7].tree),makeCommandList((yyvsp[-4].list)),(yyvsp[-2].tree));
			  }
#line 5982 "miniparser.c" /* yacc.c:1646  */
    break;

  case 238:
#line 1505 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-4].tree),makeCommandList((yyvsp[-1].list)),makeUnit());
			  }
#line 5990 "miniparser.c" /* yacc.c:1646  */
    break;

  case 239:
#line 1509 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-6].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[-2].tree));
			  }
#line 5998 "miniparser.c" /* yacc.c:1646  */
    break;

  case 240:
#line 1513 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-3].tree), makeCommandList(addElement(NULL,makeNop())), makeUnit());
			  }
#line 6006 "miniparser.c" /* yacc.c:1646  */
    break;

  case 241:
#line 1517 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-4].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[-1].tree));
			  }
#line 6014 "miniparser.c" /* yacc.c:1646  */
    break;

  case 242:
#line 1523 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDecimalConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6023 "miniparser.c" /* yacc.c:1646  */
    break;

  case 243:
#line 1528 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6032 "miniparser.c" /* yacc.c:1646  */
    break;

  case 244:
#line 1533 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDyadicConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6041 "miniparser.c" /* yacc.c:1646  */
    break;

  case 245:
#line 1538 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHexConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6050 "miniparser.c" /* yacc.c:1646  */
    break;

  case 246:
#line 1543 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHexadecimalConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6059 "miniparser.c" /* yacc.c:1646  */
    break;

  case 247:
#line 1548 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBinaryConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6068 "miniparser.c" /* yacc.c:1646  */
    break;

  case 248:
#line 1553 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePi();
			  }
#line 6076 "miniparser.c" /* yacc.c:1646  */
    break;

  case 249:
#line 1561 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEmptyList();
			  }
#line 6084 "miniparser.c" /* yacc.c:1646  */
    break;

  case 250:
#line 1565 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEmptyList();
			  }
#line 6092 "miniparser.c" /* yacc.c:1646  */
    break;

  case 251:
#line 1569 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRevertedList((yyvsp[-2].list));
			  }
#line 6100 "miniparser.c" /* yacc.c:1646  */
    break;

  case 252:
#line 1573 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRevertedFinalEllipticList((yyvsp[-3].list));
			  }
#line 6108 "miniparser.c" /* yacc.c:1646  */
    break;

  case 253:
#line 1579 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].tree));
			  }
#line 6116 "miniparser.c" /* yacc.c:1646  */
    break;

  case 254:
#line 1583 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[-2].list), (yyvsp[0].tree));
			  }
#line 6124 "miniparser.c" /* yacc.c:1646  */
    break;

  case 255:
#line 1587 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(addElement((yyvsp[-4].list), makeElliptic()), (yyvsp[0].tree));
			  }
#line 6132 "miniparser.c" /* yacc.c:1646  */
    break;

  case 256:
#line 1593 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRange((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6140 "miniparser.c" /* yacc.c:1646  */
    break;

  case 257:
#line 1597 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRange((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6148 "miniparser.c" /* yacc.c:1646  */
    break;

  case 258:
#line 1601 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRange((yyvsp[-1].tree), copyThing((yyvsp[-1].tree)));
			  }
#line 6156 "miniparser.c" /* yacc.c:1646  */
    break;

  case 259:
#line 1607 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[-1].tree));
			  }
#line 6164 "miniparser.c" /* yacc.c:1646  */
    break;

  case 260:
#line 1611 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[-1].tree));
			  }
#line 6172 "miniparser.c" /* yacc.c:1646  */
    break;

  case 261:
#line 1615 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[-1].tree));
			  }
#line 6180 "miniparser.c" /* yacc.c:1646  */
    break;

  case 262:
#line 1619 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[-1].tree));
			  }
#line 6188 "miniparser.c" /* yacc.c:1646  */
    break;

  case 263:
#line 1623 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[-1].tree));
			  }
#line 6196 "miniparser.c" /* yacc.c:1646  */
    break;

  case 264:
#line 1627 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[-1].tree));
			  }
#line 6204 "miniparser.c" /* yacc.c:1646  */
    break;

  case 265:
#line 1633 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiff((yyvsp[-1].tree));
			  }
#line 6212 "miniparser.c" /* yacc.c:1646  */
    break;

  case 266:
#line 1637 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBashevaluate(addElement(NULL,(yyvsp[-1].tree)));
			  }
#line 6220 "miniparser.c" /* yacc.c:1646  */
    break;

  case 267:
#line 1641 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGetSuppressedMessages();
			  }
#line 6228 "miniparser.c" /* yacc.c:1646  */
    break;

  case 268:
#line 1645 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGetBacktrace();
			  }
#line 6236 "miniparser.c" /* yacc.c:1646  */
    break;

  case 269:
#line 1649 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBashevaluate(addElement(addElement(NULL,(yyvsp[-1].tree)),(yyvsp[-3].tree)));
			  }
#line 6244 "miniparser.c" /* yacc.c:1646  */
    break;

  case 270:
#line 1653 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtysimplify((yyvsp[-1].tree));
			  }
#line 6252 "miniparser.c" /* yacc.c:1646  */
    break;

  case 271:
#line 1657 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRemez(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6260 "miniparser.c" /* yacc.c:1646  */
    break;

  case 272:
#line 1661 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAnnotateFunction(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)));
			  }
#line 6268 "miniparser.c" /* yacc.c:1646  */
    break;

  case 273:
#line 1665 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBind((yyvsp[-5].tree), (yyvsp[-3].value), (yyvsp[-1].tree));
			    safeFree((yyvsp[-3].value));
			  }
#line 6277 "miniparser.c" /* yacc.c:1646  */
    break;

  case 274:
#line 1670 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMin((yyvsp[-1].list));
			  }
#line 6285 "miniparser.c" /* yacc.c:1646  */
    break;

  case 275:
#line 1674 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMax((yyvsp[-1].list));
			  }
#line 6293 "miniparser.c" /* yacc.c:1646  */
    break;

  case 276:
#line 1678 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFPminimax(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)));
			  }
#line 6301 "miniparser.c" /* yacc.c:1646  */
    break;

  case 277:
#line 1682 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHorner((yyvsp[-1].tree));
			  }
#line 6309 "miniparser.c" /* yacc.c:1646  */
    break;

  case 278:
#line 1686 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalThing((yyvsp[-1].tree));
			  }
#line 6317 "miniparser.c" /* yacc.c:1646  */
    break;

  case 279:
#line 1690 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExpand((yyvsp[-1].tree));
			  }
#line 6325 "miniparser.c" /* yacc.c:1646  */
    break;

  case 280:
#line 1694 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSimplifySafe((yyvsp[-1].tree));
			  }
#line 6333 "miniparser.c" /* yacc.c:1646  */
    break;

  case 281:
#line 1698 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylor((yyvsp[-5].tree), (yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6341 "miniparser.c" /* yacc.c:1646  */
    break;

  case 282:
#line 1702 "miniparser.y" /* yacc.c:1646  */
    {
                            (yyval.tree) = makeTaylorform(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6349 "miniparser.c" /* yacc.c:1646  */
    break;

  case 283:
#line 1706 "miniparser.y" /* yacc.c:1646  */
    {
                            (yyval.tree) = makeChebyshevform(addElement(addElement(addElement(NULL, (yyvsp[-1].tree)), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6357 "miniparser.c" /* yacc.c:1646  */
    break;

  case 284:
#line 1710 "miniparser.y" /* yacc.c:1646  */
    {
                            (yyval.tree) = makeAutodiff(addElement(addElement(addElement(NULL, (yyvsp[-1].tree)), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6365 "miniparser.c" /* yacc.c:1646  */
    break;

  case 285:
#line 1714 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDegree((yyvsp[-1].tree));
			  }
#line 6373 "miniparser.c" /* yacc.c:1646  */
    break;

  case 286:
#line 1718 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNumerator((yyvsp[-1].tree));
			  }
#line 6381 "miniparser.c" /* yacc.c:1646  */
    break;

  case 287:
#line 1722 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDenominator((yyvsp[-1].tree));
			  }
#line 6389 "miniparser.c" /* yacc.c:1646  */
    break;

  case 288:
#line 1726 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSubstitute((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6397 "miniparser.c" /* yacc.c:1646  */
    break;

  case 289:
#line 1730 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeComposePolynomials((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6405 "miniparser.c" /* yacc.c:1646  */
    break;

  case 290:
#line 1734 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCoeff((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6413 "miniparser.c" /* yacc.c:1646  */
    break;

  case 291:
#line 1738 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSubpoly((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6421 "miniparser.c" /* yacc.c:1646  */
    break;

  case 292:
#line 1742 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundcoefficients((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6429 "miniparser.c" /* yacc.c:1646  */
    break;

  case 293:
#line 1746 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalapprox((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6437 "miniparser.c" /* yacc.c:1646  */
    break;

  case 294:
#line 1750 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAccurateInfnorm(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6445 "miniparser.c" /* yacc.c:1646  */
    break;

  case 295:
#line 1754 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundToFormat((yyvsp[-5].tree), (yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6453 "miniparser.c" /* yacc.c:1646  */
    break;

  case 296:
#line 1758 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEvaluate((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6461 "miniparser.c" /* yacc.c:1646  */
    break;

  case 297:
#line 1762 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeParse((yyvsp[-1].tree));
			  }
#line 6469 "miniparser.c" /* yacc.c:1646  */
    break;

  case 298:
#line 1766 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeReadXml((yyvsp[-1].tree));
			  }
#line 6477 "miniparser.c" /* yacc.c:1646  */
    break;

  case 299:
#line 1770 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeInfnorm(addElement((yyvsp[-1].list), (yyvsp[-3].tree)));
			  }
#line 6485 "miniparser.c" /* yacc.c:1646  */
    break;

  case 300:
#line 1774 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSupnorm(addElement(addElement(addElement(addElement(addElement(NULL,(yyvsp[-1].tree)),(yyvsp[-3].tree)),(yyvsp[-5].tree)),(yyvsp[-7].tree)),(yyvsp[-9].tree)));
			  }
#line 6493 "miniparser.c" /* yacc.c:1646  */
    break;

  case 301:
#line 1778 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFindZeros((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6501 "miniparser.c" /* yacc.c:1646  */
    break;

  case 302:
#line 1782 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFPFindZeros((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6509 "miniparser.c" /* yacc.c:1646  */
    break;

  case 303:
#line 1786 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtyInfnorm((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6517 "miniparser.c" /* yacc.c:1646  */
    break;

  case 304:
#line 1790 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGcd((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6525 "miniparser.c" /* yacc.c:1646  */
    break;

  case 305:
#line 1794 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEuclDiv((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6533 "miniparser.c" /* yacc.c:1646  */
    break;

  case 306:
#line 1798 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEuclMod((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6541 "miniparser.c" /* yacc.c:1646  */
    break;

  case 307:
#line 1802 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNumberRoots((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6549 "miniparser.c" /* yacc.c:1646  */
    break;

  case 308:
#line 1806 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIntegral((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6557 "miniparser.c" /* yacc.c:1646  */
    break;

  case 309:
#line 1810 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtyIntegral((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6565 "miniparser.c" /* yacc.c:1646  */
    break;

  case 310:
#line 1814 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeImplementPoly(addElement(addElement(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)), (yyvsp[-9].tree)), (yyvsp[-11].tree)));
			  }
#line 6573 "miniparser.c" /* yacc.c:1646  */
    break;

  case 311:
#line 1818 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeImplementConst((yyvsp[-1].list));
			  }
#line 6581 "miniparser.c" /* yacc.c:1646  */
    break;

  case 312:
#line 1822 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCheckInfnorm((yyvsp[-5].tree), (yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6589 "miniparser.c" /* yacc.c:1646  */
    break;

  case 313:
#line 1826 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeZeroDenominators((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6597 "miniparser.c" /* yacc.c:1646  */
    break;

  case 314:
#line 1830 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIsEvaluable((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6605 "miniparser.c" /* yacc.c:1646  */
    break;

  case 315:
#line 1834 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSearchGal((yyvsp[-1].list));
			  }
#line 6613 "miniparser.c" /* yacc.c:1646  */
    break;

  case 316:
#line 1838 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGuessDegree(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6621 "miniparser.c" /* yacc.c:1646  */
    break;

  case 317:
#line 1842 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtyFindZeros((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6629 "miniparser.c" /* yacc.c:1646  */
    break;

  case 318:
#line 1846 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHead((yyvsp[-1].tree));
			  }
#line 6637 "miniparser.c" /* yacc.c:1646  */
    break;

  case 319:
#line 1850 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundCorrectly((yyvsp[-1].tree));
			  }
#line 6645 "miniparser.c" /* yacc.c:1646  */
    break;

  case 320:
#line 1854 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeReadFile((yyvsp[-1].tree));
			  }
#line 6653 "miniparser.c" /* yacc.c:1646  */
    break;

  case 321:
#line 1858 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRevert((yyvsp[-1].tree));
			  }
#line 6661 "miniparser.c" /* yacc.c:1646  */
    break;

  case 322:
#line 1862 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSort((yyvsp[-1].tree));
			  }
#line 6669 "miniparser.c" /* yacc.c:1646  */
    break;

  case 323:
#line 1866 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMantissa((yyvsp[-1].tree));
			  }
#line 6677 "miniparser.c" /* yacc.c:1646  */
    break;

  case 324:
#line 1870 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExponent((yyvsp[-1].tree));
			  }
#line 6685 "miniparser.c" /* yacc.c:1646  */
    break;

  case 325:
#line 1874 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecision((yyvsp[-1].tree));
			  }
#line 6693 "miniparser.c" /* yacc.c:1646  */
    break;

  case 326:
#line 1878 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTail((yyvsp[-1].tree));
			  }
#line 6701 "miniparser.c" /* yacc.c:1646  */
    break;

  case 327:
#line 1882 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSqrt((yyvsp[-1].tree));
			  }
#line 6709 "miniparser.c" /* yacc.c:1646  */
    break;

  case 328:
#line 1886 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExp((yyvsp[-1].tree));
			  }
#line 6717 "miniparser.c" /* yacc.c:1646  */
    break;

  case 329:
#line 1890 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply(makeVariable(),addElement(NULL,(yyvsp[-1].tree)));
			  }
#line 6725 "miniparser.c" /* yacc.c:1646  */
    break;

  case 330:
#line 1894 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcedureFunction((yyvsp[-1].tree));
			  }
#line 6733 "miniparser.c" /* yacc.c:1646  */
    break;

  case 331:
#line 1898 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSubstitute(makeProcedureFunction((yyvsp[-3].tree)),(yyvsp[-1].tree));
			  }
#line 6741 "miniparser.c" /* yacc.c:1646  */
    break;

  case 332:
#line 1902 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog((yyvsp[-1].tree));
			  }
#line 6749 "miniparser.c" /* yacc.c:1646  */
    break;

  case 333:
#line 1906 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog2((yyvsp[-1].tree));
			  }
#line 6757 "miniparser.c" /* yacc.c:1646  */
    break;

  case 334:
#line 1910 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog10((yyvsp[-1].tree));
			  }
#line 6765 "miniparser.c" /* yacc.c:1646  */
    break;

  case 335:
#line 1914 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSin((yyvsp[-1].tree));
			  }
#line 6773 "miniparser.c" /* yacc.c:1646  */
    break;

  case 336:
#line 1918 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCos((yyvsp[-1].tree));
			  }
#line 6781 "miniparser.c" /* yacc.c:1646  */
    break;

  case 337:
#line 1922 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTan((yyvsp[-1].tree));
			  }
#line 6789 "miniparser.c" /* yacc.c:1646  */
    break;

  case 338:
#line 1926 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAsin((yyvsp[-1].tree));
			  }
#line 6797 "miniparser.c" /* yacc.c:1646  */
    break;

  case 339:
#line 1930 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAcos((yyvsp[-1].tree));
			  }
#line 6805 "miniparser.c" /* yacc.c:1646  */
    break;

  case 340:
#line 1934 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAtan((yyvsp[-1].tree));
			  }
#line 6813 "miniparser.c" /* yacc.c:1646  */
    break;

  case 341:
#line 1938 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSinh((yyvsp[-1].tree));
			  }
#line 6821 "miniparser.c" /* yacc.c:1646  */
    break;

  case 342:
#line 1942 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCosh((yyvsp[-1].tree));
			  }
#line 6829 "miniparser.c" /* yacc.c:1646  */
    break;

  case 343:
#line 1946 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTanh((yyvsp[-1].tree));
			  }
#line 6837 "miniparser.c" /* yacc.c:1646  */
    break;

  case 344:
#line 1950 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAsinh((yyvsp[-1].tree));
			  }
#line 6845 "miniparser.c" /* yacc.c:1646  */
    break;

  case 345:
#line 1954 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAcosh((yyvsp[-1].tree));
			  }
#line 6853 "miniparser.c" /* yacc.c:1646  */
    break;

  case 346:
#line 1958 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAtanh((yyvsp[-1].tree));
			  }
#line 6861 "miniparser.c" /* yacc.c:1646  */
    break;

  case 347:
#line 1962 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAbs((yyvsp[-1].tree));
			  }
#line 6869 "miniparser.c" /* yacc.c:1646  */
    break;

  case 348:
#line 1966 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeErf((yyvsp[-1].tree));
			  }
#line 6877 "miniparser.c" /* yacc.c:1646  */
    break;

  case 349:
#line 1970 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeErfc((yyvsp[-1].tree));
			  }
#line 6885 "miniparser.c" /* yacc.c:1646  */
    break;

  case 350:
#line 1974 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog1p((yyvsp[-1].tree));
			  }
#line 6893 "miniparser.c" /* yacc.c:1646  */
    break;

  case 351:
#line 1978 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExpm1((yyvsp[-1].tree));
			  }
#line 6901 "miniparser.c" /* yacc.c:1646  */
    break;

  case 352:
#line 1982 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDouble((yyvsp[-1].tree));
			  }
#line 6909 "miniparser.c" /* yacc.c:1646  */
    break;

  case 353:
#line 1986 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSingle((yyvsp[-1].tree));
			  }
#line 6917 "miniparser.c" /* yacc.c:1646  */
    break;

  case 354:
#line 1990 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeQuad((yyvsp[-1].tree));
			  }
#line 6925 "miniparser.c" /* yacc.c:1646  */
    break;

  case 355:
#line 1994 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHalfPrecision((yyvsp[-1].tree));
			  }
#line 6933 "miniparser.c" /* yacc.c:1646  */
    break;

  case 356:
#line 1998 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubledouble((yyvsp[-1].tree));
			  }
#line 6941 "miniparser.c" /* yacc.c:1646  */
    break;

  case 357:
#line 2002 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTripledouble((yyvsp[-1].tree));
			  }
#line 6949 "miniparser.c" /* yacc.c:1646  */
    break;

  case 358:
#line 2006 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleextended((yyvsp[-1].tree));
			  }
#line 6957 "miniparser.c" /* yacc.c:1646  */
    break;

  case 359:
#line 2010 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCeil((yyvsp[-1].tree));
			  }
#line 6965 "miniparser.c" /* yacc.c:1646  */
    break;

  case 360:
#line 2014 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloor((yyvsp[-1].tree));
			  }
#line 6973 "miniparser.c" /* yacc.c:1646  */
    break;

  case 361:
#line 2018 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNearestInt((yyvsp[-1].tree));
			  }
#line 6981 "miniparser.c" /* yacc.c:1646  */
    break;

  case 362:
#line 2022 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLength((yyvsp[-1].tree));
			  }
#line 6989 "miniparser.c" /* yacc.c:1646  */
    break;

  case 363:
#line 2026 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeObjectName((yyvsp[-1].tree));
			  }
#line 6997 "miniparser.c" /* yacc.c:1646  */
    break;

  case 364:
#line 2032 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 7005 "miniparser.c" /* yacc.c:1646  */
    break;

  case 365:
#line 2036 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 7013 "miniparser.c" /* yacc.c:1646  */
    break;

  case 366:
#line 2043 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecDeref();
			  }
#line 7021 "miniparser.c" /* yacc.c:1646  */
    break;

  case 367:
#line 2047 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePointsDeref();
			  }
#line 7029 "miniparser.c" /* yacc.c:1646  */
    break;

  case 368:
#line 2051 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiamDeref();
			  }
#line 7037 "miniparser.c" /* yacc.c:1646  */
    break;

  case 369:
#line 2055 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDisplayDeref();
			  }
#line 7045 "miniparser.c" /* yacc.c:1646  */
    break;

  case 370:
#line 2059 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVerbosityDeref();
			  }
#line 7053 "miniparser.c" /* yacc.c:1646  */
    break;

  case 371:
#line 2063 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeShowMessageNumbersDeref();
			  }
#line 7061 "miniparser.c" /* yacc.c:1646  */
    break;

  case 372:
#line 2067 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalDeref();
			  }
#line 7069 "miniparser.c" /* yacc.c:1646  */
    break;

  case 373:
#line 2071 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoSimplifyDeref();
			  }
#line 7077 "miniparser.c" /* yacc.c:1646  */
    break;

  case 374:
#line 2075 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylorRecursDeref();
			  }
#line 7085 "miniparser.c" /* yacc.c:1646  */
    break;

  case 375:
#line 2079 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTimingDeref();
			  }
#line 7093 "miniparser.c" /* yacc.c:1646  */
    break;

  case 376:
#line 2083 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFullParenDeref();
			  }
#line 7101 "miniparser.c" /* yacc.c:1646  */
    break;

  case 377:
#line 2087 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointDeref();
			  }
#line 7109 "miniparser.c" /* yacc.c:1646  */
    break;

  case 378:
#line 2091 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDieOnErrorDeref();
			  }
#line 7117 "miniparser.c" /* yacc.c:1646  */
    break;

  case 379:
#line 2095 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalModeDeref();
			  }
#line 7125 "miniparser.c" /* yacc.c:1646  */
    break;

  case 380:
#line 2099 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressWarningsDeref();
			  }
#line 7133 "miniparser.c" /* yacc.c:1646  */
    break;

  case 381:
#line 2103 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHopitalRecursDeref();
			  }
#line 7141 "miniparser.c" /* yacc.c:1646  */
    break;

  case 382:
#line 2110 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7151 "miniparser.c" /* yacc.c:1646  */
    break;

  case 383:
#line 2116 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7161 "miniparser.c" /* yacc.c:1646  */
    break;

  case 384:
#line 2122 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = OBJECT_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7171 "miniparser.c" /* yacc.c:1646  */
    break;

  case 385:
#line 2128 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7181 "miniparser.c" /* yacc.c:1646  */
    break;

  case 386:
#line 2134 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7191 "miniparser.c" /* yacc.c:1646  */
    break;

  case 387:
#line 2140 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7201 "miniparser.c" /* yacc.c:1646  */
    break;

  case 388:
#line 2146 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7211 "miniparser.c" /* yacc.c:1646  */
    break;

  case 389:
#line 2152 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7221 "miniparser.c" /* yacc.c:1646  */
    break;

  case 390:
#line 2158 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7231 "miniparser.c" /* yacc.c:1646  */
    break;

  case 391:
#line 2164 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = OBJECT_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7241 "miniparser.c" /* yacc.c:1646  */
    break;

  case 392:
#line 2170 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7251 "miniparser.c" /* yacc.c:1646  */
    break;

  case 393:
#line 2176 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7261 "miniparser.c" /* yacc.c:1646  */
    break;

  case 394:
#line 2182 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7271 "miniparser.c" /* yacc.c:1646  */
    break;

  case 395:
#line 2188 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7281 "miniparser.c" /* yacc.c:1646  */
    break;

  case 396:
#line 2196 "miniparser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = VOID_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7291 "miniparser.c" /* yacc.c:1646  */
    break;

  case 397:
#line 2202 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.integerval) = (yyvsp[0].integerval);
		          }
#line 7299 "miniparser.c" /* yacc.c:1646  */
    break;

  case 398:
#line 2209 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].integerval));
			  }
#line 7307 "miniparser.c" /* yacc.c:1646  */
    break;

  case 399:
#line 2213 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].integerval));
			  }
#line 7315 "miniparser.c" /* yacc.c:1646  */
    break;

  case 400:
#line 2219 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].integerval));
			  }
#line 7323 "miniparser.c" /* yacc.c:1646  */
    break;

  case 401:
#line 2223 "miniparser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = (yyvsp[-1].list);
			  }
#line 7331 "miniparser.c" /* yacc.c:1646  */
    break;


#line 7335 "miniparser.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (myScanner, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (myScanner, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, myScanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, myScanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (myScanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, myScanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, myScanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
