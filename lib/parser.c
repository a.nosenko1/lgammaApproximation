/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 1

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 65 "parser.y" /* yacc.c:339  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "base-functions.h"
#include "expression.h"
#include "assignment.h"
#include "chain.h"
#include "general.h"
#include "execute.h"

#include "parser.h"
#include "library.h"
#include "help.h"
#include "version.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Mess with the mallocs used by the parser */
extern void *parserCalloc(size_t, size_t);
extern void *parserMalloc(size_t);
extern void *parserRealloc(void *, size_t);
extern void parserFree(void *);
#undef malloc
#undef realloc
#undef calloc
#undef free
#define malloc parserMalloc
#define realloc parserRealloc
#define calloc parserCalloc
#define free parserFree
/* End of the malloc mess */

#define YYERROR_VERBOSE 1
#define YYFPRINTF sollyaFprintf

extern int yylex(YYSTYPE *lvalp, void *scanner);
extern FILE *yyget_in(void *scanner);
extern char *getCurrentLexSymbol();

void yyerror(void *scanner, const char *message) {
  char *str;
  if (!feof(yyget_in(scanner))) {
    str = getCurrentLexSymbol();
    printMessage(1,SOLLYA_MSG_SYNTAX_ERROR_ENCOUNTERED_WHILE_PARSING,"Warning: %s.\nThe last symbol read has been \"%s\".\nWill skip input until next semicolon after the unexpected token. May leak memory.\n",message,str);
    safeFree(str);
    promptToBePrinted = 1;
    lastWasSyntaxError = 1;
    considerDyingOnError();
  } 
}

int parserCheckEof() {
  FILE *myFd;

  myFd = yyget_in(scanner);
  if (myFd == NULL) return 0;
  
  return feof(myFd);
}

/* #define WARN_IF_NO_HELP_TEXT 1 */


#line 134 "parser.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_PARSER_H_INCLUDED
# define YY_YY_PARSER_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    CONSTANTTOKEN = 258,
    MIDPOINTCONSTANTTOKEN = 259,
    DYADICCONSTANTTOKEN = 260,
    HEXCONSTANTTOKEN = 261,
    HEXADECIMALCONSTANTTOKEN = 262,
    BINARYCONSTANTTOKEN = 263,
    PITOKEN = 264,
    IDENTIFIERTOKEN = 265,
    STRINGTOKEN = 266,
    LPARTOKEN = 267,
    RPARTOKEN = 268,
    LBRACKETTOKEN = 269,
    RBRACKETTOKEN = 270,
    EQUALTOKEN = 271,
    ASSIGNEQUALTOKEN = 272,
    COMPAREEQUALTOKEN = 273,
    COMMATOKEN = 274,
    EXCLAMATIONTOKEN = 275,
    SEMICOLONTOKEN = 276,
    STARLEFTANGLETOKEN = 277,
    LEFTANGLETOKEN = 278,
    RIGHTANGLEUNDERSCORETOKEN = 279,
    RIGHTANGLEDOTTOKEN = 280,
    RIGHTANGLESTARTOKEN = 281,
    RIGHTANGLETOKEN = 282,
    DOTSTOKEN = 283,
    DOTTOKEN = 284,
    QUESTIONMARKTOKEN = 285,
    VERTBARTOKEN = 286,
    ATTOKEN = 287,
    DOUBLECOLONTOKEN = 288,
    COLONTOKEN = 289,
    DOTCOLONTOKEN = 290,
    COLONDOTTOKEN = 291,
    EXCLAMATIONEQUALTOKEN = 292,
    APPROXTOKEN = 293,
    ANDTOKEN = 294,
    ORTOKEN = 295,
    PLUSTOKEN = 296,
    MINUSTOKEN = 297,
    MULTOKEN = 298,
    DIVTOKEN = 299,
    POWTOKEN = 300,
    SQRTTOKEN = 301,
    EXPTOKEN = 302,
    FREEVARTOKEN = 303,
    LOGTOKEN = 304,
    LOG2TOKEN = 305,
    LOG10TOKEN = 306,
    SINTOKEN = 307,
    COSTOKEN = 308,
    TANTOKEN = 309,
    ASINTOKEN = 310,
    ACOSTOKEN = 311,
    ATANTOKEN = 312,
    SINHTOKEN = 313,
    COSHTOKEN = 314,
    TANHTOKEN = 315,
    ASINHTOKEN = 316,
    ACOSHTOKEN = 317,
    ATANHTOKEN = 318,
    ABSTOKEN = 319,
    ERFTOKEN = 320,
    ERFCTOKEN = 321,
    LOG1PTOKEN = 322,
    EXPM1TOKEN = 323,
    DOUBLETOKEN = 324,
    SINGLETOKEN = 325,
    HALFPRECISIONTOKEN = 326,
    QUADTOKEN = 327,
    DOUBLEDOUBLETOKEN = 328,
    TRIPLEDOUBLETOKEN = 329,
    DOUBLEEXTENDEDTOKEN = 330,
    CEILTOKEN = 331,
    FLOORTOKEN = 332,
    NEARESTINTTOKEN = 333,
    HEADTOKEN = 334,
    REVERTTOKEN = 335,
    SORTTOKEN = 336,
    TAILTOKEN = 337,
    MANTISSATOKEN = 338,
    EXPONENTTOKEN = 339,
    PRECISIONTOKEN = 340,
    ROUNDCORRECTLYTOKEN = 341,
    PRECTOKEN = 342,
    POINTSTOKEN = 343,
    DIAMTOKEN = 344,
    DISPLAYTOKEN = 345,
    VERBOSITYTOKEN = 346,
    SHOWMESSAGENUMBERSTOKEN = 347,
    CANONICALTOKEN = 348,
    AUTOSIMPLIFYTOKEN = 349,
    TAYLORRECURSIONSTOKEN = 350,
    TIMINGTOKEN = 351,
    TIMETOKEN = 352,
    FULLPARENTHESESTOKEN = 353,
    MIDPOINTMODETOKEN = 354,
    DIEONERRORMODETOKEN = 355,
    SUPPRESSWARNINGSTOKEN = 356,
    RATIONALMODETOKEN = 357,
    HOPITALRECURSIONSTOKEN = 358,
    ONTOKEN = 359,
    OFFTOKEN = 360,
    DYADICTOKEN = 361,
    POWERSTOKEN = 362,
    BINARYTOKEN = 363,
    HEXADECIMALTOKEN = 364,
    FILETOKEN = 365,
    POSTSCRIPTTOKEN = 366,
    POSTSCRIPTFILETOKEN = 367,
    PERTURBTOKEN = 368,
    MINUSWORDTOKEN = 369,
    PLUSWORDTOKEN = 370,
    ZEROWORDTOKEN = 371,
    NEARESTTOKEN = 372,
    HONORCOEFFPRECTOKEN = 373,
    TRUETOKEN = 374,
    FALSETOKEN = 375,
    DEFAULTTOKEN = 376,
    MATCHTOKEN = 377,
    WITHTOKEN = 378,
    ABSOLUTETOKEN = 379,
    DECIMALTOKEN = 380,
    RELATIVETOKEN = 381,
    FIXEDTOKEN = 382,
    FLOATINGTOKEN = 383,
    ERRORTOKEN = 384,
    QUITTOKEN = 385,
    FALSEQUITTOKEN = 386,
    RESTARTTOKEN = 387,
    LIBRARYTOKEN = 388,
    LIBRARYCONSTANTTOKEN = 389,
    DIFFTOKEN = 390,
    DIRTYSIMPLIFYTOKEN = 391,
    REMEZTOKEN = 392,
    ANNOTATEFUNCTIONTOKEN = 393,
    BASHEVALUATETOKEN = 394,
    GETSUPPRESSEDMESSAGESTOKEN = 395,
    GETBACKTRACETOKEN = 396,
    FPMINIMAXTOKEN = 397,
    HORNERTOKEN = 398,
    EXPANDTOKEN = 399,
    SIMPLIFYSAFETOKEN = 400,
    TAYLORTOKEN = 401,
    TAYLORFORMTOKEN = 402,
    CHEBYSHEVFORMTOKEN = 403,
    AUTODIFFTOKEN = 404,
    DEGREETOKEN = 405,
    NUMERATORTOKEN = 406,
    DENOMINATORTOKEN = 407,
    SUBSTITUTETOKEN = 408,
    COMPOSEPOLYNOMIALSTOKEN = 409,
    COEFFTOKEN = 410,
    SUBPOLYTOKEN = 411,
    ROUNDCOEFFICIENTSTOKEN = 412,
    RATIONALAPPROXTOKEN = 413,
    ACCURATEINFNORMTOKEN = 414,
    ROUNDTOFORMATTOKEN = 415,
    EVALUATETOKEN = 416,
    LENGTHTOKEN = 417,
    OBJECTNAMETOKEN = 418,
    INFTOKEN = 419,
    MIDTOKEN = 420,
    SUPTOKEN = 421,
    MINTOKEN = 422,
    MAXTOKEN = 423,
    READXMLTOKEN = 424,
    PARSETOKEN = 425,
    PRINTTOKEN = 426,
    PRINTXMLTOKEN = 427,
    PLOTTOKEN = 428,
    PRINTHEXATOKEN = 429,
    PRINTFLOATTOKEN = 430,
    PRINTBINARYTOKEN = 431,
    SUPPRESSMESSAGETOKEN = 432,
    UNSUPPRESSMESSAGETOKEN = 433,
    PRINTEXPANSIONTOKEN = 434,
    BASHEXECUTETOKEN = 435,
    EXTERNALPLOTTOKEN = 436,
    WRITETOKEN = 437,
    ASCIIPLOTTOKEN = 438,
    RENAMETOKEN = 439,
    BINDTOKEN = 440,
    INFNORMTOKEN = 441,
    SUPNORMTOKEN = 442,
    FINDZEROSTOKEN = 443,
    FPFINDZEROSTOKEN = 444,
    DIRTYINFNORMTOKEN = 445,
    GCDTOKEN = 446,
    EUCLDIVTOKEN = 447,
    EUCLMODTOKEN = 448,
    NUMBERROOTSTOKEN = 449,
    INTEGRALTOKEN = 450,
    DIRTYINTEGRALTOKEN = 451,
    WORSTCASETOKEN = 452,
    IMPLEMENTPOLYTOKEN = 453,
    IMPLEMENTCONSTTOKEN = 454,
    CHECKINFNORMTOKEN = 455,
    ZERODENOMINATORSTOKEN = 456,
    ISEVALUABLETOKEN = 457,
    SEARCHGALTOKEN = 458,
    GUESSDEGREETOKEN = 459,
    DIRTYFINDZEROSTOKEN = 460,
    IFTOKEN = 461,
    THENTOKEN = 462,
    ELSETOKEN = 463,
    FORTOKEN = 464,
    INTOKEN = 465,
    FROMTOKEN = 466,
    TOTOKEN = 467,
    BYTOKEN = 468,
    DOTOKEN = 469,
    BEGINTOKEN = 470,
    ENDTOKEN = 471,
    LEFTCURLYBRACETOKEN = 472,
    RIGHTCURLYBRACETOKEN = 473,
    WHILETOKEN = 474,
    READFILETOKEN = 475,
    ISBOUNDTOKEN = 476,
    EXECUTETOKEN = 477,
    EXTERNALPROCTOKEN = 478,
    VOIDTOKEN = 479,
    CONSTANTTYPETOKEN = 480,
    FUNCTIONTOKEN = 481,
    OBJECTTOKEN = 482,
    RANGETOKEN = 483,
    INTEGERTOKEN = 484,
    STRINGTYPETOKEN = 485,
    BOOLEANTOKEN = 486,
    LISTTOKEN = 487,
    OFTOKEN = 488,
    VARTOKEN = 489,
    PROCTOKEN = 490,
    PROCEDURETOKEN = 491,
    RETURNTOKEN = 492,
    NOPTOKEN = 493,
    HELPTOKEN = 494,
    VERSIONTOKEN = 495
  };
#endif
/* Tokens.  */
#define CONSTANTTOKEN 258
#define MIDPOINTCONSTANTTOKEN 259
#define DYADICCONSTANTTOKEN 260
#define HEXCONSTANTTOKEN 261
#define HEXADECIMALCONSTANTTOKEN 262
#define BINARYCONSTANTTOKEN 263
#define PITOKEN 264
#define IDENTIFIERTOKEN 265
#define STRINGTOKEN 266
#define LPARTOKEN 267
#define RPARTOKEN 268
#define LBRACKETTOKEN 269
#define RBRACKETTOKEN 270
#define EQUALTOKEN 271
#define ASSIGNEQUALTOKEN 272
#define COMPAREEQUALTOKEN 273
#define COMMATOKEN 274
#define EXCLAMATIONTOKEN 275
#define SEMICOLONTOKEN 276
#define STARLEFTANGLETOKEN 277
#define LEFTANGLETOKEN 278
#define RIGHTANGLEUNDERSCORETOKEN 279
#define RIGHTANGLEDOTTOKEN 280
#define RIGHTANGLESTARTOKEN 281
#define RIGHTANGLETOKEN 282
#define DOTSTOKEN 283
#define DOTTOKEN 284
#define QUESTIONMARKTOKEN 285
#define VERTBARTOKEN 286
#define ATTOKEN 287
#define DOUBLECOLONTOKEN 288
#define COLONTOKEN 289
#define DOTCOLONTOKEN 290
#define COLONDOTTOKEN 291
#define EXCLAMATIONEQUALTOKEN 292
#define APPROXTOKEN 293
#define ANDTOKEN 294
#define ORTOKEN 295
#define PLUSTOKEN 296
#define MINUSTOKEN 297
#define MULTOKEN 298
#define DIVTOKEN 299
#define POWTOKEN 300
#define SQRTTOKEN 301
#define EXPTOKEN 302
#define FREEVARTOKEN 303
#define LOGTOKEN 304
#define LOG2TOKEN 305
#define LOG10TOKEN 306
#define SINTOKEN 307
#define COSTOKEN 308
#define TANTOKEN 309
#define ASINTOKEN 310
#define ACOSTOKEN 311
#define ATANTOKEN 312
#define SINHTOKEN 313
#define COSHTOKEN 314
#define TANHTOKEN 315
#define ASINHTOKEN 316
#define ACOSHTOKEN 317
#define ATANHTOKEN 318
#define ABSTOKEN 319
#define ERFTOKEN 320
#define ERFCTOKEN 321
#define LOG1PTOKEN 322
#define EXPM1TOKEN 323
#define DOUBLETOKEN 324
#define SINGLETOKEN 325
#define HALFPRECISIONTOKEN 326
#define QUADTOKEN 327
#define DOUBLEDOUBLETOKEN 328
#define TRIPLEDOUBLETOKEN 329
#define DOUBLEEXTENDEDTOKEN 330
#define CEILTOKEN 331
#define FLOORTOKEN 332
#define NEARESTINTTOKEN 333
#define HEADTOKEN 334
#define REVERTTOKEN 335
#define SORTTOKEN 336
#define TAILTOKEN 337
#define MANTISSATOKEN 338
#define EXPONENTTOKEN 339
#define PRECISIONTOKEN 340
#define ROUNDCORRECTLYTOKEN 341
#define PRECTOKEN 342
#define POINTSTOKEN 343
#define DIAMTOKEN 344
#define DISPLAYTOKEN 345
#define VERBOSITYTOKEN 346
#define SHOWMESSAGENUMBERSTOKEN 347
#define CANONICALTOKEN 348
#define AUTOSIMPLIFYTOKEN 349
#define TAYLORRECURSIONSTOKEN 350
#define TIMINGTOKEN 351
#define TIMETOKEN 352
#define FULLPARENTHESESTOKEN 353
#define MIDPOINTMODETOKEN 354
#define DIEONERRORMODETOKEN 355
#define SUPPRESSWARNINGSTOKEN 356
#define RATIONALMODETOKEN 357
#define HOPITALRECURSIONSTOKEN 358
#define ONTOKEN 359
#define OFFTOKEN 360
#define DYADICTOKEN 361
#define POWERSTOKEN 362
#define BINARYTOKEN 363
#define HEXADECIMALTOKEN 364
#define FILETOKEN 365
#define POSTSCRIPTTOKEN 366
#define POSTSCRIPTFILETOKEN 367
#define PERTURBTOKEN 368
#define MINUSWORDTOKEN 369
#define PLUSWORDTOKEN 370
#define ZEROWORDTOKEN 371
#define NEARESTTOKEN 372
#define HONORCOEFFPRECTOKEN 373
#define TRUETOKEN 374
#define FALSETOKEN 375
#define DEFAULTTOKEN 376
#define MATCHTOKEN 377
#define WITHTOKEN 378
#define ABSOLUTETOKEN 379
#define DECIMALTOKEN 380
#define RELATIVETOKEN 381
#define FIXEDTOKEN 382
#define FLOATINGTOKEN 383
#define ERRORTOKEN 384
#define QUITTOKEN 385
#define FALSEQUITTOKEN 386
#define RESTARTTOKEN 387
#define LIBRARYTOKEN 388
#define LIBRARYCONSTANTTOKEN 389
#define DIFFTOKEN 390
#define DIRTYSIMPLIFYTOKEN 391
#define REMEZTOKEN 392
#define ANNOTATEFUNCTIONTOKEN 393
#define BASHEVALUATETOKEN 394
#define GETSUPPRESSEDMESSAGESTOKEN 395
#define GETBACKTRACETOKEN 396
#define FPMINIMAXTOKEN 397
#define HORNERTOKEN 398
#define EXPANDTOKEN 399
#define SIMPLIFYSAFETOKEN 400
#define TAYLORTOKEN 401
#define TAYLORFORMTOKEN 402
#define CHEBYSHEVFORMTOKEN 403
#define AUTODIFFTOKEN 404
#define DEGREETOKEN 405
#define NUMERATORTOKEN 406
#define DENOMINATORTOKEN 407
#define SUBSTITUTETOKEN 408
#define COMPOSEPOLYNOMIALSTOKEN 409
#define COEFFTOKEN 410
#define SUBPOLYTOKEN 411
#define ROUNDCOEFFICIENTSTOKEN 412
#define RATIONALAPPROXTOKEN 413
#define ACCURATEINFNORMTOKEN 414
#define ROUNDTOFORMATTOKEN 415
#define EVALUATETOKEN 416
#define LENGTHTOKEN 417
#define OBJECTNAMETOKEN 418
#define INFTOKEN 419
#define MIDTOKEN 420
#define SUPTOKEN 421
#define MINTOKEN 422
#define MAXTOKEN 423
#define READXMLTOKEN 424
#define PARSETOKEN 425
#define PRINTTOKEN 426
#define PRINTXMLTOKEN 427
#define PLOTTOKEN 428
#define PRINTHEXATOKEN 429
#define PRINTFLOATTOKEN 430
#define PRINTBINARYTOKEN 431
#define SUPPRESSMESSAGETOKEN 432
#define UNSUPPRESSMESSAGETOKEN 433
#define PRINTEXPANSIONTOKEN 434
#define BASHEXECUTETOKEN 435
#define EXTERNALPLOTTOKEN 436
#define WRITETOKEN 437
#define ASCIIPLOTTOKEN 438
#define RENAMETOKEN 439
#define BINDTOKEN 440
#define INFNORMTOKEN 441
#define SUPNORMTOKEN 442
#define FINDZEROSTOKEN 443
#define FPFINDZEROSTOKEN 444
#define DIRTYINFNORMTOKEN 445
#define GCDTOKEN 446
#define EUCLDIVTOKEN 447
#define EUCLMODTOKEN 448
#define NUMBERROOTSTOKEN 449
#define INTEGRALTOKEN 450
#define DIRTYINTEGRALTOKEN 451
#define WORSTCASETOKEN 452
#define IMPLEMENTPOLYTOKEN 453
#define IMPLEMENTCONSTTOKEN 454
#define CHECKINFNORMTOKEN 455
#define ZERODENOMINATORSTOKEN 456
#define ISEVALUABLETOKEN 457
#define SEARCHGALTOKEN 458
#define GUESSDEGREETOKEN 459
#define DIRTYFINDZEROSTOKEN 460
#define IFTOKEN 461
#define THENTOKEN 462
#define ELSETOKEN 463
#define FORTOKEN 464
#define INTOKEN 465
#define FROMTOKEN 466
#define TOTOKEN 467
#define BYTOKEN 468
#define DOTOKEN 469
#define BEGINTOKEN 470
#define ENDTOKEN 471
#define LEFTCURLYBRACETOKEN 472
#define RIGHTCURLYBRACETOKEN 473
#define WHILETOKEN 474
#define READFILETOKEN 475
#define ISBOUNDTOKEN 476
#define EXECUTETOKEN 477
#define EXTERNALPROCTOKEN 478
#define VOIDTOKEN 479
#define CONSTANTTYPETOKEN 480
#define FUNCTIONTOKEN 481
#define OBJECTTOKEN 482
#define RANGETOKEN 483
#define INTEGERTOKEN 484
#define STRINGTYPETOKEN 485
#define BOOLEANTOKEN 486
#define LISTTOKEN 487
#define OFTOKEN 488
#define VARTOKEN 489
#define PROCTOKEN 490
#define PROCEDURETOKEN 491
#define RETURNTOKEN 492
#define NOPTOKEN 493
#define HELPTOKEN 494
#define VERSIONTOKEN 495

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 142 "parser.y" /* yacc.c:355  */

  doubleNode *dblnode;
  struct entryStruct *association;
  char *value;
  node *tree;
  chain *list;
  int *integerval;
  int count;
  void *other;

#line 665 "parser.c" /* yacc.c:355  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif



int yyparse (void* scanner);

#endif /* !YY_YY_PARSER_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 681 "parser.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  417
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   9322

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  241
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  47
/* YYNRULES -- Number of rules.  */
#define YYNRULES  642
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  1380

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   495

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   468,   468,   474,   482,   490,   496,   508,   516,   523,
     527,   533,   537,   543,   547,   551,   555,   559,   563,   567,
     571,   577,   581,   589,   594,   599,   607,   611,   617,   621,
     627,   634,   638,   644,   648,   652,   656,   660,   664,   668,
     672,   676,   680,   684,   688,   692,   696,   700,   704,   708,
     712,   716,   720,   724,   728,   732,   736,   743,   747,   751,
     755,   759,   763,   767,   771,   775,   779,   783,   787,   791,
     795,   799,   803,   807,   811,   815,   819,   823,   827,   831,
     835,   839,   843,   847,   851,   855,   861,   866,   871,   875,
     879,   886,   890,   894,   898,   904,   909,   914,   919,   924,
     929,   934,   938,   944,   951,   955,   959,   963,   967,   971,
     975,   979,   983,   987,   991,   995,   999,  1003,  1007,  1011,
    1017,  1021,  1025,  1029,  1033,  1037,  1041,  1045,  1049,  1053,
    1057,  1061,  1065,  1069,  1073,  1077,  1083,  1087,  1093,  1097,
    1103,  1107,  1113,  1123,  1127,  1133,  1137,  1141,  1145,  1151,
    1160,  1164,  1168,  1172,  1176,  1180,  1184,  1188,  1194,  1198,
    1202,  1206,  1210,  1214,  1220,  1224,  1228,  1232,  1239,  1243,
    1250,  1254,  1258,  1262,  1269,  1276,  1280,  1286,  1290,  1294,
    1301,  1305,  1309,  1316,  1320,  1324,  1328,  1332,  1336,  1340,
    1344,  1348,  1352,  1356,  1360,  1364,  1368,  1372,  1376,  1380,
    1384,  1388,  1392,  1396,  1400,  1404,  1408,  1412,  1416,  1420,
    1424,  1428,  1432,  1436,  1440,  1444,  1448,  1459,  1463,  1468,
    1473,  1478,  1483,  1487,  1491,  1495,  1499,  1503,  1507,  1511,
    1516,  1521,  1526,  1530,  1534,  1538,  1544,  1548,  1554,  1558,
    1562,  1566,  1570,  1574,  1578,  1582,  1586,  1592,  1597,  1602,
    1607,  1612,  1617,  1622,  1630,  1634,  1638,  1642,  1648,  1652,
    1656,  1663,  1667,  1671,  1677,  1681,  1685,  1689,  1693,  1697,
    1703,  1707,  1711,  1715,  1719,  1723,  1727,  1731,  1735,  1740,
    1744,  1748,  1752,  1756,  1760,  1764,  1768,  1772,  1776,  1780,
    1784,  1788,  1792,  1796,  1800,  1804,  1808,  1812,  1816,  1820,
    1824,  1828,  1832,  1836,  1840,  1844,  1848,  1852,  1856,  1860,
    1864,  1868,  1872,  1876,  1880,  1884,  1888,  1892,  1896,  1900,
    1904,  1908,  1912,  1916,  1920,  1924,  1928,  1932,  1936,  1940,
    1944,  1948,  1952,  1956,  1960,  1964,  1968,  1972,  1976,  1980,
    1984,  1988,  1992,  1996,  2000,  2004,  2008,  2012,  2016,  2020,
    2024,  2028,  2032,  2036,  2040,  2044,  2048,  2052,  2056,  2060,
    2064,  2068,  2072,  2076,  2080,  2084,  2088,  2092,  2098,  2103,
    2108,  2112,  2116,  2120,  2124,  2128,  2132,  2136,  2140,  2144,
    2148,  2152,  2156,  2160,  2164,  2168,  2174,  2180,  2186,  2192,
    2198,  2204,  2210,  2216,  2222,  2228,  2234,  2240,  2246,  2252,
    2260,  2266,  2273,  2277,  2283,  2287,  2294,  2299,  2304,  2309,
    2314,  2319,  2330,  2335,  2340,  2344,  2348,  2352,  2356,  2360,
    2364,  2375,  2386,  2397,  2401,  2412,  2416,  2427,  2438,  2442,
    2446,  2457,  2461,  2465,  2476,  2480,  2484,  2488,  2499,  2503,
    2514,  2525,  2536,  2547,  2558,  2569,  2580,  2591,  2602,  2613,
    2624,  2635,  2646,  2657,  2668,  2679,  2690,  2701,  2712,  2723,
    2734,  2745,  2756,  2767,  2778,  2789,  2800,  2811,  2823,  2834,
    2845,  2856,  2867,  2878,  2889,  2900,  2911,  2922,  2933,  2944,
    2955,  2966,  2977,  2988,  2999,  3010,  3021,  3032,  3043,  3054,
    3065,  3076,  3087,  3098,  3109,  3120,  3131,  3142,  3153,  3164,
    3175,  3186,  3197,  3208,  3219,  3230,  3241,  3252,  3263,  3274,
    3285,  3296,  3307,  3318,  3329,  3340,  3351,  3362,  3373,  3384,
    3395,  3406,  3417,  3428,  3439,  3450,  3461,  3472,  3483,  3494,
    3505,  3516,  3527,  3538,  3549,  3560,  3571,  3582,  3593,  3604,
    3615,  3626,  3637,  3648,  3659,  3670,  3681,  3692,  3703,  3714,
    3725,  3736,  3747,  3758,  3769,  3780,  3791,  3802,  3813,  3824,
    3835,  3846,  3857,  3868,  3879,  3890,  3901,  3912,  3923,  3934,
    3945,  3956,  3967,  3979,  3990,  4001,  4012,  4023,  4034,  4045,
    4056,  4067,  4078,  4089,  4100,  4111,  4122,  4133,  4144,  4155,
    4166,  4177,  4188,  4199,  4207,  4218,  4229,  4240,  4252,  4264,
    4275,  4286,  4297,  4308,  4319,  4330,  4334,  4338,  4342,  4346,
    4357,  4361,  4365,  4369,  4374,  4378,  4382,  4386,  4397,  4408,
    4419,  4430,  4441,  4452,  4463,  4474,  4478,  4488,  4498,  4508,
    4518,  4528,  4538,  4548,  4558,  4568,  4578,  4588,  4598,  4608,
    4618,  4628,  4638
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "\"decimal constant\"", "\"interval\"",
  "\"dyadic constant\"", "\"constant in memory notation\"",
  "\"hexadecimal constant\"", "\"binary constant\"", "\"pi\"",
  "\"identifier\"", "\"character string\"", "\"(\"", "\")\"", "\"[\"",
  "\"]\"", "\"=\"", "\":=\"", "\"==\"", "\",\"", "\"!\"", "\";\"",
  "\"*<\"", "\"<\"", "\">_\"", "\">.\"", "\">*\"", "\">\"", "\"...\"",
  "\".\"", "\"?\"", "\"|\"", "\"@\"", "\"::\"", "\":\"", "\".:\"",
  "\":.\"", "\"!=\"", "\"~\"", "\"&&\"", "\"||\"", "\"+\"", "\"-\"",
  "\"*\"", "\"/\"", "\"^\"", "\"sqrt\"", "\"exp\"", "\"_x_\"", "\"log\"",
  "\"log2\"", "\"log10\"", "\"sin\"", "\"cos\"", "\"tan\"", "\"asin\"",
  "\"acos\"", "\"atan\"", "\"sinh\"", "\"cosh\"", "\"tanh\"", "\"asinh\"",
  "\"acosh\"", "\"atanh\"", "\"abs\"", "\"erf\"", "\"erfc\"", "\"log1p\"",
  "\"expm1\"", "\"D\"", "\"SG\"", "\"HP\"", "\"QD\"", "\"DD\"", "\"TD\"",
  "\"DE\"", "\"ceil\"", "\"floor\"", "\"nearestint\"", "\"head\"",
  "\"revert\"", "\"sort\"", "\"tail\"", "\"mantissa\"", "\"exponent\"",
  "\"precision\"", "\"roundcorrectly\"", "\"prec\"", "\"points\"",
  "\"diam\"", "\"display\"", "\"verbosity\"", "\"showmessagenumbers\"",
  "\"canonical\"", "\"autosimplify\"", "\"taylorrecursions\"",
  "\"timing\"", "\"time\"", "\"fullparentheses\"", "\"midpointmode\"",
  "\"dieonerrormode\"", "\"roundingwarnings\"", "\"rationalmode\"",
  "\"hopitalrecursions\"", "\"on\"", "\"off\"", "\"dyadic\"", "\"powers\"",
  "\"binary\"", "\"hexadecimal\"", "\"file\"", "\"postscript\"",
  "\"postscriptfile\"", "\"perturb\"", "\"RD\"", "\"RU\"", "\"RZ\"",
  "\"RN\"", "\"honorcoeffprec\"", "\"true\"", "\"false\"", "\"default\"",
  "\"match\"", "\"with\"", "\"absolute\"", "\"decimal\"", "\"relative\"",
  "\"fixed\"", "\"floating\"", "\"error\"", "\"quit\"",
  "\"quit in an included file\"", "\"restart\"", "\"library\"",
  "\"libraryconstant\"", "\"diff\"", "\"dirtysimplify\"", "\"remez\"",
  "\"annotatefunction\"", "\"bashevaluate\"", "\"getsuppressedmessages\"",
  "\"getbacktrace\"", "\"fpminimax\"", "\"horner\"", "\"expand\"",
  "\"simplify\"", "\"taylor\"", "\"taylorform\"", "\"chebyshevform\"",
  "\"autodiff\"", "\"degree\"", "\"numerator\"", "\"denominator\"",
  "\"substitute\"", "\"composepolynomials\"", "\"coeff\"", "\"subpoly\"",
  "\"roundcoefficients\"", "\"rationalapprox\"", "\"accurateinfnorm\"",
  "\"round\"", "\"evaluate\"", "\"length\"", "\"objectname\"", "\"inf\"",
  "\"mid\"", "\"sup\"", "\"min\"", "\"max\"", "\"readxml\"", "\"parse\"",
  "\"print\"", "\"printxml\"", "\"plot\"", "\"printhexa\"",
  "\"printfloat\"", "\"printbinary\"", "\"suppressmessage\"",
  "\"unsuppressmessage\"", "\"printexpansion\"", "\"bashexecute\"",
  "\"externalplot\"", "\"write\"", "\"asciiplot\"", "\"rename\"",
  "\"bind\"", "\"infnorm\"", "\"supnorm\"", "\"findzeros\"",
  "\"fpfindzeros\"", "\"dirtyinfnorm\"", "\"gcd\"", "\"div\"", "\"mod\"",
  "\"numberroots\"", "\"integral\"", "\"dirtyintegral\"", "\"worstcase\"",
  "\"implementpoly\"", "\"implementconst\"", "\"checkinfnorm\"",
  "\"zerodenominators\"", "\"isevaluable\"", "\"searchgal\"",
  "\"guessdegree\"", "\"dirtyfindzeros\"", "\"if\"", "\"then\"",
  "\"else\"", "\"for\"", "\"in\"", "\"from\"", "\"to\"", "\"by\"",
  "\"do\"", "\"begin\"", "\"end\"", "\"{\"", "\"}\"", "\"while\"",
  "\"readfile\"", "\"isbound\"", "\"execute\"", "\"externalproc\"",
  "\"void\"", "\"constant\"", "\"function\"", "\"object\"", "\"range\"",
  "\"integer\"", "\"string\"", "\"boolean\"", "\"list\"", "\"of\"",
  "\"var\"", "\"proc\"", "\"procedure\"", "\"return\"", "\"nop\"",
  "\"help\"", "\"version\"", "$accept", "startsymbol", "helpmeta",
  "beginsymbol", "endsymbol", "command", "ifcommand", "forcommand",
  "commandlist", "variabledeclarationlist", "variabledeclaration",
  "identifierlist", "procbody", "simplecommand", "assignment",
  "simpleassignment", "structuring", "stateassignment",
  "stillstateassignment", "thinglist", "structelementlist",
  "structelementseparator", "structelement", "thing", "supermegaterm",
  "indexing", "megaterm", "hyperterm", "unaryplusminus", "term", "subterm",
  "basicthing", "matchlist", "matchelement", "constant", "list",
  "simplelist", "range", "debound", "headfunction", "egalquestionmark",
  "statedereference", "externalproctype", "extendedexternalproctype",
  "externalproctypesimplelist", "externalproctypelist", "help", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393,   394,
     395,   396,   397,   398,   399,   400,   401,   402,   403,   404,
     405,   406,   407,   408,   409,   410,   411,   412,   413,   414,
     415,   416,   417,   418,   419,   420,   421,   422,   423,   424,
     425,   426,   427,   428,   429,   430,   431,   432,   433,   434,
     435,   436,   437,   438,   439,   440,   441,   442,   443,   444,
     445,   446,   447,   448,   449,   450,   451,   452,   453,   454,
     455,   456,   457,   458,   459,   460,   461,   462,   463,   464,
     465,   466,   467,   468,   469,   470,   471,   472,   473,   474,
     475,   476,   477,   478,   479,   480,   481,   482,   483,   484,
     485,   486,   487,   488,   489,   490,   491,   492,   493,   494,
     495
};
# endif

#define YYPACT_NINF -1210

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-1210)))

#define YYTABLE_NINF -136

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
    1588,    15, -1210, -1210, -1210, -1210, -1210, -1210, -1210,   550,
   -1210,  7223,  4660,  7922,  7223, -1210,  9087,   318,   318,    29,
      76,   116,   168,   180,   207,   219,   223,   227,   270,   272,
     276,   278,   280,   287,   289,   292,   294,   296,   300,   302,
     341,   354,   376,   412,   503,   512,   564,   652,   655,   674,
     676,   679,   689,   691,   705,   717,   731,   742,   755,   757,
      34,    82,    89,   134,   306,   378,   100,   431,   440,   445,
     759,   450,   493,   541,   594,   602,   614, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210,  7223, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210,   771,   773,   788,   795,   797,
     799,   800,   804,   806,   813,   818,   828,   830,   837,   840,
     841,   842,   851,   852,   858,   859,   890,   893,   901,   905,
     917,   918,   922,   923,   930,   934,   935,   939,   940,   943,
     965,   969,   972,   978,   979,   981,   985,   990,  1026,  1031,
    1048,  1058,  1065,  1066,  1072,  1076,  1083,  1086,  1092,  1096,
    1097,  1100,  1103,  1104,  1108,  1131,  1145,  1147,  1162,  1166,
    1171,  1182,  1183,  1193,  1194,  1202,  7223,   129, -1210,   422,
    7223,  1210,  1219,  1228,  1230, -1210,  1232,  1235,   460,  1237,
   -1210,   122,   468,  1826,  3008,   498, -1210, -1210,   491,   390,
   -1210,   600, -1210,   524, -1210,   393,    20,   953,  9087,   415,
   -1210,    19, -1210, -1210, -1210, -1210, -1210, -1210, -1210,  5126,
    4893,  7223,  1239,   664,   664,   664,   664,   664,   664,   144,
     664,   664,   664,   664,   664,   664,   664,   664,   664,   422,
      31, -1210,    23,  5359,   607,   683,    20,   377, -1210, -1210,
   -1210,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  5592, -1210,  5592, -1210,  5592, -1210,  5592, -1210,
    5592, -1210,  5592, -1210,  7223,  5592, -1210,  5592, -1210,  5592,
   -1210,  5592, -1210,  4424,  5592, -1210,  5592, -1210,  5592, -1210,
    5592, -1210,  5592, -1210,  5592, -1210,   587,   561,  7223,  7223,
    7223,  7223,  7223,   500,   611,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,    14,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223, -1210,
     -19,   514, -1210,   685,   558,   101,   -25,  7223,   872,  7223,
     889,  7223,    22, -1210,  1235,  5825, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,   597, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210,   703, -1210, -1210,
   -1210,   743, -1210, -1210, -1210,   706, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210,   740,   902, -1210,   802,   -44,
    4188,  1043, -1210, -1210,  7223,  7223, -1210,  7223,  7922,  7922,
    7223,  7223,  7922,  7456,  7689,  7922,  7922,  7922,  7922,  7922,
    7922,  7922, -1210,  8155,  8388,  7223,  1055,  8854,  8621, -1210,
    1060,  1240,  1241,   587,   587,  1151,  1242,  1117,  1123,   587,
     133, -1210, -1210,  7223,  7223, -1210, -1210, -1210,    33,    36,
      38,    40,    42,    46,    50,    54,    56,    61,    70,    74,
      78,    84,    86,    90,    93,    95,    98,   102,   106,   108,
     114,   118,   123,   127,   131,   136,   138,   142,   146,   155,
     160,   166,   198,   310,   351,   372,   380,   382,   386, -1210,
     482,   900,   995,  1061,  1067,  1074,   405,  1079,  1085,  1090,
    1101,  1180,  1115,  1122,  1124,  1126,  1128,  1136,  7223,   410,
     414,   533,   535,    21, -1210, -1210,   675,   424,   433,   435,
     694,   697,   699,   701,   439,   442,   444,   707,   709,   711,
     716,   723,   725,   734,   739,   741,   447,   449,   452,   454,
     456,  1205,  1215,   458,   467,  1246,   477,   747,   486,   488,
     490,  1247,  1250,   492,   495,   749,  1263,   751,   791,  1268,
     753,   756,   758,   763,   765,   775,   780,   782,   787,   789,
     794,   798,   805,   816,  1275,   820,   822,   827,  1276,   829,
     839,  4424,  7223,  7223,  1274, -1210, -1210, -1210,   422,  4424,
     497,  1279,   499,  1282,    26,    88,   -13,  1280, -1210, -1210,
     501, -1210, -1210, -1210, -1210, -1210,  1284, -1210,  4424, -1210,
   -1210,   -44,  1070,   587,   587, -1210,    20,    20,   587,   587,
     953,  7922,   953,  7922,   953,   953,   953,   415,   415,   415,
     415,   415,  9087,  9087, -1210,  9087,  9087, -1210,   175,   715,
    9087, -1210,  9087,  9087, -1210, -1210,  7223,  7223,  6058,  1293,
   -1210,  6291,  1278,  1291,   374,   672, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,   531,
   -1210,  7223, -1210, -1210,  7223,  7223, -1210,  7223,  7223, -1210,
   -1210, -1210,  7223,  7223,  7223,  7223, -1210, -1210, -1210,  7223,
    7223,  7223,  7223,  7223,  7223,  7223,  7223,  7223, -1210, -1210,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210,  1267,  1283,  7223,
   -1210, -1210, -1210, -1210, -1210, -1210, -1210,  7223,  1287,  7223,
    1305,  1306,  1307,  7223,  7223,  7223,  7223,  7223,  7223,  7223,
    7223,  7223,  7223,  7223,  7223,  7223, -1210,  7223,  7223,  7223,
   -1210,  7223,  7223,  1112,   -23,    -9,  7223, -1210, -1210, -1210,
   -1210, -1210,  7223, -1210,  7223,  1294,   902,  2064,   -13, -1210,
   -1210, -1210, -1210,   953,   953, -1210, -1210, -1210, -1210, -1210,
    7223, -1210, -1210, -1210,   505,   507, -1210,  1308,  1309,   587,
    1310, -1210, -1210, -1210,     1, -1210,   846,   853,   510,   868,
     870,   875,   879,   882,   520,   529,   538,   540,   543,   545,
     885,   887,   548,  6524,  6757,  1311,   892,  6990,   568,  1313,
    1316,  1312,  1320,   897,   573,   576,   593,   596,   598,   601,
     604,   606,   608,   904,   909,   919,   610,   612,   926,   616,
    4424,  4424,  7223,   587,   948,   618,  1321, -1210,  7223, -1210,
      79,  3244,  2300,  1322, -1210, -1210, -1210,  7223, -1210,  7223,
    2536,  7223,  7223, -1210,  7223,  7223,  7223,  7223,  7223, -1210,
   -1210, -1210, -1210, -1210, -1210,  7223,  7223, -1210,  7223,   587,
    7223,   587, -1210,  7223,  7223,   587, -1210, -1210, -1210,  7223,
   -1210,  7223, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210,  7223,  7223,  7223, -1210, -1210,  7223, -1210, -1210, -1210,
     -30,    -4, -1210,   -13,   836,  7223, -1210,  7223, -1210,   764,
    7223, -1210,   808,  3480, -1210,   587,   626,  7223, -1210,   811,
    3716,  1323,   960,  1000,   629,  1325,   650,   657,  1327,   660,
     587,   587,  1011,   587,   666,  1013,  1018,  1022,   668,  1328,
    7223,  4424,  1042, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
   -1210,  1109, -1210, -1210,  1301,  2772,   -44,  1111,  1148,  7223,
   -1210,  1152,  7223, -1210,  7223, -1210,   826, -1210,  1159,  7223,
   -1210,  7223, -1210,   831, -1210,  7223,  7223, -1210, -1210, -1210,
   -1210, -1210, -1210,  7223, -1210,  7223,  7223,  7223, -1210, -1210,
     -21, -1210,  1329,  1334,  1130,  1324,  7223, -1210,   838,  3952,
   -1210,   -44,   -44,  1161,   -44,  1164,  1169,  7223, -1210,   -44,
    1176,  1181,  7223, -1210,  1336,  1337,  1027,  1040,  1050,  1052,
    4424,  1042, -1210, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
    1053,  1186,  7223, -1210,  7223, -1210,   856, -1210, -1210,   -44,
   -1210,   -44,   -44,  1190, -1210,   -44,   -44,  1196, -1210, -1210,
    7223,  7223,  7223,  7223, -1210, -1210,  1339,   -44,  1206,  1216,
    7223, -1210, -1210, -1210, -1210,   -44, -1210, -1210,   -44,  1340,
     670,  1349,  1057, -1210, -1210,   -44,   -44,  1218, -1210, -1210,
   -1210, -1210, -1210,  7223, -1210, -1210,   -44,  1350, -1210, -1210
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       0,     0,   247,   248,   249,   250,   251,   252,   253,   218,
     216,     0,     0,     0,     0,     4,     0,   164,   165,     0,
       0,   213,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   208,   209,   211,   210,   214,   215,   212,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     369,   369,   369,   369,   369,   369,   369,   369,   369,   369,
       0,   369,   369,   369,   369,   369,   369,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,   200,   201,     0,   203,   202,   204,   205,
     206,   207,    57,    58,    62,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     9,    10,
       0,     0,     0,     0,     0,   199,     0,     0,     0,    59,
       8,     0,     0,     0,     0,     0,    13,    88,    93,     0,
      91,     0,    89,   136,   143,   229,   145,   150,     0,   158,
     168,   177,   217,   222,   223,   224,   225,   228,     7,     0,
       0,     0,   218,   369,   369,   369,   369,   369,   369,   369,
     369,   369,   369,   369,   369,   369,   369,   369,   369,     0,
       0,   229,   177,     0,     0,     0,   148,     0,   170,   166,
     167,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   370,     0,   371,     0,   372,     0,   373,
       0,   374,     0,   375,     0,     0,   376,     0,   377,     0,
     378,     0,   379,     0,     0,   380,     0,   381,     0,   382,
       0,   384,     0,   383,     0,   385,     0,   143,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    18,
       0,     0,    20,     0,     0,   138,     0,     0,     0,     0,
       0,     0,     0,   234,     0,     0,     6,     1,   406,   407,
     408,   409,   410,   411,   412,   413,   414,   415,   416,   417,
     420,   421,   422,   423,   424,     3,   425,   426,   428,   429,
     432,   433,   434,   431,   435,   436,   437,   438,   439,   440,
     441,   446,   442,   443,   444,   445,   447,   448,   449,   450,
     451,   452,   453,   454,   455,   456,   457,   458,   459,   460,
     461,   462,   463,   464,   465,   466,   467,   468,   469,   470,
     471,   472,   473,   474,   476,   475,   477,   478,   479,   480,
     481,   482,   483,   486,   487,   488,   621,   620,   622,   484,
     489,   490,   491,   492,   493,   494,   495,   496,   497,   498,
     499,   500,   501,   502,   504,   503,   505,   506,   507,   508,
     509,   510,   511,   512,   513,   514,   515,   516,   517,   518,
     519,   520,   521,   522,   523,   524,   525,   526,   527,   528,
     529,   530,   531,   532,   533,   534,   535,   536,   537,   541,
     542,   543,   538,   539,   540,   546,   547,   548,   549,   550,
     551,   552,   553,   554,   555,   556,   557,   558,   559,   560,
     561,   562,   563,   564,   565,   566,   567,   617,   618,   619,
     544,   545,   571,   568,   569,   570,   572,   573,   574,   575,
     576,   577,   578,   579,   580,   581,   582,   583,   584,   585,
     586,   587,   588,   589,   590,   591,   592,   593,   594,   595,
     596,   597,   598,   599,   600,   601,   602,   603,   604,   605,
     606,   607,   608,   609,   610,   611,   612,   613,    11,    10,
      12,   616,   485,   624,   623,   626,   627,   628,   629,   630,
     631,   632,   633,   634,   635,   636,   637,   639,   640,   641,
     638,   642,   625,   614,   615,     0,     0,    17,     0,     0,
       0,     0,     2,    94,     0,     0,    92,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   169,     0,     0,     0,     0,     0,     0,   221,
       0,     0,     0,    95,    96,     0,   226,     0,     0,   258,
       0,   255,   263,     0,     0,   266,   265,   264,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   368,
     104,   105,   106,   107,   108,   109,     0,   110,   111,   112,
     113,     0,   114,   115,   116,   118,   117,   119,     0,     0,
       0,     0,     0,     0,   273,   274,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   227,   140,   141,     0,     0,
       0,     0,     0,     0,     0,    31,     0,     0,    90,    61,
       0,   418,   427,   430,   419,     5,    31,    30,    26,    14,
      16,     0,    28,   101,   102,   137,   146,   147,    99,   100,
     151,     0,   153,     0,   154,   157,   152,   161,   162,   163,
     159,   160,     0,     0,   171,     0,     0,   172,     0,   230,
       0,   181,     0,     0,   178,   220,     0,     0,     0,   230,
     254,     0,     0,     0,     0,     0,   331,   332,   333,   336,
     337,   338,   339,   340,   341,   342,   343,   344,   345,   346,
     347,   348,   349,   350,   351,   352,   353,   354,   355,   356,
     357,   359,   358,   360,   361,   362,   363,   364,   365,   322,
     325,   326,   330,   327,   328,   329,   323,   283,   235,     0,
     144,   236,   270,   271,     0,     0,   272,     0,     0,   282,
     284,   285,     0,     0,     0,     0,   290,   291,   292,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   366,   367,
     269,   268,   267,   279,   280,   303,   302,    63,    80,     0,
      67,    68,    69,    70,    71,    72,    74,     0,    76,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    73,     0,     0,     0,
     319,     0,     0,    21,     0,     0,     0,   139,    19,   324,
     219,    81,     0,   334,     0,     0,     0,     0,     0,    60,
      27,    15,    29,   155,   156,   175,   173,   176,   174,   149,
       0,   182,   180,   179,     0,     0,   233,     0,     0,   259,
       0,   256,   261,   262,     0,   237,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   142,     0,     0,     0,    32,     0,    36,
       0,     0,     0,     0,    97,    98,   232,     0,   257,     0,
       0,     0,     0,   275,     0,     0,     0,     0,     0,   293,
     294,   295,   296,   297,   298,     0,     0,   301,     0,    64,
       0,    82,    66,     0,     0,    77,    79,    85,    86,     0,
     304,     0,   306,   307,   308,   309,   310,   311,   312,   313,
     314,     0,     0,     0,   317,   318,     0,   321,    22,    25,
       0,     0,   335,     0,     0,     0,    33,     0,    35,     0,
       0,    44,     0,     0,   231,   260,     0,     0,   245,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      65,    83,     0,    78,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   400,   386,   387,   388,   389,   390,   391,
     392,     0,   401,   404,     0,     0,     0,     0,     0,     0,
      34,     0,     0,    41,     0,    43,     0,   246,     0,     0,
     243,     0,   241,     0,   276,     0,     0,   286,   287,   288,
     289,   299,   300,     0,   278,     0,     0,     0,   316,   320,
       0,    23,   402,     0,     0,     0,     0,    52,     0,     0,
      40,     0,     0,     0,     0,     0,     0,     0,    42,     0,
       0,     0,     0,   239,     0,     0,     0,     0,     0,     0,
       0,     0,   405,   393,   394,   395,   396,   397,   398,   399,
       0,     0,     0,    49,     0,    51,     0,    37,    39,     0,
      48,     0,     0,     0,   244,     0,     0,     0,   277,   281,
       0,     0,     0,     0,    24,   403,     0,     0,     0,     0,
       0,    50,    38,    45,    47,     0,   242,   240,     0,     0,
       0,     0,     0,    87,    56,     0,     0,     0,    46,   238,
      75,   305,    84,     0,    53,    55,     0,     0,    54,   315
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
   -1210, -1210, -1210,  -171,   214,     2, -1210, -1210,  -655,  -830,
   -1210,  -649,   950, -1210, -1210, -1210, -1210, -1210, -1210,  -163,
     517, -1210, -1210,   -11,  1271,    92,    -7,  -647,    -6,   619,
     -12,   187,   406, -1210, -1210, -1210, -1210, -1210, -1210, -1210,
     367, -1210, -1209,    48,    58, -1210, -1210
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,   192,   193,   194,   654,   658,   399,   402,   659,   660,
     661,   857,   413,   196,   197,   198,   199,   200,   201,   202,
     404,   848,   405,   203,   204,   241,   206,   207,   208,   209,
     210,   242,   960,   961,   212,   213,   700,   214,   215,   216,
     293,   217,  1242,  1243,  1283,  1244,   655
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
     240,   245,   195,   247,   248,   871,   246,   867,  1232,   668,
     669,   249,   250,  1139,   668,   669,   668,   669,   668,   669,
     668,   669,   653,  1282,   818,   880,   882,   884,   885,   886,
     668,   669,   855,   685,   966,   856,   218,   685,   672,  1043,
     967,   251,  1052,   673,   696,  1044,   916,   674,   686,   917,
     292,   918,   697,   919,   687,   920,   690,   675,   687,   921,
     668,   669,   819,   922,   688,   668,   669,   923,   688,   924,
     668,   669,   668,   669,   925,   668,   669,   668,   669,   668,
     669,   668,   669,   926,   326,   668,   669,   927,   252,   668,
     669,   928,   205,   668,   669,   668,   669,   929,   294,   930,
     668,   669,  1282,   931,  1045,   296,   932,  1046,   933,   668,
     669,   934,   304,   668,   669,   935,   305,   668,   669,   936,
     846,   937,   847,   668,   669,   668,   669,   938,   253,   668,
     669,   939,   668,   669,   668,   669,   940,   668,   669,   401,
     941,   668,   669,   416,   942,   668,   669,   668,   669,   943,
     298,   944,   911,   668,   669,   945,   304,   668,   669,   946,
     695,   912,   668,   669,   913,   400,   668,   669,   947,   406,
     668,   669,   628,   948,   630,   668,   669,   668,   669,   949,
     254,   668,   669,  1230,  1231,   668,   669,   211,   841,   849,
    1059,  1121,   255,  1310,   668,   669,   682,   801,   802,   668,
     669,   805,   178,  1122,   629,   668,   669,   811,   812,   693,
     694,   950,   816,  1050,   668,   669,   178,  1131,   629,   256,
    1233,  1234,  1235,  1236,  1237,  1238,  1239,  1240,  1241,   834,
     676,   257,   699,   838,  1053,   258,  1054,   668,   669,   259,
     708,   709,   710,   711,   712,   713,   714,   715,   716,   717,
     718,   719,   720,   721,   722,   723,   724,   725,   726,   727,
     728,   729,   730,   731,   732,   733,   734,   735,   736,   737,
     738,   739,   740,   741,   742,   743,   744,   745,   746,   747,
     748,   750,   260,   751,   261,   752,   205,   753,   262,   754,
     263,   755,   264,   756,   757,   628,   758,   630,   759,   265,
     760,   266,  1203,   762,   267,   763,   268,   764,   269,   765,
    1210,   766,   270,   767,   271,   761,  1195,   769,   770,   771,
     772,   773,   300,   951,   776,   777,   778,   779,   780,   781,
     782,   783,   784,   785,   786,   787,   788,   789,   790,   791,
     792,   793,   794,   795,   796,   797,   798,   799,   800,   668,
     669,   803,   804,   272,   806,   807,   808,   809,   810,    17,
      18,   813,   814,   815,   952,   817,   273,   820,   821,   822,
     823,   824,   825,   826,   827,   828,   829,   830,   831,   832,
     833,   211,   835,   836,   837,   953,   839,   840,   274,  1072,
     668,   669,  1130,   954,   302,   955,   850,  1127,   852,   956,
     854,   705,   706,   707,   860,   205,   664,   665,   657,   670,
     671,   668,   669,   668,   669,  1289,   668,   669,   957,   668,
     669,   668,   669,   962,   275,   668,   669,   963,   295,   297,
     299,   301,   303,   306,   308,   310,   312,   969,   315,   317,
     319,   321,   323,   325,   668,   669,   970,   307,   971,   668,
     669,   403,   976,   668,   669,   977,   309,   978,   683,   684,
     988,   311,   989,   668,   669,   990,   314,   991,   417,   992,
     414,   995,   668,   669,   668,   669,  1199,  1202,   668,   669,
     996,   668,   669,   668,   669,  1209,   668,   669,   668,   669,
     998,   668,   669,   668,   669,   668,   669,   668,   669,  1000,
     211,  1001,  -120,  1002,   875,  1005,   668,   669,  1006,   316,
    1039,   663,  1041,   774,  1049,   276,   668,   669,  1134,   662,
    1135,   668,   669,  1143,   277,   668,   669,   668,   669,   668,
     669,   668,   669,  1149,   668,   669,   668,   669,   668,   669,
     668,   669,  1150,   667,   668,   669,   668,   669,  1256,   668,
     669,  1151,   964,  1152,   965,  1263,  1153,   318,  1154,   668,
     669,  1157,   219,   668,   669,  1074,   220,   221,   668,   669,
     668,   669,   668,   669,   668,   669,   278,   668,   669,   668,
     669,  1166,   668,   669,   668,   669,  1172,   668,   669,  1173,
    1288,   295,   297,   299,   301,   303,   306,   308,   310,   312,
     315,   317,   319,   321,   323,   325,  1174,   668,   669,  1175,
     320,  1176,   668,   669,  1177,   668,   669,  1178,   322,  1179,
     666,  1180,   701,  1184,   775,  1185,   668,   669,   861,  1187,
     324,  1192,   668,   669,  1326,   668,   669,   668,   669,  1257,
     668,   669,  1267,   668,   669,   668,   669,   668,   669,   668,
     669,   668,   669,   873,   874,   668,   669,   668,   669,   878,
     879,   876,   877,  1269,   279,   668,   669,   280,   668,   669,
    1270,   894,   897,  1272,   898,   901,   904,   893,   896,  1274,
     695,  1278,   903,  1371,   768,  1047,   281,  1073,   282,   668,
     669,   283,   914,   915,   968,   844,   668,   669,   702,   668,
     669,   284,   703,   285,   704,   668,   669,   668,   669,   668,
     669,   668,   669,   972,   668,   669,   973,   286,   974,   862,
     975,   864,   668,   669,   842,   843,   979,  1060,   980,   287,
     981,  -103,  -103,   668,   669,   982,   668,   669,   668,   669,
     668,   669,   983,   288,   984,  1067,   668,   669,   668,   669,
     668,   669,   205,   985,   289,   668,   669,   959,   986,   863,
     987,   865,   668,   669,   668,   669,   999,   290,  1007,   291,
    1009,   313,  1012,   668,   669,  1013,   845,  1014,   668,   669,
     668,   669,  1015,   328,  1016,   329,   668,   669,   668,   669,
     668,   669,   668,   669,  1017,   668,   669,   668,   669,  1018,
     330,  1019,   668,   669,   668,   669,  1020,   331,  1021,   332,
    1010,   333,   334,  1022,   668,   669,   335,  1023,   336,   668,
     669,   668,   669,   868,  1024,   337,   668,   669,   668,   669,
     338,  1034,  1035,   668,   669,  1025,  1095,   668,   669,  1027,
     339,  1028,   340,  1033,   668,   669,  1029,   211,  1031,   341,
    1102,  1038,   342,   343,   344,   668,   669,  1246,  1032,   668,
     669,   668,   669,   345,   346,  1141,   668,   669,   668,   669,
     347,   348,  1142,   869,   870,   668,   669,  1132,   668,   669,
    1055,  1056,   851,  1057,  1058,   668,   669,  1144,  1061,  1145,
    1062,  1063,   668,   669,  1146,  1064,  1065,  1133,  1147,   853,
    1069,  1148,   349,  1140,  1155,   350,  1156,   668,   669,   668,
     669,  1163,   866,   351,   668,   669,  1171,   352,   668,   669,
    -121,   668,   669,  1181,   668,   669,   668,   669,  1182,   353,
     354,   668,   669,   205,   355,   356,   668,   669,  1183,   668,
     669,   205,   357,   668,   669,  1186,   358,   359,   668,   669,
     959,   360,   361,  1076,  1077,   362,  1078,  1079,   668,   669,
     205,  1080,  1081,  1082,  1083,   668,   669,  1191,  1084,  1085,
    1086,  1087,  1088,  1089,  1090,  1091,  1092,   363,  1211,  1265,
     628,   364,   630,  1215,   365,   677,   678,   668,   669,   679,
     366,   367,  1218,   368,   680,   681,  1096,   369,  1098,   668,
     669,  1249,   370,  1103,  1104,  1105,  1106,  1107,  1108,  1109,
    1110,  1111,  1112,  1113,  1114,  -122,  1115,  1116,  1117,  1266,
    1118,  1119,  1245,  1229,   628,  1123,   630,   628,   211,   630,
    1273,  1124,  1275,  1125,   668,   669,   211,  1276,   371,   668,
     669,  1277,   628,   372,   630,  1252,  1340,   628,  1259,   630,
     668,   669,   668,   669,   628,   211,   630,   668,   669,  1341,
     373,   668,   669,  1297,   872,   899,   668,   669,  1302,  1342,
     374,  1343,   628,   905,   630,  1322,  1373,   375,   376,   668,
     669,  -123,  1159,  1161,   377,  1051,  1165,  -124,   378,   668,
     669,   668,   669,  1350,  -125,   379,   668,   669,   380,  -126,
     668,   669,  1304,  1305,   381,  -127,   668,   669,   382,   383,
    -128,  1190,   384,   668,   669,   385,   386,  1194,   668,   669,
     387,  -129,  1188,  1189,   668,   669,  1205,   909,  1206,   668,
     669,  1212,  1291,  1213,  1214,  -130,  1216,  1217,   910,   205,
     668,   669,  -131,   388,  -132,  1219,  -134,  1220,  -133,  1221,
     668,   669,  1222,  1223,   668,   669,  -135,   389,  1224,   390,
    1225,   668,   669,   668,   669,   668,   669,   668,   669,  1292,
    1226,  1227,  1228,  1294,   391,   668,   669,  1359,   392,  1361,
    1299,   749,  1329,   393,  1247,  1331,  1248,   668,   669,  1251,
    1332,   668,   669,   958,   394,   395,  1258,  1335,   668,   669,
     668,   669,  1336,   668,   669,   396,   397,  1347,   668,   669,
    1377,  1355,   205,   205,   398,   668,   669,  1358,   993,  1280,
     668,   669,   407,   205,   205,   668,   669,  1365,   994,   668,
     669,   408,   205,  1281,   211,   668,   669,  1366,  1293,  1376,
     409,  1295,   410,  1296,   411,   668,   669,   412,  1300,   415,
    1301,   219,   906,   907,   908,   668,   669,   668,   669,   997,
    1003,  1129,  1306,  1004,  1307,  1308,  1309,  1234,  1235,  1236,
    1237,  1238,  1239,  1240,  1241,  1321,  1008,  1233,  1234,  1235,
    1236,  1237,  1238,  1239,  1240,  1241,  1333,  1011,  1026,  1030,
    1036,  1337,  1040,  1048,  1093,   205,   887,   888,   889,   890,
     891,  1042,   205,  1046,   656,  1060,  1071,   211,   211,  1070,
    1094,  1348,  1344,  1349,  1097,  1099,  1100,  1101,   211,   211,
    1120,  1136,  1126,   205,  1162,  1138,  1167,   211,  1137,  1168,
    1360,  1169,  1362,  1170,  1193,  1204,  1264,   205,  1268,  1367,
    1271,  1279,  1284,  1285,  1196,  1198,  1201,  1312,  1311,  1338,
    1339,  1320,  1363,  1370,  1208,  1313,  1314,  1315,  1316,  1317,
    1318,  1319,  1372,  1379,   858,  1037,   327,  1075,  1346,  1345,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   205,     0,     0,     0,     0,     0,     0,     0,     0,
     211,     0,     0,     0,     0,     0,     0,   211,     0,     0,
       0,     0,   205,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  1250,     0,     0,  1253,  1255,   211,     0,
       0,     0,     0,  1260,  1262,     0,     0,     0,     0,     0,
       0,     0,   211,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  1287,
    1290,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1298,     0,     0,     0,     0,     0,   211,  1303,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   211,     0,     0,
       0,     0,  1323,  1325,     0,  1327,  1328,     0,  1330,     0,
       0,     0,     0,  1334,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    1351,     0,     0,  1352,     0,  1353,  1354,     0,     0,  1356,
    1357,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  1364,     0,     0,     0,     0,     0,     0,     0,  1368,
       0,     0,  1369,     0,     0,     0,     0,     0,     0,  1374,
    1375,     0,     0,     0,     0,     0,     0,     0,     0,     1,
    1378,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,    15,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,     0,    96,    97,    98,    99,   100,   101,   102,   103,
     104,     0,     0,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,     0,     0,   177,     0,     0,
       0,     0,     0,   178,     0,   179,     0,   180,   181,   182,
     183,   184,   185,     0,   186,     0,     0,     0,     0,     0,
       0,     0,     0,   187,   188,     0,   189,   190,   191,   418,
       0,   419,   420,   421,   422,   423,   424,   425,   426,   427,
     428,   429,   430,   431,   432,   433,   434,   435,   436,   437,
     438,   439,   440,   441,   442,   443,   444,   445,   446,   447,
       0,   448,   449,   450,   451,   452,   453,   454,   455,   456,
     457,   458,   459,   460,   461,   462,   463,   464,   465,   466,
     467,   468,   469,   470,   471,   472,   473,   474,   475,   476,
     477,   478,   479,   480,   481,   482,   483,   484,   485,   486,
     487,   488,   489,   490,   491,   492,   493,   494,   495,   496,
     497,   498,   499,   500,   501,   502,   503,   504,   505,   506,
     507,   508,   509,   510,   511,   512,   513,   514,   515,   516,
     517,   518,   519,   520,   521,   522,   523,   524,   525,   526,
     527,   528,   529,   530,   531,   532,   533,   534,   535,   536,
     537,   538,   539,   540,   541,   542,   543,   544,   545,   546,
     547,   548,   549,   550,   551,   552,   553,   554,   555,   556,
     557,   558,   559,   560,   561,   562,   563,   564,   565,   566,
     567,   568,   569,   570,   571,   572,   573,   574,   575,   576,
     577,   578,   579,   580,   581,   582,   583,   584,   585,   586,
     587,   588,   589,   590,   591,   592,   593,   594,   595,   596,
     597,   598,   599,   600,   601,   602,   603,   604,   605,   606,
     607,   608,   609,   610,   611,   612,   613,   614,   615,   616,
     617,   618,   619,   620,   621,   622,   623,   624,   625,   626,
     627,   178,   628,   629,   630,   631,   632,   633,   634,   635,
     636,   637,   638,   639,   640,   641,   642,   643,   644,   645,
     646,   647,   648,   649,   650,   651,   652,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,     0,    96,    97,
      98,    99,   100,   101,   102,   103,   104,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,     0,     0,   177,     0,     0,     0,     0,     0,   178,
     628,   179,   630,   180,   181,   182,   183,   184,   185,     0,
     186,     0,     0,     0,     0,     0,     0,     0,   656,   187,
     188,  1128,   189,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    16,     0,
       0,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,     0,    96,    97,    98,    99,   100,   101,
     102,   103,   104,     0,     0,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,     0,     0,   177,
       0,     0,     0,     0,     0,   178,   628,   179,   630,   180,
     181,   182,   183,   184,   185,     0,   186,     0,     0,     0,
       0,     0,     0,     0,   656,   187,   188,  1200,   189,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,     0,
      96,    97,    98,    99,   100,   101,   102,   103,   104,     0,
       0,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,     0,     0,   177,     0,     0,     0,     0,
       0,   178,   628,   179,   630,   180,   181,   182,   183,   184,
     185,     0,   186,     0,     0,     0,     0,     0,     0,     0,
     656,   187,   188,  1207,   189,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      16,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,     0,    96,    97,    98,    99,
     100,   101,   102,   103,   104,     0,     0,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,     0,
       0,   177,     0,     0,     0,     0,     0,   178,   628,   179,
     630,   180,   181,   182,   183,   184,   185,     0,   186,     0,
       0,     0,     0,     0,     0,     0,   656,   187,   188,  1286,
     189,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,     0,    96,    97,    98,    99,   100,   101,   102,   103,
     104,     0,     0,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,     0,     0,   177,     0,     0,
       0,     0,     0,   178,   628,   179,   630,   180,   181,   182,
     183,   184,   185,     0,   186,     0,     0,     0,     0,     0,
       0,     0,   656,   187,   188,     0,   189,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,     0,    96,    97,
      98,    99,   100,   101,   102,   103,   104,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,     0,     0,   177,     0,     0,     0,     0,     0,   178,
     628,   179,   630,   180,   181,   182,   183,   184,   185,     0,
     186,     0,     0,     0,     0,     0,     0,     0,     0,   187,
     188,  1197,   189,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    16,     0,
       0,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,     0,    96,    97,    98,    99,   100,   101,
     102,   103,   104,     0,     0,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,     0,     0,   177,
       0,     0,     0,     0,     0,   178,   628,   179,   630,   180,
     181,   182,   183,   184,   185,     0,   186,     0,     0,     0,
       0,     0,     0,     0,     0,   187,   188,  1254,   189,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,     0,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,     0,
      96,    97,    98,    99,   100,   101,   102,   103,   104,     0,
       0,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,     0,     0,   177,     0,     0,     0,     0,
       0,   178,   628,   179,   630,   180,   181,   182,   183,   184,
     185,     0,   186,     0,     0,     0,     0,     0,     0,     0,
       0,   187,   188,  1261,   189,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      16,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,     0,    96,    97,    98,    99,
     100,   101,   102,   103,   104,     0,     0,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,     0,
       0,   177,     0,     0,     0,     0,     0,   178,   628,   179,
     630,   180,   181,   182,   183,   184,   185,     0,   186,     0,
       0,     0,     0,     0,     0,     0,     0,   187,   188,  1324,
     189,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,     0,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,     0,    96,    97,    98,    99,   100,   101,   102,   103,
     104,     0,     0,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,     0,     0,   177,     0,     0,
       0,     0,     0,   178,   628,   179,   630,   180,   181,   182,
     183,   184,   185,     0,   186,     0,     0,     0,     0,     0,
       0,     0,     0,   187,   188,     0,   189,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,     0,    96,    97,
      98,    99,   100,   101,   102,   103,   104,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,     0,     0,   177,     0,     0,     0,     0,     0,   178,
       0,   179,     0,   180,   181,   182,   183,   184,   185,     0,
     186,     0,     0,     0,     0,     0,     0,     0,     0,   187,
     188,     0,   189,     2,     3,     4,     5,     6,     7,     8,
     222,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,     0,     0,     0,
       0,   243,     0,     0,     0,     0,     0,     0,    16,     0,
     244,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,   223,   224,   225,
     226,   227,   228,   229,   230,   231,   232,    70,   233,   234,
     235,   236,   237,   238,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,     0,    96,    97,    98,    99,   100,   101,
       0,     0,     0,     0,     0,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,     0,   168,     0,
     170,   171,   172,   173,   174,   175,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   239,     0,     0,
     181,   182,     0,     0,   185,     0,   186,     0,     0,     0,
       0,     0,     0,     0,     0,   187,     2,     3,     4,     5,
       6,     7,     8,   222,    10,    11,     0,    12,     0,     0,
       0,     0,     0,    13,     0,    14,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,    17,    18,     0,     0,     0,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
     223,   224,   225,   226,   227,   228,   229,   230,   231,   232,
      70,   233,   234,   235,   236,   237,   238,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,     0,    96,    97,    98,
      99,   100,   101,     0,     0,     0,   691,   692,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
       0,   168,     0,   170,   171,   172,   173,   174,   175,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     239,     0,     0,   181,   182,     0,     0,   185,     0,   186,
       0,     0,     0,     0,     0,     0,     0,     0,   187,     2,
       3,     4,     5,     6,     7,     8,   222,    10,    11,   689,
      12,     0,     0,     0,     0,     0,    13,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,   223,   224,   225,   226,   227,   228,   229,
     230,   231,   232,    70,   233,   234,   235,   236,   237,   238,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,     0,
      96,    97,    98,    99,   100,   101,     0,     0,     0,     0,
       0,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,     0,   168,     0,   170,   171,   172,   173,
     174,   175,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   239,     0,     0,   181,   182,     0,     0,
     185,     0,   186,     0,     0,     0,     0,     0,     0,     0,
       0,   187,     2,     3,     4,     5,     6,     7,     8,   222,
      10,    11,     0,    12,     0,     0,     0,     0,     0,    13,
       0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
     698,     0,     0,     0,     0,     0,     0,    16,     0,     0,
      17,    18,     0,     0,     0,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   232,    70,   233,   234,   235,
     236,   237,   238,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,     0,    96,    97,    98,    99,   100,   101,     0,
       0,     0,     0,     0,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,     0,   168,     0,   170,
     171,   172,   173,   174,   175,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   239,     0,     0,   181,
     182,     0,     0,   185,     0,   186,     0,     0,     0,     0,
       0,     0,     0,     0,   187,     2,     3,     4,     5,     6,
       7,     8,   222,    10,    11,     0,    12,     0,     0,     0,
       0,     0,    13,     0,    14,     0,     0,     0,     0,     0,
       0,     0,   749,     0,     0,     0,     0,     0,     0,     0,
      16,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,    70,
     233,   234,   235,   236,   237,   238,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,     0,    96,    97,    98,    99,
     100,   101,     0,     0,     0,     0,     0,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,     0,
     168,     0,   170,   171,   172,   173,   174,   175,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   239,
       0,     0,   181,   182,     0,     0,   185,     0,   186,     0,
       0,     0,     0,     0,     0,     0,     0,   187,     2,     3,
       4,     5,     6,     7,     8,   222,    10,    11,   859,    12,
       0,     0,     0,     0,     0,    13,     0,    14,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    16,     0,     0,    17,    18,     0,     0,
       0,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,   223,   224,   225,   226,   227,   228,   229,   230,
     231,   232,    70,   233,   234,   235,   236,   237,   238,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,     0,    96,
      97,    98,    99,   100,   101,     0,     0,     0,     0,     0,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,     0,   168,     0,   170,   171,   172,   173,   174,
     175,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   239,     0,     0,   181,   182,     0,     0,   185,
       0,   186,     0,     0,     0,     0,     0,     0,     0,     0,
     187,     2,     3,     4,     5,     6,     7,     8,   222,    10,
      11,  1066,    12,     0,     0,     0,     0,     0,    13,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,    70,   233,   234,   235,   236,
     237,   238,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,     0,    96,    97,    98,    99,   100,   101,     0,     0,
       0,     0,     0,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,     0,   168,     0,   170,   171,
     172,   173,   174,   175,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   239,     0,     0,   181,   182,
       0,     0,   185,     0,   186,     0,     0,     0,     0,     0,
       0,     0,     0,   187,     2,     3,     4,     5,     6,     7,
       8,   222,    10,    11,     0,    12,     0,     0,     0,     0,
       0,    13,     0,    14,     0,     0,     0,     0,     0,  1068,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    16,
       0,     0,    17,    18,     0,     0,     0,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,    70,   233,
     234,   235,   236,   237,   238,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,     0,    96,    97,    98,    99,   100,
     101,     0,     0,     0,     0,     0,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,     0,   168,
       0,   170,   171,   172,   173,   174,   175,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   239,     0,
       0,   181,   182,     0,     0,   185,     0,   186,     0,     0,
       0,     0,     0,     0,     0,     0,   187,     2,     3,     4,
       5,     6,     7,     8,   222,    10,    11,     0,    12,     0,
       0,     0,     0,     0,    13,     0,    14,     0,     0,     0,
       0,  1158,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    16,     0,     0,    17,    18,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,    70,   233,   234,   235,   236,   237,   238,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,     0,    96,    97,
      98,    99,   100,   101,     0,     0,     0,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,     0,   168,     0,   170,   171,   172,   173,   174,   175,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   239,     0,     0,   181,   182,     0,     0,   185,     0,
     186,     0,     0,     0,     0,     0,     0,     0,     0,   187,
       2,     3,     4,     5,     6,     7,     8,   222,    10,    11,
       0,    12,     0,     0,     0,     0,     0,    13,     0,    14,
       0,     0,     0,     0,  1160,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    16,     0,     0,    17,    18,
       0,     0,     0,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,    70,   233,   234,   235,   236,   237,
     238,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
       0,    96,    97,    98,    99,   100,   101,     0,     0,     0,
       0,     0,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,     0,   168,     0,   170,   171,   172,
     173,   174,   175,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   239,     0,     0,   181,   182,     0,
       0,   185,     0,   186,     0,     0,     0,     0,     0,     0,
       0,     0,   187,     2,     3,     4,     5,     6,     7,     8,
     222,    10,    11,     0,    12,     0,     0,     0,     0,     0,
      13,     0,    14,     0,     0,     0,     0,  1164,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    16,     0,
       0,    17,    18,     0,     0,     0,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,    39,    40,    41,    42,
      43,    44,    45,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,   223,   224,   225,
     226,   227,   228,   229,   230,   231,   232,    70,   233,   234,
     235,   236,   237,   238,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,     0,    96,    97,    98,    99,   100,   101,
       0,     0,     0,     0,     0,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,   138,   139,
     140,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,     0,   168,     0,
     170,   171,   172,   173,   174,   175,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   239,     0,     0,
     181,   182,     0,     0,   185,     0,   186,     0,     0,     0,
       0,     0,     0,     0,     0,   187,     2,     3,     4,     5,
       6,     7,     8,   222,    10,    11,     0,    12,     0,     0,
       0,     0,     0,    13,     0,    14,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,    17,    18,     0,     0,     0,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
     223,   224,   225,   226,   227,   228,   229,   230,   231,   232,
      70,   233,   234,   235,   236,   237,   238,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,     0,    96,    97,    98,
      99,   100,   101,     0,     0,     0,     0,     0,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
       0,   168,     0,   170,   171,   172,   173,   174,   175,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     239,     0,     0,   181,   182,     0,     0,   185,     0,   186,
       0,     0,     0,     0,     0,     0,     0,     0,   187,     2,
       3,     4,     5,     6,     7,     8,   222,    10,    11,     0,
      12,     0,   881,     0,     0,     0,     0,     0,    14,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    16,     0,     0,    17,    18,     0,
       0,     0,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,   223,   224,   225,   226,   227,   228,   229,
     230,   231,   232,    70,   233,   234,   235,   236,   237,   238,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,     0,     0,
      96,    97,    98,    99,   100,   101,     0,     0,     0,     0,
       0,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,     0,   168,     0,   170,   171,   172,   173,
     174,   175,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   239,     0,     0,   181,   182,     0,     0,
     185,     0,   186,     0,     0,     0,     0,     0,     0,     0,
       0,   187,     2,     3,     4,     5,     6,     7,     8,   222,
      10,    11,     0,    12,     0,   883,     0,     0,     0,     0,
       0,    14,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,     0,
      17,    18,     0,     0,     0,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   232,    70,   233,   234,   235,
     236,   237,   238,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,     0,     0,    96,    97,    98,    99,   100,   101,     0,
       0,     0,     0,     0,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,   128,   129,   130,
     131,   132,   133,   134,   135,   136,   137,   138,   139,   140,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,     0,   168,     0,   170,
     171,   172,   173,   174,   175,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   239,     0,     0,   181,
     182,     0,     0,   185,     0,   186,     0,     0,     0,     0,
       0,     0,     0,     0,   187,     2,     3,     4,     5,     6,
       7,     8,   222,    10,    11,     0,    12,     0,     0,     0,
       0,     0,     0,     0,    14,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      16,     0,     0,    17,    18,     0,     0,     0,    19,    20,
      21,    22,    23,    24,    25,    26,    27,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,    40,
      41,    42,    43,    44,    45,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,    70,
     233,   234,   235,   236,   237,   238,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,     0,     0,    96,    97,    98,    99,
     100,   101,     0,     0,     0,     0,     0,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
     138,   139,   140,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,     0,
     168,     0,   170,   171,   172,   173,   174,   175,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   239,
       0,     0,   181,   182,     0,     0,   185,     0,   186,     0,
       0,     0,     0,     0,     0,     0,     0,   187,     2,     3,
       4,     5,     6,     7,     8,   222,    10,    11,     0,    12,
       0,     0,     0,     0,     0,     0,     0,    14,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   892,     0,     0,    17,    18,     0,     0,
       0,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,   223,   224,   225,   226,   227,   228,   229,   230,
     231,   232,    70,   233,   234,   235,   236,   237,   238,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,     0,     0,    96,
      97,    98,    99,   100,   101,     0,     0,     0,     0,     0,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,     0,   168,     0,   170,   171,   172,   173,   174,
     175,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   239,     0,     0,   181,   182,     0,     0,   185,
       0,   186,     0,     0,     0,     0,     0,     0,     0,     0,
     187,     2,     3,     4,     5,     6,     7,     8,   222,    10,
      11,     0,    12,     0,     0,     0,     0,     0,     0,     0,
      14,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   895,     0,     0,    17,
      18,     0,     0,     0,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,    70,   233,   234,   235,   236,
     237,   238,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
       0,     0,    96,    97,    98,    99,   100,   101,     0,     0,
       0,     0,     0,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,   138,   139,   140,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,     0,   168,     0,   170,   171,
     172,   173,   174,   175,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   239,     0,     0,   181,   182,
       0,     0,   185,     0,   186,     0,     0,     0,     0,     0,
       0,     0,     0,   187,     2,     3,     4,     5,     6,     7,
       8,   222,    10,    11,     0,    12,     0,     0,     0,     0,
       0,     0,     0,    14,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   902,
       0,     0,    17,    18,     0,     0,     0,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,    70,   233,
     234,   235,   236,   237,   238,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,     0,     0,    96,    97,    98,    99,   100,
     101,     0,     0,     0,     0,     0,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,     0,   168,
       0,   170,   171,   172,   173,   174,   175,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   239,     0,
       0,   181,   182,     0,     0,   185,     0,   186,     0,     0,
       0,     0,     0,     0,     0,     0,   187,     2,     3,     4,
       5,     6,     7,     8,   222,    10,    11,     0,    12,     0,
       0,     0,     0,     0,     0,     0,    14,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   900,     0,     0,     0,     0,     0,     0,     0,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,    70,   233,   234,   235,   236,   237,   238,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,     0,     0,    96,    97,
      98,    99,   100,   101,     0,     0,     0,     0,     0,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,     0,   168,     0,   170,   171,   172,   173,   174,   175,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   239,     0,     0,   181,   182,     0,     0,   185,     0,
     186,     0,     0,     0,     0,     0,     0,     0,     0,   187,
       2,     3,     4,     5,     6,     7,     8,   222,    10,    11,
       0,    12,     0,     0,     0,     0,     0,     0,     0,    14,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,   223,   224,   225,   226,   227,   228,
     229,   230,   231,   232,    70,   233,   234,   235,   236,   237,
     238,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,     0,
       0,    96,    97,    98,    99,   100,   101,     0,     0,     0,
       0,     0,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,     0,   168,     0,   170,   171,   172,
     173,   174,   175,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   239,     0,     0,   181,   182,     0,
       0,   185,     0,   186,     0,     0,     0,     0,     0,     0,
       0,     0,   187
};

static const yytype_int16 yycheck[] =
{
      11,    12,     0,    14,    16,   660,    13,   656,    12,    39,
      40,    17,    18,    12,    39,    40,    39,    40,    39,    40,
      39,    40,   193,  1232,    10,   672,   673,   674,   675,   676,
      39,    40,    10,    14,    13,    13,    21,    14,    18,    13,
      19,    12,   872,    23,    13,    19,    13,    27,    29,    13,
      16,    13,    29,    13,    35,    13,   219,    37,    35,    13,
      39,    40,    48,    13,    45,    39,    40,    13,    45,    13,
      39,    40,    39,    40,    13,    39,    40,    39,    40,    39,
      40,    39,    40,    13,    95,    39,    40,    13,    12,    39,
      40,    13,     0,    39,    40,    39,    40,    13,    16,    13,
      39,    40,  1311,    13,    16,    16,    13,    19,    13,    39,
      40,    13,    12,    39,    40,    13,    16,    39,    40,    13,
      19,    13,    21,    39,    40,    39,    40,    13,    12,    39,
      40,    13,    39,    40,    39,    40,    13,    39,    40,    10,
      13,    39,    40,    21,    13,    39,    40,    39,    40,    13,
      16,    13,    19,    39,    40,    13,    12,    39,    40,    13,
      16,    28,    39,    40,    31,   176,    39,    40,    13,   180,
      39,    40,   216,    13,   218,    39,    40,    39,    40,    13,
      12,    39,    40,   213,   214,    39,    40,     0,   207,   214,
      15,   214,    12,   214,    39,    40,   208,   360,   361,    39,
      40,   364,   215,   212,   217,    39,    40,   370,   371,   220,
     221,    13,   375,   868,    39,    40,   215,  1047,   217,    12,
     224,   225,   226,   227,   228,   229,   230,   231,   232,   392,
     210,    12,   243,   396,   881,    12,   883,    39,    40,    12,
     251,   252,   253,   254,   255,   256,   257,   258,   259,   260,
     261,   262,   263,   264,   265,   266,   267,   268,   269,   270,
     271,   272,   273,   274,   275,   276,   277,   278,   279,   280,
     281,   282,   283,   284,   285,   286,   287,   288,   289,   290,
     291,   292,    12,   294,    12,   296,   194,   298,    12,   300,
      12,   302,    12,   304,   305,   216,   307,   218,   309,    12,
     311,    12,  1132,   314,    12,   316,    12,   318,    12,   320,
    1140,   322,    12,   324,    12,   313,   237,   328,   329,   330,
     331,   332,    16,    13,   335,   336,   337,   338,   339,   340,
     341,   342,   343,   344,   345,   346,   347,   348,   349,   350,
     351,   352,   353,   354,   355,   356,   357,   358,   359,    39,
      40,   362,   363,    12,   365,   366,   367,   368,   369,    41,
      42,   372,   373,   374,    13,   376,    12,   378,   379,   380,
     381,   382,   383,   384,   385,   386,   387,   388,   389,   390,
     391,   194,   393,   394,   395,    13,   397,   398,    12,    15,
      39,    40,  1047,    13,    16,    13,   407,  1046,   409,    13,
     411,    24,    25,    26,   415,   313,    16,    17,   194,    16,
      17,    39,    40,    39,    40,  1245,    39,    40,    13,    39,
      40,    39,    40,    13,    12,    39,    40,    13,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    13,    71,    72,
      73,    74,    75,    76,    39,    40,    13,    16,    13,    39,
      40,    29,    13,    39,    40,    13,    16,    13,    43,    44,
      13,    16,    13,    39,    40,    13,    16,    13,     0,    13,
      10,    13,    39,    40,    39,    40,  1131,  1132,    39,    40,
      13,    39,    40,    39,    40,  1140,    39,    40,    39,    40,
      13,    39,    40,    39,    40,    39,    40,    39,    40,    13,
     313,    13,    20,    13,   667,    13,    39,    40,    13,    16,
      13,    20,    13,    13,    13,    12,    39,    40,    13,    21,
      13,    39,    40,    13,    12,    39,    40,    39,    40,    39,
      40,    39,    40,    13,    39,    40,    39,    40,    39,    40,
      39,    40,    13,    19,    39,    40,    39,    40,  1203,    39,
      40,    13,    19,    13,    19,  1210,    13,    16,    13,    39,
      40,    13,    12,    39,    40,    34,    16,    17,    39,    40,
      39,    40,    39,    40,    39,    40,    12,    39,    40,    39,
      40,    13,    39,    40,    39,    40,    13,    39,    40,    13,
    1245,   224,   225,   226,   227,   228,   229,   230,   231,   232,
     233,   234,   235,   236,   237,   238,    13,    39,    40,    13,
      16,    13,    39,    40,    13,    39,    40,    13,    16,    13,
      20,    13,    15,    13,    13,    13,    39,    40,    31,    13,
      16,    13,    39,    40,  1289,    39,    40,    39,    40,    13,
      39,    40,    13,    39,    40,    39,    40,    39,    40,    39,
      40,    39,    40,   664,   665,    39,    40,    39,    40,   670,
     671,   668,   669,    13,    12,    39,    40,    12,    39,    40,
      13,   683,   684,    13,   685,   687,   688,   683,   684,    13,
      16,    13,   688,    13,   123,   856,    12,    15,    12,    39,
      40,    12,   703,   704,    19,    10,    39,    40,    15,    39,
      40,    12,    19,    12,    21,    39,    40,    39,    40,    39,
      40,    39,    40,    19,    39,    40,    19,    12,    19,    16,
      19,    15,    39,    40,   210,   211,    19,    12,    19,    12,
      19,    16,    17,    39,    40,    19,    39,    40,    39,    40,
      39,    40,    19,    12,    19,   908,    39,    40,    39,    40,
      39,    40,   660,    19,    12,    39,    40,   768,    19,    16,
      19,    21,    39,    40,    39,    40,    19,    12,    19,    12,
      19,    12,    19,    39,    40,    19,   218,    19,    39,    40,
      39,    40,    19,    12,    19,    12,    39,    40,    39,    40,
      39,    40,    39,    40,    19,    39,    40,    39,    40,    19,
      12,    19,    39,    40,    39,    40,    19,    12,    19,    12,
      19,    12,    12,    19,    39,    40,    12,    19,    12,    39,
      40,    39,    40,    21,    19,    12,    39,    40,    39,    40,
      12,   842,   843,    39,    40,    19,   999,    39,    40,    19,
      12,    19,    12,   841,    39,    40,    19,   660,    19,    12,
    1013,   849,    12,    12,    12,    39,    40,    21,    19,    39,
      40,    39,    40,    12,    12,    19,    39,    40,    39,    40,
      12,    12,    19,   659,   660,    39,    40,  1048,    39,    40,
     892,   893,    10,   895,   896,    39,    40,    19,   900,    19,
     902,   903,    39,    40,    19,   906,   907,  1060,    19,    10,
     911,    19,    12,  1074,    19,    12,    19,    39,    40,    39,
      40,    19,    10,    12,    39,    40,    19,    12,    39,    40,
      20,    39,    40,    19,    39,    40,    39,    40,    19,    12,
      12,    39,    40,   841,    12,    12,    39,    40,    19,    39,
      40,   849,    12,    39,    40,    19,    12,    12,    39,    40,
     961,    12,    12,   964,   965,    12,   967,   968,    39,    40,
     868,   972,   973,   974,   975,    39,    40,    19,   979,   980,
     981,   982,   983,   984,   985,   986,   987,    12,  1141,    19,
     216,    12,   218,  1146,    12,    32,    33,    39,    40,    36,
      12,    12,  1155,    12,    41,    42,  1007,    12,  1009,    39,
      40,   237,    12,  1014,  1015,  1016,  1017,  1018,  1019,  1020,
    1021,  1022,  1023,  1024,  1025,    20,  1027,  1028,  1029,    19,
    1031,  1032,  1193,  1186,   216,  1036,   218,   216,   841,   218,
      19,  1042,    19,  1044,    39,    40,   849,    19,    12,    39,
      40,    19,   216,    12,   218,   237,    19,   216,   237,   218,
      39,    40,    39,    40,   216,   868,   218,    39,    40,    19,
      12,    39,    40,   237,    21,    10,    39,    40,   237,    19,
      12,    19,   216,    13,   218,   237,    19,    12,    12,    39,
      40,    20,  1093,  1094,    12,   871,  1097,    20,    12,    39,
      40,    39,    40,   237,    20,    12,    39,    40,    12,    20,
      39,    40,  1265,  1266,    12,    20,    39,    40,    12,    12,
      20,  1122,    12,    39,    40,    12,    12,  1128,    39,    40,
      12,    20,  1120,  1121,    39,    40,  1137,    10,  1139,    39,
      40,  1142,    21,  1144,  1145,    20,  1147,  1148,    15,  1047,
      39,    40,    20,    12,    20,  1156,    20,  1158,    20,  1160,
      39,    40,  1163,  1164,    39,    40,    20,    12,  1169,    12,
    1171,    39,    40,    39,    40,    39,    40,    39,    40,    21,
    1181,  1182,  1183,    21,    12,    39,    40,  1340,    12,  1342,
      21,    30,    21,    12,  1195,    21,  1197,    39,    40,  1200,
      21,    39,    40,    13,    12,    12,  1207,    21,    39,    40,
      39,    40,    21,    39,    40,    12,    12,    21,    39,    40,
    1373,    21,  1120,  1121,    12,    39,    40,    21,    13,  1230,
      39,    40,    12,  1131,  1132,    39,    40,    21,    13,    39,
      40,    12,  1140,  1231,  1047,    39,    40,    21,  1249,    21,
      12,  1252,    12,  1254,    12,    39,    40,    12,  1259,    12,
    1261,    12,    12,    12,    12,    39,    40,    39,    40,    13,
      13,  1047,  1273,    13,  1275,  1276,  1277,   225,   226,   227,
     228,   229,   230,   231,   232,  1286,    13,   224,   225,   226,
     227,   228,   229,   230,   231,   232,  1297,    19,    13,    13,
      16,  1302,    13,    13,    27,  1203,   677,   678,   679,   680,
     681,    19,  1210,    19,   234,    12,    15,  1120,  1121,    31,
      27,  1322,  1310,  1324,    27,    10,    10,    10,  1131,  1132,
     208,    13,    28,  1231,    13,    15,    13,  1140,    19,    13,
    1341,    19,  1343,    13,    13,    13,    13,  1245,    13,  1350,
      13,    13,   233,    42,  1130,  1131,  1132,    13,    19,    13,
      13,    27,    13,    13,  1140,   225,   226,   227,   228,   229,
     230,   231,    13,    13,   414,   848,    95,   961,  1320,  1311,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1289,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1203,    -1,    -1,    -1,    -1,    -1,    -1,  1210,    -1,    -1,
      -1,    -1,  1310,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,  1199,    -1,    -1,  1202,  1203,  1231,    -1,
      -1,    -1,    -1,  1209,  1210,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,  1245,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1245,
    1246,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1256,    -1,    -1,    -1,    -1,    -1,  1289,  1263,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,  1310,    -1,    -1,
      -1,    -1,  1288,  1289,    -1,  1291,  1292,    -1,  1294,    -1,
      -1,    -1,    -1,  1299,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
    1326,    -1,    -1,  1329,    -1,  1331,  1332,    -1,    -1,  1335,
    1336,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,  1347,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1355,
      -1,    -1,  1358,    -1,    -1,    -1,    -1,    -1,    -1,  1365,
    1366,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     1,
    1376,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    30,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,    -1,   124,   125,   126,   127,   128,   129,   130,   131,
     132,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,    -1,    -1,   209,    -1,    -1,
      -1,    -1,    -1,   215,    -1,   217,    -1,   219,   220,   221,
     222,   223,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   235,   236,    -1,   238,   239,   240,     3,
      -1,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      -1,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,   233,
     234,   235,   236,   237,   238,   239,   240,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,    -1,   124,   125,
     126,   127,   128,   129,   130,   131,   132,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,   199,   200,   201,   202,   203,   204,   205,
     206,    -1,    -1,   209,    -1,    -1,    -1,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   234,   235,
     236,   237,   238,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    -1,   124,   125,   126,   127,   128,   129,
     130,   131,   132,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,    -1,    -1,   209,
      -1,    -1,    -1,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   234,   235,   236,   237,   238,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,    -1,
     124,   125,   126,   127,   128,   129,   130,   131,   132,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,    -1,    -1,   209,    -1,    -1,    -1,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     234,   235,   236,   237,   238,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,    -1,   124,   125,   126,   127,
     128,   129,   130,   131,   132,    -1,    -1,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,    -1,
      -1,   209,    -1,    -1,    -1,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   234,   235,   236,   237,
     238,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,    -1,   124,   125,   126,   127,   128,   129,   130,   131,
     132,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,    -1,    -1,   209,    -1,    -1,
      -1,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   234,   235,   236,    -1,   238,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,    -1,   124,   125,
     126,   127,   128,   129,   130,   131,   132,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,   199,   200,   201,   202,   203,   204,   205,
     206,    -1,    -1,   209,    -1,    -1,    -1,    -1,    -1,   215,
     216,   217,   218,   219,   220,   221,   222,   223,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
     236,   237,   238,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    -1,   124,   125,   126,   127,   128,   129,
     130,   131,   132,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,    -1,    -1,   209,
      -1,    -1,    -1,    -1,    -1,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   235,   236,   237,   238,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,    -1,
     124,   125,   126,   127,   128,   129,   130,   131,   132,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,   171,   172,   173,
     174,   175,   176,   177,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,    -1,    -1,   209,    -1,    -1,    -1,    -1,
      -1,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   235,   236,   237,   238,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,    -1,   124,   125,   126,   127,
     128,   129,   130,   131,   132,    -1,    -1,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,    -1,
      -1,   209,    -1,    -1,    -1,    -1,    -1,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,    -1,   226,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,   236,   237,
     238,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,    -1,   124,   125,   126,   127,   128,   129,   130,   131,
     132,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,    -1,    -1,   209,    -1,    -1,
      -1,    -1,    -1,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   235,   236,    -1,   238,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,    -1,   124,   125,
     126,   127,   128,   129,   130,   131,   132,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,   199,   200,   201,   202,   203,   204,   205,
     206,    -1,    -1,   209,    -1,    -1,    -1,    -1,    -1,   215,
      -1,   217,    -1,   219,   220,   221,   222,   223,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
     236,    -1,   238,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    31,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      40,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    -1,   124,   125,   126,   127,   128,   129,
      -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,    -1,   198,    -1,
     200,   201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,
     220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,    -1,   124,   125,   126,
     127,   128,   129,    -1,    -1,    -1,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
      -1,   198,    -1,   200,   201,   202,   203,   204,   205,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,    -1,
     124,   125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,    -1,   198,    -1,   200,   201,   202,   203,
     204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   235,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      31,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,
      41,    42,    -1,    -1,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,    -1,   124,   125,   126,   127,   128,   129,    -1,
      -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,    -1,   198,    -1,   200,
     201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,
     221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,    -1,   124,   125,   126,   127,
     128,   129,    -1,    -1,    -1,    -1,    -1,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,    -1,
     198,    -1,   200,   201,   202,   203,   204,   205,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,
      -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,    -1,   124,
     125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,    -1,   198,    -1,   200,   201,   202,   203,   204,
     205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,
      -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     235,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,    -1,   124,   125,   126,   127,   128,   129,    -1,    -1,
      -1,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,    -1,   198,    -1,   200,   201,
     202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,
      -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   235,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    -1,    -1,    -1,    -1,    28,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,    -1,   124,   125,   126,   127,   128,
     129,    -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,    -1,   198,
      -1,   200,   201,   202,   203,   204,   205,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,
      -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,
      -1,    27,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,    -1,   124,   125,
     126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,    -1,   198,    -1,   200,   201,   202,   203,   204,   205,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,
      -1,    -1,    -1,    -1,    27,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,
      -1,    -1,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,   122,
      -1,   124,   125,   126,   127,   128,   129,    -1,    -1,    -1,
      -1,    -1,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   185,   186,   187,   188,   189,   190,   191,   192,
     193,   194,   195,   196,    -1,   198,    -1,   200,   201,   202,
     203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,
      -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   235,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,
      20,    -1,    22,    -1,    -1,    -1,    -1,    27,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,
      -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,    -1,   124,   125,   126,   127,   128,   129,
      -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,    -1,   198,    -1,
     200,   201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,
     220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,    -1,   124,   125,   126,
     127,   128,   129,    -1,    -1,    -1,    -1,    -1,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
      -1,   198,    -1,   200,   201,   202,   203,   204,   205,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    -1,
      14,    -1,    16,    -1,    -1,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,
      -1,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,    -1,    -1,
     124,   125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,
      -1,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,   146,   147,   148,   149,   150,   151,   152,   153,
     154,   155,   156,   157,   158,   159,   160,   161,   162,   163,
     164,   165,   166,   167,   168,   169,   170,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,    -1,   198,    -1,   200,   201,   202,   203,
     204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,
     224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   235,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    -1,    14,    -1,    16,    -1,    -1,    -1,    -1,
      -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,
      41,    42,    -1,    -1,    -1,    46,    47,    48,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,   113,   114,   115,   116,   117,   118,   119,   120,
     121,    -1,    -1,   124,   125,   126,   127,   128,   129,    -1,
      -1,    -1,    -1,    -1,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   153,   154,   155,   156,   157,   158,   159,   160,
     161,   162,   163,   164,   165,   166,   167,   168,   169,   170,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   185,   186,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,    -1,   198,    -1,   200,
     201,   202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,
     221,    -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   235,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,    -1,    -1,   124,   125,   126,   127,
     128,   129,    -1,    -1,    -1,    -1,    -1,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,    -1,
     198,    -1,   200,   201,   202,   203,   204,   205,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,
      -1,    -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    38,    -1,    -1,    41,    42,    -1,    -1,
      -1,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,    -1,    -1,   124,
     125,   126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,    -1,   198,    -1,   200,   201,   202,   203,   204,
     205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,
      -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     235,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    38,    -1,    -1,    41,
      42,    -1,    -1,    -1,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
      -1,    -1,   124,   125,   126,   127,   128,   129,    -1,    -1,
      -1,    -1,    -1,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,    -1,   198,    -1,   200,   201,
     202,   203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,
      -1,    -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   235,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,
      -1,    -1,    41,    42,    -1,    -1,    -1,    46,    47,    48,
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    96,    97,    98,
      99,   100,   101,   102,   103,   104,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,    -1,    -1,   124,   125,   126,   127,   128,
     129,    -1,    -1,    -1,    -1,    -1,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,    -1,   198,
      -1,   200,   201,   202,   203,   204,   205,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   217,    -1,
      -1,   220,   221,    -1,    -1,   224,    -1,   226,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   235,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    38,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,    -1,    -1,   124,   125,
     126,   127,   128,   129,    -1,    -1,    -1,    -1,    -1,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
     146,   147,   148,   149,   150,   151,   152,   153,   154,   155,
     156,   157,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,    -1,   198,    -1,   200,   201,   202,   203,   204,   205,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   217,    -1,    -1,   220,   221,    -1,    -1,   224,    -1,
     226,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   235,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    46,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   100,   101,   102,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     113,   114,   115,   116,   117,   118,   119,   120,   121,    -1,
      -1,   124,   125,   126,   127,   128,   129,    -1,    -1,    -1,
      -1,    -1,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   146,   147,   148,   149,   150,   151,   152,
     153,   154,   155,   156,   157,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   185,   186,   187,   188,   189,   190,   191,   192,
     193,   194,   195,   196,    -1,   198,    -1,   200,   201,   202,
     203,   204,   205,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   217,    -1,    -1,   220,   221,    -1,
      -1,   224,    -1,   226,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   235
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,     1,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    14,    20,    22,    30,    38,    41,    42,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,   146,   147,   148,   149,
     150,   151,   152,   153,   154,   155,   156,   157,   158,   159,
     160,   161,   162,   163,   164,   165,   166,   167,   168,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   197,   198,   199,
     200,   201,   202,   203,   204,   205,   206,   209,   215,   217,
     219,   220,   221,   222,   223,   224,   226,   235,   236,   238,
     239,   240,   242,   243,   244,   246,   254,   255,   256,   257,
     258,   259,   260,   264,   265,   266,   267,   268,   269,   270,
     271,   272,   275,   276,   278,   279,   280,   282,    21,    12,
      16,    17,    10,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    98,    99,   100,   101,   102,   103,   217,
     264,   266,   272,    31,    40,   264,   267,   264,   271,   269,
     269,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    16,   281,    16,   281,    16,   281,    16,   281,
      16,   281,    16,   281,    12,    16,   281,    16,   281,    16,
     281,    16,   281,    12,    16,   281,    16,   281,    16,   281,
      16,   281,    16,   281,    16,   281,   264,   265,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,    12,
      12,    12,    12,    12,    12,    12,    12,    12,    12,   247,
     264,    10,   248,    29,   261,   263,   264,    12,    12,    12,
      12,    12,    12,   253,    10,    12,    21,     0,     3,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,   133,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,   146,
     147,   148,   149,   150,   151,   152,   153,   154,   155,   156,
     157,   158,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,   171,   172,   173,   174,   175,   176,
     177,   178,   179,   180,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   198,   199,   200,   201,   202,   203,   204,   205,   206,
     207,   208,   209,   210,   211,   212,   213,   214,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   244,   245,   287,   234,   245,   246,   249,
     250,   251,    21,    20,    16,    17,    20,    19,    39,    40,
      16,    17,    18,    23,    27,    37,   210,    32,    33,    36,
      41,    42,   271,    43,    44,    14,    29,    35,    45,    13,
     260,   133,   134,   264,   264,    16,    13,    29,    31,   264,
     277,    15,    15,    19,    21,    24,    25,    26,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,    30,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   246,   264,   264,   264,   264,   264,   264,   123,   264,
     264,   264,   264,   264,    13,    13,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   260,   260,   264,   264,   260,   264,   264,   264,   264,
     264,   260,   260,   264,   264,   264,   260,   264,    10,    48,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   264,   264,   264,   260,   264,   264,   264,   260,   264,
     264,   207,   210,   211,    10,   218,    19,    21,   262,   214,
     264,    10,   264,    10,   264,    10,    13,   252,   253,    13,
     264,    31,    16,    16,    15,    21,    10,   252,    21,   245,
     245,   249,    21,   264,   264,   260,   267,   267,   264,   264,
     268,    16,   268,    16,   268,   268,   268,   270,   270,   270,
     270,   270,    38,   269,   271,    38,   269,   271,   264,    10,
      38,   271,    38,   269,   271,    13,    12,    12,    12,    10,
      15,    19,    28,    31,   264,   264,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,   264,
     273,   274,    13,    13,    19,    19,    13,    19,    19,    13,
      13,    13,    19,    19,    19,    19,    13,    13,    13,    19,
      19,    19,    19,    19,    19,    19,    19,    19,    13,    13,
      13,    13,    13,    13,    13,    13,    13,    13,    13,    19,
      13,    13,    13,    13,    13,    13,    13,    19,    13,    19,
      19,    19,    19,    19,    19,    19,    19,    19,    19,    19,
      19,    19,    19,    19,    19,    19,    13,    19,    19,    19,
      13,    19,    19,   246,   264,   264,    16,   261,   246,    13,
      13,    13,    19,    13,    19,    16,    19,   244,    13,    13,
     249,   245,   250,   268,   268,   271,   271,   271,   271,    15,
      12,   271,   271,   271,   264,   264,    13,   260,    28,   264,
      31,    15,    15,    15,    34,   273,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     264,   264,   264,    27,    27,   260,   264,    27,   264,    10,
      10,    10,   260,   264,   264,   264,   264,   264,   264,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   264,
     208,   214,   212,   264,   264,   264,    28,   252,   237,   245,
     249,   250,   244,   260,    13,    13,    13,    19,    15,    12,
     244,    19,    19,    13,    19,    19,    19,    19,    19,    13,
      13,    13,    13,    13,    13,    19,    19,    13,    27,   264,
      27,   264,    13,    19,    27,   264,    13,    13,    13,    19,
      13,    19,    13,    13,    13,    13,    13,    13,    13,    13,
      13,    19,    19,    19,    13,    13,    19,    13,   246,   246,
     264,    19,    13,    13,   264,   237,   245,   237,   245,   249,
     237,   245,   249,   250,    13,   264,   264,   237,   245,   249,
     250,   260,   264,   264,   264,   260,   264,   264,   260,   264,
     264,   264,   264,   264,   264,   264,   264,   264,   264,   260,
     213,   214,    12,   224,   225,   226,   227,   228,   229,   230,
     231,   232,   283,   284,   286,   244,    21,   264,   264,   237,
     245,   264,   237,   245,   237,   245,   249,    13,   264,   237,
     245,   237,   245,   249,    13,    19,    19,    13,    13,    13,
      13,    13,    13,    19,    13,    19,    19,    19,    13,    13,
     264,   246,   283,   285,   233,    42,   237,   245,   249,   250,
     245,    21,    21,   264,    21,   264,   264,   237,   245,    21,
     264,   264,   237,   245,   260,   260,   264,   264,   264,   264,
     214,    19,    13,   225,   226,   227,   228,   229,   230,   231,
      27,   264,   237,   245,   237,   245,   249,   245,   245,    21,
     245,    21,    21,   264,   245,    21,    21,   264,    13,    13,
      19,    19,    19,    19,   246,   285,   284,    21,   264,   264,
     237,   245,   245,   245,   245,    21,   245,   245,    21,   260,
     264,   260,   264,    13,   245,    21,    21,   264,   245,   245,
      13,    13,    13,    19,   245,   245,    21,   260,   245,    13
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   241,   242,   242,   242,   242,   242,   242,   243,   244,
     244,   245,   245,   246,   246,   246,   246,   246,   246,   246,
     246,   247,   247,   248,   248,   248,   249,   249,   250,   250,
     251,   252,   252,   253,   253,   253,   253,   253,   253,   253,
     253,   253,   253,   253,   253,   253,   253,   253,   253,   253,
     253,   253,   253,   253,   253,   253,   253,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   254,   254,   254,   254,   254,   254,   254,   254,   254,
     254,   255,   255,   255,   255,   256,   256,   256,   256,   256,
     256,   256,   256,   257,   258,   258,   258,   258,   258,   258,
     258,   258,   258,   258,   258,   258,   258,   258,   258,   258,
     259,   259,   259,   259,   259,   259,   259,   259,   259,   259,
     259,   259,   259,   259,   259,   259,   260,   260,   261,   261,
     262,   262,   263,   264,   264,   265,   265,   265,   265,   266,
     267,   267,   267,   267,   267,   267,   267,   267,   268,   268,
     268,   268,   268,   268,   269,   269,   269,   269,   270,   270,
     270,   270,   270,   270,   270,   270,   270,   271,   271,   271,
     271,   271,   271,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   272,   272,   272,   272,
     272,   272,   272,   272,   272,   272,   273,   273,   274,   274,
     274,   274,   274,   274,   274,   274,   274,   275,   275,   275,
     275,   275,   275,   275,   276,   276,   276,   276,   277,   277,
     277,   278,   278,   278,   279,   279,   279,   279,   279,   279,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   280,   280,
     280,   280,   280,   280,   280,   280,   280,   280,   281,   281,
     282,   282,   282,   282,   282,   282,   282,   282,   282,   282,
     282,   282,   282,   282,   282,   282,   283,   283,   283,   283,
     283,   283,   283,   283,   283,   283,   283,   283,   283,   283,
     284,   284,   285,   285,   286,   286,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287,   287,   287,   287,   287,   287,   287,   287,
     287,   287,   287
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     2,     1,     3,     2,     2,     1,     1,
       1,     1,     1,     1,     3,     4,     3,     2,     2,     4,
       2,     3,     5,     7,     9,     5,     2,     3,     2,     3,
       2,     1,     3,     5,     6,     5,     4,     8,     9,     8,
       7,     6,     7,     6,     5,     9,    10,     9,     8,     8,
       9,     8,     7,    11,    12,    11,    10,     1,     1,     1,
       4,     3,     1,     4,     6,     7,     6,     4,     4,     4,
       4,     4,     4,     4,     4,    12,     4,     6,     7,     6,
       4,     4,     6,     7,    12,     6,     6,    11,     1,     1,
       3,     1,     2,     1,     2,     3,     3,     6,     6,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     1,     3,     1,     3,
       1,     1,     4,     1,     4,     1,     3,     3,     2,     4,
       1,     3,     3,     3,     3,     4,     4,     3,     1,     3,
       3,     3,     3,     3,     1,     1,     2,     2,     1,     2,
       2,     3,     3,     4,     4,     4,     4,     1,     3,     4,
       4,     3,     4,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     4,
       4,     3,     1,     1,     1,     1,     3,     3,     1,     1,
       3,     6,     6,     5,     2,     4,     1,     2,     9,     6,
       8,     5,     8,     5,     7,     4,     5,     1,     1,     1,
       1,     1,     1,     1,     4,     3,     5,     6,     1,     3,
       5,     5,     5,     3,     3,     3,     3,     4,     4,     4,
       4,     4,     4,     3,     3,     6,     8,    10,     8,     4,
       4,    10,     4,     4,     4,     4,     8,     8,     8,     8,
       4,     4,     4,     6,     6,     6,     6,     6,     6,     8,
       8,     6,     4,     4,     6,    12,     6,     6,     6,     6,
       6,     6,     6,     6,     6,    14,     8,     6,     6,     4,
       8,     6,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     6,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     4,     4,
       4,     4,     4,     4,     4,     4,     4,     4,     2,     0,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     1,     1,     1,
       1,     1,     1,     3,     3,     3,     3,     3,     3,     3,
       1,     1,     1,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     2,     2,
       1,     1,     1,     1,     1,     1,     1,     2,     1,     1,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (scanner, YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value, scanner); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void* scanner)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  YYUSE (scanner);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, void* scanner)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep, scanner);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule, void* scanner)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              , scanner);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule, scanner); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, void* scanner)
{
  YYUSE (yyvaluep);
  YYUSE (scanner);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void* scanner)
{
/* The lookahead symbol.  */
int yychar;


/* The semantic value of the lookahead symbol.  */
/* Default value used for initialization, for pacifying older GCCs
   or non-GCC compilers.  */
YY_INITIAL_VALUE (static YYSTYPE yyval_default;)
YYSTYPE yylval YY_INITIAL_VALUE (= yyval_default);

    /* Number of syntax errors so far.  */
    int yynerrs;

    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex (&yylval, scanner);
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 469 "parser.y" /* yacc.c:1646  */
    {
			    parsedThing = (yyvsp[-1].tree);
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
#line 4341 "parser.c" /* yacc.c:1646  */
    break;

  case 3:
#line 475 "parser.y" /* yacc.c:1646  */
    {
			    outputMode();
                            sollyaPrintf("This is %s.\nType 'help help;' for the list of available commands. Type 'help <command>;' for help on the specific command <command>.\nType 'quit;' for quitting the %s interpreter.\n\nYou can get moral support and help with bugs by writing to %s.\n\n",PACKAGE_NAME,PACKAGE_NAME,PACKAGE_BUGREPORT);
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
#line 4353 "parser.c" /* yacc.c:1646  */
    break;

  case 4:
#line 483 "parser.y" /* yacc.c:1646  */
    {
			    outputMode();
                            sollyaPrintf("This is %s.\nType 'help help;' for the list of available commands. Type 'help <command>;' for help on the specific command <command>.\nType 'quit;' for quitting the %s interpreter.\n\nYou can get moral support and help with bugs by writing to %s.\n\n",PACKAGE_NAME,PACKAGE_NAME,PACKAGE_BUGREPORT);
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
#line 4365 "parser.c" /* yacc.c:1646  */
    break;

  case 5:
#line 491 "parser.y" /* yacc.c:1646  */
    {
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
#line 4375 "parser.c" /* yacc.c:1646  */
    break;

  case 6:
#line 497 "parser.y" /* yacc.c:1646  */
    {
			    outputMode();
			    sollyaPrintf("This is\n\n\t%s.\n\n"	VERSION_COPYRIGHT_TEXT "\nSend bug reports to <%s>\n\nThis build of %s is based on GMP %s, MPFR %s and MPFI %s.\n",PACKAGE_STRING,PACKAGE_BUGREPORT,PACKAGE_STRING,gmp_version,mpfr_get_version(),sollya_mpfi_get_version());
#if defined(HAVE_FPLLL_VERSION_STRING)
			    sollyaPrintf("It uses FPLLL as: \"%s\"\n",HAVE_FPLLL_VERSION_STRING);
#endif
			    sollyaPrintf("\n");
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
#line 4391 "parser.c" /* yacc.c:1646  */
    break;

  case 7:
#line 509 "parser.y" /* yacc.c:1646  */
    {
			    parsedThing = NULL;
			    (yyval.other) = NULL;
			    YYACCEPT;
			  }
#line 4401 "parser.c" /* yacc.c:1646  */
    break;

  case 8:
#line 517 "parser.y" /* yacc.c:1646  */
    {
			    helpNotFinished = 1;
			    (yyval.other) = NULL;
			  }
#line 4410 "parser.c" /* yacc.c:1646  */
    break;

  case 9:
#line 524 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4418 "parser.c" /* yacc.c:1646  */
    break;

  case 10:
#line 528 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4426 "parser.c" /* yacc.c:1646  */
    break;

  case 11:
#line 534 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4434 "parser.c" /* yacc.c:1646  */
    break;

  case 12:
#line 538 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 4442 "parser.c" /* yacc.c:1646  */
    break;

  case 13:
#line 544 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4450 "parser.c" /* yacc.c:1646  */
    break;

  case 14:
#line 548 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCommandList((yyvsp[-1].list));
                          }
#line 4458 "parser.c" /* yacc.c:1646  */
    break;

  case 15:
#line 552 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list)));
                          }
#line 4466 "parser.c" /* yacc.c:1646  */
    break;

  case 16:
#line 556 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCommandList((yyvsp[-1].list));
                          }
#line 4474 "parser.c" /* yacc.c:1646  */
    break;

  case 17:
#line 560 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNop();
                          }
#line 4482 "parser.c" /* yacc.c:1646  */
    break;

  case 18:
#line 564 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4490 "parser.c" /* yacc.c:1646  */
    break;

  case 19:
#line 568 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeWhile((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 4498 "parser.c" /* yacc.c:1646  */
    break;

  case 20:
#line 572 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 4506 "parser.c" /* yacc.c:1646  */
    break;

  case 21:
#line 578 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIf((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 4514 "parser.c" /* yacc.c:1646  */
    break;

  case 22:
#line 582 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIfElse((yyvsp[-4].tree),(yyvsp[-2].tree),(yyvsp[0].tree));
                          }
#line 4522 "parser.c" /* yacc.c:1646  */
    break;

  case 23:
#line 590 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFor((yyvsp[-6].value), (yyvsp[-4].tree), (yyvsp[-2].tree), makeConstantDouble(1.0), (yyvsp[0].tree));
			    safeFree((yyvsp[-6].value));
                          }
#line 4531 "parser.c" /* yacc.c:1646  */
    break;

  case 24:
#line 595 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFor((yyvsp[-8].value), (yyvsp[-6].tree), (yyvsp[-4].tree), (yyvsp[-2].tree), (yyvsp[0].tree));
			    safeFree((yyvsp[-8].value));
                          }
#line 4540 "parser.c" /* yacc.c:1646  */
    break;

  case 25:
#line 600 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeForIn((yyvsp[-4].value), (yyvsp[-2].tree), (yyvsp[0].tree));
			    safeFree((yyvsp[-4].value));
                          }
#line 4549 "parser.c" /* yacc.c:1646  */
    break;

  case 26:
#line 608 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[-1].tree));
			  }
#line 4557 "parser.c" /* yacc.c:1646  */
    break;

  case 27:
#line 612 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].tree));
			  }
#line 4565 "parser.c" /* yacc.c:1646  */
    break;

  case 28:
#line 618 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[-1].tree));
			  }
#line 4573 "parser.c" /* yacc.c:1646  */
    break;

  case 29:
#line 622 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].tree));
			  }
#line 4581 "parser.c" /* yacc.c:1646  */
    break;

  case 30:
#line 628 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVariableDeclaration((yyvsp[0].list));
			  }
#line 4589 "parser.c" /* yacc.c:1646  */
    break;

  case 31:
#line 635 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].value));
			  }
#line 4597 "parser.c" /* yacc.c:1646  */
    break;

  case 32:
#line 639 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].value));
			  }
#line 4605 "parser.c" /* yacc.c:1646  */
    break;

  case 33:
#line 645 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4613 "parser.c" /* yacc.c:1646  */
    break;

  case 34:
#line 649 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))), makeUnit());
                          }
#line 4621 "parser.c" /* yacc.c:1646  */
    break;

  case 35:
#line 653 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4629 "parser.c" /* yacc.c:1646  */
    break;

  case 36:
#line 657 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
#line 4637 "parser.c" /* yacc.c:1646  */
    break;

  case 37:
#line 661 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4645 "parser.c" /* yacc.c:1646  */
    break;

  case 38:
#line 665 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))), (yyvsp[-2].tree));
                          }
#line 4653 "parser.c" /* yacc.c:1646  */
    break;

  case 39:
#line 669 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4661 "parser.c" /* yacc.c:1646  */
    break;

  case 40:
#line 673 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc(NULL, makeCommandList(addElement(NULL,makeNop())), (yyvsp[-2].tree));
                          }
#line 4669 "parser.c" /* yacc.c:1646  */
    break;

  case 41:
#line 677 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-4].list), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4677 "parser.c" /* yacc.c:1646  */
    break;

  case 42:
#line 681 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-5].list), makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))), makeUnit());
                          }
#line 4685 "parser.c" /* yacc.c:1646  */
    break;

  case 43:
#line 685 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-4].list), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4693 "parser.c" /* yacc.c:1646  */
    break;

  case 44:
#line 689 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-3].list), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
#line 4701 "parser.c" /* yacc.c:1646  */
    break;

  case 45:
#line 693 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-7].list), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4709 "parser.c" /* yacc.c:1646  */
    break;

  case 46:
#line 697 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-8].list), makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))), (yyvsp[-2].tree));
                          }
#line 4717 "parser.c" /* yacc.c:1646  */
    break;

  case 47:
#line 701 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-7].list), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4725 "parser.c" /* yacc.c:1646  */
    break;

  case 48:
#line 705 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProc((yyvsp[-6].list), makeCommandList(addElement(NULL, makeNop())), (yyvsp[-2].tree));
                          }
#line 4733 "parser.c" /* yacc.c:1646  */
    break;

  case 49:
#line 709 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-6].value), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4741 "parser.c" /* yacc.c:1646  */
    break;

  case 50:
#line 713 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-7].value), makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))), makeUnit());
                          }
#line 4749 "parser.c" /* yacc.c:1646  */
    break;

  case 51:
#line 717 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-6].value), makeCommandList((yyvsp[-1].list)), makeUnit());
                          }
#line 4757 "parser.c" /* yacc.c:1646  */
    break;

  case 52:
#line 721 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-5].value), makeCommandList(addElement(NULL,makeNop())), makeUnit());
                          }
#line 4765 "parser.c" /* yacc.c:1646  */
    break;

  case 53:
#line 725 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-9].value), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4773 "parser.c" /* yacc.c:1646  */
    break;

  case 54:
#line 729 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-10].value), makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))), (yyvsp[-2].tree));
                          }
#line 4781 "parser.c" /* yacc.c:1646  */
    break;

  case 55:
#line 733 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-9].value), makeCommandList((yyvsp[-4].list)), (yyvsp[-2].tree));
                          }
#line 4789 "parser.c" /* yacc.c:1646  */
    break;

  case 56:
#line 737 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcIllim((yyvsp[-8].value), makeCommandList(addElement(NULL, makeNop())), (yyvsp[-2].tree));
                          }
#line 4797 "parser.c" /* yacc.c:1646  */
    break;

  case 57:
#line 744 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeQuit();
			  }
#line 4805 "parser.c" /* yacc.c:1646  */
    break;

  case 58:
#line 748 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFalseQuit();
			  }
#line 4813 "parser.c" /* yacc.c:1646  */
    break;

  case 59:
#line 752 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNop();
			  }
#line 4821 "parser.c" /* yacc.c:1646  */
    break;

  case 60:
#line 756 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNopArg((yyvsp[-1].tree));
			  }
#line 4829 "parser.c" /* yacc.c:1646  */
    break;

  case 61:
#line 760 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNopArg(makeDefault());
			  }
#line 4837 "parser.c" /* yacc.c:1646  */
    break;

  case 62:
#line 764 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRestart();
			  }
#line 4845 "parser.c" /* yacc.c:1646  */
    break;

  case 63:
#line 768 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrint((yyvsp[-1].list));
			  }
#line 4853 "parser.c" /* yacc.c:1646  */
    break;

  case 64:
#line 772 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNewFilePrint((yyvsp[0].tree), (yyvsp[-3].list));
			  }
#line 4861 "parser.c" /* yacc.c:1646  */
    break;

  case 65:
#line 776 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAppendFilePrint((yyvsp[0].tree), (yyvsp[-4].list));
			  }
#line 4869 "parser.c" /* yacc.c:1646  */
    break;

  case 66:
#line 780 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePlot(addElement((yyvsp[-1].list), (yyvsp[-3].tree)));
			  }
#line 4877 "parser.c" /* yacc.c:1646  */
    break;

  case 67:
#line 784 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintHexa((yyvsp[-1].tree));
			  }
#line 4885 "parser.c" /* yacc.c:1646  */
    break;

  case 68:
#line 788 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintFloat((yyvsp[-1].tree));
			  }
#line 4893 "parser.c" /* yacc.c:1646  */
    break;

  case 69:
#line 792 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintBinary((yyvsp[-1].tree));
			  }
#line 4901 "parser.c" /* yacc.c:1646  */
    break;

  case 70:
#line 796 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressMessage((yyvsp[-1].list));
			  }
#line 4909 "parser.c" /* yacc.c:1646  */
    break;

  case 71:
#line 800 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeUnsuppressMessage((yyvsp[-1].list));
			  }
#line 4917 "parser.c" /* yacc.c:1646  */
    break;

  case 72:
#line 804 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintExpansion((yyvsp[-1].tree));
			  }
#line 4925 "parser.c" /* yacc.c:1646  */
    break;

  case 73:
#line 808 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeImplementConst((yyvsp[-1].list));
			  }
#line 4933 "parser.c" /* yacc.c:1646  */
    break;

  case 74:
#line 812 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBashExecute((yyvsp[-1].tree));
			  }
#line 4941 "parser.c" /* yacc.c:1646  */
    break;

  case 75:
#line 816 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExternalPlot(addElement(addElement(addElement(addElement((yyvsp[-1].list),(yyvsp[-3].tree)),(yyvsp[-5].tree)),(yyvsp[-7].tree)),(yyvsp[-9].tree)));
			  }
#line 4949 "parser.c" /* yacc.c:1646  */
    break;

  case 76:
#line 820 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeWrite((yyvsp[-1].list));
			  }
#line 4957 "parser.c" /* yacc.c:1646  */
    break;

  case 77:
#line 824 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNewFileWrite((yyvsp[0].tree), (yyvsp[-3].list));
			  }
#line 4965 "parser.c" /* yacc.c:1646  */
    break;

  case 78:
#line 828 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAppendFileWrite((yyvsp[0].tree), (yyvsp[-4].list));
			  }
#line 4973 "parser.c" /* yacc.c:1646  */
    break;

  case 79:
#line 832 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAsciiPlot((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 4981 "parser.c" /* yacc.c:1646  */
    break;

  case 80:
#line 836 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintXml((yyvsp[-1].tree));
			  }
#line 4989 "parser.c" /* yacc.c:1646  */
    break;

  case 81:
#line 840 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExecute((yyvsp[-1].tree));
			  }
#line 4997 "parser.c" /* yacc.c:1646  */
    break;

  case 82:
#line 844 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintXmlNewFile((yyvsp[-3].tree),(yyvsp[0].tree));
			  }
#line 5005 "parser.c" /* yacc.c:1646  */
    break;

  case 83:
#line 848 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrintXmlAppendFile((yyvsp[-4].tree),(yyvsp[0].tree));
			  }
#line 5013 "parser.c" /* yacc.c:1646  */
    break;

  case 84:
#line 852 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeWorstCase(addElement(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)), (yyvsp[-9].tree)));
			  }
#line 5021 "parser.c" /* yacc.c:1646  */
    break;

  case 85:
#line 856 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRename((yyvsp[-3].value), (yyvsp[-1].value));
			    safeFree((yyvsp[-3].value));
			    safeFree((yyvsp[-1].value));
			  }
#line 5031 "parser.c" /* yacc.c:1646  */
    break;

  case 86:
#line 862 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRename("_x_", (yyvsp[-1].value));
			    safeFree((yyvsp[-1].value));
			  }
#line 5040 "parser.c" /* yacc.c:1646  */
    break;

  case 87:
#line 867 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExternalProc((yyvsp[-8].value), (yyvsp[-6].tree), addElement((yyvsp[-4].list), (yyvsp[-1].integerval)));
			    safeFree((yyvsp[-8].value));
			  }
#line 5049 "parser.c" /* yacc.c:1646  */
    break;

  case 88:
#line 872 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5057 "parser.c" /* yacc.c:1646  */
    break;

  case 89:
#line 876 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoprint((yyvsp[0].list));
			  }
#line 5065 "parser.c" /* yacc.c:1646  */
    break;

  case 90:
#line 880 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAssignment((yyvsp[-1].value), (yyvsp[0].tree));
			    safeFree((yyvsp[-1].value));
			  }
#line 5074 "parser.c" /* yacc.c:1646  */
    break;

  case 91:
#line 887 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5082 "parser.c" /* yacc.c:1646  */
    break;

  case 92:
#line 891 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[-1].tree);
			  }
#line 5090 "parser.c" /* yacc.c:1646  */
    break;

  case 93:
#line 895 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5098 "parser.c" /* yacc.c:1646  */
    break;

  case 94:
#line 899 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[-1].tree);
			  }
#line 5106 "parser.c" /* yacc.c:1646  */
    break;

  case 95:
#line 905 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAssignment((yyvsp[-2].value), (yyvsp[0].tree));
			    safeFree((yyvsp[-2].value));
			  }
#line 5115 "parser.c" /* yacc.c:1646  */
    break;

  case 96:
#line 910 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloatAssignment((yyvsp[-2].value), (yyvsp[0].tree));
			    safeFree((yyvsp[-2].value));
			  }
#line 5124 "parser.c" /* yacc.c:1646  */
    break;

  case 97:
#line 915 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLibraryBinding((yyvsp[-5].value), (yyvsp[-1].tree));
			    safeFree((yyvsp[-5].value));
			  }
#line 5133 "parser.c" /* yacc.c:1646  */
    break;

  case 98:
#line 920 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLibraryConstantBinding((yyvsp[-5].value), (yyvsp[-1].tree));
			    safeFree((yyvsp[-5].value));
			  }
#line 5142 "parser.c" /* yacc.c:1646  */
    break;

  case 99:
#line 925 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAssignmentInIndexing((yyvsp[-2].dblnode)->a,(yyvsp[-2].dblnode)->b,(yyvsp[0].tree));
			    safeFree((yyvsp[-2].dblnode));
			  }
#line 5151 "parser.c" /* yacc.c:1646  */
    break;

  case 100:
#line 930 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloatAssignmentInIndexing((yyvsp[-2].dblnode)->a,(yyvsp[-2].dblnode)->b,(yyvsp[0].tree));
			    safeFree((yyvsp[-2].dblnode));
			  }
#line 5160 "parser.c" /* yacc.c:1646  */
    break;

  case 101:
#line 935 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProtoAssignmentInStructure((yyvsp[-2].tree),(yyvsp[0].tree));
			  }
#line 5168 "parser.c" /* yacc.c:1646  */
    break;

  case 102:
#line 939 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProtoFloatAssignmentInStructure((yyvsp[-2].tree),(yyvsp[0].tree));
			  }
#line 5176 "parser.c" /* yacc.c:1646  */
    break;

  case 103:
#line 945 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeStructAccess((yyvsp[-2].tree),(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 5185 "parser.c" /* yacc.c:1646  */
    break;

  case 104:
#line 952 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecAssign((yyvsp[0].tree));
			  }
#line 5193 "parser.c" /* yacc.c:1646  */
    break;

  case 105:
#line 956 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePointsAssign((yyvsp[0].tree));
			  }
#line 5201 "parser.c" /* yacc.c:1646  */
    break;

  case 106:
#line 960 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiamAssign((yyvsp[0].tree));
			  }
#line 5209 "parser.c" /* yacc.c:1646  */
    break;

  case 107:
#line 964 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDisplayAssign((yyvsp[0].tree));
			  }
#line 5217 "parser.c" /* yacc.c:1646  */
    break;

  case 108:
#line 968 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVerbosityAssign((yyvsp[0].tree));
			  }
#line 5225 "parser.c" /* yacc.c:1646  */
    break;

  case 109:
#line 972 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeShowMessageNumbersAssign((yyvsp[0].tree));
			  }
#line 5233 "parser.c" /* yacc.c:1646  */
    break;

  case 110:
#line 976 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalAssign((yyvsp[0].tree));
			  }
#line 5241 "parser.c" /* yacc.c:1646  */
    break;

  case 111:
#line 980 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoSimplifyAssign((yyvsp[0].tree));
			  }
#line 5249 "parser.c" /* yacc.c:1646  */
    break;

  case 112:
#line 984 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylorRecursAssign((yyvsp[0].tree));
			  }
#line 5257 "parser.c" /* yacc.c:1646  */
    break;

  case 113:
#line 988 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTimingAssign((yyvsp[0].tree));
			  }
#line 5265 "parser.c" /* yacc.c:1646  */
    break;

  case 114:
#line 992 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFullParenAssign((yyvsp[0].tree));
			  }
#line 5273 "parser.c" /* yacc.c:1646  */
    break;

  case 115:
#line 996 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointAssign((yyvsp[0].tree));
			  }
#line 5281 "parser.c" /* yacc.c:1646  */
    break;

  case 116:
#line 1000 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDieOnErrorAssign((yyvsp[0].tree));
			  }
#line 5289 "parser.c" /* yacc.c:1646  */
    break;

  case 117:
#line 1004 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalModeAssign((yyvsp[0].tree));
			  }
#line 5297 "parser.c" /* yacc.c:1646  */
    break;

  case 118:
#line 1008 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressWarningsAssign((yyvsp[0].tree));
			  }
#line 5305 "parser.c" /* yacc.c:1646  */
    break;

  case 119:
#line 1012 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHopitalRecursAssign((yyvsp[0].tree));
			  }
#line 5313 "parser.c" /* yacc.c:1646  */
    break;

  case 120:
#line 1018 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecStillAssign((yyvsp[0].tree));
			  }
#line 5321 "parser.c" /* yacc.c:1646  */
    break;

  case 121:
#line 1022 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePointsStillAssign((yyvsp[0].tree));
			  }
#line 5329 "parser.c" /* yacc.c:1646  */
    break;

  case 122:
#line 1026 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiamStillAssign((yyvsp[0].tree));
			  }
#line 5337 "parser.c" /* yacc.c:1646  */
    break;

  case 123:
#line 1030 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDisplayStillAssign((yyvsp[0].tree));
			  }
#line 5345 "parser.c" /* yacc.c:1646  */
    break;

  case 124:
#line 1034 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVerbosityStillAssign((yyvsp[0].tree));
			  }
#line 5353 "parser.c" /* yacc.c:1646  */
    break;

  case 125:
#line 1038 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeShowMessageNumbersStillAssign((yyvsp[0].tree));
			  }
#line 5361 "parser.c" /* yacc.c:1646  */
    break;

  case 126:
#line 1042 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalStillAssign((yyvsp[0].tree));
			  }
#line 5369 "parser.c" /* yacc.c:1646  */
    break;

  case 127:
#line 1046 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoSimplifyStillAssign((yyvsp[0].tree));
			  }
#line 5377 "parser.c" /* yacc.c:1646  */
    break;

  case 128:
#line 1050 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylorRecursStillAssign((yyvsp[0].tree));
			  }
#line 5385 "parser.c" /* yacc.c:1646  */
    break;

  case 129:
#line 1054 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTimingStillAssign((yyvsp[0].tree));
			  }
#line 5393 "parser.c" /* yacc.c:1646  */
    break;

  case 130:
#line 1058 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFullParenStillAssign((yyvsp[0].tree));
			  }
#line 5401 "parser.c" /* yacc.c:1646  */
    break;

  case 131:
#line 1062 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointStillAssign((yyvsp[0].tree));
			  }
#line 5409 "parser.c" /* yacc.c:1646  */
    break;

  case 132:
#line 1066 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDieOnErrorStillAssign((yyvsp[0].tree));
			  }
#line 5417 "parser.c" /* yacc.c:1646  */
    break;

  case 133:
#line 1070 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalModeStillAssign((yyvsp[0].tree));
			  }
#line 5425 "parser.c" /* yacc.c:1646  */
    break;

  case 134:
#line 1074 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressWarningsStillAssign((yyvsp[0].tree));
			  }
#line 5433 "parser.c" /* yacc.c:1646  */
    break;

  case 135:
#line 1078 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHopitalRecursStillAssign((yyvsp[0].tree));
			  }
#line 5441 "parser.c" /* yacc.c:1646  */
    break;

  case 136:
#line 1084 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].tree));
			  }
#line 5449 "parser.c" /* yacc.c:1646  */
    break;

  case 137:
#line 1088 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].tree));
			  }
#line 5457 "parser.c" /* yacc.c:1646  */
    break;

  case 138:
#line 1094 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].association));
			  }
#line 5465 "parser.c" /* yacc.c:1646  */
    break;

  case 139:
#line 1098 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].association));
			  }
#line 5473 "parser.c" /* yacc.c:1646  */
    break;

  case 140:
#line 1104 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 5481 "parser.c" /* yacc.c:1646  */
    break;

  case 141:
#line 1108 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 5489 "parser.c" /* yacc.c:1646  */
    break;

  case 142:
#line 1114 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.association) = (entry *) safeMalloc(sizeof(entry));
			    (yyval.association)->name = (char *) safeCalloc(strlen((yyvsp[-2].value)) + 1, sizeof(char));
			    strcpy((yyval.association)->name,(yyvsp[-2].value));
			    safeFree((yyvsp[-2].value));
			    (yyval.association)->value = (void *) ((yyvsp[0].tree));
			  }
#line 5501 "parser.c" /* yacc.c:1646  */
    break;

  case 143:
#line 1124 "parser.y" /* yacc.c:1646  */
    {
			   (yyval.tree) = (yyvsp[0].tree);
			 }
#line 5509 "parser.c" /* yacc.c:1646  */
    break;

  case 144:
#line 1128 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatch((yyvsp[-2].tree),(yyvsp[0].list));
			  }
#line 5517 "parser.c" /* yacc.c:1646  */
    break;

  case 145:
#line 1134 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5525 "parser.c" /* yacc.c:1646  */
    break;

  case 146:
#line 1138 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAnd((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5533 "parser.c" /* yacc.c:1646  */
    break;

  case 147:
#line 1142 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeOr((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5541 "parser.c" /* yacc.c:1646  */
    break;

  case 148:
#line 1146 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNegation((yyvsp[0].tree));
			  }
#line 5549 "parser.c" /* yacc.c:1646  */
    break;

  case 149:
#line 1152 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.dblnode) = (doubleNode *) safeMalloc(sizeof(doubleNode));
			    (yyval.dblnode)->a = (yyvsp[-3].tree);
			    (yyval.dblnode)->b = (yyvsp[-1].tree);
			  }
#line 5559 "parser.c" /* yacc.c:1646  */
    break;

  case 150:
#line 1161 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5567 "parser.c" /* yacc.c:1646  */
    break;

  case 151:
#line 1165 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareEqual((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5575 "parser.c" /* yacc.c:1646  */
    break;

  case 152:
#line 1169 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareIn((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5583 "parser.c" /* yacc.c:1646  */
    break;

  case 153:
#line 1173 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareLess((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5591 "parser.c" /* yacc.c:1646  */
    break;

  case 154:
#line 1177 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareGreater((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5599 "parser.c" /* yacc.c:1646  */
    break;

  case 155:
#line 1181 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareLessEqual((yyvsp[-3].tree), (yyvsp[0].tree));
			  }
#line 5607 "parser.c" /* yacc.c:1646  */
    break;

  case 156:
#line 1185 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareGreaterEqual((yyvsp[-3].tree), (yyvsp[0].tree));
			  }
#line 5615 "parser.c" /* yacc.c:1646  */
    break;

  case 157:
#line 1189 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCompareNotEqual((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5623 "parser.c" /* yacc.c:1646  */
    break;

  case 158:
#line 1195 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 5631 "parser.c" /* yacc.c:1646  */
    break;

  case 159:
#line 1199 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAdd((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5639 "parser.c" /* yacc.c:1646  */
    break;

  case 160:
#line 1203 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSub((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5647 "parser.c" /* yacc.c:1646  */
    break;

  case 161:
#line 1207 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeConcat((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5655 "parser.c" /* yacc.c:1646  */
    break;

  case 162:
#line 1211 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAddToList((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5663 "parser.c" /* yacc.c:1646  */
    break;

  case 163:
#line 1215 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAppend((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5671 "parser.c" /* yacc.c:1646  */
    break;

  case 164:
#line 1221 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.count) = 0;
                          }
#line 5679 "parser.c" /* yacc.c:1646  */
    break;

  case 165:
#line 1225 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.count) = 1;
                          }
#line 5687 "parser.c" /* yacc.c:1646  */
    break;

  case 166:
#line 1229 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.count) = (yyvsp[0].count);
                          }
#line 5695 "parser.c" /* yacc.c:1646  */
    break;

  case 167:
#line 1233 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.count) = (yyvsp[0].count)+1;
                          }
#line 5703 "parser.c" /* yacc.c:1646  */
    break;

  case 168:
#line 1240 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
                          }
#line 5711 "parser.c" /* yacc.c:1646  */
    break;

  case 169:
#line 1244 "parser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = tempNode;
                          }
#line 5722 "parser.c" /* yacc.c:1646  */
    break;

  case 170:
#line 1251 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEvalConst((yyvsp[0].tree));
                          }
#line 5730 "parser.c" /* yacc.c:1646  */
    break;

  case 171:
#line 1255 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMul((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 5738 "parser.c" /* yacc.c:1646  */
    break;

  case 172:
#line 1259 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiv((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 5746 "parser.c" /* yacc.c:1646  */
    break;

  case 173:
#line 1263 "parser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeMul((yyvsp[-3].tree), tempNode);
                          }
#line 5757 "parser.c" /* yacc.c:1646  */
    break;

  case 174:
#line 1270 "parser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makeDiv((yyvsp[-3].tree), tempNode);
                          }
#line 5768 "parser.c" /* yacc.c:1646  */
    break;

  case 175:
#line 1277 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMul((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
                          }
#line 5776 "parser.c" /* yacc.c:1646  */
    break;

  case 176:
#line 1281 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiv((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
                          }
#line 5784 "parser.c" /* yacc.c:1646  */
    break;

  case 177:
#line 1287 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
                          }
#line 5792 "parser.c" /* yacc.c:1646  */
    break;

  case 178:
#line 1291 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePow((yyvsp[-2].tree), (yyvsp[0].tree));
                          }
#line 5800 "parser.c" /* yacc.c:1646  */
    break;

  case 179:
#line 1295 "parser.y" /* yacc.c:1646  */
    {
			    tempNode = (yyvsp[0].tree);
			    for (tempInteger=0;tempInteger<(yyvsp[-1].count);tempInteger++)
			      tempNode = makeNeg(tempNode);
			    (yyval.tree) = makePow((yyvsp[-3].tree), tempNode);
                          }
#line 5811 "parser.c" /* yacc.c:1646  */
    break;

  case 180:
#line 1302 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePow((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
                          }
#line 5819 "parser.c" /* yacc.c:1646  */
    break;

  case 181:
#line 1306 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrepend((yyvsp[-2].tree), (yyvsp[0].tree));
			  }
#line 5827 "parser.c" /* yacc.c:1646  */
    break;

  case 182:
#line 1310 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrepend((yyvsp[-3].tree), makeEvalConst((yyvsp[0].tree)));
			  }
#line 5835 "parser.c" /* yacc.c:1646  */
    break;

  case 183:
#line 1317 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeOn();
			  }
#line 5843 "parser.c" /* yacc.c:1646  */
    break;

  case 184:
#line 1321 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeOff();
			  }
#line 5851 "parser.c" /* yacc.c:1646  */
    break;

  case 185:
#line 1325 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDyadic();
			  }
#line 5859 "parser.c" /* yacc.c:1646  */
    break;

  case 186:
#line 1329 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePowers();
			  }
#line 5867 "parser.c" /* yacc.c:1646  */
    break;

  case 187:
#line 1333 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBinaryThing();
			  }
#line 5875 "parser.c" /* yacc.c:1646  */
    break;

  case 188:
#line 1337 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHexadecimalThing();
			  }
#line 5883 "parser.c" /* yacc.c:1646  */
    break;

  case 189:
#line 1341 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFile();
			  }
#line 5891 "parser.c" /* yacc.c:1646  */
    break;

  case 190:
#line 1345 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePostscript();
			  }
#line 5899 "parser.c" /* yacc.c:1646  */
    break;

  case 191:
#line 1349 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePostscriptFile();
			  }
#line 5907 "parser.c" /* yacc.c:1646  */
    break;

  case 192:
#line 1353 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePerturb();
			  }
#line 5915 "parser.c" /* yacc.c:1646  */
    break;

  case 193:
#line 1357 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundDown();
			  }
#line 5923 "parser.c" /* yacc.c:1646  */
    break;

  case 194:
#line 1361 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundUp();
			  }
#line 5931 "parser.c" /* yacc.c:1646  */
    break;

  case 195:
#line 1365 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundToZero();
			  }
#line 5939 "parser.c" /* yacc.c:1646  */
    break;

  case 196:
#line 1369 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundToNearest();
			  }
#line 5947 "parser.c" /* yacc.c:1646  */
    break;

  case 197:
#line 1373 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHonorCoeff();
			  }
#line 5955 "parser.c" /* yacc.c:1646  */
    break;

  case 198:
#line 1377 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTrue();
			  }
#line 5963 "parser.c" /* yacc.c:1646  */
    break;

  case 199:
#line 1381 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeUnit();
			  }
#line 5971 "parser.c" /* yacc.c:1646  */
    break;

  case 200:
#line 1385 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFalse();
			  }
#line 5979 "parser.c" /* yacc.c:1646  */
    break;

  case 201:
#line 1389 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDefault();
			  }
#line 5987 "parser.c" /* yacc.c:1646  */
    break;

  case 202:
#line 1393 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDecimal();
			  }
#line 5995 "parser.c" /* yacc.c:1646  */
    break;

  case 203:
#line 1397 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAbsolute();
			  }
#line 6003 "parser.c" /* yacc.c:1646  */
    break;

  case 204:
#line 1401 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRelative();
			  }
#line 6011 "parser.c" /* yacc.c:1646  */
    break;

  case 205:
#line 1405 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFixed();
			  }
#line 6019 "parser.c" /* yacc.c:1646  */
    break;

  case 206:
#line 1409 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloating();
			  }
#line 6027 "parser.c" /* yacc.c:1646  */
    break;

  case 207:
#line 1413 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeError();
			  }
#line 6035 "parser.c" /* yacc.c:1646  */
    break;

  case 208:
#line 1417 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleSymbol();
			  }
#line 6043 "parser.c" /* yacc.c:1646  */
    break;

  case 209:
#line 1421 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSingleSymbol();
			  }
#line 6051 "parser.c" /* yacc.c:1646  */
    break;

  case 210:
#line 1425 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeQuadSymbol();
			  }
#line 6059 "parser.c" /* yacc.c:1646  */
    break;

  case 211:
#line 1429 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHalfPrecisionSymbol();
			  }
#line 6067 "parser.c" /* yacc.c:1646  */
    break;

  case 212:
#line 1433 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleextendedSymbol();
			  }
#line 6075 "parser.c" /* yacc.c:1646  */
    break;

  case 213:
#line 1437 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVariable();
			  }
#line 6083 "parser.c" /* yacc.c:1646  */
    break;

  case 214:
#line 1441 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleDoubleSymbol();
			  }
#line 6091 "parser.c" /* yacc.c:1646  */
    break;

  case 215:
#line 1445 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTripleDoubleSymbol();
			  }
#line 6099 "parser.c" /* yacc.c:1646  */
    break;

  case 216:
#line 1449 "parser.y" /* yacc.c:1646  */
    {
			    tempString = safeCalloc(strlen((yyvsp[0].value)) + 1, sizeof(char));
			    strcpy(tempString, (yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			    tempString2 = safeCalloc(strlen(tempString) + 1, sizeof(char));
			    strcpy(tempString2, tempString);
			    safeFree(tempString);
			    (yyval.tree) = makeString(tempString2);
			    safeFree(tempString2);
			  }
#line 6114 "parser.c" /* yacc.c:1646  */
    break;

  case 217:
#line 1460 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 6122 "parser.c" /* yacc.c:1646  */
    break;

  case 218:
#line 1464 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTableAccess((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6131 "parser.c" /* yacc.c:1646  */
    break;

  case 219:
#line 1469 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIsBound((yyvsp[-1].value));
			    safeFree((yyvsp[-1].value));
			  }
#line 6140 "parser.c" /* yacc.c:1646  */
    break;

  case 220:
#line 1474 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[-3].value), (yyvsp[-1].list));
			    safeFree((yyvsp[-3].value));
			  }
#line 6149 "parser.c" /* yacc.c:1646  */
    break;

  case 221:
#line 1479 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTableAccessWithSubstitute((yyvsp[-2].value), NULL);
			    safeFree((yyvsp[-2].value));
			  }
#line 6158 "parser.c" /* yacc.c:1646  */
    break;

  case 222:
#line 1484 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 6166 "parser.c" /* yacc.c:1646  */
    break;

  case 223:
#line 1488 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 6174 "parser.c" /* yacc.c:1646  */
    break;

  case 224:
#line 1492 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 6182 "parser.c" /* yacc.c:1646  */
    break;

  case 225:
#line 1496 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 6190 "parser.c" /* yacc.c:1646  */
    break;

  case 226:
#line 1500 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[-1].tree);
			  }
#line 6198 "parser.c" /* yacc.c:1646  */
    break;

  case 227:
#line 1504 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeStructure((yyvsp[-1].list));
			  }
#line 6206 "parser.c" /* yacc.c:1646  */
    break;

  case 228:
#line 1508 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 6214 "parser.c" /* yacc.c:1646  */
    break;

  case 229:
#line 1512 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIndex((yyvsp[0].dblnode)->a, (yyvsp[0].dblnode)->b);
			    safeFree((yyvsp[0].dblnode));
			  }
#line 6223 "parser.c" /* yacc.c:1646  */
    break;

  case 230:
#line 1517 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeStructAccess((yyvsp[-2].tree),(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6232 "parser.c" /* yacc.c:1646  */
    break;

  case 231:
#line 1522 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply(makeStructAccess((yyvsp[-5].tree),(yyvsp[-3].value)),(yyvsp[-1].list));
			    safeFree((yyvsp[-3].value));
			  }
#line 6241 "parser.c" /* yacc.c:1646  */
    break;

  case 232:
#line 1527 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply((yyvsp[-4].tree),(yyvsp[-1].list));
			  }
#line 6249 "parser.c" /* yacc.c:1646  */
    break;

  case 233:
#line 1531 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply((yyvsp[-3].tree),addElement(NULL,makeUnit()));
			  }
#line 6257 "parser.c" /* yacc.c:1646  */
    break;

  case 234:
#line 1535 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = (yyvsp[0].tree);
			  }
#line 6265 "parser.c" /* yacc.c:1646  */
    break;

  case 235:
#line 1539 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTime((yyvsp[-1].tree));
                          }
#line 6273 "parser.c" /* yacc.c:1646  */
    break;

  case 236:
#line 1545 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL,(yyvsp[0].tree));
			  }
#line 6281 "parser.c" /* yacc.c:1646  */
    break;

  case 237:
#line 1549 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list),(yyvsp[-1].tree));
			  }
#line 6289 "parser.c" /* yacc.c:1646  */
    break;

  case 238:
#line 1555 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-8].tree),makeCommandList(concatChains((yyvsp[-5].list), (yyvsp[-4].list))),(yyvsp[-2].tree));
			  }
#line 6297 "parser.c" /* yacc.c:1646  */
    break;

  case 239:
#line 1559 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-5].tree),makeCommandList(concatChains((yyvsp[-2].list), (yyvsp[-1].list))),makeUnit());
			  }
#line 6305 "parser.c" /* yacc.c:1646  */
    break;

  case 240:
#line 1563 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-7].tree),makeCommandList((yyvsp[-4].list)),(yyvsp[-2].tree));
			  }
#line 6313 "parser.c" /* yacc.c:1646  */
    break;

  case 241:
#line 1567 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-4].tree),makeCommandList((yyvsp[-1].list)),makeUnit());
			  }
#line 6321 "parser.c" /* yacc.c:1646  */
    break;

  case 242:
#line 1571 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-7].tree),makeCommandList((yyvsp[-4].list)),(yyvsp[-2].tree));
			  }
#line 6329 "parser.c" /* yacc.c:1646  */
    break;

  case 243:
#line 1575 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-4].tree),makeCommandList((yyvsp[-1].list)),makeUnit());
			  }
#line 6337 "parser.c" /* yacc.c:1646  */
    break;

  case 244:
#line 1579 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-6].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[-2].tree));
			  }
#line 6345 "parser.c" /* yacc.c:1646  */
    break;

  case 245:
#line 1583 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-3].tree), makeCommandList(addElement(NULL,makeNop())), makeUnit());
			  }
#line 6353 "parser.c" /* yacc.c:1646  */
    break;

  case 246:
#line 1587 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMatchElement((yyvsp[-4].tree), makeCommandList(addElement(NULL,makeNop())), (yyvsp[-1].tree));
			  }
#line 6361 "parser.c" /* yacc.c:1646  */
    break;

  case 247:
#line 1593 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDecimalConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6370 "parser.c" /* yacc.c:1646  */
    break;

  case 248:
#line 1598 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6379 "parser.c" /* yacc.c:1646  */
    break;

  case 249:
#line 1603 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDyadicConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6388 "parser.c" /* yacc.c:1646  */
    break;

  case 250:
#line 1608 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHexConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6397 "parser.c" /* yacc.c:1646  */
    break;

  case 251:
#line 1613 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHexadecimalConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6406 "parser.c" /* yacc.c:1646  */
    break;

  case 252:
#line 1618 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBinaryConstant((yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 6415 "parser.c" /* yacc.c:1646  */
    break;

  case 253:
#line 1623 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePi();
			  }
#line 6423 "parser.c" /* yacc.c:1646  */
    break;

  case 254:
#line 1631 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEmptyList();
			  }
#line 6431 "parser.c" /* yacc.c:1646  */
    break;

  case 255:
#line 1635 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEmptyList();
			  }
#line 6439 "parser.c" /* yacc.c:1646  */
    break;

  case 256:
#line 1639 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRevertedList((yyvsp[-2].list));
			  }
#line 6447 "parser.c" /* yacc.c:1646  */
    break;

  case 257:
#line 1643 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRevertedFinalEllipticList((yyvsp[-3].list));
			  }
#line 6455 "parser.c" /* yacc.c:1646  */
    break;

  case 258:
#line 1649 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].tree));
			  }
#line 6463 "parser.c" /* yacc.c:1646  */
    break;

  case 259:
#line 1653 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[-2].list), (yyvsp[0].tree));
			  }
#line 6471 "parser.c" /* yacc.c:1646  */
    break;

  case 260:
#line 1657 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(addElement((yyvsp[-4].list), makeElliptic()), (yyvsp[0].tree));
			  }
#line 6479 "parser.c" /* yacc.c:1646  */
    break;

  case 261:
#line 1664 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRange((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6487 "parser.c" /* yacc.c:1646  */
    break;

  case 262:
#line 1668 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRange((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6495 "parser.c" /* yacc.c:1646  */
    break;

  case 263:
#line 1672 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRange((yyvsp[-1].tree), copyThing((yyvsp[-1].tree)));
			  }
#line 6503 "parser.c" /* yacc.c:1646  */
    break;

  case 264:
#line 1678 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[-1].tree));
			  }
#line 6511 "parser.c" /* yacc.c:1646  */
    break;

  case 265:
#line 1682 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[-1].tree));
			  }
#line 6519 "parser.c" /* yacc.c:1646  */
    break;

  case 266:
#line 1686 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[-1].tree));
			  }
#line 6527 "parser.c" /* yacc.c:1646  */
    break;

  case 267:
#line 1690 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMax((yyvsp[-1].tree));
			  }
#line 6535 "parser.c" /* yacc.c:1646  */
    break;

  case 268:
#line 1694 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMid((yyvsp[-1].tree));
			  }
#line 6543 "parser.c" /* yacc.c:1646  */
    break;

  case 269:
#line 1698 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDeboundMin((yyvsp[-1].tree));
			  }
#line 6551 "parser.c" /* yacc.c:1646  */
    break;

  case 270:
#line 1704 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiff((yyvsp[-1].tree));
			  }
#line 6559 "parser.c" /* yacc.c:1646  */
    break;

  case 271:
#line 1708 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtysimplify((yyvsp[-1].tree));
			  }
#line 6567 "parser.c" /* yacc.c:1646  */
    break;

  case 272:
#line 1712 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBashevaluate(addElement(NULL,(yyvsp[-1].tree)));
			  }
#line 6575 "parser.c" /* yacc.c:1646  */
    break;

  case 273:
#line 1716 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGetSuppressedMessages();
			  }
#line 6583 "parser.c" /* yacc.c:1646  */
    break;

  case 274:
#line 1720 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGetBacktrace();
			  }
#line 6591 "parser.c" /* yacc.c:1646  */
    break;

  case 275:
#line 1724 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBashevaluate(addElement(addElement(NULL,(yyvsp[-1].tree)),(yyvsp[-3].tree)));
			  }
#line 6599 "parser.c" /* yacc.c:1646  */
    break;

  case 276:
#line 1728 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRemez(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6607 "parser.c" /* yacc.c:1646  */
    break;

  case 277:
#line 1732 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAnnotateFunction(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)));
			  }
#line 6615 "parser.c" /* yacc.c:1646  */
    break;

  case 278:
#line 1736 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeBind((yyvsp[-5].tree), (yyvsp[-3].value), (yyvsp[-1].tree));
			    safeFree((yyvsp[-3].value));
			  }
#line 6624 "parser.c" /* yacc.c:1646  */
    break;

  case 279:
#line 1741 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMin((yyvsp[-1].list));
			  }
#line 6632 "parser.c" /* yacc.c:1646  */
    break;

  case 280:
#line 1745 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMax((yyvsp[-1].list));
			  }
#line 6640 "parser.c" /* yacc.c:1646  */
    break;

  case 281:
#line 1749 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFPminimax(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)));
			  }
#line 6648 "parser.c" /* yacc.c:1646  */
    break;

  case 282:
#line 1753 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHorner((yyvsp[-1].tree));
			  }
#line 6656 "parser.c" /* yacc.c:1646  */
    break;

  case 283:
#line 1757 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalThing((yyvsp[-1].tree));
			  }
#line 6664 "parser.c" /* yacc.c:1646  */
    break;

  case 284:
#line 1761 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExpand((yyvsp[-1].tree));
			  }
#line 6672 "parser.c" /* yacc.c:1646  */
    break;

  case 285:
#line 1765 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSimplifySafe((yyvsp[-1].tree));
			  }
#line 6680 "parser.c" /* yacc.c:1646  */
    break;

  case 286:
#line 1769 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylor((yyvsp[-5].tree), (yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6688 "parser.c" /* yacc.c:1646  */
    break;

  case 287:
#line 1773 "parser.y" /* yacc.c:1646  */
    {
                            (yyval.tree) = makeTaylorform(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6696 "parser.c" /* yacc.c:1646  */
    break;

  case 288:
#line 1777 "parser.y" /* yacc.c:1646  */
    {
                            (yyval.tree) = makeChebyshevform(addElement(addElement(addElement(NULL, (yyvsp[-1].tree)), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6704 "parser.c" /* yacc.c:1646  */
    break;

  case 289:
#line 1781 "parser.y" /* yacc.c:1646  */
    {
                            (yyval.tree) = makeAutodiff(addElement(addElement(addElement(NULL, (yyvsp[-1].tree)), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6712 "parser.c" /* yacc.c:1646  */
    break;

  case 290:
#line 1785 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDegree((yyvsp[-1].tree));
			  }
#line 6720 "parser.c" /* yacc.c:1646  */
    break;

  case 291:
#line 1789 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNumerator((yyvsp[-1].tree));
			  }
#line 6728 "parser.c" /* yacc.c:1646  */
    break;

  case 292:
#line 1793 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDenominator((yyvsp[-1].tree));
			  }
#line 6736 "parser.c" /* yacc.c:1646  */
    break;

  case 293:
#line 1797 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSubstitute((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6744 "parser.c" /* yacc.c:1646  */
    break;

  case 294:
#line 1801 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeComposePolynomials((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6752 "parser.c" /* yacc.c:1646  */
    break;

  case 295:
#line 1805 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCoeff((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6760 "parser.c" /* yacc.c:1646  */
    break;

  case 296:
#line 1809 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSubpoly((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6768 "parser.c" /* yacc.c:1646  */
    break;

  case 297:
#line 1813 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundcoefficients((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6776 "parser.c" /* yacc.c:1646  */
    break;

  case 298:
#line 1817 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalapprox((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6784 "parser.c" /* yacc.c:1646  */
    break;

  case 299:
#line 1821 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAccurateInfnorm(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6792 "parser.c" /* yacc.c:1646  */
    break;

  case 300:
#line 1825 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundToFormat((yyvsp[-5].tree), (yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6800 "parser.c" /* yacc.c:1646  */
    break;

  case 301:
#line 1829 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEvaluate((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6808 "parser.c" /* yacc.c:1646  */
    break;

  case 302:
#line 1833 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeParse((yyvsp[-1].tree));
			  }
#line 6816 "parser.c" /* yacc.c:1646  */
    break;

  case 303:
#line 1837 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeReadXml((yyvsp[-1].tree));
			  }
#line 6824 "parser.c" /* yacc.c:1646  */
    break;

  case 304:
#line 1841 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeInfnorm(addElement((yyvsp[-1].list), (yyvsp[-3].tree)));
			  }
#line 6832 "parser.c" /* yacc.c:1646  */
    break;

  case 305:
#line 1845 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSupnorm(addElement(addElement(addElement(addElement(addElement(NULL,(yyvsp[-1].tree)),(yyvsp[-3].tree)),(yyvsp[-5].tree)),(yyvsp[-7].tree)),(yyvsp[-9].tree)));
			  }
#line 6840 "parser.c" /* yacc.c:1646  */
    break;

  case 306:
#line 1849 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFindZeros((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6848 "parser.c" /* yacc.c:1646  */
    break;

  case 307:
#line 1853 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFPFindZeros((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6856 "parser.c" /* yacc.c:1646  */
    break;

  case 308:
#line 1857 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtyInfnorm((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6864 "parser.c" /* yacc.c:1646  */
    break;

  case 309:
#line 1861 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGcd((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6872 "parser.c" /* yacc.c:1646  */
    break;

  case 310:
#line 1865 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEuclDiv((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6880 "parser.c" /* yacc.c:1646  */
    break;

  case 311:
#line 1869 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeEuclMod((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6888 "parser.c" /* yacc.c:1646  */
    break;

  case 312:
#line 1873 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNumberRoots((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6896 "parser.c" /* yacc.c:1646  */
    break;

  case 313:
#line 1877 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIntegral((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6904 "parser.c" /* yacc.c:1646  */
    break;

  case 314:
#line 1881 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtyIntegral((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6912 "parser.c" /* yacc.c:1646  */
    break;

  case 315:
#line 1885 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeImplementPoly(addElement(addElement(addElement(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)), (yyvsp[-7].tree)), (yyvsp[-9].tree)), (yyvsp[-11].tree)));
			  }
#line 6920 "parser.c" /* yacc.c:1646  */
    break;

  case 316:
#line 1889 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCheckInfnorm((yyvsp[-5].tree), (yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6928 "parser.c" /* yacc.c:1646  */
    break;

  case 317:
#line 1893 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeZeroDenominators((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6936 "parser.c" /* yacc.c:1646  */
    break;

  case 318:
#line 1897 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeIsEvaluable((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6944 "parser.c" /* yacc.c:1646  */
    break;

  case 319:
#line 1901 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSearchGal((yyvsp[-1].list));
			  }
#line 6952 "parser.c" /* yacc.c:1646  */
    break;

  case 320:
#line 1905 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeGuessDegree(addElement(addElement((yyvsp[-1].list), (yyvsp[-3].tree)), (yyvsp[-5].tree)));
			  }
#line 6960 "parser.c" /* yacc.c:1646  */
    break;

  case 321:
#line 1909 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDirtyFindZeros((yyvsp[-3].tree), (yyvsp[-1].tree));
			  }
#line 6968 "parser.c" /* yacc.c:1646  */
    break;

  case 322:
#line 1913 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHead((yyvsp[-1].tree));
			  }
#line 6976 "parser.c" /* yacc.c:1646  */
    break;

  case 323:
#line 1917 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRoundCorrectly((yyvsp[-1].tree));
			  }
#line 6984 "parser.c" /* yacc.c:1646  */
    break;

  case 324:
#line 1921 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeReadFile((yyvsp[-1].tree));
			  }
#line 6992 "parser.c" /* yacc.c:1646  */
    break;

  case 325:
#line 1925 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRevert((yyvsp[-1].tree));
			  }
#line 7000 "parser.c" /* yacc.c:1646  */
    break;

  case 326:
#line 1929 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSort((yyvsp[-1].tree));
			  }
#line 7008 "parser.c" /* yacc.c:1646  */
    break;

  case 327:
#line 1933 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMantissa((yyvsp[-1].tree));
			  }
#line 7016 "parser.c" /* yacc.c:1646  */
    break;

  case 328:
#line 1937 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExponent((yyvsp[-1].tree));
			  }
#line 7024 "parser.c" /* yacc.c:1646  */
    break;

  case 329:
#line 1941 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecision((yyvsp[-1].tree));
			  }
#line 7032 "parser.c" /* yacc.c:1646  */
    break;

  case 330:
#line 1945 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTail((yyvsp[-1].tree));
			  }
#line 7040 "parser.c" /* yacc.c:1646  */
    break;

  case 331:
#line 1949 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSqrt((yyvsp[-1].tree));
			  }
#line 7048 "parser.c" /* yacc.c:1646  */
    break;

  case 332:
#line 1953 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExp((yyvsp[-1].tree));
			  }
#line 7056 "parser.c" /* yacc.c:1646  */
    break;

  case 333:
#line 1957 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeApply(makeVariable(),addElement(NULL,(yyvsp[-1].tree)));
			  }
#line 7064 "parser.c" /* yacc.c:1646  */
    break;

  case 334:
#line 1961 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeProcedureFunction((yyvsp[-1].tree));
			  }
#line 7072 "parser.c" /* yacc.c:1646  */
    break;

  case 335:
#line 1965 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSubstitute(makeProcedureFunction((yyvsp[-3].tree)),(yyvsp[-1].tree));
			  }
#line 7080 "parser.c" /* yacc.c:1646  */
    break;

  case 336:
#line 1969 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog((yyvsp[-1].tree));
			  }
#line 7088 "parser.c" /* yacc.c:1646  */
    break;

  case 337:
#line 1973 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog2((yyvsp[-1].tree));
			  }
#line 7096 "parser.c" /* yacc.c:1646  */
    break;

  case 338:
#line 1977 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog10((yyvsp[-1].tree));
			  }
#line 7104 "parser.c" /* yacc.c:1646  */
    break;

  case 339:
#line 1981 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSin((yyvsp[-1].tree));
			  }
#line 7112 "parser.c" /* yacc.c:1646  */
    break;

  case 340:
#line 1985 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCos((yyvsp[-1].tree));
			  }
#line 7120 "parser.c" /* yacc.c:1646  */
    break;

  case 341:
#line 1989 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTan((yyvsp[-1].tree));
			  }
#line 7128 "parser.c" /* yacc.c:1646  */
    break;

  case 342:
#line 1993 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAsin((yyvsp[-1].tree));
			  }
#line 7136 "parser.c" /* yacc.c:1646  */
    break;

  case 343:
#line 1997 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAcos((yyvsp[-1].tree));
			  }
#line 7144 "parser.c" /* yacc.c:1646  */
    break;

  case 344:
#line 2001 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAtan((yyvsp[-1].tree));
			  }
#line 7152 "parser.c" /* yacc.c:1646  */
    break;

  case 345:
#line 2005 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSinh((yyvsp[-1].tree));
			  }
#line 7160 "parser.c" /* yacc.c:1646  */
    break;

  case 346:
#line 2009 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCosh((yyvsp[-1].tree));
			  }
#line 7168 "parser.c" /* yacc.c:1646  */
    break;

  case 347:
#line 2013 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTanh((yyvsp[-1].tree));
			  }
#line 7176 "parser.c" /* yacc.c:1646  */
    break;

  case 348:
#line 2017 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAsinh((yyvsp[-1].tree));
			  }
#line 7184 "parser.c" /* yacc.c:1646  */
    break;

  case 349:
#line 2021 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAcosh((yyvsp[-1].tree));
			  }
#line 7192 "parser.c" /* yacc.c:1646  */
    break;

  case 350:
#line 2025 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAtanh((yyvsp[-1].tree));
			  }
#line 7200 "parser.c" /* yacc.c:1646  */
    break;

  case 351:
#line 2029 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAbs((yyvsp[-1].tree));
			  }
#line 7208 "parser.c" /* yacc.c:1646  */
    break;

  case 352:
#line 2033 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeErf((yyvsp[-1].tree));
			  }
#line 7216 "parser.c" /* yacc.c:1646  */
    break;

  case 353:
#line 2037 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeErfc((yyvsp[-1].tree));
			  }
#line 7224 "parser.c" /* yacc.c:1646  */
    break;

  case 354:
#line 2041 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLog1p((yyvsp[-1].tree));
			  }
#line 7232 "parser.c" /* yacc.c:1646  */
    break;

  case 355:
#line 2045 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeExpm1((yyvsp[-1].tree));
			  }
#line 7240 "parser.c" /* yacc.c:1646  */
    break;

  case 356:
#line 2049 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDouble((yyvsp[-1].tree));
			  }
#line 7248 "parser.c" /* yacc.c:1646  */
    break;

  case 357:
#line 2053 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSingle((yyvsp[-1].tree));
			  }
#line 7256 "parser.c" /* yacc.c:1646  */
    break;

  case 358:
#line 2057 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeQuad((yyvsp[-1].tree));
			  }
#line 7264 "parser.c" /* yacc.c:1646  */
    break;

  case 359:
#line 2061 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHalfPrecision((yyvsp[-1].tree));
			  }
#line 7272 "parser.c" /* yacc.c:1646  */
    break;

  case 360:
#line 2065 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubledouble((yyvsp[-1].tree));
			  }
#line 7280 "parser.c" /* yacc.c:1646  */
    break;

  case 361:
#line 2069 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTripledouble((yyvsp[-1].tree));
			  }
#line 7288 "parser.c" /* yacc.c:1646  */
    break;

  case 362:
#line 2073 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDoubleextended((yyvsp[-1].tree));
			  }
#line 7296 "parser.c" /* yacc.c:1646  */
    break;

  case 363:
#line 2077 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCeil((yyvsp[-1].tree));
			  }
#line 7304 "parser.c" /* yacc.c:1646  */
    break;

  case 364:
#line 2081 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFloor((yyvsp[-1].tree));
			  }
#line 7312 "parser.c" /* yacc.c:1646  */
    break;

  case 365:
#line 2085 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeNearestInt((yyvsp[-1].tree));
			  }
#line 7320 "parser.c" /* yacc.c:1646  */
    break;

  case 366:
#line 2089 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeLength((yyvsp[-1].tree));
			  }
#line 7328 "parser.c" /* yacc.c:1646  */
    break;

  case 367:
#line 2093 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeObjectName((yyvsp[-1].tree));
			  }
#line 7336 "parser.c" /* yacc.c:1646  */
    break;

  case 368:
#line 2099 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 7344 "parser.c" /* yacc.c:1646  */
    break;

  case 369:
#line 2103 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.other) = NULL;
			  }
#line 7352 "parser.c" /* yacc.c:1646  */
    break;

  case 370:
#line 2109 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePrecDeref();
			  }
#line 7360 "parser.c" /* yacc.c:1646  */
    break;

  case 371:
#line 2113 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makePointsDeref();
			  }
#line 7368 "parser.c" /* yacc.c:1646  */
    break;

  case 372:
#line 2117 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDiamDeref();
			  }
#line 7376 "parser.c" /* yacc.c:1646  */
    break;

  case 373:
#line 2121 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDisplayDeref();
			  }
#line 7384 "parser.c" /* yacc.c:1646  */
    break;

  case 374:
#line 2125 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeVerbosityDeref();
			  }
#line 7392 "parser.c" /* yacc.c:1646  */
    break;

  case 375:
#line 2129 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeShowMessageNumbersDeref();
			  }
#line 7400 "parser.c" /* yacc.c:1646  */
    break;

  case 376:
#line 2133 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeCanonicalDeref();
			  }
#line 7408 "parser.c" /* yacc.c:1646  */
    break;

  case 377:
#line 2137 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeAutoSimplifyDeref();
			  }
#line 7416 "parser.c" /* yacc.c:1646  */
    break;

  case 378:
#line 2141 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTaylorRecursDeref();
			  }
#line 7424 "parser.c" /* yacc.c:1646  */
    break;

  case 379:
#line 2145 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeTimingDeref();
			  }
#line 7432 "parser.c" /* yacc.c:1646  */
    break;

  case 380:
#line 2149 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeFullParenDeref();
			  }
#line 7440 "parser.c" /* yacc.c:1646  */
    break;

  case 381:
#line 2153 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeMidpointDeref();
			  }
#line 7448 "parser.c" /* yacc.c:1646  */
    break;

  case 382:
#line 2157 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeDieOnErrorDeref();
			  }
#line 7456 "parser.c" /* yacc.c:1646  */
    break;

  case 383:
#line 2161 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeRationalModeDeref();
			  }
#line 7464 "parser.c" /* yacc.c:1646  */
    break;

  case 384:
#line 2165 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeSuppressWarningsDeref();
			  }
#line 7472 "parser.c" /* yacc.c:1646  */
    break;

  case 385:
#line 2169 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.tree) = makeHopitalRecursDeref();
			  }
#line 7480 "parser.c" /* yacc.c:1646  */
    break;

  case 386:
#line 2175 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7490 "parser.c" /* yacc.c:1646  */
    break;

  case 387:
#line 2181 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7500 "parser.c" /* yacc.c:1646  */
    break;

  case 388:
#line 2187 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = OBJECT_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7510 "parser.c" /* yacc.c:1646  */
    break;

  case 389:
#line 2193 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7520 "parser.c" /* yacc.c:1646  */
    break;

  case 390:
#line 2199 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7530 "parser.c" /* yacc.c:1646  */
    break;

  case 391:
#line 2205 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7540 "parser.c" /* yacc.c:1646  */
    break;

  case 392:
#line 2211 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7550 "parser.c" /* yacc.c:1646  */
    break;

  case 393:
#line 2217 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = CONSTANT_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7560 "parser.c" /* yacc.c:1646  */
    break;

  case 394:
#line 2223 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = FUNCTION_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7570 "parser.c" /* yacc.c:1646  */
    break;

  case 395:
#line 2229 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = OBJECT_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7580 "parser.c" /* yacc.c:1646  */
    break;

  case 396:
#line 2235 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = RANGE_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7590 "parser.c" /* yacc.c:1646  */
    break;

  case 397:
#line 2241 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = INTEGER_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7600 "parser.c" /* yacc.c:1646  */
    break;

  case 398:
#line 2247 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = STRING_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7610 "parser.c" /* yacc.c:1646  */
    break;

  case 399:
#line 2253 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = BOOLEAN_LIST_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7620 "parser.c" /* yacc.c:1646  */
    break;

  case 400:
#line 2261 "parser.y" /* yacc.c:1646  */
    {
			    tempIntPtr = (int *) safeMalloc(sizeof(int));
			    *tempIntPtr = VOID_TYPE;
			    (yyval.integerval) = tempIntPtr;
			  }
#line 7630 "parser.c" /* yacc.c:1646  */
    break;

  case 401:
#line 2267 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.integerval) = (yyvsp[0].integerval);
		          }
#line 7638 "parser.c" /* yacc.c:1646  */
    break;

  case 402:
#line 2274 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].integerval));
			  }
#line 7646 "parser.c" /* yacc.c:1646  */
    break;

  case 403:
#line 2278 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement((yyvsp[0].list), (yyvsp[-2].integerval));
			  }
#line 7654 "parser.c" /* yacc.c:1646  */
    break;

  case 404:
#line 2284 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = addElement(NULL, (yyvsp[0].integerval));
			  }
#line 7662 "parser.c" /* yacc.c:1646  */
    break;

  case 405:
#line 2288 "parser.y" /* yacc.c:1646  */
    {
			    (yyval.list) = (yyvsp[-1].list);
			  }
#line 7670 "parser.c" /* yacc.c:1646  */
    break;

  case 406:
#line 2295 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a base 10 constant.\n",(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
			  }
#line 7679 "parser.c" /* yacc.c:1646  */
    break;

  case 407:
#line 2300 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a dyadic number constant.\n",(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
                          }
#line 7688 "parser.c" /* yacc.c:1646  */
    break;

  case 408:
#line 2305 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a double or single precision constant.\n",(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
                          }
#line 7697 "parser.c" /* yacc.c:1646  */
    break;

  case 409:
#line 2310 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("\"%s\" is recognized as a hexadecimal constant.\n",(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
                          }
#line 7706 "parser.c" /* yacc.c:1646  */
    break;

  case 410:
#line 2315 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("\"%s_2\" is recognized as a base 2 constant.\n",(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
                          }
#line 7715 "parser.c" /* yacc.c:1646  */
    break;

  case 411:
#line 2320 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PI_TEXT
			    outputMode(); sollyaPrintf(HELP_PI_TEXT);
#else
			    outputMode(); sollyaPrintf("Ratio circonference and diameter of a circle.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PI"
#endif
#endif
                          }
#line 7730 "parser.c" /* yacc.c:1646  */
    break;

  case 412:
#line 2331 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("\"%s\" is an identifier.\n",(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
                          }
#line 7739 "parser.c" /* yacc.c:1646  */
    break;

  case 413:
#line 2336 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("\"%s\" is a string constant.\n",(yyvsp[0].value));
			    safeFree((yyvsp[0].value));
                          }
#line 7748 "parser.c" /* yacc.c:1646  */
    break;

  case 414:
#line 2341 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Left parenthesis.\n");
                          }
#line 7756 "parser.c" /* yacc.c:1646  */
    break;

  case 415:
#line 2345 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Right parenthesis.\n");
                          }
#line 7764 "parser.c" /* yacc.c:1646  */
    break;

  case 416:
#line 2349 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Left bracket - indicates a range.\n");
                          }
#line 7772 "parser.c" /* yacc.c:1646  */
    break;

  case 417:
#line 2353 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Right bracket - indicates a range.\n");
                          }
#line 7780 "parser.c" /* yacc.c:1646  */
    break;

  case 418:
#line 2357 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Left bracket-bar - indicates a list.\n");
                          }
#line 7788 "parser.c" /* yacc.c:1646  */
    break;

  case 419:
#line 2361 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Bar-right bracket - indicates a list.\n");
                          }
#line 7796 "parser.c" /* yacc.c:1646  */
    break;

  case 420:
#line 2365 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ASSIGNMENT_TEXT
			    outputMode(); sollyaPrintf(HELP_ASSIGNMENT_TEXT);
#else
			    outputMode(); sollyaPrintf("Assignment operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASSIGNMENT"
#endif
#endif
                          }
#line 7811 "parser.c" /* yacc.c:1646  */
    break;

  case 421:
#line 2376 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FLOATASSIGNMENT_TEXT
			    outputMode(); sollyaPrintf(HELP_FLOATASSIGNMENT_TEXT);
#else
			    outputMode(); sollyaPrintf("Evaluating assignment operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FLOATASSIGNMENT"
#endif
#endif
                          }
#line 7826 "parser.c" /* yacc.c:1646  */
    break;

  case 422:
#line 2387 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EQUAL_TEXT
			    outputMode(); sollyaPrintf(HELP_EQUAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Equality test.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EQUAL"
#endif
#endif
                          }
#line 7841 "parser.c" /* yacc.c:1646  */
    break;

  case 423:
#line 2398 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Separator in lists, ranges or structures.\n");
                          }
#line 7849 "parser.c" /* yacc.c:1646  */
    break;

  case 424:
#line 2402 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_NOT_TEXT
			    outputMode(); sollyaPrintf(HELP_NOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Suppresses output on assignments or boolean negation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NOT"
#endif
#endif
                          }
#line 7864 "parser.c" /* yacc.c:1646  */
    break;

  case 425:
#line 2413 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Dereferences range bounds.\n");
                          }
#line 7872 "parser.c" /* yacc.c:1646  */
    break;

  case 426:
#line 2417 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LT_TEXT
			    outputMode(); sollyaPrintf(HELP_LT_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison less than.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LT"
#endif
#endif
                          }
#line 7887 "parser.c" /* yacc.c:1646  */
    break;

  case 427:
#line 2428 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LE_TEXT
			    outputMode(); sollyaPrintf(HELP_LE_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison less than or equal to.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LE"
#endif
#endif
                          }
#line 7902 "parser.c" /* yacc.c:1646  */
    break;

  case 428:
#line 2439 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Dereferences the lower range bound.\n");
                          }
#line 7910 "parser.c" /* yacc.c:1646  */
    break;

  case 429:
#line 2443 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Dereferences the mid-point of a range.\n");
                          }
#line 7918 "parser.c" /* yacc.c:1646  */
    break;

  case 430:
#line 2447 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_GE_TEXT
			    outputMode(); sollyaPrintf(HELP_GE_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison greater than or equal to.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GE"
#endif
#endif
			  }
#line 7933 "parser.c" /* yacc.c:1646  */
    break;

  case 431:
#line 2458 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Accessing an element in a structured type.\n");
			  }
#line 7941 "parser.c" /* yacc.c:1646  */
    break;

  case 432:
#line 2462 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Dereferences the upper range bound.\n");
                          }
#line 7949 "parser.c" /* yacc.c:1646  */
    break;

  case 433:
#line 2466 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_GT_TEXT
			    outputMode(); sollyaPrintf(HELP_GT_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison greater than.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GT"
#endif
#endif
                          }
#line 7964 "parser.c" /* yacc.c:1646  */
    break;

  case 434:
#line 2477 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Ellipsis.\n");
                          }
#line 7972 "parser.c" /* yacc.c:1646  */
    break;

  case 435:
#line 2481 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Dereferences global environment variables.\n");
                          }
#line 7980 "parser.c" /* yacc.c:1646  */
    break;

  case 436:
#line 2485 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Starts or ends a list.\n");
                          }
#line 7988 "parser.c" /* yacc.c:1646  */
    break;

  case 437:
#line 2489 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_CONCAT_TEXT
			    outputMode(); sollyaPrintf(HELP_CONCAT_TEXT);
#else
			    outputMode(); sollyaPrintf("Concatenation of lists or strings.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CONCAT"
#endif
#endif
                          }
#line 8003 "parser.c" /* yacc.c:1646  */
    break;

  case 438:
#line 2500 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("a::b prepends a to list b or appends b to list a, preprending list a to list b if both are lists.\n");
                          }
#line 8011 "parser.c" /* yacc.c:1646  */
    break;

  case 439:
#line 2504 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PREPEND_TEXT
			    outputMode(); sollyaPrintf(HELP_PREPEND_TEXT);
#else
			    outputMode(); sollyaPrintf("a.:b prepends a to list b.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PREPEND"
#endif
#endif
                          }
#line 8026 "parser.c" /* yacc.c:1646  */
    break;

  case 440:
#line 2515 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_APPEND_TEXT
			    outputMode(); sollyaPrintf(HELP_APPEND_TEXT);
#else
			    outputMode(); sollyaPrintf("a:.b appends b to list a.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for APPEND"
#endif
#endif
                          }
#line 8041 "parser.c" /* yacc.c:1646  */
    break;

  case 441:
#line 2526 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_NEQ_TEXT
			    outputMode(); sollyaPrintf(HELP_NEQ_TEXT);
#else
			    outputMode(); sollyaPrintf("Comparison not equal.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NEQ"
#endif
#endif
                          }
#line 8056 "parser.c" /* yacc.c:1646  */
    break;

  case 442:
#line 2537 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_AND_TEXT
			    outputMode(); sollyaPrintf(HELP_AND_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean and.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for AND"
#endif
#endif
                          }
#line 8071 "parser.c" /* yacc.c:1646  */
    break;

  case 443:
#line 2548 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_OR_TEXT
			    outputMode(); sollyaPrintf(HELP_OR_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean or.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for OR"
#endif
#endif
                          }
#line 8086 "parser.c" /* yacc.c:1646  */
    break;

  case 444:
#line 2559 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PLUS_TEXT
			    outputMode(); sollyaPrintf(HELP_PLUS_TEXT);
#else
			    outputMode(); sollyaPrintf("Addition.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PLUS"
#endif
#endif
                          }
#line 8101 "parser.c" /* yacc.c:1646  */
    break;

  case 445:
#line 2570 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MINUS_TEXT
			    outputMode(); sollyaPrintf(HELP_MINUS_TEXT);
#else
			    outputMode(); sollyaPrintf("Substraction.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MINUS"
#endif
#endif
                          }
#line 8116 "parser.c" /* yacc.c:1646  */
    break;

  case 446:
#line 2581 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_APPROX_TEXT
			    outputMode(); sollyaPrintf(HELP_APPROX_TEXT);
#else
			    outputMode(); sollyaPrintf("Floating-point approximation of a constant expression.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for APPROX"
#endif
#endif
                          }
#line 8131 "parser.c" /* yacc.c:1646  */
    break;

  case 447:
#line 2592 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MULT_TEXT
			    outputMode(); sollyaPrintf(HELP_MULT_TEXT);
#else
			    outputMode(); sollyaPrintf("Multiplication.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MULT"
#endif
#endif
                          }
#line 8146 "parser.c" /* yacc.c:1646  */
    break;

  case 448:
#line 2603 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIVIDE_TEXT
			    outputMode(); sollyaPrintf(HELP_DIVIDE_TEXT);
#else
			    outputMode(); sollyaPrintf("Division.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIVIDE"
#endif
#endif
                          }
#line 8161 "parser.c" /* yacc.c:1646  */
    break;

  case 449:
#line 2614 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_POWER_TEXT
			    outputMode(); sollyaPrintf(HELP_POWER_TEXT);
#else
			    outputMode(); sollyaPrintf("Exponentiation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POWER"
#endif
#endif
                          }
#line 8176 "parser.c" /* yacc.c:1646  */
    break;

  case 450:
#line 2625 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SQRT_TEXT
			    outputMode(); sollyaPrintf(HELP_SQRT_TEXT);
#else
			    outputMode(); sollyaPrintf("Square root.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SQRT"
#endif
#endif
                          }
#line 8191 "parser.c" /* yacc.c:1646  */
    break;

  case 451:
#line 2636 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EXP_TEXT
			    outputMode(); sollyaPrintf(HELP_EXP_TEXT);
#else
			    outputMode(); sollyaPrintf("Exponential.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXP"
#endif
#endif
                          }
#line 8206 "parser.c" /* yacc.c:1646  */
    break;

  case 452:
#line 2647 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_XFREEVARIABLE_TEXT
			    outputMode(); sollyaPrintf(HELP_XFREEVARIABLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Reserved default free variable _x_.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for XFREEVARIABLE"
#endif
#endif
                          }
#line 8221 "parser.c" /* yacc.c:1646  */
    break;

  case 453:
#line 2658 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LOG_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG_TEXT);
#else
			    outputMode(); sollyaPrintf("Natural logarithm.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG"
#endif
#endif
                          }
#line 8236 "parser.c" /* yacc.c:1646  */
    break;

  case 454:
#line 2669 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LOG2_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG2_TEXT);
#else
			    outputMode(); sollyaPrintf("Logarithm in base 2.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG2"
#endif
#endif
                          }
#line 8251 "parser.c" /* yacc.c:1646  */
    break;

  case 455:
#line 2680 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LOG10_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG10_TEXT);
#else
			    outputMode(); sollyaPrintf("Logarithm in base 10.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG10"
#endif
#endif
                          }
#line 8266 "parser.c" /* yacc.c:1646  */
    break;

  case 456:
#line 2691 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SIN_TEXT
			    outputMode(); sollyaPrintf(HELP_SIN_TEXT);
#else
			    outputMode(); sollyaPrintf("Sine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SIN"
#endif
#endif
                          }
#line 8281 "parser.c" /* yacc.c:1646  */
    break;

  case 457:
#line 2702 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_COS_TEXT
			    outputMode(); sollyaPrintf(HELP_COS_TEXT);
#else
			    outputMode(); sollyaPrintf("Cosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for COS"
#endif
#endif
                          }
#line 8296 "parser.c" /* yacc.c:1646  */
    break;

  case 458:
#line 2713 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TAN_TEXT
			    outputMode(); sollyaPrintf(HELP_TAN_TEXT);
#else
			    outputMode(); sollyaPrintf("Tangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAN"
#endif
#endif
                          }
#line 8311 "parser.c" /* yacc.c:1646  */
    break;

  case 459:
#line 2724 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ASIN_TEXT
			    outputMode(); sollyaPrintf(HELP_ASIN_TEXT);
#else
			    outputMode(); sollyaPrintf("Arcsine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASIN"
#endif
#endif
                          }
#line 8326 "parser.c" /* yacc.c:1646  */
    break;

  case 460:
#line 2735 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ACOS_TEXT
			    outputMode(); sollyaPrintf(HELP_ACOS_TEXT);
#else
			    outputMode(); sollyaPrintf("Arcosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ACOS"
#endif
#endif
                          }
#line 8341 "parser.c" /* yacc.c:1646  */
    break;

  case 461:
#line 2746 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ATAN_TEXT
			    outputMode(); sollyaPrintf(HELP_ATAN_TEXT);
#else
			    outputMode(); sollyaPrintf("Arctangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ATAN"
#endif
#endif
                          }
#line 8356 "parser.c" /* yacc.c:1646  */
    break;

  case 462:
#line 2757 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SINH_TEXT
			    outputMode(); sollyaPrintf(HELP_SINH_TEXT);
#else
			    outputMode(); sollyaPrintf("Hyperbolic sine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SINH"
#endif
#endif
                          }
#line 8371 "parser.c" /* yacc.c:1646  */
    break;

  case 463:
#line 2768 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_COSH_TEXT
			    outputMode(); sollyaPrintf(HELP_COSH_TEXT);
#else
			    outputMode(); sollyaPrintf("Hyperbolic cosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for COSH"
#endif
#endif
                          }
#line 8386 "parser.c" /* yacc.c:1646  */
    break;

  case 464:
#line 2779 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TANH_TEXT
			    outputMode(); sollyaPrintf(HELP_TANH_TEXT);
#else
			    outputMode(); sollyaPrintf("Hyperbolic tangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TANH"
#endif
#endif
                          }
#line 8401 "parser.c" /* yacc.c:1646  */
    break;

  case 465:
#line 2790 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ASINH_TEXT
			    outputMode(); sollyaPrintf(HELP_ASINH_TEXT);
#else
			    outputMode(); sollyaPrintf("Area sine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASINH"
#endif
#endif
                          }
#line 8416 "parser.c" /* yacc.c:1646  */
    break;

  case 466:
#line 2801 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ACOSH_TEXT
			    outputMode(); sollyaPrintf(HELP_ACOSH_TEXT);
#else
			    outputMode(); sollyaPrintf("Area cosine.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ACOSH"
#endif
#endif
                          }
#line 8431 "parser.c" /* yacc.c:1646  */
    break;

  case 467:
#line 2812 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ATANH_TEXT
			    outputMode(); sollyaPrintf(HELP_ATANH_TEXT);
#else

			    outputMode(); sollyaPrintf("Area tangent.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ATANH"
#endif
#endif
                          }
#line 8447 "parser.c" /* yacc.c:1646  */
    break;

  case 468:
#line 2824 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ABS_TEXT
			    outputMode(); sollyaPrintf(HELP_ABS_TEXT);
#else
			    outputMode(); sollyaPrintf("Absolute value.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ABS"
#endif
#endif
                          }
#line 8462 "parser.c" /* yacc.c:1646  */
    break;

  case 469:
#line 2835 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ERF_TEXT
			    outputMode(); sollyaPrintf(HELP_ERF_TEXT);
#else
			    outputMode(); sollyaPrintf("Error function.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ERF"
#endif
#endif
                          }
#line 8477 "parser.c" /* yacc.c:1646  */
    break;

  case 470:
#line 2846 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ERFC_TEXT
			    outputMode(); sollyaPrintf(HELP_ERFC_TEXT);
#else
			    outputMode(); sollyaPrintf("Complementary error function.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ERFC"
#endif
#endif
                          }
#line 8492 "parser.c" /* yacc.c:1646  */
    break;

  case 471:
#line 2857 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LOG1P_TEXT
			    outputMode(); sollyaPrintf(HELP_LOG1P_TEXT);
#else
			    outputMode(); sollyaPrintf("Natural logarithm of 1 plus argument.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LOG1P"
#endif
#endif
                          }
#line 8507 "parser.c" /* yacc.c:1646  */
    break;

  case 472:
#line 2868 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EXPM1_TEXT
			    outputMode(); sollyaPrintf(HELP_EXPM1_TEXT);
#else
			    outputMode(); sollyaPrintf("Exponential of argument minus 1.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXPM1"
#endif
#endif
                          }
#line 8522 "parser.c" /* yacc.c:1646  */
    break;

  case 473:
#line 2879 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DOUBLE_TEXT
			    outputMode(); sollyaPrintf(HELP_DOUBLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Double precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DOUBLE"
#endif
#endif
                          }
#line 8537 "parser.c" /* yacc.c:1646  */
    break;

  case 474:
#line 2890 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SINGLE_TEXT
			    outputMode(); sollyaPrintf(HELP_SINGLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Single precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SINGLE"
#endif
#endif
                          }
#line 8552 "parser.c" /* yacc.c:1646  */
    break;

  case 475:
#line 2901 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_QUAD_TEXT
			    outputMode(); sollyaPrintf(HELP_QUAD_TEXT);
#else
			    outputMode(); sollyaPrintf("Quad precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for QUAD"
#endif
#endif
                          }
#line 8567 "parser.c" /* yacc.c:1646  */
    break;

  case 476:
#line 2912 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_HALFPRECISION_TEXT
			    outputMode(); sollyaPrintf(HELP_HALFPRECISION_TEXT);
#else
			    outputMode(); sollyaPrintf("Half-precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HALFPRECISION"
#endif
#endif
                          }
#line 8582 "parser.c" /* yacc.c:1646  */
    break;

  case 477:
#line 2923 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DOUBLEDOUBLE_TEXT
			    outputMode(); sollyaPrintf(HELP_DOUBLEDOUBLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Double-double precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DOUBLEDOUBLE"
#endif
#endif
                          }
#line 8597 "parser.c" /* yacc.c:1646  */
    break;

  case 478:
#line 2934 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TRIPLEDOUBLE_TEXT
			    outputMode(); sollyaPrintf(HELP_TRIPLEDOUBLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Triple-double precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TRIPLEDOUBLE"
#endif
#endif
                          }
#line 8612 "parser.c" /* yacc.c:1646  */
    break;

  case 479:
#line 2945 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DOUBLEEXTENDED_TEXT
			    outputMode(); sollyaPrintf(HELP_DOUBLEEXTENDED_TEXT);
#else
			    outputMode(); sollyaPrintf("Double-extended precision rounding operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DOUBLEEXTENDED"
#endif
#endif
                          }
#line 8627 "parser.c" /* yacc.c:1646  */
    break;

  case 480:
#line 2956 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_CEIL_TEXT
			    outputMode(); sollyaPrintf(HELP_CEIL_TEXT);
#else
			    outputMode(); sollyaPrintf("Ceiling.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CEIL"
#endif
#endif
                          }
#line 8642 "parser.c" /* yacc.c:1646  */
    break;

  case 481:
#line 2967 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FLOOR_TEXT
			    outputMode(); sollyaPrintf(HELP_FLOOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Floor.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FLOOR"
#endif
#endif
                          }
#line 8657 "parser.c" /* yacc.c:1646  */
    break;

  case 482:
#line 2978 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_NEARESTINT_TEXT
			    outputMode(); sollyaPrintf(HELP_NEARESTINT_TEXT);
#else
			    outputMode(); sollyaPrintf("Nearest integer with even tie cases rule.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NEARESTINT"
#endif
#endif
                          }
#line 8672 "parser.c" /* yacc.c:1646  */
    break;

  case 483:
#line 2989 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_HEAD_TEXT
			    outputMode(); sollyaPrintf(HELP_HEAD_TEXT);
#else
			    outputMode(); sollyaPrintf("Head of a list.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HEAD"
#endif
#endif
                          }
#line 8687 "parser.c" /* yacc.c:1646  */
    break;

  case 484:
#line 3000 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ROUNDCORRECTLY_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUNDCORRECTLY_TEXT);
#else
			    outputMode(); sollyaPrintf("Round a bounding to the nearest floating-point value such that correct rounding is possible.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUNDCORRECTLY"
#endif
#endif
                          }
#line 8702 "parser.c" /* yacc.c:1646  */
    break;

  case 485:
#line 3011 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_READFILE_TEXT
			    outputMode(); sollyaPrintf(HELP_READFILE_TEXT);
#else
			    outputMode(); sollyaPrintf("Reads a file into a string.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for READFILE"
#endif
#endif
                          }
#line 8717 "parser.c" /* yacc.c:1646  */
    break;

  case 486:
#line 3022 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_REVERT_TEXT
			    outputMode(); sollyaPrintf(HELP_REVERT_TEXT);
#else
			    outputMode(); sollyaPrintf("Reverts a list that is not finally elliptic.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for REVERT"
#endif
#endif
                          }
#line 8732 "parser.c" /* yacc.c:1646  */
    break;

  case 487:
#line 3033 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SORT_TEXT
			    outputMode(); sollyaPrintf(HELP_SORT_TEXT);
#else
			    outputMode(); sollyaPrintf("Sorts a list of constants in ascending order.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SORT"
#endif
#endif
                          }
#line 8747 "parser.c" /* yacc.c:1646  */
    break;

  case 488:
#line 3044 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TAIL_TEXT
			    outputMode(); sollyaPrintf(HELP_TAIL_TEXT);
#else
			    outputMode(); sollyaPrintf("Tail of a list.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAIL"
#endif
#endif
                          }
#line 8762 "parser.c" /* yacc.c:1646  */
    break;

  case 489:
#line 3055 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PREC_TEXT
			    outputMode(); sollyaPrintf(HELP_PREC_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable precision.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PREC"
#endif
#endif
                          }
#line 8777 "parser.c" /* yacc.c:1646  */
    break;

  case 490:
#line 3066 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_POINTS_TEXT
			    outputMode(); sollyaPrintf(HELP_POINTS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable number of points.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POINTS"
#endif
#endif
                          }
#line 8792 "parser.c" /* yacc.c:1646  */
    break;

  case 491:
#line 3077 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIAM_TEXT
			    outputMode(); sollyaPrintf(HELP_DIAM_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable diameter.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIAM"
#endif
#endif
                          }
#line 8807 "parser.c" /* yacc.c:1646  */
    break;

  case 492:
#line 3088 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DISPLAY_TEXT
			    outputMode(); sollyaPrintf(HELP_DISPLAY_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable display mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DISPLAY"
#endif
#endif
                          }
#line 8822 "parser.c" /* yacc.c:1646  */
    break;

  case 493:
#line 3099 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_VERBOSITY_TEXT
			    outputMode(); sollyaPrintf(HELP_VERBOSITY_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable verbosity.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for VERBOSITY"
#endif
#endif
                          }
#line 8837 "parser.c" /* yacc.c:1646  */
    break;

  case 494:
#line 3110 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SHOWMESSAGENUMBERS_TEXT
			    outputMode(); sollyaPrintf(HELP_SHOWMESSAGENUMBERS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable activating the displaying of message numbers.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SHOWMESSAGENUMBERS"
#endif
#endif
                          }
#line 8852 "parser.c" /* yacc.c:1646  */
    break;

  case 495:
#line 3121 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_CANONICAL_TEXT
			    outputMode(); sollyaPrintf(HELP_CANONICAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable canonical output.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CANONICAL"
#endif
#endif
                          }
#line 8867 "parser.c" /* yacc.c:1646  */
    break;

  case 496:
#line 3132 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_AUTOSIMPLIFY_TEXT
			    outputMode(); sollyaPrintf(HELP_AUTOSIMPLIFY_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable automatic simplification.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for AUTOSIMPLIFY"
#endif
#endif
                          }
#line 8882 "parser.c" /* yacc.c:1646  */
    break;

  case 497:
#line 3143 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TAYLORRECURSIONS_TEXT
			    outputMode(); sollyaPrintf(HELP_TAYLORRECURSIONS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable recursions of Taylor evaluation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAYLORRECURSIONS"
#endif
#endif
                          }
#line 8897 "parser.c" /* yacc.c:1646  */
    break;

  case 498:
#line 3154 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TIMING_TEXT
			    outputMode(); sollyaPrintf(HELP_TIMING_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable timing of computations.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TIMING"
#endif
#endif
                          }
#line 8912 "parser.c" /* yacc.c:1646  */
    break;

  case 499:
#line 3165 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TIME_TEXT
			    outputMode(); sollyaPrintf(HELP_TIME_TEXT);
#else
			    outputMode(); sollyaPrintf("High-level time procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TIME"
#endif
#endif
                          }
#line 8927 "parser.c" /* yacc.c:1646  */
    break;

  case 500:
#line 3176 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FULLPARENTHESES_TEXT
			    outputMode(); sollyaPrintf(HELP_FULLPARENTHESES_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable fully parenthized mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FULLPARENTHESES"
#endif
#endif
                          }
#line 8942 "parser.c" /* yacc.c:1646  */
    break;

  case 501:
#line 3187 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MIDPOINTMODE_TEXT
			    outputMode(); sollyaPrintf(HELP_MIDPOINTMODE_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable midpoint mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MIDPOINTMODE"
#endif
#endif
                          }
#line 8957 "parser.c" /* yacc.c:1646  */
    break;

  case 502:
#line 3198 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIEONERRORMODE_TEXT
			    outputMode(); sollyaPrintf(HELP_DIEONERRORMODE_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable for die-on-error mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIEONERRORMODE"
#endif
#endif
                          }
#line 8972 "parser.c" /* yacc.c:1646  */
    break;

  case 503:
#line 3209 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RATIONALMODE_TEXT
			    outputMode(); sollyaPrintf(HELP_RATIONALMODE_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable rational mode.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RATIONALMODE"
#endif
#endif
                          }
#line 8987 "parser.c" /* yacc.c:1646  */
    break;

  case 504:
#line 3220 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ROUNDINGWARNINGS_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUNDINGWARNINGS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable activating warnings about rounding.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUNDINGWARNINGS"
#endif
#endif
                          }
#line 9002 "parser.c" /* yacc.c:1646  */
    break;

  case 505:
#line 3231 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_HOPITALRECURSIONS_TEXT
			    outputMode(); sollyaPrintf(HELP_HOPITALRECURSIONS_TEXT);
#else
			    outputMode(); sollyaPrintf("Global environment variable recursions of Hopital evaluation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HOPITALRECURSIONS"
#endif
#endif
                          }
#line 9017 "parser.c" /* yacc.c:1646  */
    break;

  case 506:
#line 3242 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ON_TEXT
			    outputMode(); sollyaPrintf(HELP_ON_TEXT);
#else
			    outputMode(); sollyaPrintf("Something is switched on.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ON"
#endif
#endif
                          }
#line 9032 "parser.c" /* yacc.c:1646  */
    break;

  case 507:
#line 3253 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_OFF_TEXT
			    outputMode(); sollyaPrintf(HELP_OFF_TEXT);
#else
			    outputMode(); sollyaPrintf("Something is switched off.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for OFF"
#endif
#endif
                          }
#line 9047 "parser.c" /* yacc.c:1646  */
    break;

  case 508:
#line 3264 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DYADIC_TEXT
			    outputMode(); sollyaPrintf(HELP_DYADIC_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is dyadic output.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DYADIC"
#endif
#endif
                          }
#line 9062 "parser.c" /* yacc.c:1646  */
    break;

  case 509:
#line 3275 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_POWERS_TEXT
			    outputMode(); sollyaPrintf(HELP_POWERS_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is dyadic output with powers.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POWERS"
#endif
#endif
                          }
#line 9077 "parser.c" /* yacc.c:1646  */
    break;

  case 510:
#line 3286 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_BINARY_TEXT
			    outputMode(); sollyaPrintf(HELP_BINARY_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is binary.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BINARY"
#endif
#endif
                          }
#line 9092 "parser.c" /* yacc.c:1646  */
    break;

  case 511:
#line 3297 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_HEXADECIMAL_TEXT
			    outputMode(); sollyaPrintf(HELP_HEXADECIMAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is hexadecimal.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HEXADECIMAL"
#endif
#endif
                          }
#line 9107 "parser.c" /* yacc.c:1646  */
    break;

  case 512:
#line 3308 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FILE_TEXT
			    outputMode(); sollyaPrintf(HELP_FILE_TEXT);
#else
			    outputMode(); sollyaPrintf("A file will be specified.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FILE"
#endif
#endif
                          }
#line 9122 "parser.c" /* yacc.c:1646  */
    break;

  case 513:
#line 3319 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_POSTSCRIPT_TEXT
			    outputMode(); sollyaPrintf(HELP_POSTSCRIPT_TEXT);
#else
			    outputMode(); sollyaPrintf("A postscript file will be specified.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POSTSCRIPT"
#endif
#endif
                          }
#line 9137 "parser.c" /* yacc.c:1646  */
    break;

  case 514:
#line 3330 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_POSTSCRIPTFILE_TEXT
			    outputMode(); sollyaPrintf(HELP_POSTSCRIPTFILE_TEXT);
#else
			    outputMode(); sollyaPrintf("A postscript file and a file will be specified.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for POSTSCRIPTFILE"
#endif
#endif
                          }
#line 9152 "parser.c" /* yacc.c:1646  */
    break;

  case 515:
#line 3341 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PERTURB_TEXT
			    outputMode(); sollyaPrintf(HELP_PERTURB_TEXT);
#else
			    outputMode(); sollyaPrintf("Perturbation is demanded.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PERTURB"
#endif
#endif
                          }
#line 9167 "parser.c" /* yacc.c:1646  */
    break;

  case 516:
#line 3352 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RD_TEXT
			    outputMode(); sollyaPrintf(HELP_RD_TEXT);
#else
			    outputMode(); sollyaPrintf("Round towards minus infinity.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RD"
#endif
#endif
                          }
#line 9182 "parser.c" /* yacc.c:1646  */
    break;

  case 517:
#line 3363 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RU_TEXT
			    outputMode(); sollyaPrintf(HELP_RU_TEXT);
#else
			    outputMode(); sollyaPrintf("Round towards plus infinity.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RU"
#endif
#endif
                          }
#line 9197 "parser.c" /* yacc.c:1646  */
    break;

  case 518:
#line 3374 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RZ_TEXT
			    outputMode(); sollyaPrintf(HELP_RZ_TEXT);
#else
			    outputMode(); sollyaPrintf("Round towards zero.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RZ"
#endif
#endif
                          }
#line 9212 "parser.c" /* yacc.c:1646  */
    break;

  case 519:
#line 3385 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RN_TEXT
			    outputMode(); sollyaPrintf(HELP_RN_TEXT);
#else
			    outputMode(); sollyaPrintf("Round to nearest.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RN"
#endif
#endif
                          }
#line 9227 "parser.c" /* yacc.c:1646  */
    break;

  case 520:
#line 3396 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_HONORCOEFFPREC_TEXT
			    outputMode(); sollyaPrintf(HELP_HONORCOEFFPREC_TEXT);
#else
			    outputMode(); sollyaPrintf("Honorate the precision of the coefficients.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HONORCOEFFPREC"
#endif
#endif
                          }
#line 9242 "parser.c" /* yacc.c:1646  */
    break;

  case 521:
#line 3407 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TRUE_TEXT
			    outputMode(); sollyaPrintf(HELP_TRUE_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean constant true.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TRUE"
#endif
#endif
                          }
#line 9257 "parser.c" /* yacc.c:1646  */
    break;

  case 522:
#line 3418 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FALSE_TEXT
			    outputMode(); sollyaPrintf(HELP_FALSE_TEXT);
#else
			    outputMode(); sollyaPrintf("Boolean constant false.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FALSE"
#endif
#endif
                          }
#line 9272 "parser.c" /* yacc.c:1646  */
    break;

  case 523:
#line 3429 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DEFAULT_TEXT
			    outputMode(); sollyaPrintf(HELP_DEFAULT_TEXT);
#else
			    outputMode(); sollyaPrintf("Default value.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DEFAULT"
#endif
#endif
                          }
#line 9287 "parser.c" /* yacc.c:1646  */
    break;

  case 524:
#line 3440 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MATCH_TEXT
			    outputMode(); sollyaPrintf(HELP_MATCH_TEXT);
#else
			    outputMode(); sollyaPrintf("match ... with ... construct.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MATCH"
#endif
#endif
                          }
#line 9302 "parser.c" /* yacc.c:1646  */
    break;

  case 525:
#line 3451 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_WITH_TEXT
			    outputMode(); sollyaPrintf(HELP_WITH_TEXT);
#else
			    outputMode(); sollyaPrintf("match ... with ... construct.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for WITH"
#endif
#endif
                          }
#line 9317 "parser.c" /* yacc.c:1646  */
    break;

  case 526:
#line 3462 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ABSOLUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_ABSOLUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider an absolute error.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ABSOLUTE"
#endif
#endif
                          }
#line 9332 "parser.c" /* yacc.c:1646  */
    break;

  case 527:
#line 3473 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DECIMAL_TEXT
			    outputMode(); sollyaPrintf(HELP_DECIMAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Display mode is decimal.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DECIMAL"
#endif
#endif
                          }
#line 9347 "parser.c" /* yacc.c:1646  */
    break;

  case 528:
#line 3484 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RELATIVE_TEXT
			    outputMode(); sollyaPrintf(HELP_RELATIVE_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider a relative error.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RELATIVE"
#endif
#endif
                          }
#line 9362 "parser.c" /* yacc.c:1646  */
    break;

  case 529:
#line 3495 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FIXED_TEXT
			    outputMode(); sollyaPrintf(HELP_FIXED_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider fixed-point numbers.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FIXED"
#endif
#endif
                          }
#line 9377 "parser.c" /* yacc.c:1646  */
    break;

  case 530:
#line 3506 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FLOATING_TEXT
			    outputMode(); sollyaPrintf(HELP_FLOATING_TEXT);
#else
			    outputMode(); sollyaPrintf("Consider floating-point numbers.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FLOATING"
#endif
#endif
                          }
#line 9392 "parser.c" /* yacc.c:1646  */
    break;

  case 531:
#line 3517 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ERROR_TEXT
			    outputMode(); sollyaPrintf(HELP_ERROR_TEXT);
#else
			    outputMode(); sollyaPrintf("Type error meta-value.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ERROR"
#endif
#endif
                          }
#line 9407 "parser.c" /* yacc.c:1646  */
    break;

  case 532:
#line 3528 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_QUIT_TEXT
			    outputMode(); sollyaPrintf(HELP_QUIT_TEXT);
#else
			    outputMode(); sollyaPrintf("Exit from the tool.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for QUIT"
#endif
#endif
                          }
#line 9422 "parser.c" /* yacc.c:1646  */
    break;

  case 533:
#line 3539 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_QUIT_TEXT
			    outputMode(); sollyaPrintf(HELP_QUIT_TEXT);
#else
			    outputMode(); sollyaPrintf("Exit from the tool - help is called inside a read macro.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for QUIT"
#endif
#endif
                          }
#line 9437 "parser.c" /* yacc.c:1646  */
    break;

  case 534:
#line 3550 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RESTART_TEXT
			    outputMode(); sollyaPrintf(HELP_RESTART_TEXT);
#else
			    outputMode(); sollyaPrintf("Restart the tool.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RESTART"
#endif
#endif
                          }
#line 9452 "parser.c" /* yacc.c:1646  */
    break;

  case 535:
#line 3561 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LIBRARY_TEXT
			    outputMode(); sollyaPrintf(HELP_LIBRARY_TEXT);
#else
			    outputMode(); sollyaPrintf("Library binding dereferencer.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LIBRARY"
#endif
#endif
                          }
#line 9467 "parser.c" /* yacc.c:1646  */
    break;

  case 536:
#line 3572 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LIBRARYCONSTANT_TEXT
			    outputMode(); sollyaPrintf(HELP_LIBRARYCONSTANT_TEXT);
#else
			    outputMode(); sollyaPrintf("Library constant binding dereferencer.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LIBRARYCONSTANT"
#endif
#endif
                          }
#line 9482 "parser.c" /* yacc.c:1646  */
    break;

  case 537:
#line 3583 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIFF_TEXT
			    outputMode(); sollyaPrintf(HELP_DIFF_TEXT);
#else
			    outputMode(); sollyaPrintf("Differentiation: diff(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIFF"
#endif
#endif
                          }
#line 9497 "parser.c" /* yacc.c:1646  */
    break;

  case 538:
#line 3594 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_BASHEVALUATE_TEXT
			    outputMode(); sollyaPrintf(HELP_BASHEVALUATE_TEXT);
#else
			    outputMode(); sollyaPrintf("Executes a string as a bash command and returns the output as a string.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BASHEVALUATE"
#endif
#endif
                          }
#line 9512 "parser.c" /* yacc.c:1646  */
    break;

  case 539:
#line 3605 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_GETSUPPRESSEDMESSAGES_TEXT
			    outputMode(); sollyaPrintf(HELP_GETSUPPRESSEDMESSAGES_TEXT);
#else
			    outputMode(); sollyaPrintf("Get a list of message numbers that have been suppressed.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GETSUPPRESSEDMESSAGES"
#endif
#endif
                          }
#line 9527 "parser.c" /* yacc.c:1646  */
    break;

  case 540:
#line 3616 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_GETBACKTRACE_TEXT
			    outputMode(); sollyaPrintf(HELP_GETBACKTRACE_TEXT);
#else
			    outputMode(); sollyaPrintf("Get a backtrace of the procedure calling stack.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GETBACKTRACE"
#endif
#endif
                          }
#line 9542 "parser.c" /* yacc.c:1646  */
    break;

  case 541:
#line 3627 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIRTYSIMPLIFY_TEXT
			    outputMode(); sollyaPrintf(HELP_DIRTYSIMPLIFY_TEXT);
#else
			    outputMode(); sollyaPrintf("Simplify with floating-point evaluation: dirtysimplify(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIRTYSIMPLIFY"
#endif
#endif
                          }
#line 9557 "parser.c" /* yacc.c:1646  */
    break;

  case 542:
#line 3638 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_REMEZ_TEXT
			    outputMode(); sollyaPrintf(HELP_REMEZ_TEXT);
#else
			    outputMode(); sollyaPrintf("Remez: remez(func,degree|monoms,range[,weight[,quality]]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for REMEZ"
#endif
#endif
                          }
#line 9572 "parser.c" /* yacc.c:1646  */
    break;

  case 543:
#line 3649 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ANNOTATEFUNCTION_TEXT
			    outputMode(); sollyaPrintf(HELP_ANNOTATEFUNCTION_TEXT);
#else
			    outputMode(); sollyaPrintf("Function annotation: annotatefunction(f,p,dom,delta[,t]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ANNOTATEFUNCTION"
#endif
#endif
                          }
#line 9587 "parser.c" /* yacc.c:1646  */
    break;

  case 544:
#line 3660 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MIN_TEXT
			    outputMode(); sollyaPrintf(HELP_MIN_TEXT);
#else
			    outputMode(); sollyaPrintf("min(val1,val2,...,valn): computes the minimum of the constant expressions vali.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MIN"
#endif
#endif
                          }
#line 9602 "parser.c" /* yacc.c:1646  */
    break;

  case 545:
#line 3671 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MAX_TEXT
			    outputMode(); sollyaPrintf(HELP_MAX_TEXT);
#else
			    outputMode(); sollyaPrintf("max(val1,val2,...,valn): computes the maximum of the constant expressions vali.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MAX"
#endif
#endif
                          }
#line 9617 "parser.c" /* yacc.c:1646  */
    break;

  case 546:
#line 3682 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FPMINIMAX_TEXT
			    outputMode(); sollyaPrintf(HELP_FPMINIMAX_TEXT);
#else
			    outputMode(); sollyaPrintf("Fpminimax: fpminimax(func,degree|monoms,formats,range|pointslist[,absolute|relative[,fixed|floating[,constrainedPart[, minimaxpoly]]]]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FPMINIMAX"
#endif
#endif
                          }
#line 9632 "parser.c" /* yacc.c:1646  */
    break;

  case 547:
#line 3693 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_HORNER_TEXT
			    outputMode(); sollyaPrintf(HELP_HORNER_TEXT);
#else
			    outputMode(); sollyaPrintf("Horner: horner(func)\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for HORNER"
#endif
#endif
                          }
#line 9647 "parser.c" /* yacc.c:1646  */
    break;

  case 548:
#line 3704 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EXPAND_TEXT
			    outputMode(); sollyaPrintf(HELP_EXPAND_TEXT);
#else
			    outputMode(); sollyaPrintf("Expand: expand(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXPAND"
#endif
#endif
                          }
#line 9662 "parser.c" /* yacc.c:1646  */
    break;

  case 549:
#line 3715 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SIMPLIFY_TEXT
			    outputMode(); sollyaPrintf(HELP_SIMPLIFY_TEXT);
#else
			    outputMode(); sollyaPrintf("Simplification without rounding error: simplify(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SIMPLIFY"
#endif
#endif
                          }
#line 9677 "parser.c" /* yacc.c:1646  */
    break;

  case 550:
#line 3726 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TAYLOR_TEXT
			    outputMode(); sollyaPrintf(HELP_TAYLOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Taylor expansion: taylor(func,degree,point).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAYLOR"
#endif
#endif
                          }
#line 9692 "parser.c" /* yacc.c:1646  */
    break;

  case 551:
#line 3737 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_TAYLORFORM_TEXT
			    outputMode(); sollyaPrintf(HELP_TAYLORFORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Taylor form computation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for TAYLORFORM"
#endif
#endif
                          }
#line 9707 "parser.c" /* yacc.c:1646  */
    break;

  case 552:
#line 3748 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_CHEBYSHEVFORM_TEXT
			    outputMode(); sollyaPrintf(HELP_CHEBYSHEVFORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Chebyshev form computation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CHEBYSHEVFORM"
#endif
#endif
                          }
#line 9722 "parser.c" /* yacc.c:1646  */
    break;

  case 553:
#line 3759 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_AUTODIFF_TEXT
			    outputMode(); sollyaPrintf(HELP_AUTODIFF_TEXT);
#else
			    outputMode(); sollyaPrintf("Automatic differentiation.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for AUTODIFF"
#endif
#endif
                          }
#line 9737 "parser.c" /* yacc.c:1646  */
    break;

  case 554:
#line 3770 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DEGREE_TEXT
			    outputMode(); sollyaPrintf(HELP_DEGREE_TEXT);
#else
			    outputMode(); sollyaPrintf("Degree of a polynomial: degree(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DEGREE"
#endif
#endif
                          }
#line 9752 "parser.c" /* yacc.c:1646  */
    break;

  case 555:
#line 3781 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_NUMERATOR_TEXT
			    outputMode(); sollyaPrintf(HELP_NUMERATOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Numerator of an expression: numerator(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NUMERATOR"
#endif
#endif
                          }
#line 9767 "parser.c" /* yacc.c:1646  */
    break;

  case 556:
#line 3792 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DENOMINATOR_TEXT
			    outputMode(); sollyaPrintf(HELP_DENOMINATOR_TEXT);
#else
			    outputMode(); sollyaPrintf("Denominator of an expression: denominator(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DENOMINATOR"
#endif
#endif
                          }
#line 9782 "parser.c" /* yacc.c:1646  */
    break;

  case 557:
#line 3803 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SUBSTITUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_SUBSTITUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("Substitute func2 for free variable in func: substitute(func,func2).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUBSTITUTE"
#endif
#endif
                          }
#line 9797 "parser.c" /* yacc.c:1646  */
    break;

  case 558:
#line 3814 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_COMPOSEPOLYNOMIALS_TEXT
			    outputMode(); sollyaPrintf(HELP_COMPOSEPOLYNOMIALS_TEXT);
#else
			    outputMode(); sollyaPrintf("Compose two polynomials p and q and round coefficients of p(q).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for COMPOSEPOLYNOMIALS"
#endif
#endif
                          }
#line 9812 "parser.c" /* yacc.c:1646  */
    break;

  case 559:
#line 3825 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_COEFF_TEXT
			    outputMode(); sollyaPrintf(HELP_COEFF_TEXT);
#else
			    outputMode(); sollyaPrintf("i-th coefficient of a polynomial: coeff(func,degree).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for COEFF"
#endif
#endif
                          }
#line 9827 "parser.c" /* yacc.c:1646  */
    break;

  case 560:
#line 3836 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SUBPOLY_TEXT
			    outputMode(); sollyaPrintf(HELP_SUBPOLY_TEXT);
#else
			    outputMode(); sollyaPrintf("Subpolynomial consisting in monomials: subpoly(func,list of degrees).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUBPOLY"
#endif
#endif
                          }
#line 9842 "parser.c" /* yacc.c:1646  */
    break;

  case 561:
#line 3847 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ROUNDCOEFFICIENTS_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUNDCOEFFICIENTS_TEXT);
#else
			    outputMode(); sollyaPrintf("Round coefficients of a polynomial to format: roundcoefficients(func,list of formats).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUNDCOEFFICIENTS"
#endif
#endif
                          }
#line 9857 "parser.c" /* yacc.c:1646  */
    break;

  case 562:
#line 3858 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RATIONALAPPROX_TEXT
			    outputMode(); sollyaPrintf(HELP_RATIONALAPPROX_TEXT);
#else
			    outputMode(); sollyaPrintf("Rational approximation: rationalapprox(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RATIONALAPPROX"
#endif
#endif
                          }
#line 9872 "parser.c" /* yacc.c:1646  */
    break;

  case 563:
#line 3869 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ACCURATEINFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_ACCURATEINFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Faithful rounded infinity norm: accurateinfnorm(func,bits,range,domains to exclude).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ACCURATEINFNORM"
#endif
#endif
                          }
#line 9887 "parser.c" /* yacc.c:1646  */
    break;

  case 564:
#line 3880 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ROUND_TEXT
			    outputMode(); sollyaPrintf(HELP_ROUND_TEXT);
#else
			    outputMode(); sollyaPrintf("Round to a given format: round(constant,precision,rounding mode).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ROUND"
#endif
#endif
                          }
#line 9902 "parser.c" /* yacc.c:1646  */
    break;

  case 565:
#line 3891 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EVALUATE_TEXT
			    outputMode(); sollyaPrintf(HELP_EVALUATE_TEXT);
#else
			    outputMode(); sollyaPrintf("Evaluate a function in a point or interval: round(func,constant|range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EVALUATE"
#endif
#endif
                          }
#line 9917 "parser.c" /* yacc.c:1646  */
    break;

  case 566:
#line 3902 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LENGTH_TEXT
			    outputMode(); sollyaPrintf(HELP_LENGTH_TEXT);
#else
			    outputMode(); sollyaPrintf("Length of a list: length(list).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LENGTH"
#endif
#endif
                          }
#line 9932 "parser.c" /* yacc.c:1646  */
    break;

  case 567:
#line 3913 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_OBJECTNAME_TEXT
			    outputMode(); sollyaPrintf(HELP_OBJECTNAME_TEXT);
#else
			    outputMode(); sollyaPrintf("Name of an object: objectname(obj).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for OBJECTNAME"
#endif
#endif
                          }
#line 9947 "parser.c" /* yacc.c:1646  */
    break;

  case 568:
#line 3924 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PARSE_TEXT
			    outputMode(); sollyaPrintf(HELP_PARSE_TEXT);
#else
			    outputMode(); sollyaPrintf("Parse a string to function: parse(string).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PARSE"
#endif
#endif
                          }
#line 9962 "parser.c" /* yacc.c:1646  */
    break;

  case 569:
#line 3935 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PRINT_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINT_TEXT);
#else
			    outputMode(); sollyaPrintf("Print something: print(thing1, thing2, ...).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINT"
#endif
#endif
                          }
#line 9977 "parser.c" /* yacc.c:1646  */
    break;

  case 570:
#line 3946 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PRINTXML_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTXML_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a function in XML: printxml(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTXML"
#endif
#endif
                          }
#line 9992 "parser.c" /* yacc.c:1646  */
    break;

  case 571:
#line 3957 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_READXML_TEXT
			    outputMode(); sollyaPrintf(HELP_READXML_TEXT);
#else
			    outputMode(); sollyaPrintf("Reads a function in XML: readxml(filename).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for READXML"
#endif
#endif
                          }
#line 10007 "parser.c" /* yacc.c:1646  */
    break;

  case 572:
#line 3968 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PLOT_TEXT
			    outputMode(); sollyaPrintf(HELP_PLOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Plot (a) function(s) in a range: plot(func,func2,...,range).\n");
			    outputMode(); sollyaPrintf("There are further options.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PLOT"
#endif
#endif
                          }
#line 10023 "parser.c" /* yacc.c:1646  */
    break;

  case 573:
#line 3980 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PRINTHEXA_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTHEXA_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a constant in hexadecimal: printhexa(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTHEXA"
#endif
#endif
                          }
#line 10038 "parser.c" /* yacc.c:1646  */
    break;

  case 574:
#line 3991 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PRINTFLOAT_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTFLOAT_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a constant in hexadecimal simple precision: printfloat(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTFLOAT"
#endif
#endif
                          }
#line 10053 "parser.c" /* yacc.c:1646  */
    break;

  case 575:
#line 4002 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PRINTBINARY_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTBINARY_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a constant in binary: printbinary(constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTBINARY"
#endif
#endif
                          }
#line 10068 "parser.c" /* yacc.c:1646  */
    break;

  case 576:
#line 4013 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SUPPRESSMESSAGE_TEXT
			    outputMode(); sollyaPrintf(HELP_SUPPRESSMESSAGE_TEXT);
#else
			    outputMode(); sollyaPrintf("Suppress a message with a certain message number.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUPPRESSMESSAGE"
#endif
#endif
                          }
#line 10083 "parser.c" /* yacc.c:1646  */
    break;

  case 577:
#line 4024 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_UNSUPPRESSMESSAGE_TEXT
			    outputMode(); sollyaPrintf(HELP_UNSUPPRESSMESSAGE_TEXT);
#else
			    outputMode(); sollyaPrintf("Unsuppress a message with a certain message number.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for UNSUPPRESSMESSAGE"
#endif
#endif
                          }
#line 10098 "parser.c" /* yacc.c:1646  */
    break;

  case 578:
#line 4035 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PRINTEXPANSION_TEXT
			    outputMode(); sollyaPrintf(HELP_PRINTEXPANSION_TEXT);
#else
			    outputMode(); sollyaPrintf("Print a polynomial as an expansion of double precision numbers: printexpansion(func).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRINTEXPANSION"
#endif
#endif
                          }
#line 10113 "parser.c" /* yacc.c:1646  */
    break;

  case 579:
#line 4046 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_BASHEXECUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_BASHEXECUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("Execute a command in a shell: bashexecute(string).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BASHEXECUTE"
#endif
#endif
                          }
#line 10128 "parser.c" /* yacc.c:1646  */
    break;

  case 580:
#line 4057 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EXTERNALPLOT_TEXT
			    outputMode(); sollyaPrintf(HELP_EXTERNALPLOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Here should be some help text.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXTERNALPLOT"
#endif
#endif
                          }
#line 10143 "parser.c" /* yacc.c:1646  */
    break;

  case 581:
#line 4068 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_WRITE_TEXT
			    outputMode(); sollyaPrintf(HELP_WRITE_TEXT);
#else
			    outputMode(); sollyaPrintf("Write something without adding spaces and newlines: write(thing1, thing2, ...).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for WRITE"
#endif
#endif
                          }
#line 10158 "parser.c" /* yacc.c:1646  */
    break;

  case 582:
#line 4079 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ASCIIPLOT_TEXT
			    outputMode(); sollyaPrintf(HELP_ASCIIPLOT_TEXT);
#else
			    outputMode(); sollyaPrintf("Plot a function in a range using an ASCII terminal: asciiplot(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ASCIIPLOT"
#endif
#endif
                          }
#line 10173 "parser.c" /* yacc.c:1646  */
    break;

  case 583:
#line 4090 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RENAME_TEXT
			    outputMode(); sollyaPrintf(HELP_RENAME_TEXT);
#else
			    outputMode(); sollyaPrintf("Rename free variable string1 to string2: rename(string1, string2).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RENAME"
#endif
#endif
                          }
#line 10188 "parser.c" /* yacc.c:1646  */
    break;

  case 584:
#line 4101 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_BIND_TEXT
			    outputMode(); sollyaPrintf(HELP_BIND_TEXT);
#else
			    outputMode(); sollyaPrintf("bind(p,ident,term): bind argument ident of procedure p to term, returning a procedure with one argument less.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BIND"
#endif
#endif
                          }
#line 10203 "parser.c" /* yacc.c:1646  */
    break;

  case 585:
#line 4112 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_INFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_INFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Certified infinity norm: infnorm(func,range[,prooffile[,list of funcs]]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INFNORM"
#endif
#endif
                          }
#line 10218 "parser.c" /* yacc.c:1646  */
    break;

  case 586:
#line 4123 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SUPNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_SUPNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Validated supremum norm: supnorm(poly,func,range,mode,accuracy).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUPNORM"
#endif
#endif
                          }
#line 10233 "parser.c" /* yacc.c:1646  */
    break;

  case 587:
#line 4134 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FINDZEROS_TEXT
			    outputMode(); sollyaPrintf(HELP_FINDZEROS_TEXT);
#else
			    outputMode(); sollyaPrintf("Certified bounding of zeros: findzeros(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FINDZEROS"
#endif
#endif
                          }
#line 10248 "parser.c" /* yacc.c:1646  */
    break;

  case 588:
#line 4145 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FPFINDZEROS_TEXT
			    outputMode(); sollyaPrintf(HELP_FPFINDZEROS_TEXT);
#else
			    outputMode(); sollyaPrintf("Approximate zeros of a function: fpfindzeros(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FPFINDZEROS"
#endif
#endif
                          }
#line 10263 "parser.c" /* yacc.c:1646  */
    break;

  case 589:
#line 4156 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIRTYINFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_DIRTYINFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Floating-point infinity norm: dirtyinfnorm(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIRTYINFNORM"
#endif
#endif
			  }
#line 10278 "parser.c" /* yacc.c:1646  */
    break;

  case 590:
#line 4167 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_GCD_TEXT
			    outputMode(); sollyaPrintf(HELP_GCD_TEXT);
#else
			    outputMode(); sollyaPrintf("Greatest common divisor: gcd(a, b).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GCD"
#endif
#endif
			  }
#line 10293 "parser.c" /* yacc.c:1646  */
    break;

  case 591:
#line 4178 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIVEUCL_TEXT
			    outputMode(); sollyaPrintf(HELP_DIVEUCL_TEXT);
#else
			    outputMode(); sollyaPrintf("Euclidian division (quotient): div(a, b).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EUCLDIV"
#endif
#endif
			  }
#line 10308 "parser.c" /* yacc.c:1646  */
    break;

  case 592:
#line 4189 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MODEUCL_TEXT
			    outputMode(); sollyaPrintf(HELP_MODEUCL_TEXT);
#else
			    outputMode(); sollyaPrintf("Euclidian division (rest): mod(a, b).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EUCLMOD"
#endif
#endif
			  }
#line 10323 "parser.c" /* yacc.c:1646  */
    break;

  case 593:
#line 4200 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_NUMBERROOTS_TEXT
			    outputMode(); sollyaPrintf(HELP_NUMBERROOTS_TEXT);
#else
			    outputMode(); sollyaPrintf("Computes the number of real roots of a polynomial on a domain.\n");
#endif
                          }
#line 10335 "parser.c" /* yacc.c:1646  */
    break;

  case 594:
#line 4208 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_INTEGRAL_TEXT
			    outputMode(); sollyaPrintf(HELP_INTEGRAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Certified integral: integral(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INTEGRAL"
#endif
#endif
                          }
#line 10350 "parser.c" /* yacc.c:1646  */
    break;

  case 595:
#line 4219 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIRTYINTEGRAL_TEXT
			    outputMode(); sollyaPrintf(HELP_DIRTYINTEGRAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Floating-point integral: dirtyintegral(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIRTYINTEGRAL"
#endif
#endif
                          }
#line 10365 "parser.c" /* yacc.c:1646  */
    break;

  case 596:
#line 4230 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_WORSTCASE_TEXT
			    outputMode(); sollyaPrintf(HELP_WORSTCASE_TEXT);
#else
			    outputMode(); sollyaPrintf("Print all worst-cases under a certain bound: worstcase(func,constant,range,constant,constant[,file]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for WORSTCASE"
#endif
#endif
                          }
#line 10380 "parser.c" /* yacc.c:1646  */
    break;

  case 597:
#line 4241 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_IMPLEMENTPOLY_TEXT
			    outputMode(); sollyaPrintf(HELP_IMPLEMENTPOLY_TEXT);
#else
			    outputMode(); sollyaPrintf("Implement a polynomial in C: implementpoly(func,range,constant,format,string,string2[,honorcoeffprec[,string3]]).\n");
			    outputMode(); sollyaPrintf("Implements func in range with error constant with entering format named in function\nstring writing to file string2 honoring the precision of the coefficients or not with a proof in file string3.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for IMPLEMENTPOLY"
#endif
#endif
			  }
#line 10396 "parser.c" /* yacc.c:1646  */
    break;

  case 598:
#line 4253 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_IMPLEMENTCONSTANT_TEXT
			    outputMode(); sollyaPrintf(HELP_IMPLEMENTCONSTANT_TEXT);
#else
			    outputMode(); sollyaPrintf("Implement a constant expression in arbitrary precision with MPFR: implementconstant(constant)\n");
			    outputMode(); sollyaPrintf("Generates code able to evaluate the given constant at any precision, with a guaranteed error.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for IMPLEMENTCONST"
#endif
#endif
                          }
#line 10412 "parser.c" /* yacc.c:1646  */
    break;

  case 599:
#line 4265 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_CHECKINFNORM_TEXT
			    outputMode(); sollyaPrintf(HELP_CHECKINFNORM_TEXT);
#else
			    outputMode(); sollyaPrintf("Checks whether an infinity norm is bounded: checkinfnorm(func,range,constant).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CHECKINFNORM"
#endif
#endif
                          }
#line 10427 "parser.c" /* yacc.c:1646  */
    break;

  case 600:
#line 4276 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ZERODENOMINATORS_TEXT
			    outputMode(); sollyaPrintf(HELP_ZERODENOMINATORS_TEXT);
#else
			    outputMode(); sollyaPrintf("Searches floating-point approximations to zeros of denominators: zerodenominators(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ZERODENOMINATORS"
#endif
#endif
                          }
#line 10442 "parser.c" /* yacc.c:1646  */
    break;

  case 601:
#line 4287 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ISEVALUABLE_TEXT
			    outputMode(); sollyaPrintf(HELP_ISEVALUABLE_TEXT);
#else
			    outputMode(); sollyaPrintf("Tests if func is evaluable on range: isevaluable(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ISEVALUABLE"
#endif
#endif
                          }
#line 10457 "parser.c" /* yacc.c:1646  */
    break;

  case 602:
#line 4298 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SEARCHGAL_TEXT
			    outputMode(); sollyaPrintf(HELP_SEARCHGAL_TEXT);
#else
			    outputMode(); sollyaPrintf("Searches Gal values for func (or list of func): searchgal(func|list of func, constant, integer, integer, format|list of formats, constant|list of constants).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SEARCHGAL"
#endif
#endif
                          }
#line 10472 "parser.c" /* yacc.c:1646  */
    break;

  case 603:
#line 4309 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_GUESSDEGREE_TEXT
			    outputMode(); sollyaPrintf(HELP_GUESSDEGREE_TEXT);
#else
			    outputMode(); sollyaPrintf("Guesses the degree needed for approximating func: guessdegree(func,range,constant[,weight]).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for GUESSDEGREE"
#endif
#endif
                          }
#line 10487 "parser.c" /* yacc.c:1646  */
    break;

  case 604:
#line 4320 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_DIRTYFINDZEROS_TEXT
			    outputMode(); sollyaPrintf(HELP_DIRTYFINDZEROS_TEXT);
#else
			    outputMode(); sollyaPrintf("Finds zeros of a function dirtily: dirtyfindzeros(func,range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for DIRTYFINDZEROS"
#endif
#endif
                          }
#line 10502 "parser.c" /* yacc.c:1646  */
    break;

  case 605:
#line 4331 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("If construct: if condition then command or if condition then command else command.\n");
                          }
#line 10510 "parser.c" /* yacc.c:1646  */
    break;

  case 606:
#line 4335 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("If construct: if condition then command or if condition then command else command.\n");
                          }
#line 10518 "parser.c" /* yacc.c:1646  */
    break;

  case 607:
#line 4339 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("If construct: if condition then command else command\n");
                          }
#line 10526 "parser.c" /* yacc.c:1646  */
    break;

  case 608:
#line 4343 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command\nor for i in list do command.\n");
                          }
#line 10534 "parser.c" /* yacc.c:1646  */
    break;

  case 609:
#line 4347 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_IN_TEXT
			    outputMode(); sollyaPrintf(HELP_IN_TEXT);
#else
			    outputMode(); sollyaPrintf("In construct: for in construct and containment operator.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for IN"
#endif
#endif
                          }
#line 10549 "parser.c" /* yacc.c:1646  */
    break;

  case 610:
#line 4358 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
                          }
#line 10557 "parser.c" /* yacc.c:1646  */
    break;

  case 611:
#line 4362 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
                          }
#line 10565 "parser.c" /* yacc.c:1646  */
    break;

  case 612:
#line 4366 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 by const3 do command.\n");
                          }
#line 10573 "parser.c" /* yacc.c:1646  */
    break;

  case 613:
#line 4370 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("For construct: for i from const to const2 [by const3] do command.\n");
			    outputMode(); sollyaPrintf("While construct: while condition do command.\n");
                          }
#line 10582 "parser.c" /* yacc.c:1646  */
    break;

  case 614:
#line 4375 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Begin-end construct: begin command; command; ... end.\n");
                          }
#line 10590 "parser.c" /* yacc.c:1646  */
    break;

  case 615:
#line 4379 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Begin-end construct: begin command; command; ... end.\n");
                          }
#line 10598 "parser.c" /* yacc.c:1646  */
    break;

  case 616:
#line 4383 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("While construct: while condition do command.\n");
                          }
#line 10606 "parser.c" /* yacc.c:1646  */
    break;

  case 617:
#line 4387 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_INF_TEXT
			    outputMode(); sollyaPrintf(HELP_INF_TEXT);
#else
			    outputMode(); sollyaPrintf("Dereferencing the infimum of a range: inf(range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INF"
#endif
#endif
                          }
#line 10621 "parser.c" /* yacc.c:1646  */
    break;

  case 618:
#line 4398 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MID_TEXT
			    outputMode(); sollyaPrintf(HELP_MID_TEXT);
#else
			    outputMode(); sollyaPrintf("Dereferencing the midpoint of a range: mid(range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MID"
#endif
#endif
                          }
#line 10636 "parser.c" /* yacc.c:1646  */
    break;

  case 619:
#line 4409 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_SUP_TEXT
			    outputMode(); sollyaPrintf(HELP_SUP_TEXT);
#else
			    outputMode(); sollyaPrintf("Dereferencing the supremum of a range: sup(range).\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for SUP"
#endif
#endif
                          }
#line 10651 "parser.c" /* yacc.c:1646  */
    break;

  case 620:
#line 4420 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EXPONENT_TEXT
			    outputMode(); sollyaPrintf(HELP_EXPONENT_TEXT);
#else
			    outputMode(); sollyaPrintf("exponent(constant): returns an integer such that constant scaled by the power of 2\nof this integer is an odd or zero integer.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXPONENT"
#endif
#endif
                          }
#line 10666 "parser.c" /* yacc.c:1646  */
    break;

  case 621:
#line 4431 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_MANTISSA_TEXT
			    outputMode(); sollyaPrintf(HELP_MANTISSA_TEXT);
#else
			    outputMode(); sollyaPrintf("mantissa(constant): returns an odd or zero integer equal to constant scaled by an integer power of 2.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for MANTISSA"
#endif
#endif
                          }
#line 10681 "parser.c" /* yacc.c:1646  */
    break;

  case 622:
#line 4442 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PRECISION_TEXT
			    outputMode(); sollyaPrintf(HELP_PRECISION_TEXT);
#else
			    outputMode(); sollyaPrintf("precision(constant): returns the least number of bits constant can be written on.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PRECISION"
#endif
#endif
                          }
#line 10696 "parser.c" /* yacc.c:1646  */
    break;

  case 623:
#line 4453 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EXECUTE_TEXT
			    outputMode(); sollyaPrintf(HELP_EXECUTE_TEXT);
#else
			    outputMode(); sollyaPrintf("execute(string): executes an %s script contained in a file named string.\n",PACKAGE_NAME);
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXECUTE"
#endif
#endif
                          }
#line 10711 "parser.c" /* yacc.c:1646  */
    break;

  case 624:
#line 4464 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_ISBOUND_TEXT
			    outputMode(); sollyaPrintf(HELP_ISBOUND_TEXT);
#else
			    outputMode(); sollyaPrintf("isbound(identifier): returns a boolean indicating if identifier is bound.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for ISBOUND"
#endif
#endif
                          }
#line 10726 "parser.c" /* yacc.c:1646  */
    break;

  case 625:
#line 4475 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Prints the version of the software.\n");
                          }
#line 10734 "parser.c" /* yacc.c:1646  */
    break;

  case 626:
#line 4478 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_EXTERNALPROC_TEXT
			    outputMode(); sollyaPrintf(HELP_EXTERNALPROC_TEXT);
#else
			    outputMode(); sollyaPrintf("externalplot(identifier, file, argumentypes -> resulttype): binds identifier to an external procedure with signature argumenttypes -> resulttype in file.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for EXTERNALPROC"
#endif
#endif
                          }
#line 10749 "parser.c" /* yacc.c:1646  */
    break;

  case 627:
#line 4488 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_VOID_TEXT
			    outputMode(); sollyaPrintf(HELP_VOID_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the void type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for VOID"
#endif
#endif
                          }
#line 10764 "parser.c" /* yacc.c:1646  */
    break;

  case 628:
#line 4498 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_CONSTANT_TEXT
			    outputMode(); sollyaPrintf(HELP_CONSTANT_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the constant type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for CONSTANT"
#endif
#endif
                          }
#line 10779 "parser.c" /* yacc.c:1646  */
    break;

  case 629:
#line 4508 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_FUNCTION_TEXT
			    outputMode(); sollyaPrintf(HELP_FUNCTION_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the function type for externalproc or a procedure-based function.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for FUNCTION"
#endif
#endif
                          }
#line 10794 "parser.c" /* yacc.c:1646  */
    break;

  case 630:
#line 4518 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_OBJECT_TEXT
			    outputMode(); sollyaPrintf(HELP_OBJECT_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the object type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for OBJECT"
#endif
#endif
                          }
#line 10809 "parser.c" /* yacc.c:1646  */
    break;

  case 631:
#line 4528 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RANGE_TEXT
			    outputMode(); sollyaPrintf(HELP_RANGE_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the range type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RANGE"
#endif
#endif
                          }
#line 10824 "parser.c" /* yacc.c:1646  */
    break;

  case 632:
#line 4538 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_INTEGER_TEXT
			    outputMode(); sollyaPrintf(HELP_INTEGER_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the integer type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for INTEGER"
#endif
#endif
                          }
#line 10839 "parser.c" /* yacc.c:1646  */
    break;

  case 633:
#line 4548 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_STRING_TEXT
			    outputMode(); sollyaPrintf(HELP_STRING_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the string type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for STRING"
#endif
#endif
                          }
#line 10854 "parser.c" /* yacc.c:1646  */
    break;

  case 634:
#line 4558 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_BOOLEAN_TEXT
			    outputMode(); sollyaPrintf(HELP_BOOLEAN_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the boolean type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for BOOLEAN"
#endif
#endif
                          }
#line 10869 "parser.c" /* yacc.c:1646  */
    break;

  case 635:
#line 4568 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LISTOF_TEXT
			    outputMode(); sollyaPrintf(HELP_LISTOF_TEXT);
#else
			    outputMode(); sollyaPrintf("Represents the list type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LISTOF"
#endif
#endif
                          }
#line 10884 "parser.c" /* yacc.c:1646  */
    break;

  case 636:
#line 4578 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_LISTOF_TEXT
			    outputMode(); sollyaPrintf(HELP_LISTOF_TEXT);
#else
			    outputMode(); sollyaPrintf("Used in list of type for externalproc.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for LISTOF"
#endif
#endif
                          }
#line 10899 "parser.c" /* yacc.c:1646  */
    break;

  case 637:
#line 4588 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_VAR_TEXT
			    outputMode(); sollyaPrintf(HELP_VAR_TEXT);
#else
			    outputMode(); sollyaPrintf("Declares a local variable.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for VAR"
#endif
#endif
                          }
#line 10914 "parser.c" /* yacc.c:1646  */
    break;

  case 638:
#line 4598 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_NOP_TEXT
			    outputMode(); sollyaPrintf(HELP_NOP_TEXT);
#else
			    outputMode(); sollyaPrintf("Does nothing.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for NOP"
#endif
#endif
                          }
#line 10929 "parser.c" /* yacc.c:1646  */
    break;

  case 639:
#line 4608 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PROC_TEXT
			    outputMode(); sollyaPrintf(HELP_PROC_TEXT);
#else
			    outputMode(); sollyaPrintf("Defines a nameless procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PROC"
#endif
#endif
                          }
#line 10944 "parser.c" /* yacc.c:1646  */
    break;

  case 640:
#line 4618 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_PROCEDURE_TEXT
			    outputMode(); sollyaPrintf(HELP_PROCEDURE_TEXT);
#else
			    outputMode(); sollyaPrintf("Defines a named procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for PROCEDURE"
#endif
#endif
                          }
#line 10959 "parser.c" /* yacc.c:1646  */
    break;

  case 641:
#line 4628 "parser.y" /* yacc.c:1646  */
    {
#ifdef HELP_RETURN_TEXT
			    outputMode(); sollyaPrintf(HELP_RETURN_TEXT);
#else
			    outputMode(); sollyaPrintf("Returns an expression in a procedure.\n");
#if defined(WARN_IF_NO_HELP_TEXT) && WARN_IF_NO_HELP_TEXT
#warning "No help text for RETURN"
#endif
#endif
                          }
#line 10974 "parser.c" /* yacc.c:1646  */
    break;

  case 642:
#line 4639 "parser.y" /* yacc.c:1646  */
    {
			    outputMode(); sollyaPrintf("Type \"help <keyword>;\" for help on the keyword <keyword>.\nFor example type \"help implementpoly;\" for help on the command \"implementpoly\".\n\n");
			    sollyaPrintf("Possible keywords in %s are:\n",PACKAGE_NAME);
			    sollyaPrintf("- !\n");
			    sollyaPrintf("- !=\n");
			    sollyaPrintf("- &&\n");
			    sollyaPrintf("- (\n");
			    sollyaPrintf("- )\n");
			    sollyaPrintf("- *\n");
			    sollyaPrintf("- +\n");
			    sollyaPrintf("- ,\n");
			    sollyaPrintf("- -\n");
			    sollyaPrintf("- .\n");
			    sollyaPrintf("- ...\n");
			    sollyaPrintf("- .:\n");
			    sollyaPrintf("- /\n");
			    sollyaPrintf("- :.\n");
			    sollyaPrintf("- :=\n");
			    sollyaPrintf("- ; (separator in ranges)\n");
			    sollyaPrintf("- <\n");
			    sollyaPrintf("- =\n");
			    sollyaPrintf("- ==\n");
			    sollyaPrintf("- >\n");
			    sollyaPrintf("- @\n");
			    sollyaPrintf("- {\n");
			    sollyaPrintf("- [|\n");
			    sollyaPrintf("- |]\n");
			    sollyaPrintf("- ||\n");
			    sollyaPrintf("- }\n");
			    sollyaPrintf("- ~\n");
			    sollyaPrintf("- [\n");
			    sollyaPrintf("- ]\n");
			    sollyaPrintf("- ^\n");
			    sollyaPrintf("- _x_\n");
			    sollyaPrintf("- D\n");
			    sollyaPrintf("- DD\n");
			    sollyaPrintf("- DE\n");
			    sollyaPrintf("- HP\n");
			    sollyaPrintf("- QD\n");
			    sollyaPrintf("- Pi\n");
			    sollyaPrintf("- RD\n");
			    sollyaPrintf("- RN\n");
			    sollyaPrintf("- RU\n");
			    sollyaPrintf("- RZ\n");
			    sollyaPrintf("- SG\n");
			    sollyaPrintf("- TD\n");
			    sollyaPrintf("- abs\n");
			    sollyaPrintf("- absolute\n");
			    sollyaPrintf("- accurateinfnorm\n");
			    sollyaPrintf("- acos\n");
			    sollyaPrintf("- acosh\n");
			    sollyaPrintf("- annotatefunction\n");
			    sollyaPrintf("- asciiplot\n");
			    sollyaPrintf("- asin\n");
			    sollyaPrintf("- asinh\n");
			    sollyaPrintf("- atan\n");
			    sollyaPrintf("- atanh\n");
			    sollyaPrintf("- autodiff\n");
			    sollyaPrintf("- autosimplify\n");
			    sollyaPrintf("- bashevaluate\n");
			    sollyaPrintf("- bashexecute\n");
			    sollyaPrintf("- begin\n");
			    sollyaPrintf("- binary\n");
			    sollyaPrintf("- bind\n");
			    sollyaPrintf("- boolean\n");
			    sollyaPrintf("- by\n");
			    sollyaPrintf("- canonical\n");
			    sollyaPrintf("- ceil\n");
			    sollyaPrintf("- chebyshevform\n");
			    sollyaPrintf("- checkinfnorm\n");
			    sollyaPrintf("- coeff\n");
			    sollyaPrintf("- composepolynomials\n");
			    sollyaPrintf("- constant\n");
			    sollyaPrintf("- cos\n");
			    sollyaPrintf("- cosh\n");
			    sollyaPrintf("- decimal\n");
			    sollyaPrintf("- default\n");
			    sollyaPrintf("- degree\n");
			    sollyaPrintf("- denominator\n");
			    sollyaPrintf("- diam\n");
			    sollyaPrintf("- dieonerrormode\n");
			    sollyaPrintf("- diff\n");
			    sollyaPrintf("- dirtyfindzeros\n");
			    sollyaPrintf("- dirtyinfnorm\n");
			    sollyaPrintf("- dirtyintegral\n");
			    sollyaPrintf("- dirtysimplify\n");
			    sollyaPrintf("- display\n");
			    sollyaPrintf("- div\n");
			    sollyaPrintf("- do\n");
			    sollyaPrintf("- double\n");
			    sollyaPrintf("- doubledouble\n");
			    sollyaPrintf("- doubleextended\n");
			    sollyaPrintf("- dyadic\n");
			    sollyaPrintf("- else\n");
			    sollyaPrintf("- end\n");
			    sollyaPrintf("- erf\n");
			    sollyaPrintf("- erfc\n");
			    sollyaPrintf("- error\n");
			    sollyaPrintf("- evaluate\n");
			    sollyaPrintf("- execute\n");
			    sollyaPrintf("- exp\n");
			    sollyaPrintf("- expand\n");
			    sollyaPrintf("- expm1\n");
			    sollyaPrintf("- exponent\n");
			    sollyaPrintf("- externalplot\n");
			    sollyaPrintf("- externalproc\n");
			    sollyaPrintf("- false\n");
			    sollyaPrintf("- file\n");
			    sollyaPrintf("- findzeros\n");
			    sollyaPrintf("- fixed\n");
			    sollyaPrintf("- floating\n");
			    sollyaPrintf("- floor\n");
			    sollyaPrintf("- for\n");
			    sollyaPrintf("- fpfindzeros\n");
			    sollyaPrintf("- fpminimax\n");
			    sollyaPrintf("- from\n");
			    sollyaPrintf("- fullparentheses\n");
			    sollyaPrintf("- function\n");
			    sollyaPrintf("- gcd\n");
			    sollyaPrintf("- getsuppressedmessages\n");
			    sollyaPrintf("- getbacktrace\n");
			    sollyaPrintf("- guessdegree\n");
			    sollyaPrintf("- halfprecision\n");
			    sollyaPrintf("- head\n");
			    sollyaPrintf("- help\n");
			    sollyaPrintf("- hexadecimal\n");
			    sollyaPrintf("- honorcoeffprec\n");
			    sollyaPrintf("- hopitalrecursions\n");
			    sollyaPrintf("- horner\n");
			    sollyaPrintf("- if\n");
			    sollyaPrintf("- implementpoly\n");
			    sollyaPrintf("- implementconstant\n");
			    sollyaPrintf("- in\n");
			    sollyaPrintf("- inf\n");
			    sollyaPrintf("- infnorm\n");
			    sollyaPrintf("- integer\n");
			    sollyaPrintf("- integral\n");
			    sollyaPrintf("- isbound\n");
			    sollyaPrintf("- isevaluable\n");
			    sollyaPrintf("- length\n");
			    sollyaPrintf("- library\n");
			    sollyaPrintf("- libraryconstant\n");
			    sollyaPrintf("- list\n");
			    sollyaPrintf("- log\n");
			    sollyaPrintf("- log10\n");
			    sollyaPrintf("- log1p\n");
			    sollyaPrintf("- log2\n");
			    sollyaPrintf("- mantissa\n");
			    sollyaPrintf("- match\n");
			    sollyaPrintf("- max\n");
			    sollyaPrintf("- mid\n");
			    sollyaPrintf("- midpointmode\n");
			    sollyaPrintf("- min\n");
			    sollyaPrintf("- mod\n");
			    sollyaPrintf("- nearestint\n");
			    sollyaPrintf("- numberroots\n");
			    sollyaPrintf("- nop\n");
			    sollyaPrintf("- numerator\n");
			    sollyaPrintf("- object\n");
			    sollyaPrintf("- objectname\n");
			    sollyaPrintf("- of\n");
			    sollyaPrintf("- off\n");
			    sollyaPrintf("- on\n");
			    sollyaPrintf("- parse\n");
			    sollyaPrintf("- perturb\n");
			    sollyaPrintf("- pi\n");
			    sollyaPrintf("- plot\n");
			    sollyaPrintf("- points\n");
			    sollyaPrintf("- postscript\n");
			    sollyaPrintf("- postscriptfile\n");
			    sollyaPrintf("- powers\n");
			    sollyaPrintf("- prec\n");
			    sollyaPrintf("- precision\n");
			    sollyaPrintf("- print\n");
			    sollyaPrintf("- printbinary\n");
			    sollyaPrintf("- printdouble\n");
			    sollyaPrintf("- printexpansion\n");
			    sollyaPrintf("- printfloat\n");
			    sollyaPrintf("- printhexa\n");
			    sollyaPrintf("- printsingle\n");
			    sollyaPrintf("- printxml\n");
			    sollyaPrintf("- proc\n");
			    sollyaPrintf("- procedure\n");
			    sollyaPrintf("- quad\n");
			    sollyaPrintf("- quit\n");
			    sollyaPrintf("- range\n");
			    sollyaPrintf("- rationalapprox\n");
			    sollyaPrintf("- rationalmode\n");
			    sollyaPrintf("- readfile\n");
			    sollyaPrintf("- readxml\n");
			    sollyaPrintf("- relative\n");
			    sollyaPrintf("- remez\n");
			    sollyaPrintf("- rename\n");
			    sollyaPrintf("- restart\n");
			    sollyaPrintf("- return\n");
			    sollyaPrintf("- revert\n");
			    sollyaPrintf("- round\n");
			    sollyaPrintf("- roundcoefficients\n");
			    sollyaPrintf("- roundcorrectly\n");
			    sollyaPrintf("- roundingwarnings\n");
			    sollyaPrintf("- searchgal\n");
			    sollyaPrintf("- showmessagenumbers\n");
			    sollyaPrintf("- simplify\n");
			    sollyaPrintf("- sin\n");
			    sollyaPrintf("- single\n");
			    sollyaPrintf("- sinh\n");
			    sollyaPrintf("- sort\n");
			    sollyaPrintf("- sqrt\n");
			    sollyaPrintf("- string\n");
			    sollyaPrintf("- subpoly\n");
			    sollyaPrintf("- substitute\n");
			    sollyaPrintf("- sup\n");
			    sollyaPrintf("- supnorm\n");
			    sollyaPrintf("- suppressmessage\n");
			    sollyaPrintf("- tail\n");
			    sollyaPrintf("- tan\n");
			    sollyaPrintf("- tanh\n");
			    sollyaPrintf("- taylor\n");
			    sollyaPrintf("- taylorform\n");
			    sollyaPrintf("- taylorrecursions\n");
			    sollyaPrintf("- then\n");
			    sollyaPrintf("- time\n");
			    sollyaPrintf("- timing\n");
			    sollyaPrintf("- to\n");
			    sollyaPrintf("- tripledouble\n");
			    sollyaPrintf("- true\n");
			    sollyaPrintf("- unsuppressmessage\n");
			    sollyaPrintf("- var\n");
			    sollyaPrintf("- verbosity\n");
			    sollyaPrintf("- version\n");
			    sollyaPrintf("- void\n");
			    sollyaPrintf("- while\n");
			    sollyaPrintf("- with\n");
			    sollyaPrintf("- worstcase\n");
			    sollyaPrintf("- write\n");
			    sollyaPrintf("- zerodenominators\n");
			    sollyaPrintf("\n");
                          }
#line 11217 "parser.c" /* yacc.c:1646  */
    break;


#line 11221 "parser.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (scanner, YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (scanner, yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval, scanner);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp, scanner);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (scanner, YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval, scanner);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp, scanner);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
